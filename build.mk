include autoc.mk
include abi.mk
WORKSPACE?=paw

ALL_GOALS:=all info rebuild clean build
WORKSPACE_DIR:=.paw/all/$(WORKSPACE)

$(info OTG_DIR=$(OTG_DIR))
export OTG_DIR
$(info DST_DIR=$(DST_DIR))
export DST_DIR
$(info SHARED_DIR=$(SHARED_DIR))
export SHARED_DIR
$(info AUTOC_HOST_DIR=$(AUTOC_HOST_DIR))
$(info AUTOC_HOST_CAUGHT_MK=$(AUTOC_HOST_CAUGHT_MK))
$(info AUTOC_HOST_CAUGHT_INC=$(AUTOC_HOST_CAUGHT_INC))

$(info HPAW_AUTOC_DIR=$(HPAW_AUTOC_DIR))
$(info HPAW_CAUGHT_MK=$(HPAW_CAUGHT_MK))
$(info HPAW_CAUGHT_INC=$(HPAW_CAUGHT_INC))

$(info NPAW_AUTOC_DIR=$(NPAW_AUTOC_DIR))
$(info NPAW_CAUGHT_MK=$(NPAW_CAUGHT_MK))
$(info NPAW_CAUGHT_INC=$(NPAW_CAUGHT_INC))

$(info WORKSPACE=$(WORKSPACE))
export WORKSPACE
$(info WORKSPACE_DIR=$(WORKSPACE_DIR))
export WORKSPACE_DIR
$(info HPAW_DIR=$(HPAW_DIR))
$(info NPAW_DIR=$(NPAW_DIR))
$(info PAW_DIR=$(PAW_DIR))

$(info NPAW_ABI_OPTS=$(NPAW_ABI_OPTS))
export NPAW_ABI_OPTS
$(info PAW_DATA_MODEL=$(PAW_DATA_MODEL))
$(info PAW_ENDIAN=$(PAW_ENDIAN))
$(info PAW_MODES=$(PAW_MODES))
$(info PAW_ARCH=$(PAW_ARCH))
$(info HPAW_ABI=$(HPAW_ABI))
$(info NPAW_ABI=$(NPAW_ABI))
$(info PAW_ABI=$(PAW_ABI))
$(info PAW_VER=$(PAW_VER))
export PAW_VER
$(info PAW_OS=$(PAW_OS))
export PAW_OS

$(info APP_EXT=$(APP_EXT))
export APP_EXT
$(info LIB_EXT=$(LIB_EXT))
export LIB_EXT
$(info OS_PATH_SEP=$(OS_PATH_SEP))
export OS_PATH_SEP
$(info OS_DIR_SEP=$(OS_DIR_SEP))
export OS_DIR_SEP

$(GOALS): autoc host.mk
	$(call boxup,$(SUBHR),make -f $(WORKSPACE_DIR)/build.mk $(GOALS))

info: autoc
	@echo Possible targets: $(ALL_GOALS)
	@echo GOALS = $(GOALS)

.PHONY: $(ALL_GOALS)
