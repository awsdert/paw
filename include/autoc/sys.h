#pragma once

#if defined __32BIT__ || defined __386__ || defined _M_I386 \
	|| defined __i386__ || (defined __DMC__ && defined _M_IX86)
#	define _AUTOC_SYS32 1
#endif

#if defined _WIN64
#	define AUTOC_SYS_MSW64 "MSW64"
#elif defined __DOS__ || defined MSDOS
#	ifdef _AUTOC_SYS32
#		define AUTOC_SYS_DOS32 "DOS32"
#	else
#		define AUTOC_SYS_DOS16 "DOS16"
#	endif
#elif defined _WINDOWS || defined __WINDOWS_386__ || defined __WINDOWS__
#	define AUTOC_SYS_MSW16 "MSW16"
#elif defined __NT__ || defined _WIN32
#	define AUTOC_SYS_MSW32 "MSW32"
#endif

#if defined AUTOC_SYS_MSW64 || defined AUTOC_SYS_MSW32 || defined AUTOC_SYS_MSW16
#	define AUTOC_SYS_MSW "MSW"
#endif

#if defined AUTOC_SYS_DOS32 || defined AUTOC_SYS_DOS16
#	define AUTOC_SYS_DOS "DOS"
#endif

#if defined(__APPLE__) && defined(__MACH__)
#	define AUTOC_SYS_MAC
#endif

#if defined __UNIX__ || defined __unix__ || defined unix
#	define AUTOC_SYS_UNIX "Unix"
#endif

#if defined AUTOC_SYS_UNIX || defined AUTOC_SYS_MAC
#	define AUTOC_SYS_UNIX_MIMIC
#	include <sys/param.h>
#endif

#ifdef __OS2__
#	ifdef _AUTOC_SYS32
#		define AUTOC_SYS_OSX32 "OSX32"
#	else
#		define AUTOC_SYS_OSX16 "OSX16"
#	endif
#endif

#ifdef AUTOC_SYS_OSX32
#	define AUTOC_SYS_OS2 AUTOC_SYS_OSX32
#elif defined AUTOC_SYS_OSX16
#	define AUTOC_SYS_OS2 AUTOC_SYS_OSX16
#endif

#if defined __LINUX__ || defined __linux__ || defined linux
#	define AUTOC_SYS_LINUX "Linux"
#endif

#if defined __POSIX__ || defined __posix__ || defined posix \
	|| defined _POSIX_C_SOURCE || defined _POSIX1_SOURCE \
	|| defined _POSIX_SOURCE
#	define AUTOC_SYS_POSIX "Posix"
#endif

#if defined __DARWIN__ || defined __darwin__ || defined darwin
#	define AUTOC_SYS_DARWIN "Darwin"
#endif

/* FIXME: Check what the actual predefine is, this one was only used as
 * placeholder to remind myself about it later */
#ifdef __IBM_EBCDIC__
#	define AUTOC_SYS_EBCDIC EBCDIC
#endif

#ifdef BSD
#	define AUTOC_SYS_BSD "BSD"
#endif

#ifdef AUTOC_SYS_DOS
#	define AUTOC_SYS_DIR_SEP_STR "\\"
#	define AUTOC_SYS_DIR_SEP_CHR '\\'
#	define AUTOC_OS AUTOC_SYS_DOS
#elif defined AUTOC_SYS_MSW
#	define AUTOC_SYS_DIR_SEP_STR "\\"
#	define AUTOC_SYS_DIR_SEP_CHR '\\'
#	define AUTOC_OS AUTOC_SYS_MSW
#else
#	include <libunwind.h>
#	define AUTOC_SYS_DIR_SEP_STR "/"
#	define AUTOC_SYS_DIR_SEP_CHR '/'
#	ifdef AUTOC_SYS_MAC
#		define AUTOC_OS AUTOC_SYS_MAC
#	elif defined AUTOC_SYS_OS2
#		define AUTOC_OS AUTOC_SYS_OS2
#	elif defined AUTOC_SYS_DARWIN
#		define AUTOC_OS AUTOC_SYS_DARWIN
#	elif defined AUTOC_SYS_BSD
#		define AUTOC_OS AUTOC_SYS_BSD
#	elif defined AUTOC_SYS_LINUX
#		define AUTOC_OS AUTOC_SYS_LINUX
#	elif defined AUTOC_SYS_UNIX
#		define AUTOC_OS AUTOC_SYS_UNIX
#	elif defined AUTOC_SYS_POSIX
#		define AUTOC_OS AUTOC_SYS_POSIX
#	else
#		include <config_autoc.h>
#		ifndef AUTOC_SYS_CUSTOM
#			error "You must define the system details in autoc_config.h!"
#			include <abort_compilation.h>
#		endif
#		define AUTOC_OS AUTOC_SYS_CUSTOM
#	endif
#endif
