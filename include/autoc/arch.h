#pragma once
#include "cc.h"
/* Using https://sourceforge.net/p/predef/wiki/Architectures/ to construct
 * this header */

#include "arch/Alpha.h"
#include "arch/AMD.h"
#include "arch/ARM.h"
#include "arch/Convex.h"
#include "arch/HPPA.h"
#include "arch/Intel.h"
#include "arch/MIPS.h"
#include "arch/PowerPC.h"
#include "arch/RISC.h"
#include "arch/x86_64.h"
#include "arch/x86.h"

#if defined AUTOC_ARCH_RISC
#	define AUTOC_ARCH AUTOC_ARCH_RISC
#elif defined AUTOC_ARCH_x86_64
#	define AUTOC_ARCH AUTOC_ARCH_x86_64
#elif defined AUTOC_ARCH_x86
#	define AUTOC_ARCH AUTOC_ARCH_x86
#elif defined AUTOC_ARCH_ARM
#	define AUTOC_ARCH AUTOC_ARCH_ARM
#elif defined AUTOC_ARCH_AMD
#	define AUTOC_ARCH AUTOC_ARCH_AMD
#elif defined AUTOC_ARCH_POWERPC
#	define AUTOC_ARCH AUTOC_ARCH_POWERPC
#elif defined AUTOC_ARCH_INTEL
#	define AUTOC_ARCH AUTOC_ARCH_INTEL
#else
#	define AUTOC_ARCH Unkown
#endif

#define AUTOC_ARCH_NAMESTRING AUTOC_TOSTR(AUTOC_ARCH)

#if (AUTOC_APPEND2(AUTOC_ARCH_,AUTOC_ARCH,_BYTE_WIDTH))
#	define AUTOC_ARCH_BYTE_WIDTH AUTOC_ARCH_##AUTOC_ARCH##_BYTE_WIDTH
#elif defined __CHAR_BIT__
#	define AUTOC_ARCH_BYTE_WIDTH __CHAR_BIT__
#endif

/* General Registers */
#ifdef AUTOC_DM_P64
#	define AUTOC_ARCH_Unkown_GREG_WIDTH 64
#elif defined AUTOC_DM_P32
#	define AUTOC_ARCH_Unkown_GREG_WIDTH 32
#elif defined __LONG_WIDTH__
#	define AUTOC_ARCH_Unkown_GREG_WIDTH __LONG_WIDTH__
#elif defined __INT_WIDTH__
#	define AUTOC_ARCH_Unkown_GREG_WIDTH __INT_WIDTH__
#else
#	define AUTOC_ARCH_Unkown_GREG_WIDTH (AUTOC_ARCH_BYTE_WIDTH * sizeof(int))
#endif

/* General Registers */
#if (AUTOC_APPEND2(AUTOC_ARCH_,AUTOC_ARCH,_GREG_WIDTH))
#	define AUTOC_ARCH_GREG_WIDTH AUTOC_ARCH_##AUTOC_ARCH##_GREG_WIDTH
#else
#	define AUTOC_ARCH_GREG_WIDTH AUTOC_ARCH_Unkown_GREG_WIDTH
#endif

/* Floating Registers */
#if (AUTOC_APPEND2(AUTOC_ARCH_,AUTOC_ARCH,_FREG_WIDTH))
#	define AUTOC_ARCH_FREG_WIDTH AUTOC_ARCH_##AUTOC_ARCH##_FREG_WIDTH
#endif

#ifdef AUTOC_DM_SILP64
#	define AUTOC_DM_P64 AUTOC_DM_SILP64
#elif defined AUTOC_DM_ILP64
#	define AUTOC_DM_P64 AUTOC_DM_ILP64
#elif defined AUTOC_DM_LP64
#	define AUTOC_DM_P64 AUTOC_DM_LP64
#elif defined AUTOC_DM_LLP64
#	define AUTOC_DM_P64 AUTOC_DM_LLP64
#endif

#ifdef AUTOC_DM_SILP32
#	define AUTOC_DM_P32 AUTOC_DM__SILP32
#elif defined AUTOC_DM_ILP32
#	define AUTOC_DM_P32 AUTOC_DM_ILP32
#elif defined AUTOC_DM_LP32
#	define AUTOC_DM_P32 AUTOC_DM__LP32
#endif

#if defined AUTOC_DM_P64
#	define AUTOC_DM AUTOC_DM_P64
#elif defined AUTOC_DM_P32
#	define AUTOC_DM AUTOC_DM_P32
#endif

#define autocintqi int __attribute__((mode(QI)))
#define autocinthi int __attribute__((mode(HI)))
#define autocintsi int __attribute__((mode(SI)))
#define autocintdi int __attribute__((mode(DI)))
#define autocintti int __attribute__((mode(TI)))
#if AUTOC_ARCH_GREG_WIDTH > 64
#define autocintoi int __attribute__((mode(OI)))
#else
#define autocintoi _BitInt(AUTOC_ARCH_BYTE_WIDTH * 32)
#endif
#if AUTOC_ARCH_GREG_WIDTH > 128
#define autocintxi int __attribute__((mode(XI)))
#else
#define autocintxi _BitInt(AUTOC_ARCH_BYTE_WIDTH * 64)
#endif

typedef unsigned _BitInt(1) autocbool;
typedef   signed autocintqi autocintqid;
typedef unsigned autocintqi autocintqiu;
typedef   signed autocinthi autocinthid;
typedef unsigned autocinthi autocinthiu;
typedef   signed autocintsi autocintsid;
typedef unsigned autocintsi autocintsiu;
typedef   signed autocintdi autocintdid;
typedef unsigned autocintdi autocintdiu;
typedef   signed autocintti autocinttid;
typedef unsigned autocintti autocinttiu;
typedef   signed autocintoi autocintoid;
typedef unsigned autocintoi autocintoiu;
typedef   signed autocintxi autocintxid;
typedef unsigned autocintxi autocintxiu;

#ifdef AUTO_DM
#	define AUTOC_DM_SFX(SFX) AUTOC_DEFER_MACRO(AUTOC_APPEND2(AUTOC_DM_,AUTOC_DM_,SFX))
#	define AUTOC_DM_CHAR_WIDTH  AUTOC_DM_SFX(_CHAR_WIDTH))
#	define AUTOC_DM_SHRT_WIDTH  AUTOC_DM_SFX(_SHRT_WIDTH))
#	define AUTOC_DM_INT_WIDTH   AUTOC_DM_SFX(_INT_WIDTH))
#	define AUTOC_DM_LONG_WIDTH  AUTOC_DM_SFX(_LONG_WIDTH))
#	define AUTOC_DM_LLONG_WIDTH AUTOC_DM_SFX(_LLONG_WIDTH))
#	define AUTOC_DM_PTR_WIDTH   AUTOC_DM_SFX(_PTR_WIDTH))
#	if !(AUTOC_DM_LLONG_WIDTH)
#		undef AUTOC_DM_LLONG_WIDTH
#	endif
#elif defined __CHAR_BIT__
#	define AUTOC_DM_CHAR_WIDTH  __CHAR_BIT__
#	define AUTOC_DM_SHRT_WIDTH  __SHORT_WIDTH__
#	define AUTOC_DM_INT_WIDTH   __INT_WIDTH__
#	define AUTOC_DM_LONG_WIDTH  __LONG_WIDTH__
#	ifdef __LONG_LONG_WIDTH__
#		define AUTOC_DM_LLONG_WIDTH __LONG_LONG_WIDTH__
#	endif
#endif

#ifndef bitsof
#	define bitsof(T) (sizeof(T) * AUTOC_DM_CHAR_WIDTH)
#endif

#ifndef AUTOC_INT_ENDIAN
#	if (AUTOC_APPEND2(AUTOC_ARCH_,AUTOC_ARCH,_INT_ENDIAN))
#		define AUTOC_INT_ENDIAN AUTOC_ARCH_##AUTOC_ARCH##_INT_ENDIAN
#	elif defined _AUTOC_ARCH_BDIRECT_ORDER
#		define AUTOC_INT_ENDIAN AUTOC_BDIRECT_ORDER
#	elif defined _AUTOC_ARCH_LENDIAN_ORDER
#		define AUTOC_INT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif defined _AUTOC_ARCH_BENDIAN_ORDER
#		define AUTOC_INT_ENDIAN AUTOC_BENDIAN_ORDER
#	endif
#endif
