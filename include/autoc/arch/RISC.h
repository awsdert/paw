#pragma once

#include "HPPA.h"

#if defined AUTOC_ARCH__HPPA20
#	define AUTOC_ARCH__RISC20 AUTOC_ARCH__HPPA20
#elif defined _PA_RISC2_0
#	define AUTOC_ARCH__RISC20 RISC20
#endif

#if defined AUTOC_ARCH__HPPA11
#	define AUTOC_ARCH__RISC11 AUTOC_ARCH__HPPA11
#elif defined _PA_RISC1_1
#	define AUTOC_ARCH__RISC11 RISC11
#endif

#if defined AUTOC_ARCH_HPPA
#	define AUTOC_ARCH__RISC10 AUTOC_ARCH__HPPA
#elif defined _PA_RISC1_0
#	define AUTOC_ARCH__RISC10 RISC10
#endif

#if defined AUTOC_ARCH__RISC20
#	define AUTOC_ARCH_RISC AUTOC_ARCH__RISC20
#elif defined AUTOC_ARCH__RISC11
#	define AUTOC_ARCH_RISC AUTOC_ARCH__RISC11
#elif defined AUTOC_ARCH__RISC10
#	define AUTOC_ARCH_RISC AUTOC_ARCH__RISC10
#endif
