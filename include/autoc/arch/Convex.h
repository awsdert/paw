#pragma once

#if defined __convex_c38__
#	define AUTOC_ARCH__ConvexC38 ConvexC38
#endif

#if defined __convex_c37__
#	define AUTOC_ARCH__ConvexC37 ConvexC37
#endif

#if defined __convex_c36__
#	define AUTOC_ARCH__ConvexC36 ConvexC36
#endif

#if defined __convex_c35__
#	define AUTOC_ARCH__ConvexC35 ConvexC35
#endif

#if defined __convex_c34__
#	define AUTOC_ARCH__ConvexC34 ConvexC34
#endif

#if defined __convex_c33__
#	define AUTOC_ARCH__ConvexC33 ConvexC33
#endif

#if defined __convex_c32__
#	define AUTOC_ARCH__ConvexC32 ConvexC32
#endif

#if defined __convex_c3__
#	define AUTOC_ARCH__ConvexC3 ConvexC3
#endif

#if defined __convex_c2__
#	define AUTOC_ARCH__ConvexC2 ConvexC2
#endif

#if defined __convex_c1__
#	define AUTOC_ARCH__ConvexC1 ConvexC1
#endif

#ifdef AUTOC_ARCH__ConvexC38
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC38
#elif defined AUTOC_ARCH__ConvexC37
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC37
#elif defined AUTOC_ARCH__ConvexC36
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC36
#elif defined AUTOC_ARCH__ConvexC35
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC35
#elif defined AUTOC_ARCH__ConvexC34
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC34
#elif defined AUTOC_ARCH__ConvexC33
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC33
#elif defined AUTOC_ARCH__ConvexC32
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC32
#elif defined AUTOC_ARCH__ConvexC3
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC3
#elif defined AUTOC_ARCH__ConvexC2
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC2
#elif defined AUTOC_ARCH__ConvexC1
#	define AUTOC_ARCH__ConvexC AUTOC_ARCH__ConvexC1
#endif

/* Leaves room for any series I don't happen to know about from the list I
 * found */
#ifdef AUTOC_ARCH__ConvexC
#	define AUTOC_ARCH__Convex AUTOC_ARCH__ConvexC
#endif
