#pragma once
#include "ARM.h"
#include "AMD.h"
#include "Intel.h"
#include "PowerPC.h"

#if	defined __X86__ || defined __x86__ || defined _M_X86 || defined _WIN32 \
	|| defined _X86_ || defined __THW_INTEL__ || defined __I86__ \
	|| defined __INTEL__
#	define AUTOC_ARCH__x86 x86
#endif

#ifdef AUTOC_ARCH__ARM
#	define AUTOC_ARCH_x86 AUTOC_ARCH__ARM
#elif defined AUTOC_ARCH__AARCH
#	define AUTOC_ARCH_x86 AUTOC_ARCH__AARCH
#elif defined AUTOC_ARCH__AMD
#	define AUTOC_ARCH_x86 AUTOC_ARCH__AMD
#elif defined AUTOC_ARCH__PPC
#	define AUTOC_ARCH_x86 AUTOC_ARCH__PPC
#elif defined AUTOC_ARCH__PowerPC
#	define AUTOC_ARCH_x86 AUTOC_ARCH__PowerPC
#elif defined AUTOC_ARCH__i986
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i986
#elif defined AUTOC_ARCH__i886
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i886
#elif defined AUTOC_ARCH__i786
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i786
#elif defined AUTOC_ARCH__i686
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i686
#elif defined AUTOC_ARCH__i586
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i586
#elif defined AUTOC_ARCH__i486
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i486
#elif defined AUTOC_ARCH__i386
#	define AUTOC_ARCH_x86 AUTOC_ARCH__i386
#elif defined AUTOC_ARCH__PowerPC
#	define AUTOC_ARCH_x86 AUTOC_ARCH__PowerPC
#elif defined AUTOC_ARCH__X86
#	define AUTOC_ARCH_x86 AUTOC_ARCH__x86
#endif

#define AUTOC_ARCH__x86_BYTE_WIDTH 8
#define AUTOC_ARCH__x86_GREG_WIDTH 32
#define AUTOC_ARCH__x86_FREG_WIDTH 128
