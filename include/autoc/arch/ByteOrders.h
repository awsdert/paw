#pragma once

/* No clue whatsoever what endian is in use */
#define AUTOC_UNKONWN_ORDER 0
/* Endian Defined at runtime */
#define AUTOC_RUNTIME_ORDER 0x00000001L
/* int[0] = 0x78, Little Endian */
#define AUTOC_LENDIAN_ORDER 0x78563412L
/* int[0] = 0x12, Big Endian */
#define AUTOC_BENDIAN_ORDER 0x12345678L
/* int[0] = 0x56, PDP-11 Middle Endian */
#define AUTOC_PENDIAN_ORDER 0x56781234L
/* int[0] = 0x34, Honeywell 316 Middle Endian */
#define AUTOC_HENDIAN_ORDER 0x34127856L

/* Bi-Directional Endian - only use this at compile time, runtime should
 * include only the current endian in the bitmask */
#define AUTOC_BDIRECT_ORDER \
	(AUTOC_RUNTIME_ORDER | AUTOC_BENDIAN_ORDER | AUTOC_LENDIAN_ORDER)

#if defined __MIPSEL__ || defined __MIPSEL || defined _MIPSEL \
	|| define __ARMEL__ || defined __AARCH64EL__ || defined __THUMBEL__
#	define _AUTOC_ARCH_LENDIAN_ORDER
#endif

#if defined __MIPSEB__ || defined __MIPSEB || defined _MIPSEB \
	|| define __ARMEB__ || defined __AARCH64EB__ || defined __THUMBEB__
#	define _AUTOC_ARCH_BENDIAN_ORDER
#endif

#if defined _AUTOC_ARCH_LENDIAN_ORDER && defined _AUTOC_ARCH_BENDIAN_ORDER
#	define _AUTOC_ARCH_BDIRECT_ORDER
#endif

#ifdef __BYTE_ORDER__
#	if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_BENDIAN_ORDER
#	elif __BYTE_ORDER__ == __ORDER_PDP_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_PENDIAN_ORDER
#	endif
#elif defined __BYTE_ORDER
#	if __BYTE_ORDER == __ORDER_LITTLE_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif __BYTE_ORDER == __ORDER_BIG_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_BENDIAN_ORDER
#	elif __BYTE_ORDER == __ORDER_PDP_ENDIAN__
#		define AUTOC_INT_ENDIAN AUTOC_PENDIAN_ORDER
#	endif
#endif

#ifdef __FLOAT_WORD_ORDER__
#	if __FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif __FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_BENDIAN_ORDER
#	elif __FLOAT_WORD_ORDER__ == __ORDER_PDP_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_PENDIAN_ORDER
#	endif
#elif defined __FLOAT_WORD_ORDER
#	if __FLOAT_WORD_ORDER == __ORDER_LITTLE_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif __FLOAT_WORD_ORDER == __ORDER_BIG_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_BENDIAN_ORDER
#	elif __FLOAT_WORD_ORDER == __ORDER_PDP_ENDIAN__
#		define AUTOC_FLT_ENDIAN AUTOC_PENDIAN_ORDER
#	endif
#endif
