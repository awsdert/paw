#pragma once
#include "ARM.h"
#include "AMD.h"
#include "Intel.h"
#include "PowerPC.h"

#if	defined __X86_64__ || defined __x86_64__ || defined _M_X64 || \
	defined _WIN64
#	define AUTOC_ARCH__x86_64 x86_64
#endif

#ifdef AUTOC_ARCH__ARM64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__ARM64
#elif defined AUTOC_ARCH__AARCH64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__AARCH64
#elif defined AUTOC_ARCH__AMD64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__AMD64
#elif defined AUTOC_ARCH__PPC64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__PPC64
#elif defined AUTOC_ARCH__PowerPC64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__PowerPC64
#elif defined AUTOC_ARCH__IA64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__IA64
#elif defined AUTOC_ARCH__PowerPC64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__PowerPC64
#elif defined AUTOC_ARCH__X86_64
#	define AUTOC_ARCH_x86_64 AUTOC_ARCH__x86_64
#endif

#define AUTOC_ARCH__x86_64_BYTE_WIDTH 8
#define AUTOC_ARCH__x86_64_GREG_WIDTH 64
#define AUTOC_ARCH__x86_64_FREG_WIDTH 256
