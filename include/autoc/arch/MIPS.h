#pragma once

#ifdef _M_RX000
#	define AUTOC_ARCH__MIPS_RX000 MIPS_RX000
#endif

#ifdef AUTOC_ARCH__MIPS_RX000
#	define AUTOC_ARCH_MIPS AUTOC_ARCH__MIPS_RX000
#endif
