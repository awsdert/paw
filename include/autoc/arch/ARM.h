#pragma once

#if	defined __ARM64__ || defined __arm64__ || defined _M_ARM64 \
	|| defined __arm64 || defined __TARGET_ARCH_ARM64 || defined _ARM64
#	define AUTOC_ARCH__ARM64	ARM64
#endif

#if defined __AARCH64__ || defined __aarch64__ || defined _M_AARCH64
#	define AUTOC_ARCH__AARCH64	ARM64
#endif

#if	defined __ARM__ || defined __arm__ || defined _M_ARM || defined _ARM \
	|| defined __arm || defined __TARGET_ARCH_ARM || defined _ARM
#	define AUTOC_ARCH__ARM		ARM
#endif

#if defined __AARCH__ || defined __aarch__ || defined _M_AARCH
#	define AUTOC_ARCH__AARCH	ARM
#endif

#if defined __THUMB__ || defined __thumb__ || defined _M_ARMT \
	|| defined __thumb || defined __TARGET_ARCH_THUMB
#	define  AUTOC_ARCH__Thumb Thumb
#endif

#define AUTOC_ARCH__ARM64_BYTE_WIDTH 8
#define AUTOC_ARCH__ARM64_GREG_WIDTH 64
#define AUTOC_ARCH__ARM64_FREG_WIDTH 256

#define AUTOC_ARCH__ARM_BYTE_WIDTH 8
#define AUTOC_ARCH__ARM_GREG_WIDTH 32
#define AUTOC_ARCH__ARM_FREG_WIDTH 128

#ifdef AUTOC_ARCH__ARM64
#	define AUTOC_ARCH_ARM AUTOC_ARCH__ARM64
#elif defined AUTOC_ARCH__AARCH64
#	define AUTOC_ARCH_ARM AUTOC_ARCH__AARCH64
#elif defined AUTOC_ARCH__ARM
#	define AUTOC_ARCH_ARM AUTOC_ARCH__ARM
#elif defined AUTOC_ARCH__AARCH
#	define AUTOC_ARCH_ARM AUTOC_ARCH__AARCH
#endif
