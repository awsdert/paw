#pragma once

#ifdef __I86__
#	define _AUTOC_ARCH_X86_VER (__I86__ * 100)
#elif defined _M_IX86
#	define _AUTOC_ARCH_X86_VER _M_IX86
#else
#	define _AUTOC_ARCH_X86_VER -1
#endif

#if	defined __IA64__ || defined __ia64__ || defined _M_IA64 || defined __ia64 \
	|| defined __itanium__
#	define AUTOC_ARCH__IA64 IA64
#endif

#if	defined __I986__ || defined __i986__ || defined _M_I986 || defined __986 \
	|| _AUTOC_ARCH_X86_VER == 900 || defined __986__
#	define AUTOC_ARCH__i986 I986
#endif

#if	defined __I886__ || defined __i886__ || defined _M_I886 || defined __886 \
	|| _AUTOC_ARCH_X86_VER == 800 || defined __886__
#	define AUTOC_ARCH__i886 I886
#endif

#if	defined __I786__ || defined __i786__ || defined _M_I786 || defined __786 \
	|| _AUTOC_ARCH_X86_VER == 700 || defined __786__
#	define AUTOC_ARCH__i786 I786
#endif

#if	defined __I686__ || defined __i686__ || defined _M_I686 || defined __686 \
	|| _AUTOC_ARCH_X86_VER == 600 || defined __686__
#	define AUTOC_ARCH__i686 I686
#endif

#if	defined __I586__ || defined __i586__ || defined _M_I586 || defined __586 \
	|| _AUTOC_ARCH_X86_VER == 500 || defined __586__
#	define AUTOC_ARCH__i586 I586
#endif

#if	defined __I486__ || defined __i486__ || defined _M_I486 || defined __486 \
	|| _AUTOC_ARCH_X86_VER == 400 || defined __486__
#	define AUTOC_ARCH__i486 I486
#endif

#if	defined __I386__ || defined __i386__ || defined _M_I386 || defined __386 \
	|| _AUTOC_ARCH_X86_VER == 300 || defined __386__ || defined __IA32__
#	define AUTOC_ARCH__i386 I386
#endif

#if	defined __I286__ || defined __i286__ || defined _M_I286 || defined __286
#	define AUTOC_ARCH__i286 I286
#endif

#if	defined __I8086__ || defined __i8086__ || defined _M_I8086 \
	|| defined __8086 || defined _M_I86
#	define AUTOC_ARCH__i8086 I8086
#endif

#define AUTOC_ARCH__IA64_BYTE_WIDTH  8
#define AUTOC_ARCH__IA64_GREG_WIDTH  64
#define AUTOC_ARCH__IA64_FREG_WIDTH  256

#define AUTOC_ARCH__I986_BYTE_WIDTH  8
#define AUTOC_ARCH__I986_GREG_WIDTH  32
#define AUTOC_ARCH__I986_FREG_WIDTH  128

#define AUTOC_ARCH__I886_BYTE_WIDTH  8
#define AUTOC_ARCH__I886_GREG_WIDTH  32
#define AUTOC_ARCH__I886_FREG_WIDTH  128

#define AUTOC_ARCH__I786_BYTE_WIDTH  8
#define AUTOC_ARCH__I786_GREG_WIDTH  32
#define AUTOC_ARCH__I786_FREG_WIDTH  128

#define AUTOC_ARCH__I686_BYTE_WIDTH  8
#define AUTOC_ARCH__I686_GREG_WIDTH  32
#define AUTOC_ARCH__I686_FREG_WIDTH  128

#define AUTOC_ARCH__I586_BYTE_WIDTH  8
#define AUTOC_ARCH__I586_GREG_WIDTH  32
#define AUTOC_ARCH__I586_FREG_WIDTH  128

#define AUTOC_ARCH__I486_BYTE_WIDTH  8
#define AUTOC_ARCH__I486_GREG_WIDTH  32
#define AUTOC_ARCH__I486_FREG_WIDTH  128

#define AUTOC_ARCH__I386_BYTE_WIDTH  8
#define AUTOC_ARCH__I386_GREG_WIDTH  32
#define AUTOC_ARCH__I386_FREG_WIDTH  128

#define AUTOC_ARCH__I286_BYTE_WIDTH  8
#define AUTOC_ARCH__I286_GREG_WIDTH  16
#define AUTOC_ARCH__I286_FREG_WIDTH  64

#define AUTOC_ARCH__I8086_BYTE_WIDTH 8
#define AUTOC_ARCH__I8086_GREG_WIDTH 16
#define AUTOC_ARCH__I8086_FREG_WIDTH 64

/* Create a common name to work with */
#ifdef AUTOC_ARCH__IA64
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__IA64
#elif defined AUTOC_ARCH__i986
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i986
#	define AUTOC_ARCH_X86_VER 900
#elif defined AUTOC_ARCH__i886
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i886
#	define AUTOC_ARCH_X86_VER 800
#elif defined AUTOC_ARCH__i786
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i786
#	define AUTOC_ARCH_X86_VER 700
#elif defined AUTOC_ARCH__i686
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i686
#	define AUTOC_ARCH_X86_VER 600
#elif defined AUTOC_ARCH__i586
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i586
#	define AUTOC_ARCH_X86_VER 500
#elif defined AUTOC_ARCH__i486
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i486
#	define AUTOC_ARCH_X86_VER 400
#elif defined AUTOC_ARCH__i386
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i386
#	define AUTOC_ARCH_X86_VER 300
#elif defined AUTOC_ARCH__i286
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i286
#	define AUTOC_ARCH_X86_VER 200
#elif defined AUTOC_ARCH__i8086
#	define AUTOC_ARCH_INTEL AUTOC_ARCH__i8086
#	define AUTOC_ARCH_X86_VER 0
#endif

#if !defined AUTOC_ARCH_X86_VER && _AUTOC_ARCH_X86_VER >= 0
#	define AUTOC_ARCH_X86_VER _AUTOC_ARCH_X86_VER
#endif
