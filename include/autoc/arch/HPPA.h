#pragma once

#if defined __hppa20__ || defined __HPPA20__ || defined __hppa20 \
	|| defined __PA8000__
#	define AUTOC_ARCH__HPPA20 HPPA20
#elif defined __hppa11__ || defined __HPPA11__ || defined __hppa11 \
	|| defined __PA7100__
#	define AUTOC_ARCH__HPPA11 HPPA11
#elif defined __hppa__ || defined __HPPA__ || defined __hppa
#	define AUTOC_ARCH__HPPA HPPA
#endif

#ifdef AUTOC_ARCH__HPPA20
#	define AUTOC_ARCH_HPPA AUTOC_ARCH__HPPA20
#elif defined AUTOC_ARCH__HPPA11
#	define AUTOC_ARCH_HPPA AUTOC_ARCH__HPPA11
#elif defined AUTOC_ARCH__HPPA
#	define AUTOC_ARCH_HPPA AUTOC_ARCH__HPPA
#endif
