#pragma once

#if defined __SH5__ || defined __sh5__
#	define AUTOC_ARCH__SUPERH5 SUPERH5
#endif

#if defined __SH4__ || defined __sh4__
#	define AUTOC_ARCH__SUPERH4 SUPERH4
#endif

#if defined __SH3__ || defined __sh3__
#	define AUTOC_ARCH__SUPERH3 SUPERH3
#endif

#if defined __SH2__ || defined __sh2__
#	define AUTOC_ARCH__SUPERH2 SUPERH2
#endif

#if defined __SH1__ || defined __sh1__
#	define AUTOC_ARCH__SUPERH1 SUPERH1
#endif

#if defined __SH__ || defined __sh__
#	define AUTOC_ARCH__SUPERH SUPERH
#endif
