#pragma once

#if defined __mc68060__ || defined __MC68060__ || defined MC68060 \
	||  defined __mc68060
#	define AUTOC_ARCH__MC68060 MC68060
#endif

#if defined __mc68040__ || defined __MC68040__ || defined MC68040 \
	||  defined __mc68040
#	define AUTOC_ARCH__MC68040 MC68040
#endif

#if defined __mc68030__ || defined __MC68030__ || defined MC68030 \
	||  defined __mc68030
#	define AUTOC_ARCH__MC68030 MC68030
#endif

#if defined __mc68020__ || defined __MC68020__ || defined MC68020 \
	||  defined __mc68020
#	define AUTOC_ARCH__MC68020 MC68020
#endif

#if defined __mc68010__ || defined __MC68010__ || defined MC68010 \
	||  defined __mc68010
#	define AUTOC_ARCH__MC68010 MC68010
#endif

#if defined __mc68000__ || defined __MC68000__ || defined MC68000 \
	||  defined __mc68000
#	define AUTOC_ARCH__MC68000 MC68000
#endif

#if defined __M68K__ || defined __m68k__ || defined _M_M68K \
	|| defined __MC68K__
#	define AUTOC_ARCH__MC68K
#endif
