#pragma once

#if	defined __PPC64__ || defined __ppc64__ || defined _M_PPC64 \
	|| defined _ARCH_PPC64
#	define AUTOC_ARCH__PPC64 PowerPC64
#endif

#if	defined __PowerPC64__ || defined __powerpc64__
#	define AUTOC_ARCH__PowerPC64 PowerPC64
#endif

#if	defined __PPC__ || defined __ppc__ || defined _M_PPC \
	|| defined _ARCH_PPC
#	define AUTOC_ARCH__PPC PowerPC
#endif

#if	defined __PowerPC__ || defined __powerpc__
#	define AUTOC_ARCH__PowerPC PowerPC
#endif

#define AUTOC_ARCH__PowerPC64_BYTE_WIDTH 8
#define AUTOC_ARCH__PowerPC64_GREG_WIDTH 64
#define AUTOC_ARCH__PowerPC64_FREG_WIDTH 256

#define AUTOC_ARCH__PowerPC_BYTE_WIDTH 8
#define AUTOC_ARCH__PowerPC_GREG_WIDTH 32
#define AUTOC_ARCH__PowerPC_FREG_WIDTH 128

#ifdef AUTOC_ARCH__PowerPC64
#	define AUTOC_ARCH_PowerPC AUTOC_ARCH__PowerPC64
#elif defined AUTOC_ARCH__PowerPC
#	define AUTOC_ARCH_PowerPC AUTOC_ARCH__PowerPC
#endif
