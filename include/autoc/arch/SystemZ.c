#pragma once

#if defined __ZARCH__ || defined __zarch__ || defined _M_ZARCH \
	|| defined __zarch || defined __THW_ZARCH__ || defined __SYSC_ZARCH__
#	define AUTOC_ARCH__SystemZarch SystemZarch
#endif

#if defined __S390X__ || defined __s390x__ || defined _M_S390X \
	|| defined __s390x || defined __THW_390X__ || defined __SYSC_S390X__
#	define AUTOC_ARCH__SystemZ390X SystemZ390X
#endif

#if defined __S390__ || defined __s390__ || defined _M_S390 \
	|| defined __s390 || defined __THW_390__ || defined __SYSC_S390__
#	define AUTOC_ARCH__SystemZ390 SystemZ390
#endif

#if defined __S370__ || defined __s370__ || defined _M_S370 \
	|| defined __s370 || defined __THW_370__ || defined __SYSC_S370__
#	define AUTOC_ARCH__SystemZ370 SystemZ370
#endif

#ifdef AUTOC_ARCH__SystemZ390X
#	define AUTOC_ARCH_SystemZ AUTOC_ARCH__SystemZ390X
#elif defined AUTOC_ARCH__SystemZ390
#	define AUTOC_ARCH_SystemZ AUTOC_ARCH__SystemZ390
#elif defined AUTOC_ARCH__SystemZ370
#	define AUTOC_ARCH_SystemZ AUTOC_ARCH__SystemZ370
#elif defined AUTOC_ARCH__SystemZarch
#	define AUTOC_ARCH_SystemZ AUTOC_ARCH__SystemZarch
#endif
