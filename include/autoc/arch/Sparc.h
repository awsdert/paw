#pragma once

#if defined __sparcv9__ || defined __sparcv9
#	defined AUTOC_ARCH__SPARC_V9 SPARK_V9
#endif

#if defined __sparcv8__ || defined __sparcv8
#	defined AUTOC_ARCH__SPARC_V8 SPARK_V8
#endif

#if defined __sparc__ || defined __sparc
#	defined AUTOC_ARCH__SPARC SPARK
#endif
