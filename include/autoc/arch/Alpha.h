#pragma once

#ifdef __alpha_ev6__
#	define AUTOC_ARCH__Alpha_EV6 AlphaEV6
#endif

#if defined __alpha_ev5__
#	define AUTOC_ARCH__Alpha_EV5 AlphaEV5
#endif

#if defined __alpha_ev4__
#	define AUTOC_ARCH__Alpha_EV4 AlphaEV4
#endif

#if defined __alpha_ev3__
#	define AUTOC_ARCH__Alpha_EV3 AlphaEV3
#endif

#if defined __alpha_ev2__
#	define AUTOC_ARCH__Alpha_EV2 AlphaEV2
#endif

#if defined __alpha__ || defined _M_ALPHA || defined __alpha
#	define AUTOC_ARCH__Alpha Alpha
#endif

#ifdef ARCH__AlphaEV6
#	define AUTOC_ARCH_Alpha AUTOC_ARCH_Alpha_EV6
#elif defined ARCH__AlphaEV5
#	define AUTOC_ARCH_Alpha AUTOC_ARCH_Alpha_EV5
#elif defined ARCH__AlphaEV4
#	define AUTOC_ARCH_Alpha AUTOC_ARCH_Alpha_EV4
#elif defined ARCH__AlphaEV3
#	define AUTOC_ARCH_Alpha AUTOC_ARCH_Alpha_EV3
#elif defined ARCH__AlphaEV2
#	define AUTOC_ARCH_Alpha AUTOC_ARCH_Alpha_EV2
#elif AUTOC_ARCH_Alpha
#	define AUTOC_ARCH_Alpha AUTOC_ARCH__Alpha
#endif
