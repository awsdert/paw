#pragma once

#if	defined __AMD64__ || defined __amd64__ || defined _M_AMD64 \
	|| defined __amd64
#	define AUTOC_ARCH__AMD64	AMD64
#endif

#if	defined __AMD__ || defined __amd__ || defined _M_AMD
#	define AUTOC_ARCH__AMD		AMD
#endif

#define AUTOC_ARCH__AMD64_BYTE_WIDTH 8
#define AUTOC_ARCH__AMD64_GREG_WIDTH 64
#define AUTOC_ARCH__AMD64_FREG_WIDTH 256

#define AUTOC_ARCH__AMD_BYTE_WIDTH 8
#define AUTOC_ARCH__AMD_GREG_WIDTH 32
#define AUTOC_ARCH__AMD_FREG_WIDTH 128

#ifdef AUTOC_ARCH__AMD64
#	define AUTOC_ARCH_AMD AUTOC_ARCH__AMD64
#elif defined AUTOC_ARCH__AMD
#	define AUTOC_ARCH_AMD AUTOC_ARCH__AMD
#endif
