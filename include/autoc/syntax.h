#pragma once

#if defined( __STDC_VERSION__ )
#	define AUTOC_STDC_MAJOR (__STDC_VERSION__ / 100)
#	define AUTOC_STDC_MINOR (__STDC_VERSION__ % 100)
#	define AUTOC_STDC_PATCH 0L
#elif defined(__STRICT_ANSI__) || defined(_ANSI_SOURCE)
#	define AUTOC_STDC_MAJOR 1989L
#	define AUTOC_STDC_MINOR 0L
#	define AUTOC_STDC_PATCH 0L
#elif defined(__STDC__)
#	define AUTOC_STDC_MAJOR 1989L
#	define AUTOC_STDC_MINOR 0L
#	define AUTOC_STDC_PATCH 0L
#else
#	define AUTOC_STDC_MAJOR 0L
#	define AUTOC_STDC_MINOR 0L
#	define AUTOC_STDC_PATCH 0L
#endif

#define AUTOC_STDC_VERSION \
	AUTOC_MKVERINT(AUTOC_STDC_MAJOR,AUTOC_STDC_MINOR,AUTOC_STDC_PATCH)

#ifdef __cplusplus_cli
#	define AUTOC_CPP __cplusplus_cli
#elif defined __cplusplus
#	define AUTOC_CPP __cplusplus
#elif AUTOC_STDC_MAJOR
#	define AUTOC_STDC stdc
#elif defined __STDC__
#	define AUTOC_STDC stdc
#else
#	define AUTOC_GNUC gnuc
#endif

#ifdef AUTOC_CPP
#	define AUTOC_C_LINK extern "C"
#	define AUTOC_C_OPEN extern "C" {
#	define AUTOC_C_SHUT }
#else
#	define AUTOC_C_LINK
#	define AUTOC_C_OPEN
#	define AUTOC_C_SHUT
#endif
