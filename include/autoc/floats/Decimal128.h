#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Decimal128_RADIX   10
#define AUTOC_SPEC_FPN_Decimal128_SIG_POS 31
#define AUTOC_SPEC_FPN_Decimal128_EXP_DIG 0
#define AUTOC_SPEC_FPN_Decimal128_MAN_DIG 17
#define AUTOC_SPEC_FPN_Decimal128_DEC_DIG 110
#define AUTOC_SPEC_FPN_Decimal128_HAS_INF 1
#define AUTOC_SPEC_FPN_Decimal128_HAS_NAN 1
