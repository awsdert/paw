#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of, is for nvidia gpus */

#define AUTOC_SPEC_FPN_Tensor19_RADIX   2
#define AUTOC_SPEC_FPN_Tensor19_SIG_POS 18
#define AUTOC_SPEC_FPN_Tensor19_EXP_DIG 8
#define AUTOC_SPEC_FPN_Tensor19_EXP_BIAS 127
#define AUTOC_SPEC_FPN_Tensor19_MAN_DIG 10
#define AUTOC_SPEC_FPN_Tensor19_DEC_DIG 0
#define AUTOC_SPEC_FPN_Tensor19_IMP_BIT 1
#define AUTOC_SPEC_FPN_Tensor19_HAS_INF 1
#define AUTOC_SPEC_FPN_Tensor19_HAS_NAN 1
