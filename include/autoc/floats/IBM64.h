#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_IBM64_RADIX   16
#define AUTOC_SPEC_FPN_IBM64_SIG_POS 63
#define AUTOC_SPEC_FPN_IBM64_EXP_DIG 7
#define AUTOC_SPEC_FPN_IBM64_MAN_DIG 56
#define AUTOC_SPEC_FPN_IBM64_DEC_DIG 0
#define AUTOC_SPEC_FPN_IBM64_HAS_INF 0
#define AUTOC_SPEC_FPN_IBM64_HAS_NAN 0
