#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Microsoft16_RADIX   2
#define AUTOC_SPEC_FPN_Microsoft16_SIG_POS 7
#define AUTOC_SPEC_FPN_Microsoft16_EXP_DIG 8
#define AUTOC_SPEC_FPN_Microsoft16_EXP_BIAS 128
#define AUTOC_SPEC_FPN_Microsoft16_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Microsoft16_MAN_DIG 7
#define AUTOC_SPEC_FPN_Microsoft16_HAS_INF 0
#define AUTOC_SPEC_FPN_Microsoft16_HAS_NAN 0
