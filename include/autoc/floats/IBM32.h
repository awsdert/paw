#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_IBM32_RADIX   16
#define AUTOC_SPEC_FPN_IBM32_SIG_POS 31
#define AUTOC_SPEC_FPN_IBM32_EXP_DIG 7
#define AUTOC_SPEC_FPN_IBM32_MAN_DIG 24
#define AUTOC_SPEC_FPN_IBM32_DEC_DIG 0
#define AUTOC_SPEC_FPN_IBM32_HAS_INF 0
#define AUTOC_SPEC_FPN_IBM32_HAS_NAN 0
