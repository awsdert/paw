#pragma once
#include "General.h"

/* Alternate names: minifloat */

#define AUTOC_SPEC_FPN_Binary8_RADIX   2
#define AUTOC_SPEC_FPN_Binary8_SIG_POS 7
#define AUTOC_SPEC_FPN_Binary8_EXP_DIG 4
#define AUTOC_SPEC_FPN_Binary8_EXP_BIAS 7
#define AUTOC_SPEC_FPN_Binary8_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary8_MAN_DIG 3
#define AUTOC_SPEC_FPN_Binary8_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary8_IMP_BIT 1
#define AUTOC_SPEC_FPN_Binary8_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary8_HAS_NAN 1
