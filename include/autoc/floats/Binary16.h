#pragma once
#include "General.h"

/* Alternate names: half */

#define AUTOC_SPEC_FPN_Binary16_RADIX   2
#define AUTOC_SPEC_FPN_Binary16_SIG_POS 15
#define AUTOC_SPEC_FPN_Binary16_EXP_DIG 5
#define AUTOC_SPEC_FPN_Binary16_EXP_BIAS 15
#define AUTOC_SPEC_FPN_Binary16_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary16_MAN_DIG 10
#define AUTOC_SPEC_FPN_Binary16_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary16_IMP_BIT 1
#define AUTOC_SPEC_FPN_Binary16_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary16_HAS_NAN 1
