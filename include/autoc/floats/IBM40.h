#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_IBM40_RADIX   16
#define AUTOC_SPEC_FPN_IBM40_SIG_POS 39
#define AUTOC_SPEC_FPN_IBM40_EXP_DIG 7
#define AUTOC_SPEC_FPN_IBM40_MAN_DIG 32
#define AUTOC_SPEC_FPN_IBM40_DEC_DIG 0
#define AUTOC_SPEC_FPN_IBM40_HAS_INF 0
#define AUTOC_SPEC_FPN_IBM40_HAS_NAN 0
