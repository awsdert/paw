#pragma once
#include "General.h"

/* Alternate names: octuple */

#define AUTOC_SPEC_FPN_Binary256_RADIX   2
#define AUTOC_SPEC_FPN_Binary256_SIG_POS 255
#define AUTOC_SPEC_FPN_Binary256_EXP_DIG 19
#define AUTOC_SPEC_FPN_Binary256_EXP_BIAS 262143
#define AUTOC_SPEC_FPN_Binary256_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary256_MAN_DIG 236
#define AUTOC_SPEC_FPN_Binary256_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary256_IMP_BIT 0
#define AUTOC_SPEC_FPN_Binary256_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary256_HAS_NAN 1
