#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Microsoft32_RADIX   2
#define AUTOC_SPEC_FPN_Microsoft32_SIG_POS 23
#define AUTOC_SPEC_FPN_Microsoft32_EXP_DIG 8
#define AUTOC_SPEC_FPN_Microsoft32_EXP_BIAS 128
#define AUTOC_SPEC_FPN_Microsoft32_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Microsoft32_MAN_DIG 23
#define AUTOC_SPEC_FPN_Microsoft32_HAS_INF 0
#define AUTOC_SPEC_FPN_Microsoft32_HAS_NAN 0
