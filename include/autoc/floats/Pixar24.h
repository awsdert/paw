#pragma once
#include "General.h"

/* Alternate names: half */

#define AUTOC_SPEC_FPN_Pixar24_RADIX   2
#define AUTOC_SPEC_FPN_Pixar24_SIG_POS 23
#define AUTOC_SPEC_FPN_Pixar24_EXP_DIG 8
#define AUTOC_SPEC_FPN_Pixar24_EXP_BIAS 127
#define AUTOC_SPEC_FPN_Pixar24_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Pixar24_MAN_DIG 15
#define AUTOC_SPEC_FPN_Pixar24_DEC_DIG 0
#define AUTOC_SPEC_FPN_Pixar24_IMP_BIT 1
#define AUTOC_SPEC_FPN_Pixar24_HAS_INF 0
#define AUTOC_SPEC_FPN_Pixar24_HAS_NAN 0
