#pragma once
#include "General.h"

/* Alternate names: quadruple */

#define AUTOC_SPEC_FPN_Binary128_RADIX   2
#define AUTOC_SPEC_FPN_Binary128_SIG_POS 127
#define AUTOC_SPEC_FPN_Binary128_EXP_DIG 15
#define AUTOC_SPEC_FPN_Binary128_EXP_BIAS 16383
#define AUTOC_SPEC_FPN_Binary128_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary128_MAN_DIG 112
#define AUTOC_SPEC_FPN_Binary128_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary128_IMP_BIT 1
#define AUTOC_SPEC_FPN_Binary128_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary128_HAS_NAN 1
