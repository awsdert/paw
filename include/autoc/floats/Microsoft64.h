#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Microsoft64_RADIX   2
#define AUTOC_SPEC_FPN_Microsoft64_SIG_POS 55
#define AUTOC_SPEC_FPN_Microsoft64_EXP_DIG 8
#define AUTOC_SPEC_FPN_Microsoft64_EXP_BIAS 128
#define AUTOC_SPEC_FPN_Microsoft64_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Microsoft64_MAN_DIG 55
#define AUTOC_SPEC_FPN_Microsoft64_HAS_INF 0
#define AUTOC_SPEC_FPN_Microsoft64_HAS_NAN 0
