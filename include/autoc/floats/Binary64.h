#pragma once
#include "General.h"

/* Alternate names: double */

#define AUTOC_SPEC_FPN_Binary64_RADIX   2
#define AUTOC_SPEC_FPN_Binary64_SIG_POS 63
#define AUTOC_SPEC_FPN_Binary64_EXP_DIG 11
#define AUTOC_SPEC_FPN_Binary64_EXP_BIAS 1023
#define AUTOC_SPEC_FPN_Binary64_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary64_MAN_DIG 52
#define AUTOC_SPEC_FPN_Binary64_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary64_IMP_BIT 1
#define AUTOC_SPEC_FPN_Binary64_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary64_HAS_NAN 1
