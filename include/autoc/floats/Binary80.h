#pragma once
#include "General.h"

/* Alternate names: extended double */

#define AUTOC_SPEC_FPN_Binary80_RADIX   2
#define AUTOC_SPEC_FPN_Binary80_SIG_POS 79
#define AUTOC_SPEC_FPN_Binary80_EXP_DIG 15
#define AUTOC_SPEC_FPN_Binary80_EXP_BIAS 16383
#define AUTOC_SPEC_FPN_Binary80_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary80_MAN_DIG 63
#define AUTOC_SPEC_FPN_Binary80_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary80_IMP_BIT 0
#define AUTOC_SPEC_FPN_Binary80_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary80_HAS_NAN 1
