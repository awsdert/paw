#pragma once
#include "../arch.h"

#ifdef AUTOC_CC_GNU
#	define AUTOC_FLT_RADIX       __FLT_RADIX__
#	ifdef AUTOC_CC_CLANG
#		define AUTOC_FLT_ROUNDS     (__builtin_flt_rounds())
#	else
#		define AUTOC_FLT_ROUNDS -1
#	endif
#	define AUTOC_FLT_MODE(MODE)  __attribute__((mode(MODE)))
#	if AUTOC_CC_GNU_MAJOR >= 7
#		define AUTOC_FLT_FABS(T)    ((T)__builtin_fabsq)
#		define AUTOC_FLT_INF(T)     ((T)__builtin_inf())
#		define AUTOC_FLT_NAN(T,X)   ((T)__builtin_nan(x))
#		define AUTOC_FLT_NANS(T,X)  ((T)__builtin_nans(x))
#	endif
#	ifdef __FLT_EVAL_METHOD__
#		define AUTOC_FLT_EVAL_METHOD __FLT_EVAL_METHOD__
#	else
#		define AUTOC_FLT_EVAL_METHOD -2
#	endif
#	if defined AUTOC_ARCH__ARM64 || defined AUTOC_ARCH__AARCH64
#		pragma GCC push_options "-mfp16-format"
#		define AUTOC_FLT16_TYPE __fp16
#	elif defined AUTOC_ARCH_x86_64 || defined AUTOC_ARCH_x86
#		define AUTOC_FLT16_TYPE _Float16
#	endif
#endif

#if !defined AUTOC_FLT_ENDIAN && defined AUTOC_ARCH_FREG_WIDTH
#	if (AUTOC_APPEND2(AUTOC_ARCH_,AUTOC_ARCH,_FLT_ENDIAN))
#		define AUTOC_FLT_ENDIAN AUTOC_ARCH_##AUTOC_ARCH##_FLT_ENDIAN
#	elif defined _AUTOC_ARCH_BDIRECT_ORDER
#		define AUTOC_FLT_ENDIAN AUTOC_BDIRECT_ORDER
#	elif defined _AUTOC_ARCH_LENDIAN_ORDER
#		define AUTOC_FLT_ENDIAN AUTOC_LENDIAN_ORDER
#	elif defined _AUTOC_ARCH_BENDIAN_ORDER
#		define AUTOC_FLT_ENDIAN AUTOC_BENDIAN_ORDER
#	endif
#endif

/* Capabilities*/

#if defined __FLT_HAS_INFINITY__
#	define AUTOC_ARCH_FLT_INF 1
#endif

#if defined __LDBL_HAS_INFINITY__
#	define AUTOC_ARCH_LDB_INF 1
#endif

#if defined __FLT_HAS_QUIET_NAN__
#	define AUTOC_ARCH_FLT_NAN 1
#endif

#if defined __LDBL_HAS_QUIET_NAN__
#	define AUTOC_ARCH_LDB_NAN 1
#endif

/* Taken from https://en.wikipedia.org/wiki/IEEE_754
| Mode             | Example value                 |
|                  | +11.5 | +12.5 | −11.5 | −12.5 |
| nearest even int | +12.0 | +12.0 | −12.0 | −12.0 |
| nearest from 0   | +12.0 | +13.0 | −12.0 | −13.0 |
| toward 0         | +11.0 | +12.0 | −11.0 | −12.0 |
| toward +infinity | +12.0 | +13.0 | −11.0 | −12.0 |
| toward −infinity | +11.0 | +12.0 | −12.0 | −13.0 |
*/

#define AUTOC_ROUND_NEARESTFROM0(SIGN,NEARER0,EVEN) !(NEARER0)
#define AUTOC_ROUND_NEARESTEVEN(SIGN,NEARER0,EVEN) ((EVEN))
/* Not used in any standards as far as I can tell but might as well define it */
#define AUTOC_ROUND_NEARESTODD(SIGN,NEARER0,EVEN) (!(EVEN))
#define AUTOC_ROUND_TOPOSINF(SIGN,NEARER0,EVEN) (!(SIGN))
#define AUTOC_ROUND_TONEGINF(SIGN,NEARER0,EVEN) !!(SIGN)
#define AUTOC_ROUND_TOWARDS0(SIGN,NEARER0,EVEN) 0
