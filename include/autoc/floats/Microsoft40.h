#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Microsoft40_RADIX   2
#define AUTOC_SPEC_FPN_Microsoft40_SIG_POS 31
#define AUTOC_SPEC_FPN_Microsoft40_EXP_DIG 8
#define AUTOC_SPEC_FPN_Microsoft40_EXP_BIAS 128
#define AUTOC_SPEC_FPN_Microsoft40_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Microsoft40_MAN_DIG 31
#define AUTOC_SPEC_FPN_Microsoft40_HAS_INF 0
#define AUTOC_SPEC_FPN_Microsoft40_HAS_NAN 0
