#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Decimal64_RADIX   10
#define AUTOC_SPEC_FPN_Decimal64_SIG_POS 63
#define AUTOC_SPEC_FPN_Decimal64_EXP_DIG 0
#define AUTOC_SPEC_FPN_Decimal64_MAN_DIG 13
#define AUTOC_SPEC_FPN_Decimal64_DEC_DIG 50
#define AUTOC_SPEC_FPN_Decimal64_HAS_INF 1
#define AUTOC_SPEC_FPN_Decimal64_HAS_NAN 1
