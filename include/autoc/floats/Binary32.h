#pragma once
#include "General.h"

/* Alternate names: single */

#define AUTOC_SPEC_FPN_Binary32_RADIX   2
#define AUTOC_SPEC_FPN_Binary32_SIG_POS 31
#define AUTOC_SPEC_FPN_Binary32_EXP_DIG 8
#define AUTOC_SPEC_FPN_Binary32_EXP_BIAS 127
#define AUTOC_SPEC_FPN_Binary32_EXP_SIGN 1
#define AUTOC_SPEC_FPN_Binary32_MAN_DIG 23
#define AUTOC_SPEC_FPN_Binary32_DEC_DIG 0
#define AUTOC_SPEC_FPN_Binary32_IMP_BIT 0
#define AUTOC_SPEC_FPN_Binary32_HAS_INF 1
#define AUTOC_SPEC_FPN_Binary32_HAS_NAN 1
