#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_GG711Audio8_RADIX   2
#define AUTOC_SPEC_FPN_GG711Audio8_SIG_POS 7
#define AUTOC_SPEC_FPN_GG711Audio8_EXP_DIG 3
#define AUTOC_SPEC_FPN_GG711Audio8_EXP_BIAS 0
#define AUTOC_SPEC_FPN_GG711Audio8_EXP_SIGN 0
#define AUTOC_SPEC_FPN_GG711Audio8_MAN_DIG 4
#define AUTOC_SPEC_FPN_GG711Audio8_DEC_DIG 0
#define AUTOC_SPEC_FPN_GG711Audio8_IMP_BIT 1
#define AUTOC_SPEC_FPN_GG711Audio8_HAS_INF 0
#define AUTOC_SPEC_FPN_GG711Audio8_HAS_NAN 0
