#pragma once
#include "General.h"

/* Alternate names: none that I'm aware of */

#define AUTOC_SPEC_FPN_Decimal32_RADIX   10
#define AUTOC_SPEC_FPN_Decimal32_SIG_POS 31
#define AUTOC_SPEC_FPN_Decimal32_EXP_DIG 0
#define AUTOC_SPEC_FPN_Decimal32_MAN_DIG 11
#define AUTOC_SPEC_FPN_Decimal32_DEC_DIG 20
#define AUTOC_SPEC_FPN_Decimal32_HAS_INF 1
#define AUTOC_SPEC_FPN_Decimal32_HAS_NAN 1
