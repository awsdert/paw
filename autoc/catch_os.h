#pragma once
typedef char const *string;
typedef   signed char schar;
typedef unsigned char uchar;
typedef unsigned short int ushort;
typedef unsigned int       uint;
typedef unsigned long  int ulong;
typedef   signed long long int llong;
typedef unsigned long long int ullong;

#define _FILE_OFFSET_BITS 64
#define _LARGEFILE64_SOURCE 1

#include <limits.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdarg.h>
#include <autoc/sys.h>

typedef struct { size_t len; string txt; } strvec;
typedef struct { size_t len; char  *txt; size_t max; } strbuf;
typedef struct { size_t total; strvec vec; } strneedsobj;

#ifdef AUTOC_SYS_MSW64
#	define ON_MSW 64
#elif defined( AUTOC_SYS_MSW32 )
#	define ON_MSW 32
#elif defined( AUTOC_SYS_MSW16 )
#	define ON_MSW 16
#else
#	define ON_MSW 0
#endif

#ifdef AUTOC_SYS_DOS32
#	define ON_DOS 32
#elif defined( AUTOC_SYS_DOS16 )
#	define ON_DOS 16
#else
#	define ON_DOS 0
#endif

#if ON_MSW
#	include <windows.h>
#	define ON_OSX 0
typedef TCHAR tchar_t;
typedef LONG offset_t;
typedef LARGE_INTEGER loffset_t;
typedef LARGE_INTEGER offset64_t;
inline size_t getcpuregistersize(void)
{
	CONTEXT ctx = {0};
	return sizeof(ctx.Dr0);
}
#	ifdef __x86_64__
inline size_t getcpuregistercount(void)
{
	KNONVOLATILE_CONTEXT_POINTERS knvp;
	return sizeof(knvp.DUMMYUNIONNAME2.IntegerContext) /
		sizeof(ctx.IntegerContext[0]);
}
#	else
inline size_t getcpuregistercount(void)
{
	KNONVOLATILE_CONTEXT_POINTERS knvp;
	return sizeof(knvp) / sizeof(knvp.Lr);
}
#	endif
#else
#	include <unistd.h>
#	include <sys/types.h>
#	include <sys/user.h>
#	include <sys/utsname.h>
#	include <sys/mman.h>
#	include <sys/stat.h>
#	include <fcntl.h>
// TODO: Implement this correctly, macintosh not accounted for
#	ifdef __APPLE__
#		define ON_OSX 1
#	else
#		define ON_OSX 0
#	endif

typedef char tchar_t;
typedef off_t offset_t;
typedef loff_t loffset_t;
typedef off64_t offset64_t;
typedef struct
{
	struct user_regs_struct ints;
	struct user_fpregs_struct floats;
#ifndef __x86_64__
	struct user_fpxregs_struct xfloats;
#endif
} abi_regs;
// We'll come back to this
//typedef struct user_fpxregs_struct abi_fpx_regs
inline size_t getcpuregistersize(void)
	{ struct user_regs_struct ints; return sizeof(ints.eflags); }
inline size_t getcpuregistercount(void)
	{ struct user_regs_struct ints; return sizeof(ints) / sizeof(ints.eflags); }
#endif

typedef struct
{
	int mswelse;
	string osname;
	string dirsep;
	string appext;
	string libext;
	string pathsep;
	string linesep;
	string cwdname;
	string abiopts;
	string refshvar;
	string mkshenvinit;
	string initenvname;
	string termenvname;
	string *compositors;
	strbuf safeosname;
#if ON_MSW < 1
	struct utsname uts;
#endif
} osnames_t;
