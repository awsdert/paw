#include "autoc.h"
/* Prevent oversized binary with non-inline functions */
int autocintxiu_cmp( autocintxiu a, autocintxiu b ) { return (int)(a > b) - (int)(a < b); }
autocintxiu autocintxiu_bwa( autocintxiu a, autocintxiu b ) { return a & b; }
autocintxiu autocintxiu_bwo( autocintxiu a, autocintxiu b ) { return a | b; }
autocintxiu autocintxiu_bwx( autocintxiu a, autocintxiu b ) { return a ^ b; }
autocintxiu autocintxiu_add( autocintxiu a, autocintxiu b ) { return a + b; }
autocintxiu autocintxiu_sub( autocintxiu a, autocintxiu b ) { return a - b; }
autocintxiu autocintxiu_shl( autocintxiu n, size_t by ) { return n << by; }
autocintxiu autocintxiu_shr( autocintxiu n, size_t by ) { return n >> by; }
size_t autocintxiu__len( autocintxiu n, size_t width, autocbool find0 )
{
	autocintxiu bit = find0;
	bit <<= (width - 1);
	while ( width )
	{
		if ( (n & bit) == bit )
			return width;
		bit >>= 1;
		--width;
	}
	return 0;
}
autocintxiu autocintxiu_mul( autocintxiu a, autocintxiu b ) { return a * b; }
inline size_t autocintxiu_len( autocintxiu n ) { return autocintxiu__len( n, bitsof(n), 0 ); }
extern inline size_t autocintxiu_len( autocintxiu n );
autocintxiudiv autocintxiu_div( autocintxiu a, autocintxiu b )
{
	autocintxiudiv divu = {0};
	if ( !b )
		divu.rem = a;
	else
	{
		divu.quo = a / b;
		divu.rem = a % b;
	}
	return divu;
}
size_t cc_toa_toosmall( char *dst, size_t max )
{
	switch ( max )
	{
	case 0: return 0;
	case 1: *dst = 0; return 0;
	}
	*dst = 0x18; /* Cancel */
	dst[1] = 0;
	return 1;
}
size_t ccn36toa( char *dst, size_t max, autocintxiu val, size_t base, char charA )
{
	autocintxiudiv div = {0};
	char txt[bitsof(autocintxiu)+1] = {0};
	size_t i = bitsof(autocintxiu), len = 0;
	if ( !max )
		return 0;
	dst[max-1]=0;
	do
	{
		div = autocintxiu_div(val,base);
		txt[--i] = (div.rem < 10)
			?   '0' +  div.rem
			: charA + (div.rem - 10);
		val = div.quo;
	}
	while ( val );
	len = bitsof(autocintxiu) - i;
	if ( len >= max )
		return cc_toa_toosmall( dst, len );
	memcpy( dst, txt + i, len );
	dst[len] = 0;
	return len;
}
extern inline size_t ccXtoa( char *dst, size_t max, autocintxiu val );
extern inline size_t ccxtoa( char *dst, size_t max, autocintxiu val );
extern inline size_t ccutoa( char *dst, size_t max, autocintxiu val );
extern inline size_t ccotoa( char *dst, size_t max, autocintxiu val );
extern inline size_t ccbtoa( char *dst, size_t max, autocintxiu val );
size_t ccitoa( char *dst, size_t max, autocintxid val )
{
	if ( !max )
		return 0;
	if ( val >= 0 )
		return ccutoa( dst, max, val );
	size_t len = ccutoa( dst + 1, max - 1, -val );
	if ( !len )
		return cc_toa_toosmall( dst, max );
	*dst = '-';
	return len + 1;
}
void calcint_type( calcint_t *dst, size_t width, TYPE T )
{

	union
	{
		autocintxid  d;
		autocintxiu u;
	} intmax, intmin;
	int x80isneg = 1;
	struct { size_t type, width; }
		types[TYPE_COUNT+1] = {{0}}, *type = types, *stop = types + TYPE_COUNT;

	for ( uint i = 0; i < TYPE_COUNT; ++i )
	{
		type[i].type = TYPE_ANY + i;
		type[i].width = WIDTHOF_ANY + i;
	}

	memset( dst, 0, sizeof(*dst) );

	if ( T && T < TYPE_COUNT )
		type = types + T;
	else
		for ( type = types + 1; type < stop && type->width < width; ++type );
	dst->width	= width;
	dst->size	= width / CHAR_BIT;
	dst->umax	= ~(dst->umax);
	intmax.u = dst->umax >> 1;
	intmin.d = -(intmax.d);
	x80isneg = (intmin.d - 1 < 0);
	dst->min = intmin.d - (int) x80isneg;
	dst->max = intmax.d + (int)!x80isneg;
}

int calcflt( uint radix, archtfuint min, string name )
{
	typedef union
	{
		archtf tf;
		archxf xf;
		archdf df;
		archsf sf;
		archhf hf;
		archtfuint val;
	} flt_union;
	typedef struct
	{
		string sfx;
		size_t mant_width;
		size_t misc_width;
	} flt_struct;
	flt_union maxval = { .val = min };
	flt_struct radix2details[] =
	{
		{  "f16", AUTOC_SPEC_FPN_Binary16_MAN_DIG, AUTOC_SPEC_FPN_Binary16_EXP_DIG },
		{  "f32", AUTOC_SPEC_FPN_Binary32_MAN_DIG, AUTOC_SPEC_FPN_Binary32_EXP_DIG },
		{  "f64", AUTOC_SPEC_FPN_Binary64_MAN_DIG, AUTOC_SPEC_FPN_Binary64_EXP_DIG },
		{  "f80", AUTOC_SPEC_FPN_Binary80_MAN_DIG, AUTOC_SPEC_FPN_Binary80_EXP_DIG },
		{ "f128", AUTOC_SPEC_FPN_Binary128_MAN_DIG, AUTOC_SPEC_FPN_Binary128_EXP_DIG },
		{ "f256", AUTOC_SPEC_FPN_Binary256_MAN_DIG, AUTOC_SPEC_FPN_Binary256_EXP_DIG },
		{ NULL, 0, 0 }
	}, *fltinfo = NULL;
	flt_struct radix10details[] =
	{
		{  "d32", AUTOC_SPEC_FPN_Decimal32_MAN_DIG, AUTOC_SPEC_FPN_Decimal32_DEC_DIG },
		{  "d64", AUTOC_SPEC_FPN_Decimal64_MAN_DIG, AUTOC_SPEC_FPN_Decimal64_DEC_DIG },
		{ "d128", AUTOC_SPEC_FPN_Decimal128_MAN_DIG, AUTOC_SPEC_FPN_Decimal128_DEC_DIG },
		{ NULL, 0, 0 }
	};
	flt_struct radix16details[] =
	{
		{ "f32", AUTOC_SPEC_FPN_IBM32_MAN_DIG, AUTOC_SPEC_FPN_IBM32_DEC_DIG },
		{ "f40", AUTOC_SPEC_FPN_IBM40_MAN_DIG, AUTOC_SPEC_FPN_IBM40_DEC_DIG },
		{ "f64", AUTOC_SPEC_FPN_IBM64_MAN_DIG, AUTOC_SPEC_FPN_IBM64_DEC_DIG },
		{ NULL, 0, 0 }
	};
	string sfx = NULL;
	size_t width = autocintxiu_len( min ), total = 0;
	switch ( radix )
	{
	case  2: fltinfo = radix2details;  break;
	case 10: fltinfo = radix10details; break;
	case 16: fltinfo = radix10details; break;
	default: return -1;
	}
	for ( ; fltinfo->sfx; ++fltinfo )
	{
		total = 1 + fltinfo->mant_width + fltinfo->misc_width;
		if ( width == total )
			break;
	}
	if ( !(fltinfo->sfx) )
			return -1;
	return 0;
}
