#include "autoc.h"
#include <fenv.h>
#undef MAX
#define MAX (bitsof(autocintxid)+2)
#define CAP (MAX * 2)
#define TRUECAP (MAX + CAP)
extern inline ssize_t vfputf( FILE *file, string format, va_list va );
extern inline ssize_t fputf( FILE *file, string format, ... );
extern inline ssize_t vfgetf( FILE *file, string format, va_list va );
extern inline ssize_t fgetf( FILE *file, string format, ... );
extern inline size_t fput( FILE *file, void const *data, size_t size, size_t n );
extern inline size_t fget( FILE *file, void *dest, size_t size, size_t n );
int failedmacro( common_t *common, string name )
{
	fprintf( stderr, "Failed to write '" PFX "%s' macro!", name );
	return myexit( -1, common, "Aborting..." );
}
int failedsubmacro( common_t *common, string name, string sfx )
{
	fprintf( stderr, "Failed to write '" PFX "%s_%s' macro!", name, sfx );
	return myexit( -1, common, "Aborting..." );
}
int print_macro_with_txt( common_t *common, string name, string txt, string quo )
{
	strbuf *temp = &(common->regtxt);
	if ( overidestrbuf( temp, quo, (txt && txt[0]) ? txt : "", quo, NULL ) < 0 )
		return -1;
	ssize_t did = fputf( common->file, common->macrodefine, name, temp->txt );
	if ( did < 0 )
		return failedmacro( common, name );
	return did;
}
int print_macro_with_var( common_t *common, string name, string var, string txt )
{
	strbuf *temp = &(common->regtxt);
	if ( overidestrbuf( temp, (txt && txt[0]) ? txt : "", NULL ) < 0 )
		return -1;
	ssize_t did = fputf( common->file, common->macromethod, name, var, temp->txt );
	if ( did < 0 )
		return failedmacro( common, name );
	return did;
}
int print_intlim_text( common_t *common, string name, string subname, string text )
{
	strbuf *macro = &(common->macsfx);
	return (overidestrbuf( macro, name, "_", subname, NULL ) < 0)
		? failedsubmacro( common, name, subname )
		: print_macro_with_txt( common, macro->txt, text, "" );
}
int print_intlim_func
(
	common_t *common,
	string name,
	string subname,
	string vars,
	string text
)
{
	strbuf *macro = &(common->macsfx);
	return (overidestrbuf( macro, name, "_", subname, NULL ) < 0)
		? failedsubmacro( common, name, subname )
		: print_macro_with_var( common, macro->txt, vars, text );
}
int print_macro_with_str( common_t *common, string name, string str, bool andtxt )
{
	strbuf *macro = &(common->literal);
	ssize_t did = 0;
	if ( andtxt )
	{
		if ( overidestrbuf( macro, name, "_TXT", NULL ) < 0 )
			return failedsubmacro( common, name, "TXT" );
		did = print_macro_with_txt( common, macro->txt, str, "" );
	}
	if ( overidestrbuf( macro, name, "_STR", NULL ) < 0 )
		return failedsubmacro( common, name, "_STR" );
	return did + print_macro_with_txt
		( common, macro->txt, str, common->esc_dquo );
}
int print_macro_with_sfx( common_t *common, string name, string sfx, bool skipstr )
{
	strbuf *temp = &(common->macsfx);
	int didstr = 0, did = overidestrbuf( temp, name, "_WAS_", sfx, NULL );
	if ( did < 0 )
		return -1;
	did = fputf( common->file, common->macrodefine, temp->txt, "1" );
	if ( !skipstr )
		didstr = print_macro_with_str( common, name, sfx, true );
	return (did < 0 || didstr < 0) ? -1 : did + didstr;
}

int print_intlim_uint( common_t *common, string name, string subname, autocintxiu value, int sfxU, string sfx )
{
	strbuf *literal = &(common->literal);
	if ( !(literal->txt) || literal->max < TRUECAP )
		return failedsubmacro( common, name, subname );
	char *num = literal->txt;
	char *txt = num + MAX;
	if ( ccutoa( num, MAX, value ) == 0 )
		return failedsubmacro( common, name, subname );
	snprintf( txt, CAP, "%s%c%s", num, sfxU, sfx );
	return print_intlim_text( common, name, subname, txt );
}
int print_intlim_dint
	( common_t *common, string name, string subname, autocintxid value, string sfx )
{
	strbuf *literal = &(common->literal);
	if ( !(literal->txt) || literal->max < TRUECAP )
		return failedsubmacro( common, name, subname );
	char *num = literal->txt;
	char *txt = num + MAX;
	if ( ccitoa( num, MAX, value ) == 0 )
		return failedsubmacro( common, name, subname );
	snprintf( txt, CAP, "%s%s", num, sfx );
	return print_intlim_text( common, name, subname, txt );
}
int print_intlim_dmin
	( common_t *common, string name, string subname, autocintxid max, string sfx )
{
	strbuf *literal = &(common->literal);
	if ( !(literal->txt) || literal->max < TRUECAP )
		return failedsubmacro( common, name, subname );
	char *num = literal->txt;
	char *txt = num + MAX;
	if ( ccitoa( num, MAX, max ) == 0 )
		return failedsubmacro( common, name, subname );
	snprintf( txt, CAP, "(-%s%s - 1)", num, sfx );
	return print_intlim_text( common, name, subname, txt );
}

int print_comment( common_t *common, string line, ... )
{
	va_list va;
	ssize_t len = -1, did = 0, done = 0;
	strbuf *Line = &(common->regtxt);
	string pfx = common->mlinepfx;
	string sfx = common->mlinesfx;
	if ( !line )
		return 0;
	va_start( va, line );
	if ( !pfx || !sfx )
	{
		pfx = common->slinepfx;
		sfx = NULL;
	}
	else
	{
		done = fputf( common->file, "%s\n", pfx );
		if ( done < 0 )
		{
			fprintf
			(
				stderr,
				"Failed to write multi-line comment prefix '%s'!\n",
				common->mlinepfx
			);
			return myexit( -1, common, "Aborting..." );
		}
		pfx = "";
	}
	while ( line )
	{
		did = fputf( common->file, "%s%s\n", pfx, line );
		if ( did < 0 )
		{
			fprintf( stderr, "Failed to write comment line '%s'!\n", line );
			return myexit( -1, common, "Aborting..." );
		}
		line = va_arg( va, string );
		if ( did < 0 )
			return -1;
		done += did;
	}
	if ( !sfx )
		return done;
	did = fputf( common->file, "%s\n", sfx );
	if ( did < 0 )
		return -1;
	return done + did;
}
/* Name here refers to the macro name, such as SHRT instead of short */
int printchar_limits
	( common_t *common, string name, size_t width, _Bool sign, string pfx )
{
	limits lim = {0};
	bitint bi = {{0}};
	getlimits( &lim, &bi, width, 0 );
	print_macro_with_sfx( common, name, sign ? "SIGNED" : "UNSIGNED", 1 );
	print_intlim_text( common, name, "PFX", pfx );
	print_intlim_text( common, name, "EXT", lim.ext );
	print_intlim_text( common, name, "TYPE", (width > CHAR_BIT && !sign)
		? lim.utype : lim.type );
	print_intlim_dint( common, name, "WIDTH", width, "" );
	print_intlim_dint( common, name, "SIZE", lim.size, "" );
	print_intlim_text( common, name, "SFX", lim.sfx );
	if ( lim.cap.u == ~0ULL && !sign )
	{
		print_intlim_uint( common, name, "MAX", lim.cap.u, 'U', lim.sfx );
		print_intlim_uint( common, name, "MIN",         0, 'U', lim.sfx );
	}
	else
	{
		print_intlim_dint( common, name, "MAX", lim.max.d, lim.sfx );
		print_intlim_dint( common, name, "MIN", lim.min.d, lim.sfx );
	}
	return 0;
}

int printxint_limits
(
	common_t *common,
	size_t width,
	string name,
	string dname,
	string uname,
	TYPE T
)
{
	bitint *bi = &(common->bint);
	limits *lim = &(common->limo);
	getlimits( lim, bi, width, T );
	//calcint_type( &(common->calcint), width, T );
	if ( name )
	{
		print_intlim_text( common, name, "EXT", lim->ext );
		print_intlim_text( common, name, "TYPE", lim->type );
		print_intlim_text( common, name, "STYPE", lim->stype );
		print_intlim_text( common, name, "UTYPE", lim->utype );
		print_intlim_dint( common, name, "WIDTH", width, "" );
		print_intlim_dint( common, name, "SIZE", lim->size, "" );
		if ( lim->sfx )
			print_intlim_text( common, name, "SFX", lim->sfx );
	}
	if ( !dname && !uname )
		return 0;
	if ( dname )
	{
		print_intlim_dint( common, dname, "MAX", lim->max.d, "" );
		print_intlim_dmin( common, dname, "MIN", lim->max.d, "" );
	}
	if ( uname )
		print_intlim_uint( common, uname, "MAX", lim->cap.u, (width < bitsof(int)) ? 0 : 'U', "" );
	return 0;
}
int printsint_limits( common_t *common, size_t width, string name, TYPE T )
	{ return printxint_limits( common, width, name, name, NULL, T ); }
int printint_limits( common_t *common, size_t width, string name, TYPE T )
{
	size_t len = strlen(name);
	char *uname = alloca(len+2);
	uname[0] = 'U';
	strncpy( uname + 1, name, len );
	uname[len+1] = 0;
	return printxint_limits( common, width, name, name, uname, T );
}
inline int print_cpuid( common_t *common, uint leaf )
{
	char name[bitsof(uint)] = {0};
	uint eax = 0, ebx = 0, ecx = 0, edx = 0;
	snprintf( name, sizeof(name), "CPUx%X", leaf );
	__get_cpuid( 0, &eax, &ebx, &ecx, &edx );
	int dideax = print_intlim_uint( common, name, "EAX", eax, 0, "" );
	int didebx = print_intlim_uint( common, name, "EBX", ebx, 0, "" );
	int didecx = print_intlim_uint( common, name, "ECX", ecx, 0, "" );
	int didedx = print_intlim_uint( common, name, "EDX", edx, 0, "" );
	return (dideax < 0 || didebx < 0 || didecx < 0 || didedx < 0)
		? -1 : dideax + didebx + didecx + didedx;
}
extern inline int print_cpuid( common_t *common, uint leaf );
int printall( FILE *file, common_t *common )
{
	osnames_t *names = common->osnames;
	strbuf *literal = &(common->literal);
	char_union neg1 = { -1 };
	if ( !(literal->txt = calloc(TRUECAP,1)) )
		return -1;
	literal->max = TRUECAP;
	if ( !(common->asinc) )
	{
		ssize_t len = -1, did = -1;
		print_comment
		(
			common,
			"It is excessively difficult to gather this information via",
			"makefiles and/or the shell so we generate this file with",
			"autoc to help identify the ABI paths to place objects,",
			"libraries and applications into", NULL
		);
		fputf( common->file, "mswelse=$%d\n",names->mswelse );
		len = snprintf
		(
			literal->txt, literal->max,
			"newpath=$(if $1,$1%s)$(PATH)$(if $2,%s$2)\n",
			names->pathsep, names->pathsep
		);
		fput( common->file, literal->txt, 1, len );
		did = print_macro_with_txt( common, "abiopts", names->abiopts, "" );
		len = overidestrbuf
		(
			literal, names->mkshenvinit,
			names->initenvname, names->cwdname, names->termenvname, NULL
		);
		did = print_macro_with_txt( common, "SHELL_CWD", literal->txt, "" );
	}
	else
	{
		print_comment
		(
			common,
			"It is assumed that your GNU might not provide the built-in",
			"macros like __CHAR_BIT__ hence why this file is generated by",
			__FILE__ " for use with pawalltypes.h", NULL
		);
	}
	if ( !ON_MSW )
	{
		print_macro_with_str( common, "COMPOSITOR", names->compositors[0], 1 );
		for ( size_t i = 0; names->compositors[i]; ++i )
			print_macro_with_sfx( common, "COMPOSITOR", names->compositors[i], true );
	}
	//register unsigned AUTOC_FLT_MODE(CC) reg = ~0;
	print_macro_with_txt( common, "DIR_SEP", names->dirsep, common->esc_squo );
	print_macro_with_txt( common, "PATH_SEP", names->pathsep, common->esc_squo );
	print_macro_with_str( common, "CWD", names->cwdname, 1 );
	print_macro_with_sfx( common, "OS", names->osname, 0 );
	print_macro_with_sfx( common, "ARCH", ARCH_NAME, 0 );
	print_macro_with_sfx( common, "ENDIAN", which_endian(common), 0 );
	print_macro_with_sfx( common, "DATA_MODEL", which_data_model(common), 0 );
	print_intlim_dint( common, "FLT", "ROUNDS", fegetround(), "" );
	print_intlim_dint( common, "BITINT", "MAXWIDTH", BITINT_MAXWIDTH, "" );
	//print_intlim_dint( common, "LONGWIDTH", "MORETHAN_REGISTER", bitsof(long) > bitsof(reg), "" );
	printchar_limits( common, "CHAR",	bitsof(char),     neg1.c   < 0, "" );
	printchar_limits( common, "CHAR8",	bitsof(char8_t),  neg1.c8  < 0, "u8" );
	printchar_limits( common, "CHAR16",	bitsof(char16_t), neg1.c16 < 0, "u" );
	printchar_limits( common, "WCHAR",	bitsof(wchar_t),  neg1.lc  < 0, "L" );
	printchar_limits( common, "CHAR32",	bitsof(char32_t), neg1.c32 < 0, "U" );
	printchar_limits( common, "TCHAR", bitsof(tchar_t),  neg1.tc  < 0, (bitsof(tchar_t) > 1) ? "L" : "" );
	printint_limits( common, bitsof(autocintqid), "QI", TYPE_QI );
	printint_limits( common, bitsof(autocinthid), "HI", TYPE_HI );
	printint_limits( common, bitsof(autocintsid), "SI", TYPE_SI );
	printint_limits( common, bitsof(autocintdid), "DI", TYPE_DI );
	printint_limits( common, bitsof(autocinttid), "TI", TYPE_TI );
	printint_limits( common, bitsof(autocintoid), "OI", TYPE_OI );
	printint_limits( common, bitsof(autocintxid), "XI", TYPE_XI );
	print_cpuid(common,0);
	printint_limits( common, bitsof(register_t), "REGISTER", 0 );
	print_intlim_dint( common, "REGISTER", "WIDTH_TIMES_2", bitsof(register_t)*2, "" );
	print_intlim_dint( common, "REGISTER", "WIDTH_TIMES_4", bitsof(register_t)*4, "" );
	print_intlim_dint( common, "REGISTER", "COUNT", getcpuregistercount(), "" );
	printxint_limits( common, 128, "TETRAINT", "TETRAINT",	"TETRAUINT", 0 );
	printxint_limits( common, bitsof(autocintxid), "BITINT", "BITINT",	"BITUINT", 0 );
	printxint_limits( common, bitsof(void*),		"PTR", NULL, NULL, 0 );
	printsint_limits( common, bitsof(ptrdiff_t),	"PTRDIFF", 0 );
	print_intlim_dint( common, "SIZE", "WIDTH_TIMES_2", bitsof(size_t)*2, "" );
	printxint_limits( common, bitsof(void(*)()),	"FUNCPTR", 	NULL,	"FUNCPTR",    0  );
	printxint_limits( common, bitsof(size_t),	"SIZE", 	"SSIZE",	"SIZE",    0  );
	printint_limits( common, bitsof(intptr_t),		"INTPTR", 0  );
	printint_limits( common, bitsof(intmax_t),		"INTMAX", 0  );
	printint_limits( common, bitsof(int_least8_t),	"INT_LEAST8", 0  );
	printint_limits( common, bitsof(int_least16_t),	"INT_LEAST16", 0  );
	printint_limits( common, bitsof(int_least32_t),	"INT_LEAST32", 0  );
	printint_limits( common, bitsof(int_least64_t),	"INT_LEAST64", 0  );
	printint_limits( common, bitsof(int_fast8_t),	"INT_FAST8", 0  );
	printint_limits( common, bitsof(int_fast16_t),	"INT_FAST16", 0  );
	printint_limits( common, bitsof(int_fast32_t),	"INT_FAST32", 0  );
	printint_limits( common, bitsof(int_fast64_t),	"INT_FAST64", 0  );
	printsint_limits( common, bitsof(offset_t),		"FOFFSET", 0  );
	printsint_limits( common, bitsof(loffset_t),	"LOFFSET",    0  );
	printsint_limits( common, bitsof(offset64_t),	"OFF64",    0  );
	return 0;
}
