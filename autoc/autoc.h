#pragma once
#include "catch_os.h"
#include "catch_arch.h"
#include "calc_type.h"

#include <uchar.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef union
{
	llong    lld;
	char     c;
	char8_t  c8;
	char16_t c16;
	wchar_t  lc;
	char32_t c32;
	tchar_t  tc;
} char_union;

#define PFX "CAUGHT_"

typedef union { autocintxiu u; autocintxid d; } limit;
typedef struct
{
	char  sfx[8];
	char type[32], stype[32], utype[32];
} bitint;
typedef struct
{
	size_t width, size;
	limit  cap, max, min;
	string sfx, ext, utype, stype, type;
} limits;

typedef struct
{
	bool asinc;
	FILE *file;
	bitint bint;
	limits limo;
	string path;
	strbuf regtxt, literal, macsfx, minexp;
	osnames_t *osnames;
	/* Less time spent on creating the object like this */
	calcint_t calcint;
	string esc_squo, esc_dquo;
	string macrodefine, macromethod;
	string mlinepfx, mlinesfx, mlinenxt, slinepfx;
} common_t;

typedef int (*va_next_cb)( va_list va, void *ud );

int myexit(int ret, common_t *common, string msg);

int		va_list_leng( va_list va, va_next_cb cb, void *ud );
strvec	stringlen( string str );
int		isstringarg( va_list va, void *ud );
int		isstrvecarg( va_list va, void *ud );
int		stringarg( va_list va, void *ud );
int		strvecarg( va_list va, void *ud );

size_t	strlenlower( char *str );
size_t	strneedsv( strvec *vec, va_list va );
size_t	strneeds( strvec *vec, ... );
int		overidestrbufv( strbuf *str, string txt, va_list va );
int		overidestrbuf( strbuf *str, string txt, ... );
strbuf	allocstrbufv( string txt, va_list va );
strbuf	allocstrbuf( string txt, ... );
void	freestrbuf( strbuf *str );

int getosnames( osnames_t *osnames );
string which_endian(common_t *common);
string which_data_model(common_t *common);
size_t getlimits( limits *dst, bitint *bi, size_t width, TYPE T );
int printchar_limits( common_t *common, string name, size_t width, _Bool sign, string pfx );
int printxint_limits( common_t *common, size_t width, string name, string dname, string uname, TYPE T );
int printsint_limits( common_t *common, size_t width, string name, TYPE T );
int printint_limits( common_t *common, size_t width, string name, TYPE T );
int print_intlim_dint( common_t *common, string name, string subname, autocintxid value, string sfx );
int print_intlim_uint( common_t *common, string name, string subname, autocintxiu value, int sfxU, string sfx );
int print_intlim_text( common_t *common, string name, string subname, string text );
int print_intlim_func( common_t *common, string name, string subname, string vars, string text );
int print_macro_with_sfx( common_t *common, string name, string sfx, bool skipstr );
int print_macro_with_txt( common_t *common, string name, string txt, string quo );
int print_macro_with_str( common_t *common, string name, string str, bool andtxt );
int printall( FILE *file, common_t *common );


inline ssize_t vfputf( FILE *file, string format, va_list va )
{
	ssize_t did = vfprintf( file, format, va );
	fflush( file );
	return did;
}

inline ssize_t fputf( FILE *file, string format, ... )
{
	va_list va;
	va_start( va, format );
	ssize_t did = vfputf( file, format, va );
	va_end( va );
	return did;
}

inline ssize_t vfgetf( FILE *file, string format, va_list va )
{
	ssize_t did = vfscanf( file, format, va );
	fflush( file );
	return did;
}

inline ssize_t fgetf( FILE *file, string format, ... )
{
	va_list va;
	va_start( va, format );
	ssize_t did = vfgetf( file, format, va );
	va_end( va );
	return did;
}

inline size_t fput( FILE *file, void const *data, size_t size, size_t n )
{
	size_t did = fwrite( data, size, n, file );
	fflush( file );
	return did;
}
inline size_t fget( FILE *file, void *dest, size_t size, size_t n )
	{ return fread( dest, size, n, file ); }
