#include "autoc.h"
extern inline size_t getcpuregistersize(void);
extern inline size_t getcpuregistercount(void);

common_t common_mak(void)
{
	common_t c =
	{
		.asinc		= 0,
		.file		= NULL,
		.bint		= {{0}},
		.limo		= {0},
		.path		= "include/caught/" AUTOC_TOSTR(AUTOC_ARCH)  ".mk",
		.regtxt		= {0},
		.literal	= {0},
		.macsfx		= {0},
		.minexp		= {0},
		.osnames	= NULL,
		.calcint	= {0},
		.esc_squo	= "",
		.esc_dquo	= "",
		.macrodefine = PFX "%s:=%s\n",
		.macromethod = PFX "%s%.0s=%s\n",
		.mlinepfx	= NULL,
		.mlinesfx	= NULL,
		.mlinenxt	= NULL,
		.slinepfx	= "# "
	};
	return c;
};

common_t common_inc(void)
{
	common_t c =
	{
		.asinc	= true,
		.file		= NULL,
		.bint		= {{0}},
		.limo		= {0},
		.path		= "include/caught/" AUTOC_TOSTR(AUTOC_ARCH)  ".h",
		.regtxt		= {0},
		.literal	= {0},
		.macsfx		= {0},
		.minexp		= {0},
		.osnames	= NULL,
		.calcint	= {0},
		.esc_squo	= "'",
		.esc_dquo	= "\"",
		.macrodefine = "#define " PFX "%s %s\n",
		.macromethod = "#define " PFX "%s(%s) %s\n",
		.mlinepfx = "/* ",
		.mlinesfx = " */",
		.mlinenxt = " * ",
		.slinepfx = "// ",
	};
	return c;
};

int cleanup_common( int ret, common_t *common )
{
	if ( common->file )
	{
		fflush( common->file );
		fclose( common->file );
	}
	freestrbuf( &(common->literal) );
	freestrbuf( &(common->macsfx) );
	freestrbuf( &(common->regtxt) );
	memset( common, 0, sizeof(*common) );
	return ret;
}

size_t strlenlower( char *str )
{
	size_t i = 0;
	for ( ; str[i]; ++i )
		str[i] = tolower(str[i]);
	return i;
}

int myexit(int ret, common_t *common, string msg)
{
	int hashinc = 0, openinc = '"', shutinc = '"';
	string openerr = "$(", shuterr = ")";
	if ( common->asinc )
	{
		hashinc = '#';
		openinc = '<';
		shutinc = '>';
		openerr = "#";
		shuterr = "";
	}
	fputf( stderr, "\nerror: '%s\n'", msg );
	if ( common->file )
	{
		fputf
		(
			common->file,
			"\n%serror \"%s\"%s\n%cinclude %cabort_compilation%c\n",
			openerr, msg, shuterr, hashinc, openinc, shutinc
		);
		exit( cleanup_common( EXIT_FAILURE, common ) );
	}
	return ret;
}

int xmain(common_t common)
{
	char_union neg1 = { -1 };
	fputf( stdout, "Creating/Overwriting '%s'... ", common.path );
	common.file = fopen( common.path, "w" );
	if ( !(common.file) )
	{
		fputf( stdout, "\nFailed to open '%s'!\n", common.path );
		return -1;
	}
	setbuf( common.file, NULL );

	return cleanup_common( printall( NULL, &common ), &common );
}
string succeeded( int result ) { return (result == 0) ? "succeeded" : "failed"; }
int main(int argc, char **argv )
{
	common_t mak = common_mak();
	common_t inc = common_inc();
	osnames_t osnames = {0};
	setbuf(stdout,NULL);
	setbuf(stderr,NULL);
	strbuf incpath = {0}, makpath = {0};
	if ( getosnames( &osnames ) != 0 )
	{
		fputf( stdout, "getosnames() failed!\n" );
		return EXIT_FAILURE;
	}
	mak.osnames = &osnames;
	inc.osnames = &osnames;
	for ( int a = 1; a < argc; ++a )
	{
		char *arg = argv[a];
		if ( strstr( arg, "-o" ) == arg )
		{
			string dir = (arg[2] == '=') ? arg + 3 : argv[++a];
			freestrbuf( &incpath );
			freestrbuf( &makpath );
			incpath = allocstrbuf( dir, "/caught.h"  );
			makpath = allocstrbuf( dir, "/caught.mk" );
			inc.path = incpath.txt;
			mak.path = makpath.txt;
		}
		else if ( strstr( arg, "--host" ) == arg )
		{
			inc.path = AUTOC_HOST_DIR "/caught.h";
			mak.path = AUTOC_HOST_DIR "/caught.mk";
		}
	}
	int didmak = xmain( mak );
	fprintf( stdout, "%s\n", succeeded(didmak) );
	int didinc = xmain( inc );
	fprintf( stdout, "%s\n", succeeded(didinc) );
	done:
	freestrbuf( &(osnames.safeosname) );
	freestrbuf( &incpath );
	freestrbuf( &makpath );
	return ((didmak | didinc) == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

size_t getlimits( limits *dst, bitint *bi, size_t width, TYPE T )
{
	union
	{
		ullong ll;
		ulong l;
		uint i;
		ushort h;
		uchar c;
	} umax = {~0ULL};
	size_t move = 0;
	limits types[TYPE_COUNT] = {{0}};
	size_t maxc = sizeof(bi->type);
	memset( bi, 0, sizeof(*bi) );
	memset( dst, 0, sizeof(*dst) );
	if ( bitsof(limit) < width )
	{
		fputs("bitsof(limit) < width)\n", stderr);
		return 0;
	}
	dst->width = width;
	dst->size = width / CHAR_BIT;
	move = bitsof(limit) - width;
	dst->cap.u = ~(dst->cap.u) >> move;
	dst->max.u =   dst->cap.u >> 1;
	dst->min.u = ~(dst->max.u);
	dst->sfx = "";
	dst->ext = "";
	snprintf( bi->sfx, 8, "W%zu", width );
	snprintf( bi->type,  maxc,  "        _BitInt(%zu)", width );
	snprintf( bi->stype, maxc, "  signed _BitInt(%zu)", width );
	snprintf( bi->utype, maxc, "unsigned _BitInt(%zu)", width );
#define settype( T ) \
	dst->type = #T; dst->stype = #T "d"; dst->utype = #T "u"

	switch ( T )
	{
	case TYPE_QI: settype(autocintqi); return move;
	case TYPE_HI: settype(autocinthi); return move;
	case TYPE_SI: settype(autocintsi); return move;
	case TYPE_DI: settype(autocintdi); return move;
	case TYPE_TI: settype(autocintti); return move;
	case TYPE_OI: settype(autocintoi); return move;
	case TYPE_XI: settype(autocintxi); return move;
	default:
	}
	switch ( width )
	{
	case WIDTHOF_QI: settype(autocintqi); return move;
	case WIDTHOF_HI: settype(autocinthi); return move;
	case WIDTHOF_SI: settype(autocintsi); return move;
	case WIDTHOF_DI: settype(autocintdi); return move;
	case WIDTHOF_TI: settype(autocintti); return move;
	case WIDTHOF_OI: settype(autocintoi); return move;
	case WIDTHOF_XI: settype(autocintxi); return move;
	}
	dst->sfx = &(bi->sfx[0]);
	dst->type = &(bi->type[0]);
	dst->stype = &(bi->stype[0]);
	dst->utype = &(bi->utype[0]);
#undef settype
	return move;
}



string which_endian( common_t *common )
{
	size_t v = 12345678L, i = 0;
	unsigned char *raw = (unsigned char*)&v;
	for ( i = 0; i < sizeof(long); i += 4, raw += 4 )
	{
		switch ( raw[0] )
		{
		case 12u: return "be";
		case 78u: return "le";
		case 34u: return "me";
		case 56u: return "pdp";
		}
	}
	return "endian";
}
string which_data_model(common_t *common)
{
	int i = 0;
	static char dm[8] = {0};
	int dm_sp = (sizeof(void*) == sizeof(short));
	int dm_ip = (sizeof(void*) == sizeof(int));
	int dm_lp = (sizeof(void*) == sizeof(long));
	int dm_llp = (sizeof(void*) == sizeof(long long));
	memset( dm, 0, sizeof(dm) );
	if ( dm_llp && !dm_lp )
	{
		dm[i++] = 'l';
		dm[i++] = 'l';
	}
	else
	{
		dm[i] = dm_sp * 's'; i += dm_sp;
		dm[i] = dm_ip * 'i'; i += dm_ip;
		dm[i] = dm_lp * 'l'; i += dm_lp;
	}
	dm[i++] = 'p';
	return dm;
}
