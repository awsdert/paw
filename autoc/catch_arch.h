#pragma once
#include <limits.h>
#include <cpuid.h>
#include <autoc/arch.h>
#include <autoc/floats.h>

#define ARCH_NAME AUTOC_ARCH_NAMESTRING

typedef unsigned int uint;
typedef   signed _BitInt(AUTOC_ARCH_GREG_WIDTH) archint;
typedef unsigned _BitInt(AUTOC_ARCH_GREG_WIDTH) archuint;
#define ARCHINT_C(LIT)	((archint)LIT)
#define ARCHUINT_C(LIT)	((archuint)LIT)

#define FUNCPTR_SIZE   sizeof(void(*)())
#define FUNCPTR_WIDTH  bitsof(void(*)())

#define ATTR_MODE(MODE) __attribute__((mode(MODE)))

//typedef float ATTR_MODE(QF)  archqf;
//typedef float ATTR_MODE(TQF) archtqf;
typedef float ATTR_MODE(HF)  archhf;
typedef float ATTR_MODE(SF)  archsf;
typedef float ATTR_MODE(DF)  archdf;
typedef float ATTR_MODE(XF)  archxf;
typedef float ATTR_MODE(TF)  archtf;
typedef unsigned _BitInt(bitsof(archtf)) archtfuint;

#ifndef WEBREF_WIDTH
#	define WEBEF_WIDTH 256
#endif
/* (server generated ID << 128) | IP */
typedef   signed _BitInt(WEBEF_WIDTH) webref;
#define WEBEF_C(LIT) LIT##W##WEBEF_WIDTH


/* pid, tid, thread reference, data reference */
typedef   signed _BitInt(bitsof(register_t)*4) appref;
#define APPREF_C(LIT)	LIT##W##APPREF_WIDTH
inline appref appref_v( appref appid, appref antid, appref refid )
{
	/* The CPU registers hold the biggest integer the system will ever use for
	 * these IDs so it's completely safe to fixate on those I expect */
	return appid
		| (antid << bitsof(register_t))
		| (refid << (bitsof(register_t)*2));
}

