#include "autoc.h"
int va_list_leng( va_list va, va_next_cb cb, void *ud )
{
	int num = 0, len = 0;
	va_list cpy;
	va_copy( cpy, va );
	while ( (num = cb( cpy, ud )) > 0 )
		len += num;
	va_end(cpy);
	return num == 0 ? len : -1;
}

strvec stringlen( string str )
{
	strvec vec = {0};
	if ( !str ) return vec;
	vec.len = strlen(str);
	vec.txt = str;
	return vec;
}

int isstringarg( va_list va, void *ud )
{
	string txt = va_arg( va, string );
	(void)ud;
	return (txt != NULL);
}

int isstrvecarg( va_list va, void *ud )
{
	strvec *vec = va_arg( va, strvec* );
	(void)ud;
	return (vec != NULL);
}

int stringarg( va_list va, void *ud )
{
	strneedsobj *needs = ud;
	needs->vec = stringlen( va_arg( va, string ) );
	needs->total += needs->vec.len;
	return (needs->vec.txt != NULL);
}

int strvecarg( va_list va, void *ud )
{
	strneedsobj *needs = ud;
	strvec *tmp = va_arg( va, strvec* );
	if ( !tmp )
		return 0;
	needs->vec = *tmp = stringlen(tmp->txt);
	needs->total += tmp->len;
	return 1;
}

size_t strneedsv( strvec *vec, va_list va )
{
	strneedsobj needs = {0};
	needs.vec = stringlen(vec->txt);
	needs.total = vec->len = needs.vec.len;
	(void)va_list_leng( va, strvecarg, &needs );
	return needs.total;
}
size_t strneeds( strvec *vec, ... )
{
	va_list va;
	va_start( va, vec );
	size_t len = strneeds( vec, va );
	va_end( va );
	return len;
}

int overidestrbufv( strbuf *str, string txt, va_list va )
{
	strneedsobj needs = {0};
	needs.vec = stringlen(txt);
	needs.total = needs.vec.len;
	int i = 0, valen = va_list_leng( va, isstringarg, &needs );
	size_t vecscap = sizeof(strvec) * (valen + 2);
	strvec *vecs = alloca( vecscap ), *vec = NULL;
	memset( vecs, 0, vecscap );
	vecs[0] = stringlen( txt );
	needs.total = vecs[0].len;
	for ( i = 0; i < valen; ++i )
	{
		vec = vecs + i + 1;
		stringarg( va, &needs );
		*vec = needs.vec;
	}
	str->len = 0;
	size_t max = needs.total + 1;
	if ( str->max < max )
	{
		void *tmp = str->txt ? realloc( str->txt, max ) : malloc( max );
		if ( !tmp )
		{
			if ( str->txt )
				str->txt[0] = 0;
			return -1;
		}
		memset( tmp, 0, max );
		str->max = max;
		str->txt = tmp;
	}
	memset( str->txt, 0, str->max );
	++valen;
	for ( i = 0; i <= valen; ++i )
	{
		vec = vecs + i;
		memcpy( str->txt + str->len, vec->txt, vec->len );
		str->len += vec->len;
	}
	str->txt[str->len] = 0;
	return 0;
}

int overidestrbuf( strbuf *str, string txt, ... )
{
	va_list va;
	va_start( va, buf );
	int res = overidestrbufv( str, txt, va );
	va_end( va );
	return res;
}

strbuf allocstrbufv( string txt, va_list va )
{
	strbuf str = {0};
	overidestrbufv( &str, txt, va );
	return str;
}

strbuf allocstrbuf( string txt, ... )
{
	va_list va;
	va_start( va, buf );
	strbuf str = allocstrbufv( txt, va );
	va_end( va );
	return str;
}

void freestrbuf( strbuf *str )
{
	if ( str->txt )
	{
		free( str->txt );
		memset( str, 0, sizeof(*str) );
	}
}
