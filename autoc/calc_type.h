#pragma once
#include "catch_os.h"
#include "catch_arch.h"
#include <string.h>
#include <limits.h>

#define _CCINT_WIDTH(A,B) ((A) > (B) ? (A) : (B))
#define CCINT_WIDTH _CCINT_WIDTH(bitsof(register_t)*4,bitsof(archtf))

typedef struct { autocintxid quo, rem; } autocintxiddiv;
typedef struct { autocintxiu quo, rem; } autocintxiudiv;
#define CCINT_C(LIT)	((autocintxid)LIT)
#define CCUINT_C(LIT)	((autocintxiu)LIT)

#define INTSFX_MAX bitsof(autocintxiddiv)
#define INTSFX_MAXLEN (INTSFX_MAX - 1)
#define INTTYPE_MAX 32
#define INTTYPE_MAXLEN 31

typedef char ccsfx[INTSFX_MAX];
typedef char cctype[INTTYPE_MAX];

typedef enum
{
	TYPE_ANY = 0,
	TYPE_QI,
	TYPE_HI,
	TYPE_SI,
	TYPE_DI,
	TYPE_TI,
	TYPE_OI,
	TYPE_XI,
	TYPE_COUNT
} TYPE;

enum
{
	WIDTHOF_ANY = 0,
	WIDTHOF_QI = bitsof(autocintqid),
	WIDTHOF_HI = bitsof(autocinthid),
	WIDTHOF_SI = bitsof(autocintsid),
	WIDTHOF_DI = bitsof(autocintdid),
	WIDTHOF_TI = bitsof(autocinttid),
	WIDTHOF_OI = bitsof(autocintoid),
	WIDTHOF_XI = bitsof(autocintxid),
	WIDTHOF_COUNT
};

typedef struct
{
	size_t	size, width;
	autocintxiu	umax;
	autocintxid	max, min;
	cctype	type, stype, utype;
} calcint_t;

/* sfx is for when a suffix is mandated for the type (such as L for long or
 * LL for llong  */
void calcint_type( calcint_t *dst, size_t width, TYPE type );
size_t ccn36toa( char *dst, size_t max, autocintxiu val, size_t base, char charA );
inline size_t ccXtoa( char *dst, size_t max, autocintxiu val )
	{ return ccn36toa( dst, max, val, 16, 'A' ); }
inline size_t ccxtoa( char *dst, size_t max, autocintxiu val )
	{ return ccn36toa( dst, max, val, 16, 'a' ); }
inline size_t ccutoa( char *dst, size_t max, autocintxiu val )
	{ return ccn36toa( dst, max, val, 10, 0 ); }
inline size_t ccotoa( char *dst, size_t max, autocintxiu val )
	{ return ccn36toa( dst, max, val, 8, 0 ); }
inline size_t ccbtoa( char *dst, size_t max, autocintxiu val )
	{ return ccn36toa( dst, max, val, 2, 0 ); }
size_t ccitoa( char *dst, size_t max, autocintxid val );
