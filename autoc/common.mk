TOPHR===================================
SUBHR=----------------------------------
PRJHR=~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
define boxup
	@echo "$1init$1"
	$2
	@echo "$1term$1"
endef

AUTOC_HOST_APP_EXT:=$(filter-out .make,.$(lastword $(subst ., ,$(MAKE))))
APP_EXT?=AUTOC_HOST_APP_EXT

GOALS:=$(or $(filter-out make $(STRIP_GOALS),$(MAKECMDGOALS)),info)

OTG_DIR?=$(or $(TOP_DIR:%/=%),.)
DST_DIR?=$(OTG_DIR)
SHARED_DIR:=$(DST_DIR)/.paw
AUTOC_HOST_DIR:=$(SHARED_DIR)/autoc/$(MAKE_HOST)
AUTOC_HOST_CAUGHT_MK:=$(AUTOC_HOST_DIR)/caught.mk
AUTOC_HOST_CAUGHT_INC:=$(AUTOC_HOST_DIR)/caught.h

exec=$(info $1$(shell $1))
dirents=$(patsubst $1%,%,$(wildcard $1$2))
echoents=echo $1 holds $(call dirents,$1,$2)
cp=$(call exec,cp $1)
mkdir=$(if $(wildcard $1),,$(call exec,mkdir "$1"))
mkdirs=$(foreach d,$1,$(call mkdir,$d))

$(call mkdirs,$(SHARED_DIR) $(SHARED_DIR)/autoc $(AUTOC_HOST_DIR))
