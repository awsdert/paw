include autoc/common.mk
OUT:=autoc/autoc$(AUTOC_HOST_APP_EXT)
INC_DIR:=include
AUTOC_SOURCES:=$(wildcard autoc/*.c)
AUTOC_OBJECTS:=$(AUTOC_SOURCES:%=%.o)
AUTOC_HEADERS:=$(wildcard autoc/*.h $(INC_DIR)/autoc/*.h)
CLEAN:=$(filter-out %.h %.c %.mk,$(wildcard autoc/autoc autoc/autoc.* *.o))
CFLAGS+=-g -std=gnu2x -D GNU_SOURCE -D MAKE_HOST="\"$(MAKE_HOST)\""

export CPATH:=$(INC_DIR)
CFLAGS+=-D AUTOC_HOST_DIR="\"$(AUTOC_HOST_DIR)\""
CFLAGS+=-D AUTOC_HOST_CAUGHT_MK="\"$(AUTOC_HOST_CAUGHT_MK)\""
CFLAGS+=-D AUTOC_HOST_CAUGHT_INC="\"$(AUTOC_HOST_CAUGHT_INC)\""

all: build

%/autoc/caught: rebuild
	./$(OUT) -o "$*"

host.mk: build
	./$(OUT) --host

run: build
	./$(OUT) --host

rebuild: clean build

clean:
	$(if $(CLEAN),$(RM) $(CLEAN),@echo "Nothing to clean")

build: $(OUT)

$(OUT): $(AUTOC_OBJECTS)
	$(CC) -o ./$@ $^ -lm

%.c.o: %.c
	$(CC) $(CFLAGS) -o ./$@ -c $<

%.c: $(AUTOC_HEADERS)

.PHONY: all run build rebuild clean host.mk
