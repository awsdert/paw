#include "autoc.h"
int getosnames( osnames_t *names )
{
#	define COMPOSITOR_TTY "tty"
#	define COMPOSITOR_X11 "x11"
#	define COMPOSITOR_NONE "none"
#	define COMPOSITOR_WIN32 "win32"
#	define COMPOSITOR_WAYLAND "wayland"
static string compositors[]
		= { COMPOSITOR_WAYLAND, COMPOSITOR_X11, COMPOSITOR_TTY, NULL };
static string compositors_way[]
		= { COMPOSITOR_WAYLAND, COMPOSITOR_TTY, NULL };
static string *compositors_x11 		= compositors + 1;
static string *compositors_tty 		= compositors + 2;
static string compositors_none[]	= { COMPOSITOR_NONE, NULL };
	string  xdg_session_type	= getenv("XDG_SESSION_TYPE");
	if ( ON_MSW )
	{
		names->osname = "msw";
		names->dirsep = "\\";
		names->appext = ".exe";
		names->libext = ".dll";
		names->pathsep = ";";
		names->linesep = "\r\n";
		names->mswelse = 1;
		names->cwdname = "CD";
		names->abiopts = (ON_MSW == 64) ? "-m64" : "-m32";
		names->mkshenvinit = "";
		names->initenvname = "%";
		names->termenvname = "%";
	}
	else if ( ON_DOS )
	{
		names->osname = "dos";
		names->dirsep = "\\";
		names->appext = ".cmd";
		names->libext = ".lib";
		names->pathsep = ";";
		names->linesep = "\r\n";
		names->mswelse = 1;
		names->cwdname = "CD";
		names->abiopts = "";
		names->mkshenvinit = "";
		names->initenvname = "%";
		names->termenvname = "%";
	}
	else
	{
#	if !ON_MSW
		struct utsname *uts = &(names->uts);
		if ( uname( uts ) != 0 )
			return fputf( stdout, "uname( &uts ) failed!\n" );
		strlenlower( uts->sysname );
		names->osname = uts->sysname;
#	else
		names->osname = ARCH_NAME;
#	endif
		names->dirsep = "/";
		names->pathsep = ":";
		names->linesep = ON_OSX ? "\r" : "\n";
		names->mswelse = 2;
		names->cwdname = "PWD";
		names->refshvar = "$";
		names->abiopts = "";
		names->mkshenvinit = "$";
		names->initenvname = "${";
		names->termenvname = "}";
		if ( getenv("WAYLAND_DISPLAY") )
			names->compositors = getenv("DISPLAY") ? compositors : compositors_way;
		else if ( getenv("DISPLAY") )
			names->compositors = compositors_x11;
		else if ( xdg_session_type )
			names->compositors = compositors_tty;
		else
			names->compositors = compositors_none;
	}
	/* "linux" is among the named macros gcc or unistd directly define which
	 * causes issues with constructing paths (devolves to 1/... instead of
	 * linux/...) so instead we prefix the name to ensure that doesn't happen.
	 * If you need the string version without the prefix just add 4 to the start
	 * of the CAUGHT_OS_STR macro and concate that if needed */
	names->safeosname = allocstrbuf( "api_", names->osname, NULL );
	return -((int)!(names->safeosname.txt));
}

