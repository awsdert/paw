include autoc/common.mk
include $(AUTOC_HOST_CAUGHT_MK)

# Paw
CC:=winegcc
tolower=$(guile (string-downcase "$1"))
mswcmd=$(call mswelse,$(shell $1))
psxcmd=$(call mswelse,,$(shell $1))
_seekall=$(foreach i,$(wildcard $1*/),$(call _seekall,$i,$2)) $(wildcard $1$2)
seekall=$(call _seekall,$1,$2)
endwords=$(wordlist $1,$(words $2),$2)
grab=$($3$(subst $1, ,$2))

caseval=$(if $(filter var,$(word $1,$3)),$($(word $2,$3)),$(word $2,$3))
case=$(if $(filter $(call caseval,1,3,$2),$1),$(call caseval,2,4,$2))
printcase=$(info case "$(call caseval,1,3,$2)": return "$(call caseval,2,4,$2)")
switch=$(or $(strip $(foreach i,$4,$(call case,$1,$(subst $2, ,$i)))),$3)
printswitch=$(info switch "$1")$(foreach i,$4,$(call printcase,$1,$(subst $2, ,$i)))$(info default: return "$3")

# Operating systems
OS_PATH_SEP:=$(call mswelse,;,:)
OS_DIR_SEP:=$(call mswelse,\,/)
# OS commands
SET_NAME=$(call mswelse,set $1,$1)

# All file types
ABIFLAGS:=-fPIC
SRCFLAGS:=$(ABIFLAGS) -march=native -std=gnu2x -pedantic-errors
SRCFLAGS+=-fexec-charset=utf-8 -finput-charset=utf-8
# Individual file types (C:*.c, CXX:*.c++)
CFLAGS:=$(SRCFLAGS) $(if $(filter clang,$(CC)),--stdlib=libstdc)
CXXFLAGS:=$(SRCFLAGS) $(if $(filter clang,$(CC)),--stdlib=libstdc++)
# This one is for executing scripts/binaries by the GNUmakefile
NPAW?=$(CAUGHT_OS_TXT)
PAW_ARCH?=$(CAUGHT_ARCH_TXT)
PAW_ENDIAN?=$(CAUGHT_ENDIAN_TXT)
PAW_DATA_MODEL?=$(CAUGHT_DATA_MODEL_TXT)

#$(call printswitch,$(NPAW),;,.so,txt;txt;msw;.dll txt;txt;darwin;.dylib)
#$(call printswitch,$(NPAW),;,.elf,txt;txt;msw;.exe txt;txt;darwin;.app)
LIB_EXT:=$(call switch,$(NPAW),;,.so,txt;txt;msw;.dll txt;txt;darwin;.dylib)
APP_EXT:=$(call switch,$(NPAW),;,.elf,txt;txt;msw;.exe txt;txt;darwin;.app)

# $x/.paw/paw$PAW_VER/$NPAW_ABI/$PAW_MODE is searched before
# .paw/paw$PAW_VER/$PAW_ABI/$PAW_MODE
# $x/.paw/$PAW_ABI/all is always treated as the fallback search path before
# the paths already present in the PATH enviroment variable are searched
# In this way software in the paw ecosystem can be designed for all platforms
# first then optionally have extended versions made to support OS specific
# features, much better than developing in reverse (OS before cross-platform)
PAW_MODES:=all debug timed tests
PAW_MODE?=all
PAW_VER?=1

# ABI Names
PAW_ABI:=all_$(PAW_ARCH)
NPAW_ABI:=$(NPAW)_$(PAW_ARCH)_$(PAW_ENDIAN)$(PAW_DATA_MODEL)
HPAW_ABI:=$(CAUGHT_OS_TXT)_$(CAUGHT_ARCH_TXT)_$(CAUGHT_ENDIAN_TXT)$(CAUGHT_DATA_MODEL_TXT)
PAW_ABIs:=$(PAW_ABI) $(NPAW_ABI)

# version, abi
abi_dir_is=.paw/$1/$2
# version
paw_dir_is=.paw/paw$1/$(PAW_ABI)
npaw_dir_is=.paw/paw$1/$(NPAW_ABI)
hpaw_dir_is=.paw/paw$1/$(NPAW_ABI)
PAW_DIR:=$(OTG_DIR)/$(call paw_dir_is,$(PAW_VER))
NPAW_DIR:=$(OTG_DIR)/$(call npaw_dir_is,$(PAW_VER))
HPAW_DIR:=$(OTG_DIR)/$(call hpaw_dir_is,$(PAW_VER))

NPAW_ABI_OPTS:=$(PAW_ABI_OPTS) $(abiopts)

%/:
	mkdir "$(dir $@)"

HPAW_AUTOC_DIR:=$(HPAW_DIR)/all/autoc
HPAW_CAUGHT_MK:=$(HPAW_AUTOC_DIR)/caught.mk
HPAW_CAUGHT_INC:=$(HPAW_AUTOC_DIR)/caught.h

NPAW_AUTOC_DIR:=$(NPAW_DIR)/all/autoc
NPAW_CAUGHT_MK:=$(NPAW_AUTOC_DIR)/caught.mk
NPAW_CAUGHT_INC:=$(NPAW_AUTOC_DIR)/caught.h

$(call mkdir,$(NPAW_AUTOC_DIR))
tmp=$(if $(wildcard $1),,$(info cp "$2" "$1"$(shell cp "$2" "$1")))
$(call tmp,$(NPAW_CAUGHT_MK),$(AUTOC_HOST_CAUGHT_MK))
$(call tmp,$(NPAW_CAUGHT_INC),$(AUTOC_HOST_CAUGHT_INC))
tmp:=
