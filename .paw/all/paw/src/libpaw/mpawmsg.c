#define PAWMSGID__RESERVED_TXT \
	"PAWMSGID__RESERVED, Should be impossible to trigger by mistake."
#define PAWMSGID__UNKNOWN_TXT "Message id given was unrecognised by libpaw"

#define PAWMSG_O_ENABLE_EXECEXITS 0x1
#define PAWMSG_O_ENABLE_INTERUPTS 0x2

/** @brief Change what actions are permitted in pawsetmsg
 * @return Previous actions that were permitted
 * @note The may be exceptions/signals that paw decides must trigger an exit
 * or interrupt, in those cases these options will have no effect. This is
 * mainly provided for lanuages like lua that use higher level debugging. */
pawe32u pawsetopts( pawe32u opts );

PAW_ANT_CB void pawsetmsg( pawmsg msgid, pawe32u opts )
{
	pawmsg *msg = &(mpawthisant->nmsg);
	memset( &msg, 0, sizeof(*msg) );
	msg->msgid = msgid;
	msg->subid = msgid >> 32;
	switch ( msgid )
	{
	LIBPAWMSG_SET( msgid, PAWMSGID_CUSTOM );
	LIBPAWMSG_SET( msgid, PAWMSGID_VENDOR );
	case PAWMSGID__RESERVED:
		interrupt = true;
		break;
	LIBPAWMSG_SET( msgid, PAWMSGID_FAILURE );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_SUCCESS );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_NO_SYS );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_NO_CODE );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_MISMATCH );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_CORRUPT );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_UXEVENT );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_HW_FAULT );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_SW_FAULT );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_INTERUPT );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_INTERUPTED );break;
	LIBPAWMSG_SET( msgid, PAWMSGID_NOT_RANGED );break;
	LIBPAWMSG_SET( msgid, PAWMSGID_NOT_ENOUGH );break;
	LIBPAWMSG_SET( msgid, PAWMSGID_INITIALISE );break;
	LIBPAWMSG_SET( msgid, PAWMSGID_DEADLOCK );	break;
	LIBPAWMSG_SET( msgid, PAWMSGID_ALREADY );	break;
	default:
	}
	if ( msgid && (opts & PAWMSG_O_ENABLE_INTERUPTS) )
		raise(SIGINT);
	if ( msgid && (opts & PAWMSG_O_ENABLE_EXECEXITS) )
		exit(EXIT_FAILURE);
	return msgid;
}
