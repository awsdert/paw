#include "mpaw.h"
/* This file is purely for forks of paw that implement features paw itself
 * does not yet implement/expose. Aside from this comment, the include and
 * the default definition of pawisvnd() and pawvnd() nothing else will ever
 * be added to this file. Add whatever you please from beyond the #endif. Just
 * make sure to also define VPAW_BUILD and implement the functions yourself.
 * Likewise the vendor directory contents will never change so add whatever
 * you like to it also. */
#ifndef VPAW_BUILD
PAW_ANT_CB pawb pawisvnd( pawmbs name )
	{ return (pawmbscmpn( u8"default", 7, name, pawmbslen(name) ) == 7); }
PAW_ANT_CB npawd pawvndv( mpawref *ref, pawmbs cls, va_list va )
{
	if ( !cls )
		return pawrefcallv(ref,NULL,va);
	return NULL;
}
#endif
