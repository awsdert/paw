#include "mpaw.h"
npawd npawsys_sem_endCB( libpawhook_endCB *cb, libpawhook hook )
{
	if ( *cb != npawsys_sem_endCB )
		return PAWMSGID_MISMATCH;
	if ( hook.p )
	{
		npawsem_end( hook.p );
		return 0;
	}
	return PAWMSGID_BAD_HOOK;
}

npawd npawsys_success_endCB( libpawhook_endCB *cb, libpawhook hook )
{
	(void)cb;
	(void)hook;
	return 0;
}

/* Just means it should never be handled until paw terminates */
npawd npawsys_w84term_endCB( libpawhook_endCB *cb, libpawhook hook )
{
	(void)cb;
	(void)hook;
	return -2;
}

npawd npawsys_no_code_endCB( libpawhook_endCB *cb, libpawhook hook )
{
	(void)cb;
	(void)hook;
	return PAWMSGID_NO_CODE;
}

npawd npawsys_no_code_dupCB( libpawhook_dupCB *cb, libpawhook *dst, libpawhook hook )
{
	(void)cb;
	(void)dst;
	(void)hook;
	return PAWMSGID_NO_CODE;
}
npawd npawsys_no_code_dp2CB( libpawhook_dp2CB *cb, libpawhook  dst, libpawhook hook )
{
	(void)cb;
	(void)dst;
	(void)hook;
	return PAWMSGID_NO_CODE;
}

pawref _npawsys_default( pawmbs file, pawru line )
{
	pawref sys = {0};
	sys.swid  = -1;
	sys._line = line;
	sys._file = file;
	sys.endCB = npawsys_success_endCB;
	sys.dupCB = npawsys_no_code_dupCB;
	sys.dp2CB = npawsys_no_code_endCB;
	return sys;
}

pawref* npawsys_nextavailble( void )
{
	npawvm *syshooks = npawvm_all->data;
	pawref *released = (void*)(syshooks->hooks.released.next);
}

pawref* npawsys_newdref( npawd ref )
{
}
