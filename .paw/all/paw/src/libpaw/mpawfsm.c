#include "mpaw.h"
/* Cross-platform mremap style function.
 * TODO: Check for oversights */
void* mpawmap
(
	void *base,
	pawru len,
	npawprot prot,
	npawopt opts,
	npawsys fsp
)
{
	pawcu *viewbase = base;
	pawru pagesize = pawgetpagesize();
	npawsys *reserved = NULL, fsm = -1;
	if ( base )
	{
		reserved = (void*)(viewbase - pagesize);
		return npawfsm64_view( base, len, prot, reserved[1], fsp );
	}
	pawsys fsm = npawfsm64_open(fso);
	if ( fsm < 0 )
		return NULL;
	viewbase = npawfsm64_view( NULL, len, prot, len, fsm, fsp );
	if ( !viewbase )
		goto shut_fsm;
	reserved = (void*)(viewbase - pagesize);
	reserved = npawvm64_resize( reserved, pagesize, prot, NPAWVM_O_FIXEDADDR );
	if ( reserved )
	{
		/* This resolves WIN32 issue of hiding the map handle somewhere. Since
		 * libpaw is being designed with porting to any system in mind I made
		 * this the standard behaviour to prevent issues should the npaw
		 * functions ever suddenly take a different definition in future
		 * system targets. */
		reserved[0] = fso;
		reserved[1] = fsm;
		return viewbase;
	}
	npawfsm_free( viewbase, len );
	shut_fsm:
	npawfsm_shut( fsm );
	return NULL;
}
