#include "../lpaw.h"
#include CAUGHT_OS "/pawfs.c"

void _initpawfs_envs( libpaw_constants *vm )
{
	pawfs_env_cb *env = vm->pawfs_envs;

	pawfs_env_anysh_ptr = vm->pawfs_envs;
	pawfs_env_anysh_num = &(vm->pawfs_envc);
	vm->pawfs_envc = sizeof(vm->pawfs_envs) / sizeof(pawfs_env_cb);

	pawfs_env_cmdsh_ptr = env;
	env->open = env->shut = '%';

	pawfs_env_shell_ptr = ++env;
	env->name = '$';
	env->open = '(';
	env->shut = ')';

#if PAW_OS_MSW
	pawfs_env_locsh_ptr = pawfs_env_cmdsh_ptr;
#else
	pawfs_env_locsh_ptr = pawfs_env_shell_ptr;
#endif
}

pawd _pawfs_chkoneonly( pawfs_envfind *find, pawfs_env_cb *env  )
{
	if ( *name != env->name )
		return -1;
	find->name = find->text + 1;
	find->leng = 1;
}

PAW_ANT_CB pawd pawfs__envpathn
	( pawmbc *dst, pawd max, pawmbs src, pawd len, pawfs_env_cb cb )
{
	pawd i = 0, j = 0, cpy = 0;
	pawmbs text = src + len;
	pawfs_env find = {0};
	for ( i = 0; i < len; i += find.full )
	{
		find = cb( src + i, len - i );
		if ( find.fill )
			++j;
		else if ( find.name )
		{
			text = pawenv_getnamen( find.name, find.leng );
			j += pawmbslen( text );
		}
		else
		{
			find.full = 1;
			++j;
		}
		find.full += (pawd)(!(find.full));
	}
	if ( max <= j )
		return j;
	for ( i = 0; i < len; i += find.full )
	{
		find = cb( src + i, len - i );
		if ( find.fill )
			dst[j++] = find.fill;
		else if ( find.name )
		{
			text = pawenv_getnamen( find.name, find.leng );
			if ( !text )]
				return -1;
			cpy = pawmbslen( text );
			pawrawcpy( dst + j, text, cpy );
			j += cpy;
		}
		else
		{
			find.full = 1;
			dst[j++] = src[i];
		}
		find.full += (pawd)(!(find.full));
	}
	return 0;
}

PAW_ANT_CB pawd pawfs_abspathn( pawmbc *dst, pawd max, pawmbs src, pawd len );
PAW_ANT_CB pawd pawfs_syspathn( pawmbc *dst, pawd max, pawmbs src, pawd len );

extern PAW_QCK_CB pawd pawfs_env_cmdsh( pawmbs txt, pawd len );
extern PAW_QCK_CB pawd pawfs_env_shell( pawmbs txt, pawd len );
extern PAW_QCK_CB pawd pawfs_env_anysh( pawmbs txt, pawd len );
/** @brief pawfs_env_cmdsh_ptr OR pawfs_env_shell_ptr */
pawfs_env_cb pawfs_env_locshCB = pawfs_env_shell;

extern PAW_FQK_CB pawd pawfs__envpath
	( pawmbc *dst, pawd max, pawmbs src, pawfs_env_cb cb );
extern PAW_FQK_CB pawd pawfs_envpathn
	( pawmbc *dst, pawd max, pawmbs src, pawd len );
extern PAW_FQK_CB pawd pawfs_envpath( pawmbc *dst, pawd max, pawmbs src );
extern PAW_FQK_CB pawd pawfs_abspath( pawmbc *dst, pawd max, pawmbs src );
extern PAW_FQK_CB pawd pawfs_syspath( pawmbc *dst, pawd max, pawmbs src );
