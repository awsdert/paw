#pragma once
#define _LIBPAW 1
#define NPAW_BUILD 1

#include <paw/pawval/pawalltypes.h>
#include <paw/pawval.h>
#include <paw/pawval/npawts.h>
#include <paw/pawsys/pawmbe.h>
#include <paw/pawsys/pawenv.h>
#include <paw/pawsys/pawmod.h>
#include <paw/pawsys/pawapp.h>

PAW_C_API_OPEN

#include <npaw/npaw_types.h>

/* Since modifying environment variables directly is not thread safe we'll
 * instead provide a local buffer of buffers to work with then pass it on
 * when into envp like arguments. It may be more costly to launch but it's far
 * better  */
typedef struct
{
#define CH pawsmem
#include <paw/pawval/inherit_mpawrawdst.h>
	/* Used only to create the expected char** / char* lists needed for exec()
	 * like functions. */
	mpawrawdst envp;
} mpawenv;
typedef struct _mpawsem mpawsem;
typedef struct _mpawant mpawant;

void* mpawrawdst_internalCB( void **old, pawru *was, pawru get );

typedef struct
{
	pawru       depth;
	pawru	  precise;
	npawtsvec sympath;
	npawtsvec abspath;
	mpawmemsrc   segslist;
	mpawmemsrc   namelist;
}  mpawfso_segptrs;

/* Collect path segments into a series of null terminated strings, symbolic
 * links are not followed */
PAW_ANT_CB int mpawfso_sympath( npawtc *path, pawru leng );
/** @brief Collect path segments into a series of null terminated strings
 * @note symbolic links are followed. Depth parameter is for if you know that
 * some part of the initial path segments is an absolute path. Always case insensitive.
 *  */
PAW_ANT_CB int mpawfso_abspath( npawtc *path, pawru leng, pawru depth );
PAW_ANT_CB int _mpawfso_find( pawb caseinsensitive, npawtsvec path );
PAW_FQK_CB int mpawfso_findn( npawts path, pawru leng )
	{ return _mpawfso_find( 0, npawtsnewvecn( path, leng ) ); }
PAW_FQK_CB int mpawfso_ifindn( npawts path, pawru leng )
	{ return _mpawfso_find( 1, npawtsnewvecn( path, leng ) ); }
PAW_FQK_CB int mpawfso_find( npawts path )
	{ return mpawfso_findn( path, npawtslen(path) ); }
PAW_FQK_CB int mpawfso_ifind( npawts path )
	{ return mpawfso_ifindn( path, npawtslen(path) ); }

/* Since libpaw is not trying to replace libc/mscrt/etc but provide a
 * consistent ABI it's perfectly reasonable to link to them */
#include <errno.h>
#include <assert.h>
#include <signal.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct
{
	pawref   *file;
	pawru     fullsize, size;
	npawlu     opts;
	npawu      prot;
	/* Purely a convenience member, only set once */
	void     *data;
} npawvm;

typedef struct
{
#define CH npawtsmem
#include <paw/pawval/inherit_mpawrawdst.h>
	npawtsmem pool;
} mpawenvbufs;

void	 npawenv_clrpool( void );
npawtsvec npawenv_getpool( void );
int	 npawenv_putpool( npawtsvec pool, pawb keep_buffers );
/* Grabs from environ or similar */
npawtsvec npawenv_getdef( npawts key );
/* Ant pool only */
void	 npawenv_remkey( npawtsvec key );
npawtsvec npawenv_getkey( npawtsvec key );
int	 npawenv_putkey( npawtsvec key, npawtsvec val, pawb replace );

enum
{
	pawenvm_text = 0,
	pawenvm_list,
	pawenvm_PUT_NUMERIC,
	pawenvm_CMP_NUMERIC,
	pawenvm_PUT_MONETARY,
	pawenvm_CMP_MONETARY,
	pawenvm_PUT_DATETIME,
	pawenvm_CMP_DATETIME,
	pawenvm_PUT_TIMEZONE,
	pawenvm_CMP_TIMEZONE,
	pawenvm_PUT_LANGUAGE,
	pawenvm_CMP_LANGUAGE,
	pawenvm_PUT_TERATORY,
	pawenvm_CMP_TERATORY,
	pawenvm_PUT_MESSAGES,
	pawenvm_CMP_MESSAGES,
	pawenvm__count
};

typedef struct { pawref locales[pawenvm__count]; } pawlocales;

struct _mpawsem
{
	mpawant  *wrlock; /* MUST be NULL if rdlock is set */
	pawtieabs rdlock; /* MUST be empty if wrlock is set */
	/* Temporary lock point, deadlocks are avoided by checking the above
	 * parameters prior to lock attempts.
	 * Originally would use CRITICAL_SECTION on windows but I found out it
	 * just uses a semaphore under the hood so now I'm just going direct to
	 * semaphores only. When single threaded platforms are involved I can
	 * just default to an integer and make use of setlongjump() etc */
	npawsem sem;
};

extern __thread struct _mpawant *mpawthisant;

struct _mpawref
{
	pawru     _line;
	pawmbs    _file;
	void	*vmbase;
	pawties   hooks;
	mpawref_cb expireCB;
	mpawref_cb launchCB;
	union
	{
		signed _BitInt(CAUGHT_REGISTER_WIDTH_TIMES_2) val;
		struct
		{
			pawexe tid;
			pawexe pid;
		} obj;
	} IDs;
	/* Used for threads to lock/unlock the hook's contents */
	pawref *sync;
	union
	{
		void	*addr;
		/* OpenGL often returns Gluint IDs so this is for that kind of thing */
		pawru    hwid;
		/* Similar purpose as above */
		pawrd    swid;
		/* No need to complicate matters with trying to allocate the buffer
		 * struct as well, just make it always available */
		mpawrawdst	data;
		npawsem	sem;
		npawexe	ant;
		npawvm	vm;
		npawfsd fsd;
		pawsys fso;
		pawsys fsm;
		npawfsp fsp;
	} hook;
};

struct _mpawant
{
	/* Where allocators dump their allocations */
	pawru    allocationsize;
	void    *allocation;
	/* Thread's Arithmetic Logic Unit's modifiable copies of value working
	 * with */
	pawru	 alucap;
	uchar	*alumem;
	/* In order for other threads to access the thread's pages they need to
	 * be able to syncronise control of them so a static sync point is needed
	 * for that */
	mpawsem nsem;
	/* Gotta store that tid somewhere */
	pawexe  tid;
	pawexe  antid;
	/* Absolute reference list */
	pawtie  pages;
	/* Path buffers */
	mpawrawdst  sympathsegs;
	mpawrawdst  abspathsegs;
	npawtsmem abspath;
	/* Use for grabbing enviroment variables */
	npawtsmem envname;
	npawtsmem envdata;
	/* Use for grabbing system/media root */
	npawtsmem sysroot;
	pawref *pawsrcpipe;
	pawref *pawdstpipe;
	pawref *pawlogpipe;
	pawref *pawthisant;
	/* Encoding buffers */
	mpawrawdst mbesrctext;
	mpawrawdst mbedsttext;
	mpawref ref;
	pawmsg msg;
	//
	mpawenvbufs envpool;
};

#include <npaw/npaw.h>

typedef struct { pawru used, size, refc; pawties ties; } mpawvm;
int	 mpawsys_getinfo(void);
pawru	 pawgetpagesize(void);
/* Should vary according to usage, for now will default to INTPTR_MAX / 0xFFFF */
pawru	 pawgetminpagesetsize(void);

/* Fixed reference IDs */
enum
{
	sysref_enum_stdin = 0,
	sysref_enum_stdout,
	sysref_enum_stderr,
	sysref_enum_stdio,
	/* Read only page */
	sysref_enum_constants,
	sysref__count
};

typedef struct
{
	void *ud;
	pawmbsmem name;
} pawconstant;

/* The structure in the page going to assign for the constants then make read
 * only after setting them */
typedef struct
{
	pawrd    sizeofallconsts;
	pawageNS ageNS_units[pawageNS_unit_count];
	pawageQS ageQS_units[pawageQS_unit_count];
	/* member 0 is always invalid */
	pawconstant extra[1];
} pawconstants;

/* This will be for adding new constants to the read only page when supported */
typedef pawvd (*pawaddconst_cb)( pawmbs name, void *ud );

extern pawb libpaw_quit;
/* Offset from libpaw_rdonly */
extern pawconstants *pawconsts;

/* These don't distinguish global pointers from thread locals so you're
 * responsible for making sure locks are used for globals that need them.*/

/* Get an thread local refid */
void pawdelref( mpawid id );
int pawlocref( paws name );
void pawsetengname( mpawid id, paws engname );
void pawsetlocname( mpawid id, pawmbs locname );
int pawresize( mpawid id, pawp data, pawru size );
/* We use pawp here because pawmbs is not defined yet and relies on this header
 * for various function definitions */
PAW_ANT_CB int mpawnewref
	( pawp file, pawru line, paws engname, pawmbs locname, pawp data, pawru size );
#define __PAWNEWREF( FILE, LINE, ENGNAME, LOCNAME, DATA, SIZE ) \
	_pawnewref( u8##FILE, LINE, ENGNAME, LOCNAME, DATA, SIZE )
#define _PAWNEWREF( FILE, LINE, ENGNAME, LOCNAME, DATA, SIZE ) \
	__PAWNEWREF( FILE, LINE, ENGNAME, LOCNAME, DATA, SIZE )
#define pawnewref( name, data, size ) \
	_PAWNEWREF( __FILE__, __LINE__, name, u8##name, data, size )
#define pawnewref_withlocname( engname, locname, data, size ) \
	_PAWNEWREF( __FILE__, __LINE__, engname, locname, data, size )
pawrd mpawrawdst_movpos( mpawid id, pawrd pos, int from );
pawrd mpawrawdst_setpos( mpawid id, pawrd pos );
pawrd mpawrawdst_getpos( mpawid id );
pawrd mpawrawdst_getcap( mpawid id );
int mpawrawdst_putabs( mpawid id, pawrd abs, pawp  src, pawrd set, pawrd *did );
int mpawrawdst_getabs( mpawid id, pawrd abs, void *dst, pawrd get, pawrd *did );
int mpawrawdst_putraw( mpawid id, pawp  src, pawru set, pawru *did );
int mpawrawdst_getraw( mpawid id, void *dst, pawru get, pawru *did );

void mpawinit_paweint( pawconstants *consts );
void mpawinit_pawage( pawconstants *consts );
int mpawant_termCB( mpawid id, void *ud );

pawconstants* libpaw_init_static(void);
int mpawant_ignite( mpawref *ref );
int mpawant_launch( mpawref *ref, pawrd argc, pawmbs *argv, mpawref_cb cb );
void mpawref_expire( mpawref *ref );
int mpawinitnormalants( mpawref *ants );
void  mpawtermnormalants( mpawref *ants, int count );

PAW_ANT_CB pawsys mpawfso_open
	( pawmbs file, pawru line, pawmbs path, pawelu opts );
#define LPAWFSO_OPEN( PATH, OPTS ) mpawfso_open( PAWMBS_ERRSRC, PATH, LINE )
int mpawmbe_mbs2s( pawmbs text, pawru leng );
int mpawmbe_s2mbs( paws text, pawru leng );
int mpawmbe_mbs2ls( pawmbs txt, pawru len );
int mpawmbe_ls2mbs( npawls txt, pawru len );
int mpawmbe_mbs2cs( pawmbs txt, pawru len, npawcp codepage );
int mpawmbe_cs2mbs( pawp   raw, pawru end, npawcp codepage );
#if NPAWTC_MAX > PAWCD_MAX
PAW_FQK_CB int mpawmbe_mbs2ts( pawmbs text, pawru leng )
	{ return mpawmbe_mbs2ls( text, leng ); }
PAW_FQK_CB int mpawmbe_ts2mbs( npawts text, pawru leng )
	{ return mpawmbe_ls2mbs( text, leng ); }
#else
PAW_FQK_CB int mpawmbe_mbs2ts( pawmbs text, pawru leng )
	{ return mpawmbe_mbs2s( text, leng ); }
PAW_FQK_CB int mpawmbe_ts2mbs( npawts text, pawru leng )
	{ return mpawmbe_s2mbs( text, leng ); }
#endif

PAW_C_API_SHUT
