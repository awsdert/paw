
#define PAWPTY_CSI "\x1B["
#define PAWPTY_CSI_CLEAR_BOTHWAYS 2
#define PAWPTY_CSI_CLEAR_BACKWARD 1
#define PAWPTY_CSI_CLEAR_FOREWARD 0
/* Clear Screen */
#define PAWPTY_CLS	PAWPTY_CSI "%dJ"
/* Clear line */
#define PAWPTY_CLR	PAWPTY_CSI "%dK"

#define PAWPTY_GFX_REQUEST	PAWPTY_CSI "%dm"
enum
{
	PAWPTY_STD_COLOUR_BLACK	= 0
	PAWPTY_STD_COLOUR_RED,
	PAWPTY_STD_COLOUR_GREEN,
	PAWPTY_STD_COLOUR_YELLOW,
	PAWPTY_STD_COLOUR_BLUE,
	PAWPTY_STD_COLOUR_MAGENTA,
	PAWPTY_STD_COLOUR_CYAN,
	PAWPTY_STD_COLOUR_WHITE,
	PAWPTY_STD_COLOUR_COUNT
};

#define PAWPTY_SGR_SELECT_FG_DIM PAWPTY_CSI "3%dm"
#define PAWPTY_SGR_SELECT_FG_LIT PAWPTY_CSI "9%dm"
#define PAWPTY_SGR_SELECT_FG_256 PAWPTY_CSI "38m;5;%d"
#define PAWPTY_SGR_SELECT_FG_RGB PAWPTY_CSI "38m;2;%d;%d;%d"
#define PAWPTY_SGR_SELECT_BG_DIM PAWPTY_CSI "4%dm"
#define PAWPTY_SGR_SELECT_BG_LIT PAWPTY_CSI "10%dm"
#define PAWPTY_SGR_SELECT_BG_256 PAWPTY_CSI "48m;5;%d"
#define PAWPTY_SGR_SELECT_BG_RGB PAWPTY_CSI "48m;2;%d;%d;%d"

#define PAWPTY_SGR_CURSOR_NULL_BLINK 25
#define PAWPTY_SGR_CURSOR_SLOW_BLINK 5
#define PAWPTY_SGR_CURSOR_FAST_BLINK 6

#define PAWPTY_SCROLL_U_LINES	PAWPTY_CSI "%dS"
#define PAWPTY_SCROLL_D_LINES	PAWPTY_CSI "%dT"

#define PAWPTY_REPORT_FOCUS		PAWPTY_CSI "?1004h"
#define PAWPTY_IGNORE_FOCUS		PAWPTY_CSI "?1004l"

#define PAWPTY_ENABLE_ALTBUF	PAWPTY_CSI "?1049h"
#define PAWPTY_IGNORE_ALTBUF	PAWPTY_CSI "?1049l"

/* Custom SGR Commands, Not finalised yet, just mind mapping for now
 *
 * Going to experiment with using the space character to take full control
 * of drawing in the terminal, if successful then will try make wrapper
 * modules for vulkan, opengl, directx, metal etc under the same concept with
 * the aim of removing the ABI issues associated with them but retaining the
 * high speed of baseline calls (you just need to construct the draw loop's
 * commands fully before passing it on */
#define PAWPTY_MAKE_GFXAPP		PAWPTY_CSI "200m"
#define PAWPTY_VOID_GFXAPP		PAWPTY_CSI "201m;%d"
#define PAWPTY_LINK_GFXAPP		PAWPTY_CSI "202m;%d"
#define PAWPTY_MAKE_SHADER		PAWPTY_CSI "203m;%d;%.*s"
#define PAWPTY_VOID_SHADER		PAWPTY_CSI "204m;%d"
#define PAWPTY_BIND_SHADER		PAWPTY_CSI "205m;%d;%d"
#define PAWPTY_MAKE_GFXCTX		PAWPTY_CSI "206m"
#define PAWPTY_VOID_GFXCTX		PAWPTY_CSI "207m;%d"
#define PAWPTY_MAKE_GFXBUF		PAWPTY_CSI "208m;%jvd"
#define PAWPTY_VOID_GFXBUF		PAWPTY_CSI "209m;%d"
#define PAWPTY_MAKE_GFXIMG		PAWPTY_CSI "210m;%jvd"
#define PAWPTY_VOID_GFXIMG		PAWPTY_CSI "211m;%d"

/* So that multiple printf calls are not needed for the same set of render calls */
#define PAWPTY_MAKE_GRPDRW		PAWPTY_CSI "212m;%.*s"
#define PAWPTY_VOID_GRPDRW		PAWPTY_CSI "213m;%d"
#define PAWPTY_DRAW_GRPDRW		PAWPTY_CSI "214m;%d"

#define PAWPTY_DRAW_SPECS		PAWPTY_CSI "215m"
#define PAWPTY_DRAW_LINES		PAWPTY_CSI "216m"
#define PAWPTY_DRAW_LINESTRIP	PAWPTY_CSI "217m"
#define PAWPTY_DRAW_TRIOS		PAWPTY_CSI "218m"
#define PAWPTY_DRAW_TRIOSTRIP	PAWPTY_CSI "219m"

#define PAWPTY_BIND_ATTR		PAWPTY_CSI "220m;%d"
#define PAWPTY_FREE_ATTR		PAWPTY_CSI "221m;%d"
#define PAWPTY_BIND_ATTR		PAWPTY_CSI "220m;%d"
#define PAWPTY_FREE_ATTR		PAWPTY_CSI "221m;%d"
