#include "..lpaw.h"
static npawfsomask fso_opts[64] = {0};

PAW_ANT_CB void intefine_fso_opts(void)
{
	npawfsomask *opts = fso_opts;
	opts[ NPAWFSO_O_TRIMREAL   ] = O_TRUNC;
	opts[ NPAWFSO_O_MKALWAYS   ] = O_CREAT | O_EXCL;

	opts[ 4 + NPAWFSO_O_RODATA ] = O_RDONLY;
	opts[ 4 + NPAWFSO_O_RWDATA ] = O_RDWR;
	opts[ 4 + NPAWFSO_O_ROEXEC ] = O_RDONLY | SPAW_O_EXEC;
	opts[ 4 + NPAWFSO_O_RWEXEC ] = O_RDWR   | SPAW_O_EXEC;

	opts[ NPAWFSO_O_NO_WAITING ] = SPAW_O_NONBLOCK;
	opts[ NPAWFSO_O_IS_RDONLY  ] = (CAST(pawfsomask, 0444)<<32);
	opts[ NPAWFSO_O_IS_FOLDER  ] = (CAST(pawfsomask, 0755)<<32) | O_DIRECTORY;
	opts[ NPAWFSO_O_IS_SYMLINK ] = (CAST(pawfsomask, 0755)<<32) | O_PATH;
	opts[ NPAWFSO_O_IS_REGFILE ] = (CAST(pawfsomask, 0644)<<32);

	opts[ NPAWFSO_O_IS_EXEFILE ] = (CAST(pawfsomask, 0755)<<32);
	opts[ NPAWFSO_O_IS_ARCHIVE ] = (CAST(pawfsomask, 0644)<<32);

	opts[ NPAWFSO_O_IGNORELINK ] = O_NOFOLLOW;
	opts[ NPAWFSO_O_BIGOFFSETS ] = SPAW_O_LARGEFILE;
	opts[ NPAWFSO_O_SCANRANDOM ] = (CAST(npawfsomask,POSIX_FADV_RANDOM) << 64);
	opts[ NPAWFSO_O_SCANLINEAR ] = (CAST(npawfsomask,POSIX_FADV_SEQUENTIAL) << 64);
	opts[ NPAWFSO_O_PREFER_RAM ] = O_SHORT_LIVED;
	opts[ NPAWFSO_O_DIRECTWRHW ] = O_DIRECT;
	opts[ NPAWFSO_O_DEL_ONSHUT ] = O_TMPFILE;

	opts[ NPAWFSO_O_APPEND     ] = O_APPEND;
	opts[ NPAWFSO_O_DIR_ADDSUB ] = O_WRITE;
	opts[ NPAWFSO_O_SYS_TRAITS ] = (CAST(pawfsomask,S_ISVTX|0200)<<32);
	opts[ NPAWFSO_O_DEV_TRAITS ] = (CAST(pawfsomask,S_ISVTX|0666)<<32);

	opts[ NPAWFSO_O_POSIX_SYNC_BOTH  ] = O_SYNC;
	opts[ NPAWFSO_O_POSIX_SYNC_DATA  ] = O_DSYNC;
	opts[ NPAWFSO_O_POSIX_DEL_ONEXEC ] = O_CLOEXEC;
	opts[ NPAWFSO_O_POSIX_NO_CONSOLE ] = O_NOCTTY;
}
PAW_ANT_CB pawmsg npawfso_setopt( npawfsomask *dst, pawfsomask opts )
{
	npawfsomask mask = 0;
	pawfsomask opt = 24;
	pawfsomask tmp = (*opts >> 16) & 0x3;
	if ( tmp == 3 )
	{
		/* Help the dev identify the error */
		*dst &= tmp << 16;
		return PAWMSGID_INVALIDOPT;
	}
	mask |= fso_opts[tmp];
	tmp = (opts >> 18) & 0x7;
	switch ( tmp )
	{
	case
	}
	while ( opt )
	{
		mask |= (opts & opt) ? fso_opts[opt] : 0;
		opt <<= 1;
	}
	return mask;
}
PAW_ANT_CB pawmsg npawfso_getopt( pawfsomask *dst, npawfsomask opts )
{
	npawfsomask tmp = 0;
	pawfsomask mask = 0;
	pawfsomask opt = 1;
	while ( opt )
	{
		tmp = fso_opts[opt];
		mask |= ((opts & tmp) == tmp) ? opt : 0;
		opt <<= 1;
	}
	return mask;
}
