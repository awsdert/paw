#include "../lpaw.h"

void _npawappexit( int code ) { exit(code); }
void _npawantexit( int code ) { _exit(code); }

__thread void (*npawexit)( int code ) = _npawappexit;

PAW_ANT_CB void npawant_useantexit( void ) { npawexit = _npawantexit; }

void pawsyn_free( void *addr ) { libpaw_thisant.sync_addr = NULL; }

PAW_ANT_CB pawd pawsyn_lock( void *addr )
{
	pawd i = 0, id = libpaw_thisant, fd = -1;
	npawant self = {NULL}, ant = {NULL};
	pawru own_offset = sizeof(self) * id, ant_offset = 0;
	/* Check if jumped the gun and tried to syncronise before paw is
	 * initialised */
	if ( id < 0 )
	{
		errno = ENOENT;
		return -1;
	}
	/* Whether our threads are in 1 process or split across processes, this
	 * syncropoint will remain the same without issue */
	fd = libpaw_static->ants_file;
	pread( fd, &self, sizeof(self), own_offset );
	self.sync_time = time(NULL);
	self.sync_prec = clock();
	self.sync_addr = addr;
	pwrite( fd, &self, sizeof(self), own_offset );
	for ( i = 0, ant_offset = 0; i < libpaw_static->max_ants; ++i )
	{
		pread( fd, &ant, sizeof(ant), ant_offset );
		if ( ant.sync_addr != addr || ant.sync_time > self.sync_time )
			continue;
		if ( ant.sync_time < self.sync_time || ant.sync_prec <= self.sync_prec )
		{
			self.sync_addr = NULL;
			pwrite( fd, &self, sizeof(self), own_offset );
			return -1;
		}
		if ( ant.sync_prec > self.sync_prec )
			continue;
	}
	return 0;
}
PAW_ANT_CB pawd pawsyn_wait( void *addr )
{
	while ( pawsyn_lock( addr ) != 0 )
	{
		if ( libpaw_quit )
			return -1;
		npawant_yield();
	}
}
