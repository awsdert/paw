#include "../../lpaw.h"

__thread npawsysinf libpaw_sysinf = {0};
pawd npawgetsysinf( void )
{
	pawd ret = 0;
	sysinf *si = &libpaw_sysinf;
	long conf = sysconf( _SC_PAGESIZE );
	if ( conf > 0 )
		si->pagesize = conf;
	else
	{
		/* Default to a likely valid value */
		si->pagesize = PAWIO_RARE;
		ret = -1;
	}
	/* Doing it this way because probably will go after other values later */
	return ret;
}
PAW_ANT_CB pawru pawgetpagesize(void) { return sysconf( _SC_PAGESIZE ); }

#ifdef _LARGEFILE64_SOURCE
PAW_FQK_CB pawd	sysopen( paws path, pawd opts, pawd perm )
	{ return open64( path, opts, perm ); }
#endif

pawd npawvm_hookCB( pawd action, lpawref *ref, va_list va )
{
	npawvm *vm = &(ref->union_chunk.virt);
	if ( ref->hook != vm )
		return PAWMSGID_MISMATCH;
	switch ( action )
	{
	case LIBPAWREF_ACTION_END:
		munmap( vm->data, vm->fullsize );
		memset( vm, 0, sizeof(*vm) );
		return 0;
	}
	return PAWMSGID_NO_CODE;
}
