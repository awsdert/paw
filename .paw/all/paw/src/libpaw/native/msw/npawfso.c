typedef union { pawrd d; pawe64d d64; LARGE_INTEGER sys; } fsopos;
PAW_FQK_CB pawe64d _npawsys_seek( pawref file, pawe64d pos, int from )
{
	fsopos abs = {0}, mov = {0};
	mov.d64 = pos;
	return SetFilePointerEx( file, mov.sys, &(abs.sys), from )
		? abs.d64 : pawe64d_v(-1);
}
PAW_ANT_CB npawfsp npawsys_seek( pawref file, npawfsp pos, int from )
{
	return (pawe64d_cmp(pos.d64,pawe64d_v(pos.d)) == 0)
		? _npawsys_seek( file, pos.d64, from ) : -1;
}
PAW_ANT_CB npawfsp npawsys64_seek( pawref file, npawfsp pos, int from )
	{ return npawsys64_seek( file, pos.d64, from ); }

/* Specifically for cleaning contents of mappings */
const uchar initmap[BUFSIZ] = {0};

npawu sysreg_secure_flags_mpaw2native( pawu prot )
{
	switch ( prot )
	{
		case NPAWVM_PROT_RDONLY:
			return FILE_MAP_READ;
		case NPAWVM_PROT_SCRIPT:
			return FILE_MAP_READ | FILE_MAP_EXECUTE;
		case NPAWVM_PROT_DUPLEX:
			return FILE_MAP_ALL_ACCESS;
		case NPAWVM_PROT_UNSAFE:
			return FILE_MAP_ALL_ACCESS | FILE_MAP_EXECUTE;
	}
	return 0;
}
#ifdef _LARGEFILE64_SOURCE
void* sysmapfso64
(
	mpawreg *fsomap,
	pawru size,
	pawu prot,
	pawlu opts,
	pawref fso,
	pawe64d pos
)
{
	npawfsp p = {0};
	void *base = NULL;
	pawu fprot = sysreg_secure_flags_mpaw2native( prot );
	pawb clamped = pawe64d_to_npawfsp(pos,&p);
	if ( !fso || !fprot || clamped )
		return NULL;
	HANDLE map = CreateFileMapping( fso, NULL, prot, 0, 0, size, NULL );
	if ( !map )
		return NULL;
	if ( fsomap->cap < size )
	{
		pawrd set = size - fsomap->cap;
		SetFilePos( fsomap->sys, 0, fsomap->cap );
		while ( set >= BUFSIZ )
		{
			WriteFile( fsomap->sys, initmap, BUFSIZ, NULL );
			set -= BUFSIZ;
		}
		if ( set )
			WriteFile( fsomap->sys, initmap, set, NULL );
		fsomap->cap = size;
	}
	base = MapViewOfFile( map, fprot, p.off64[1], p.off64[0], size );
	if ( !base )
	{
		CloseHandle( map );
		return NULL;
	}
	if ( prev )
		UnMapViewOfFile( prev );
	if ( fsomap->map )
		CloseHandle( fsomap->map );
	fsomap->map = map;
	return base;
}
#endif

void* sysmapfso
(
	pawref *sysreg,
	pawru size,
	pawu prot,
	pawlu opts,
	pawref *fso,
	pawrd pos
)
{
	npawfsp p = {0};
	mpawreg *reg = &(sysreg->union_chunk.region);
	void *base = NULL;
	pawu fprot = 0;
	pawb clamped = pawjvd_to_npawfsp(pos,&p);
	fprot = sysreg_secure_flags_mpaw2native( prot );
	if ( !fso || !fprot || clamped )
		return NULL;
	HANDLE map = CreateFileMapping( fso, NULL, prot, 0, 0, size, NULL );
	if ( !map )
		return NULL;
	if ( fsomap->cap < size )
	{
		pawrd set = size - fsomap->cap;
		SetFilePos( fsomap->sys, 0, fsomap->cap );
		while ( set >= BUFSIZ )
		{
			WriteFile( fsomap->sys, initmap, BUFSIZ, NULL );
			set -= BUFSIZ;
		}
		if ( set )
			WriteFile( fsomap->sys, initmap, set, NULL );
		fsomap->cap = size;
	}
	base = MapViewOfFile( map, fprot, 0, p.off64[0], size );
	if ( !base )
	{
		CloseHandle( map );
		return NULL;
	}
	if ( prev )
		UnMapViewOfFile( prev );
	if ( fsomap->map )
		CloseHandle( fsomap->map );
	fsomap->map = map;
	return base;
}

PAW_ANT_CB void* mpawmap_create
	( mpawref *file, mpawref *map, mpawref *sec, int prot, pawru minsize, pawru maxsize, npawts name )
{
	ref->union_chunk.handle = CreateFileMapping( INVALID_HANDLE_VALUE, NULL, prot, minsize, maxsize, name );
}

void* npawmap_replace
(
	pawref *ref,
	pawref *sec,
	int  pageprot,
	pawe64u offset,
	pawru size,
	npawts name
)
{
	mpawmap *map = &(ref->union_chunk.mapping);
	if ( ref->hook != map )
	{
		pawsetmsg( ref->refid ), PAWMSGID_CORRUPT, PAWSUBID_LIBREF, 1, 0 );
		return -1;
	}
	pawref *file = map->file;
	HANDLE  invalid = INVALID_HANDLE_VALUE;
	HANDLE *srchook = file ? file->hook : NULL,
	if ( !srchook )
		srchook = &_invalid;
	else if ( srchook != &(memfd->union_chunk.handle) )
	{
		pawsetmsg( file->refid ), PAWMSGID_CORRUPT, PAWSUBID_LIBREF, 1, 0 );
		return -1;
	}
	pawref *memfd = map->memfd;
	HANDLE *maphook = memfd->hook,
	if ( maphook && maphook != &(memfd->union_chunk.handle) )
	{
		pawsetmsg( memfd->refid ), PAWMSGID_CORRUPT, PAWSUBID_LIBREF, 1, 0 );
		return -1;
	}
	HANDLE dsthook = CreateFileMapping
		( *srchook, sec ? sec->hook : NULL, pageprot, size, size, name );
	if ( !dsthook )
		return -1;
	DWORD mapprot = 0;
	switch ( pageprot )
	{
	case NPAWVM_PROT_RDONLY: mapprot = FILE_MAP_READ;	break;
	case NPAWVM_PROT_WRONLY: mapprot = FILE_MAP_WRITE;	break;
	default: mapprot FILE_MAP_ALL_ACCESS;
	}
	void *data = NULL;
	if ( !file )
	{
		data = MapViewOfFile
		(
			dsthook,
			FILE_MAP_WRITE,
			offset >> 32,
			offset & 0xFFFFFFFFW32,
			size
		);
		if ( !data )
		{
			CloseHandle( dsthandle );
			return -1;
		}
		memcpy( data, map->data, map->fullsize );
		UnMapViewOfFile( data );
	}
	data = MapViewOfFile
		( dsthook, mapprot, offset >> 32, offset & 0xFFFFFFFFW32, size );
	if ( !data )
	{
		CloseHandle( dsthandle );
		return -1;
	}
	return data;

}

enum
{
	PAW_OCTALS_OTHER = 0,
	PAW_OCTALS_GROUP,
	PAW_OCTALS_OWNER,
	PAW_OCTALS_COUNT
};

typedef struct
{
	EXPLICIT_ACCESS[PAW_OCTALS_COUNT] ealist;
} spawfsperm;

// https://stackoverflow.com/questions/42140977/windows-create-file-access-that-only-allows-owner-to-access-the-file-win32
void spawfso_unixtowin32( spawfsperm *fsperm, DWORD perm, DWORD grfAccessMode )
{
	EXPLICIT_ACCESS *ea = NULL;
	npawts trusteenames[PAW_OCTALS_COUNT] =
		{ TEXT("EVERYONE"), TEXT("CREATOR_GROUP"), TEXT("CREATOR_OWNER") };
	DWORD mov = 0, oct = 0;
	memset( fsperm, 0, sizeof(*fsperm) );
	for ( DWORD i = 0; i < PAW_OCTALS_COUNT; ++i )
	{
		mov = i * 3;
		ea = fsperm->ealist + i;
		/* unix mask and win32 masks match their relative positions so abuse
		 * that where possible  */
		oct = (perm & (07 << mov)) >> mov;
		ea->grfAccessPermissions	= (oct == 07) ? GENERIC_ALL : oct << 29,
		ea->grfAccessMode			= grfAccessMode;
		ea->grfInheritance			= NO_INHERITANCE;
		ea->Trustee.TrusteeForm		= TRUSTEE_IS_NAME;
		ea->Trustee.TrusteeType		= TRUSTEE_IS_WELL_KNOWN_GROUP;
		ea->Trustee.ptstrName		= trusteenames[i];
	}
	fsperm->ealist[PAW_OCTALS_OWNER].Trustee.TrusteeType = TRUSTEE_IS_USER;
}

pawmsg spawfso_getperm( npawsys fso, DWORD *perm )
{
	ACL *dacl = NULL;
	spawfsperm fsperm;
	SECURITY_DESCRIPTOR *sid = NULL;
	DWORD rc1 = ERROR_SUCCESS, rc2 = rc1;
	spawfso_unix2win32(&fsperm,0,GET_ACCESS);
	//DWORD ec1 = 0, ec2 = 0;
	rc1 = GetSecurityInfo
	(
		fso, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION
		| OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION,
		NULL, NULL, &dacl, NULL, &sid
	);
	//ec1 = GetLastError();
	if ( rc1 == ERROR_SUCCESS )
		rc2 = GetExplicitEntriesFromAcl( dacl, 3, fsperm.ealist );
	//ec2 = GetLastError();

	*perm = 0;
	if ( sid )
		LocalFree( sid );
	sid = NULL;

	if ( rc1 != ERROR_SUCCESS || rc2 != ERROR_SUCCESS )
		return -1;

	for ( i = 1; i < PAW_OCTALS_COUNT; i <<= 1 )
	{
		rc1 = (fsperm->ealist[i].grfAccessPermissions & 0xF0000000);
		rc2 = (rc1 & GENERIC_ALL) ? 07 : rc1 >> 29;
		*perm |= rc2 << (i * 3);
	}

	return 0;
}

// https://stackoverflow.com/questions/690780/how-to-create-directory-with-all-rights-granted-to-everyone
pawmsg spawfso_setperm( npawsys fso, DWORD perm )
{
	ACL *dacl = NULL;
	spawfsperm fsperm;
	DWORD rc = ERROR_FAILURE;
	spawfso_unix2win32(&fsperm,perm,SET_ACCESS);
	SetEntriesInAcl(3, fsperm.ealist, NULL, &dacl);

	// TODO: Interpret resulting code
	if ( rc != ERROR_SUCCESS )
		return -1;

	SECURITY_ATTRIBUTES secattr =
	{
		.nLength = sizeof(SECURITY_ATTRIBUTES);
		.lpSecurityDescriptor = secdesc;
		.bInheritHandle = FALSE;
	};

	rc = SetSecurityInfo
	(
		fso, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION
		| OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION,
		NULL, NULL, &dacl, NULL
	);
	return (rc != ERROR_SUCCESS) ? -1 : 0;
}

typedef struct { char text[bitsof(pawru)]; } pawintustr;
inline pawb npawgetrngbuf( void *buf, npawhu cap )
	{ return RtlGenRandom(buf,cap); }

inline pawintustr pawintgetustr( pawru rng )
{
	npawzu i = 0, j = 0, k = 0;
	npawzu rngleng = pawintu_divciel( max, sizeof(DWORD) );
	npawzu rngsize = rngleng * sizeof(DWORD);
	DWORD rngbuff = alloca(rngsize);
	npawgetrngbuf(rngbuff,rngsize);
	for ( ; i < rngleng; ++i )
	{
		/* Randomise letter case when inserting rng base name */
		for ( k = rngbuff[i] & 0xF; rngbuff[i]; rngbuff[i] >>= 4 )
			dst[j] = (k < 0xA) ? '0' + k : 'A' + k + (k & 0x1) * ('a' - 'A');
	}
}

npawsys npawfso_opentmp( paws dirpath, paweu perm )
{
	typedef DWORD[4] npawfso_rngbuff;
	npawzu i = 0, j = 0, k = 0, dirleng = pawslen(dirpath);
	char path = alloca(dirleng+1+bitsof(npawfso_rngbuff);
	for ( ; i < dirleng; ++i )
		path[i] = (dirpath[i] == '/') ? '\\' : dirpath[i];
	path[dirleng] = '\\';
	npawrngstr( bitsof(npawfso_rngbuff), path + dirleng + 1 );
	return -1;
}
