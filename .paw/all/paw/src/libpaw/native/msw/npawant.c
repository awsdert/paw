#include "../lpaw.h"

void _npawappexit( DWORD code ) { ExitProcess(code); }
void _npawantexit( DWORD code ) { ExitThread(code); }

__thread void (*npawexit)( DWORD code ) = _npawappexit;

PAW_ANT_CB void npawant_useantexit( void ) { npawexit = _npawantexit; }
