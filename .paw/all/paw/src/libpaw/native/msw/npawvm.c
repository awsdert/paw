#include "npaw.h"
__thread npawsysinf npawsysinf = {0};
int  npawgetsysinfo(  void ) { GetSystemInfo(&npawsysinf); return 0; }
PAW_ANT_CB pawru pawgetpagesize( void ) { return npawsysinf.dwPageSize; }

int npawinit( void )
{
	SetErrorMode(SEM_FAILCRITICALERRORS);
}

int npawmfo_open( lpawref *dst, npawts dir, pawlu mode, pawlu opts )
{
	npawtc tmpname[MAX_PATH+1] = {0};
	UINT unique = GetTempFileName( dir, NPAWTC_C(""), 0, tmpname );
	if ( !unique )
	{
		pawsetmsg(-1);
		return -1;
	}
	pawru dlen = npawtslen(dir);
	pawru tlen = npawtslen(tmpname);
	pawru leng = dlen + tleng;
	npawtc *path = __builtin_alloca(leng+1);
	memcpy( path, dir, sizeof(npawtc) * dlen );
	memcpy( path + dlen, tmpname, sizeof(npawtc) * tlen );
	path[leng] = 0;
	dst->hook = CreateFile( path, mode, opts );
	if ( dst->hook == INVALID_HANDLE )
	{
		pawsetmsg( -1 );
		dst->hook = NULL;
		return -1;
	}
	return 0;
}

PAW_ANT_CB npawfsm npawmfo_create( npawts name, pawru size, npawopt opts )
	{ return npawmfo_open( INVALID_NPAWFSO, NULL, prot, size, name ); }


inline DWORD npawmapopt2sys( DWORD opt )
{
	switch ( opt )
	{
	case NPAWVM_PROT_RDONLY: return FILE_MAP_READ;
	case NPAWVM_PROT_WRONLY: return FILE_MAP_WRITE;
	}
	return FILE_MAP_ALL_ACCESS;
}

void* npawmap
(
	upawabs addr,
	pawru  size,
	paweu  pageprot,
	npawsys fsohook,
	pawe64d offset
)
{
	npawzu pagesize = pawgetpagesize();
	DWORD prot = npawmapopt2sys(pageprot);
	npawsys maphook = badhook, *handles = NULL;
	const HANDLE badhook = INVALID_HANDLE_VALUE;
	if ( addr.top )
	{
		handles = (npawsys*)(addr.top - pagesize);
		maphook = handles[0];
		fsohook = handles[1];
	}
	else if ( !size )
		return NULL;
	else
	{
		maphook = CreateFileMapping( fsohook, NULL, prot, size, size, name );
		if ( maphook == badhook )
			return -1;
		fsohook = fd;
	}
	if ( !size )
	{
		if ( !addr.top )
			return -l;
		UnMapViewOfFile( data );
		CloseHandle( maphook );
	}
	void *data = MapViewOfFile
	(
		maphook,
		fsohook ? prot : FILE_MAP_WRITE,
		offset >> 32,
		offset & 0xFFFFFFFFL,
		size
	);
	if ( !file )
	{

		if ( !data )
		{
			CloseHandle( dsthandle );
			return -1;
		}
		memcpy( data, map->data, map->fullsize );
		UnMapViewOfFile( data );
	}
	data = MapViewOfFile
		( dsthook, mapprot, offset >> 32, offset & 0xFFFFFFFFL, size );
	if ( !data )
	{
		CloseHandle( dsthandle );
		return -1;
	}
	return data;

}
