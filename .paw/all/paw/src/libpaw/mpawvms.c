pawref *pawsrcpipeis(void) { return mpawthisant->pawsrcpipe; }
pawref *pawdstpipeis(void) { return mpawthisant->pawdstpipe; }
pawref *pawlogpipeis(void) { return mpawthisant->pawlogpipe; }
pawref *pawthisantis(void) { return mpawthisant->pawthisant; }
npawd    pawvmem_newCB( pawmbs file, pawru line, mpawrawdst *mem, pawru get )
{
	pawru pagesize = libpaw_getpagesize();
	pawru cap = pawintu_mulciel( cap, pagesize );
	void *top = npawvm_resize( mem->top, size, NPAWVM_PROT_DUPLEX, 0 );
	if ( top )
	{
		mem->top = top;
		mem->cap = cap;
		mem->lim = get;
		return 0;
	}
	npawd subid = 0;
	pawsetmsg( libpawgeterr( &subid ), subid, 0, 0 );
	return -1;
}

pawmem pawvmem_new( pawmbs file, pawru line, pawru get )
{
	pawmem mem = {0};
	mem.newCB = pawvmem_newCB;
	(void)pawmem_fitcap( &mem, get );
	return mem;
}

void pawterm_thisant(void)
{
	pawmem_fitcap( &pawdsttext, 0 );
	pawmem_fitcap( &pawsrctext, 0 );
}

npawd pawinit_thisant(void)
{
	if ( pawsrctext.top )
	{
		pawsetmsg( PAWMSGID_ALREADY, PAWSUBID_THREAD, 1, 0 );
		return -1;
	}
	pawsrctext = pawvmem_new( PAWMBS_ERRSRC, BUFSIZ );
	if ( !(pawsrctext.top) )
	{
		pawterm_thisant();
		return -1;
	}
	pawdsttext = pawvmem_new( PAWMBS_ERRSRC, BUFSIZ );
	if ( !(pawdsttext.top) )
	{
		pawterm_thisant();
		return -1;
	}
	return 0;
}

PAW_APP_CB npawd pawinit( pawrun_cb runCB, void *ud )
{
	npawd res = -1;
	res = pawinit_thisant();
	if ( res < 0 )
		return res;
	res = runCB( ud );
	pawterm_thisant();
	return res;
}
