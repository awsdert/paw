#pragma once

typedef struct
{
	pawru     _line;
	pawmbs    _file;
	pawref *file;
	pawref *memfd;
	pawu       prot;
	pawru      size;
	void      *addr;
	npawfsp   pos;
} libpawmap;
typedef struct
{
	pawru      _line;
	pawmbs     _file;
	npawfsp *start;
	npawfsp *bytes;
	pawref  *file;
	pawref  *area;
	npawfsp _pos[2];
} libpawreg;

/** @brief Create region handle from fso (or just double reference it) **/
pawd  libpawreg_new( pawref *fso, void *pos, void *len, pawru size );
/** @brief Map/Remap memory to a file region, optionally to a specific file
 * @note Main memory file is used if fso is NULL
 * @note pawd region, void *pos, pawru size are expected if file isn't NULL **/
void* libpawmap_new( void *addr, pawru want, pawu prot, pawref *fso, ... );
