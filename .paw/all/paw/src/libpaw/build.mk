include abi.mk
PRJ:=libpaw
OUT:=$(PRJ)$(LIB_EXT)
PRJ_DIR:=$(SRC_DIR)/$(PRJ)
DEBUG_DIR:=$(NPAW_DIR)/debug/paw
TIMED_DIR:=$(NPAW_DIR)/timed/paw
TESTS_DIR:=$(NPAW_DIR)/tests/paw

PRJ_DIRS:=$(wildcard $(PRJ_DIR)/*/)
PRJ_DIRS:=$(filter-out $(wildcard $(PRJ_DIR)/native/*/),$(PRJ_DIRS))
PRJ_DIRS+=$(wildcard $(PRJ_DIR)/native/$(NPAW)/)
PRJ_DIRS:=$(PRJ_DIRS:$(PRJ_DIR)/%/=%)

INC_FILES:=$(wildcard $(INC_DIR)/*.h)
INC_FILES:=$(filter-out $(INC_DIR)/template% $(INC_DIR)/inherit%,$(INC_FILES))
C_INCS:=$(PRJ_DIR)/_mpaw.h $(PRJ_DIR)/mpaw.h $(call seekall,$(INC_DIR)/paw,*.h)

CPATH:=$(PRJ_DIR)$(OS_PATH_SEP)$(PRJ_DIR)/native/$(NPAW)$(OS_PATH_SEP)$(CPATH)

ALL_TOP_DIRS:=$(foreach m,$(PAW_MODES),$(NPAW_DIR)/$m/paw)
ALL_DIRS:=$(foreach d,$(ALL_TOP_DIRS),$d $(PAW_DIRS:%=$d/%))

mkprjdirs=$(call mkdir,$1) $(foreach s,$(PRJ_DIRS),$(call mkdir,$1/$s))
$(foreach m,$(PAW_MODES),$(call mkdir,$(NPAW_DIR)/$m))
$(foreach m,$(PAW_MODES),$(call mkdir,$(NPAW_DIR)/$m/paw))
$(foreach m,$(PAW_MODES),$(call mkprjdirs,$(NPAW_DIR)/$m/paw/$(PRJ)))

C_SRCS:=$(wildcard $(PRJ_DIR)/*.c $(PRJ_DIRS:%=$(PRJ_DIR)/%/*.c))
C_SRCS:=$(C_SRCS:$(PRJ_DIR)/%=%)
C_OBJS:=$(C_SRCS:%=$(PRJ)/%.o)
LIBS:=cglm

all: build

rebuild: clean build

$(info CPATH=$(CPATH))
export CPATH

$(info CFLAGS=$(CFLAGS))
export CFLAGS

export WORKSPACE_DIR
export HPAW_DIR
export NPAW_DIR
export PAW_DIR
export OTG_DIR

export NPAW_ABI_OPTS
export PAW_DATA_MODEL
export PAW_ENDIAN
export PAW_MODES
export PAW_ARCH
export HPAW_ABI
export NPAW_ABI
export PAW_ABI
export PAW_VER
export PAW_OS

export APP_EXT
export LIB_EXT
export OS_PATH_SEP
export OS_DIR_SEP

$(info OUT=$(OUT))
export OUT

build: $(ALL_TOP_DIRS:%=%/$(OUT))

clean: $(wildcard $(ALL_DIRS:%=%/*.o) $(ALL_TOP_DIRS:%=%/$(OUT)))
	rm $<

BFLAGS:=$(NPAW_ABI_OPTS) -shared

$(NPAW_DIR)/all/paw/$(OUT): $(DEBUG_DIR)/$(OUT)
	$(CP) "$^" "$@"
$(DEBUG_DIR)/$(OUT): $(C_OBJS:%=$(DEBUG_DIR)/%)
	$(CC) $(BFLAGS) -ggdb -o "$@" $(^:%="%") $(LIBS:%=-l %)
	objcopy --only-keep-debug "$@" "$@.gdbsym"
	strip --strip-debug --strip-unneeded "$@"
	objcopy --add-gnu-debuglink="./$(OUT).gdbsym" "$@"
$(TIMED_DIR)/$(OUT): $(C_OBJS:%=$(TIMED_DIR)/%)
	$(CC) $(BFLAGS) -pg   -o "$@" $(^:%="%") $(LIBS:%=-l %)
$(TESTS_DIR)/$(OUT): $(C_OBJS:%=$(TESTS_DIR)/%)
	$(CC) $(BFLAGS) -ggdb -o "$@" $(^:%="%") $(LIBS:%=-l %)

CFLAGS+=$(NPAW_ABI_OPTS)

$(DEBUG_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) -ggdb -o "$@" -c "$(CAUGHT_SHELL_CWD)/$<"
$(TIMED_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) -pg   -o "$@" -c "$(CAUGHT_SHELL_CWD)/$<"
$(TESTS_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) -ggdb -o "$@" -c "$(CAUGHT_SHELL_CWD)/$<"

$(C_SRCS): $(C_INCS)

.PHONY: all rebuild build clean

