#define COMPILE_MPAW_LOCALS 1
#include "mpaw.h"

/* pawmsg is only used here to ensure same size as the otherwise unused chunk
 * of pawsys. Full credit to aGerman via this thread:
 *
 * https://cboard.cprogramming.com/c-programming/182052-what-checksum-would-you-recommend-particular-scenario.html
 *
 * For the 1st variant of this function */
PAW_ANT_CB pawmsg mpawgenchksum( pawp data, pawru size, pawru width, pawmsg tok )
{
  pawru const shift = width - 1;
  uchar const *i = data, *end = i + size;
  for ( ; i < end; ++i )
    tok = ((tok << 1) | ((tok >> shift) & 1)) ^ (pawmsg)(*i);
  return tok & (((pawmsg)1 << width) - 1);
}

int execpaw( int argc, npawts *argv, mpawref_cb cb )
{
	mpawref ants[pawappm__ant_count] = {0};
	mpawinitnormalants(ants);
	mpawant_launch(ants+pawappm_libant,argc,argv,cb);
	mpawtermnormalants(ants,pawappm__ant_count);
	return -1;
}
void  mpawtermnormalants( mpawref *ants, int count )
{
	while ( --count > 0 )
			mpawref_expire( ants + count );
	memset( ants, 0, sizeof(pawref) * pawappm__ant_count );
}
int mpawinitnormalants( mpawref *ants )
{
	int i = 1;
	mpawref *ant = NULL;
	for ( i = 1; i < pawappm__ant_count; ++i )
	{
		ant = ants + i;
		if ( mpawant_ignite(ant) == 0 )
			continue;
		mpawtermnormalants(ants,i);
		return -1;
	}
	return 0;
}
PAW_ANT_CB mpawref* mpawref_assign( pawmbs file, pawru line )
{
	/* TODO: Allocate via internal method */
	mpawref *ref = calloc( 1, sizeof(mpawref) );
	if ( !ref )
		return NULL;
	ref->_file = file;
	ref->_line = line;
	return ref;
}
PAW_ANT_CB void mpawref_remove( mpawref* ref )
{
	if ( ref->expireCB )
		ref->expireCB(ref,NULL);
	ref->expireCB = NULL;
	ref->_file = NULL;
	ref->_line = 0;
	/* TODO: Release via internal method */
	free( ref );
}
extern PAW_QCK_CB pawcsc mpawcsmem_inscsv( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va );
PAW_ANT_CB mpawref* mpawfso_shutCB( mpawref *ref, void *ud )
	{ (void)ud; npawfso_shut( ref->hook.fso ); return NULL; }
PAW_ANT_CB int mpawfso_open
	( pawmbs file, pawru line, pawmbs path, pawelu opts )
{
	mpawref *ref = &(mpawthisant->ref);
	npawopt opt = 0;
	npawopt perm = 644;
	pawb icase = 1, modeset = 0;
	npawfso_params cfg = {0};
	npawtsvec const *name = NULL;
	if ( mpawmbe_mbs2ts( path, npawtslen(path) ) != 0 )
		return INVALID_NPAWFSO;
	npawtsvec *initialpath = (void*)&(mpawthisant->mbedsttext);
	if ( opts & O_DIRECTORY )
		perm |= 0111;
	int msgid = _mpawfso_find( icase, *initialpath );
	mpawfso_segptrs *segptrs = NULL;
	pawru i = 0, leng = 0, segc = 0;
	pawb precise = !!(opts & PAWFSO_O_PRECISECASE);
	memset( ref, 0, sizeof(*ref) );
	ref->hook.fso = INVALID_NPAWFSO;
	if ( msgid )
		return msgid;
	segptrs = mpawthisant->abspathsegs.top;
	segc = segptrs->segslist.end / sizeof(npawtsvec);
	leng = segptrs->namelist.end / sizeof(npawtsvec);
	pawb matches = precise ? segptrs->precise : leng;
	switch ( opts & 0x7 )
	{
		case PAWFSO_BIAS_OPENIFREAL:
		case PAWFSO_BIAS_TRIMIFREAL:
			if ( !matches )
				return PAWMSGID_NO_MATCH;
			name = segptrs->namelist.top;
			if ( precise )
			{
				name += matches;
				break;
			}
			if ( leng > 2 )
				return PAWMSGID_AMBIGUOUS;
			++name;
			break;
		case PAWFSO_BIAS_MAKEORFAIL:
		case PAWFSO_BIAS_OPENALWAYS:
		case PAWFSO_BIAS_TRIMALWAYS:
			if ( matches )
				return PAWMSGID_ALREADY;
			break;
		default:
			return PAWMSGID_INVALIDOPT;
	}
	ref->hook.fso = npawfso_open( segptrs->abspath.top, &cfg );
	if ( ref->hook.fso == INVALID_NPAWFSO )
		return -1;
	ref->expireCB = mpawfso_shutCB;
	ref->_file = file;
	ref->_line = line;
	return -1;
}
PAW_ANT_CB void* pawgetallocation(pawru *size)
{
	void *data = mpawthisant->allocation;
	if ( size ) *size = mpawthisant->allocationsize;
	mpawthisant->allocationsize = 0;
	mpawthisant->allocation = NULL;
	return data;
}
PAW_ANT_CB void  pawsetallocation(void* allocation)
{
	mpawthisant->allocation = allocation;
}
PAW_ANT_CB pawed pawrefcallv( pawmbs file, pawru line, mpawref *ref, va_list va )
{
	pawelu opts = 0;
	pawru call = va_arg( va, pawru );
	switch ( call )
	{
	case 0: return 0;
	case PAWCALL_NEWFSO:
		/* Bias */
		opts = *va_arg( va, pawru* );
		/* Mode */
		opts |= *va_arg( va, pawe64u* );
		return mpawfso_open( file, line, va_arg(va,pawmbs), opts );
	}
	return -1;
}
PAW_ANT_CB pawe32u pawrngmath_lehmer( pawe32u seed )
{
	pawe32u base = pawe32u_add( seed, 0xE120FC15L );
	pawe32u temp = pawe32u_mul( base, 0x4A39b70DL );
	pawe32u num1 = pawe32u_bwx( pawe32u_shr( temp, 32 ), temp );
	temp = pawe32u_mul( num1, 0x12FAD5C9L );
	return pawe32u_bwx( pawe32u_shr( temp, 32 ), temp );
}
