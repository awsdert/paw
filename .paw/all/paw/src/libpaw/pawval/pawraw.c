#include "../lpaw.h"
#include <string.h>
#include <malloc.h>
PAW_ANT_CB void* pawstdget( void *ud, pawru size )
{
	if ( !size )
	{
		if ( ud ) free( ud );
		return NULL;
	}
	return ud ? realloc( ud, size ) : malloc( size );
}
PAW_ANT_CB pawru lpawrawclr( void *dst, pawru clr )
{
	(void)memset( dst, 0, clr );
	return clr;
}
PAW_ANT_CB pawru pawrawset( void *dst, pawcu val, pawru set )
{
	pawd tmpi = 0;
	pawru size = sizeof(pawd);
	pawcu *tmpv = (pawcu*)&tmpi;
	while ( size-- ) tmpv[size] = val;
	(void)memset( dst, tmpi, set );
	return set;
}
PAW_ANT_CB pawru pawrawcpy( void *dst, pawp src, pawru cpy )
	{ (void)memcpy( dst, src, cpy ); return cpy; }
PAW_ANT_CB pawru pawrawrcp( void *dst, pawp src, pawru cpy )
{
	pawcu const *s = src;
	pawru left = cpy;
	pawcu *d = dst;
	while ( left-- )
		*(d--) = *(s--);
	return cpy;
}
PAW_ANT_CB pawru pawrawcmp( pawp a, pawp b, pawru cap )
{
	pawru i = 0;
	pawcu const *A = a, *B = b;
	for ( ; i < cap; ++i )
	{
		if ( A[i] != B[i] )
			return i;
	}
	return -1;
}
PAW_ANT_CB pawru pawrawrcmp( pawp a, pawp b, pawru cap )
{
	pawru i = cap;
	pawcu const *A = a, *B = b;
	A -= cap;
	B -= cap;
	while ( i-- )
	{
		if ( A[i] != B[i] )
			return cap - (i+1);
	}
	return -1;
}

PAW_ANT_CB void*  pawdata_new( pawrd cap, void **memptr );
PAW_ANT_CB void*  pawdata_dup( void *data );
PAW_ANT_CB void   pawdata_rem( void *data );
PAW_ANT_CB pawrd  pawdata_getdef( void *data );
PAW_ANT_CB pawrd  pawdata_getdef( void *data );
PAW_ANT_CB void*  pawdata_getptr( void *data );
PAW_ANT_CB pawp   pawdata_tmpcpy( void *data );
PAW_ANT_CB void   pawdata_remcpy( void *data );
PAW_ANT_CB void*  pawdata_tmpbuf( void *data );
PAW_ANT_CB void   pawdata_remtmp( void *data );

/* The main modules */
typedef struct
{
	void *module;
	pawed (*getcCB)( pawmbe *mbe, pawp  src, pawru lim );
	pawed (*putcCB)( pawmbe *mbe, void *dst, pawru cap );
} lpawmbemod;
extern lpawmbemod* lpawmbemod_char;
extern lpawmbemod* lpawmbemod_utf8;
extern lpawmbemod* lpawmbemod_utf16;
extern lpawmbemod* lpawmbemod_utf32;
extern lpawmbemod* lpawmbemod_wchar_t;

/* Placeholder function for unused code that will probably be moved elsewhere
 * once I decide what to do with it. */
PAW_ANT_CB pawd   pawmbe_s2mbs( paws srcstr, pawru srcleng )
{
	pawru si = 0, di = 0, dstsize = 0, dstmax = 0, did = 0;
	lpawmbemod *srcmod = lpawmbemod_char;
	lpawmbemod *dstmod = lpawmbemod_utf8;
	pawmbc *dstmbs = NULL;
	mbstate_t mb = {0};
	/* Store relative length of string */
	mbrlen( srcstr, srcleng, &mb );
	errno = 0;
	while ( si < strleng )
	{
		if ( dstsize - di < PAWMBC_MAX_ENCODED_CHARS + 1 )
		{
			pawru left = srcleng - si;
			pawru need = dstmax + ((left+1) * PAWMBC_MAX_ENCODED_CHARS);
			pawru size = need * sizeof(pawmbc);
			pawmbc *tmp = realloc( dst, size );
			if ( !tmp )
			{
				free( dst );
				return -1;
			}
			memset( tmp + dstmax, 0, size - dstsize );
			dstsize = size;
			dstmax = need;
			dst = tmp;
		}
		did = mbrtoc8( dstmbs + di, srcstr + si, srcleng - si, &mb );
		switch ( did )
		{
		/* '\0' consumed */
		case 0: ++si; ++di; continue;
		/* Illegal sequance */
		case (pawru)-1: return -1;
		/* Incomplete sequence */
		case (pawru)-2: errno = ENODATA; return -1;
		/* Written but nothing consumed */
		case (pawru)-3: break;
		/* Consumed X characters */
		default: si += did;
		}
		di += pawmbslen( dstmbs + di );
	}
	free( dst );
	return 0;
}

extern PAW_QCK_CB pawru lpawmemdst_setcap( lpawmemdst *mem, pawru cap );
extern PAW_QCK_CB pawru lpawmemdst_setlim( lpawmemdst *mem, pawru lim );
extern PAW_FQK_CB pawru lpawmemdst_setmax( lpawmemdst *mem, pawru T, pawru max );
extern PAW_FQK_CB pawru lpawmemdst_setlen( lpawmemdst *mem, pawru T, pawru len );
extern PAW_QCK_CB pawru lpawmemdst_fitcap( lpawmemdst *mem, pawru cap );
extern PAW_FQK_CB pawru lpawmemdst_fitlim( lpawmemdst *mem, pawru lim );
extern PAW_FQK_CB pawru lpawmemdst_fitmax( lpawmemdst *mem, pawru T, pawru max );
extern PAW_FQK_CB pawru lpawmemdst_fitlen( lpawmemdst *mem, pawru T, pawru len );
extern PAW_FQK_CB pawru lpawmemdst_capcap( lpawmemdst *mem, pawru cap );
extern PAW_FQK_CB pawru lpawmemdst_caplim( lpawmemdst *mem, pawru lim );
extern PAW_FQK_CB pawru lpawmemdst_capmax( lpawmemdst *mem, pawru T, pawru max );
extern PAW_FQK_CB pawru lpawmemdst_caplen( lpawmemdst *mem, pawru T, pawru len );
extern PAW_FQK_CB void lpawmemdst_delete( lpawmemdst *mem );
extern PAW_QCK_CB lpawmemdst _lpawmemdst_new( pawmbs file, pawru line, pawru pad );
extern PAW_QCK_CB lpawmemdst _lpawmemdst_newcap( pawmbs file, pawru line, pawru cap );
extern PAW_QCK_CB lpawmemdst _lpawmemdst_newlim( pawmbs file, pawru line, pawru lim );
extern PAW_QCK_CB lpawmemdst _lpawmemdst_newmax( pawmbs file, pawru line, pawru T, pawru max );
extern PAW_QCK_CB lpawmemdst _lpawmemdst_newlen( pawmbs file, pawru line, pawru T, pawru len );

/* malloc/realloc/free with memset( top + pos, 0, get - pos ); auto applied */
PAW_ANT_CB pawru lpawmemdst_standardCB( void **old, pawru *was, pawru pos, pawru get )
{
	if ( !get )
	{
		if ( *old )
			free( *old );
		*old = NULL;
		*was = 0;
		return 0;
	}
	if ( get > *was )
	{
		void *top = *old ? realloc( *old, get ) : malloc( get );
		if ( top )
			*old = top;
		else
			get = *was;
	}
	if ( pos < get )
		memset( pawv_addbytes( *old, pos ), 0, get - pos );
	return get;
}
