#include "../lpaw.h"
#include <npaw/pawvm.c>
typedef struct _sysrawmap sysrawmap;
struct _sysrawmap
{
	sysmem    list;
	sysmem    page;
	npawref    fso;
	npawfsp    pos;
	libpawreg fsomap;
};
sysrawmap* sysrawmap_new( pawru want, pawu prot )
{
	pawts tmpdir = sysgetenvTMPDIR();
	pawtsbuf pawdir = pawtsbuf_newcatstrf( tmpdir, PAWTC_C("/.paw") );
	if ( !(pawdir.top) )
		return NULL;
	npawref fso = sysmemfd_open( pawdir.top, 0, 0 );
	pawtsbuf_clrall( &pawdir );
	libpawreg fsomap = {0};
}
pawvu sysmem_take( sysmem *list, pawvu pos, pawvu want )
{
	pawvu take = pawintu_mulciel( want, sizeof(sysmem) ), nxt = pos + take;
	sysmem *mem = pawint_vptradd( list, pos ), *tmp = NULL;
	if ( nxt < mem->block.next )
	{
		tmp = pawint_vptradd( list, nxt );
		tmp->size = tmp->used = 0;
		tmp->next = mem->next;
		tmp->prev = pos;
		mem->next = nxt;
	}
	if ( mem->empty.prev )
	{
		tmp = pawint_vptradd( list, mem->empty.prev );
		tmp->empty.next = mem->empty.next;
	}
	if ( mem->empty.next )
	{
		tmp = pawint_vptradd( list, mem->empty.next );
		tmp->empty.next = mem->empty.prev;
	}
	if ( mem->block.next )
	{
		tmp = pawint_vptradd( list, mem->block.next );
		mem->taken.prev = tmp->taken.prev;
		tmp->taken.prev = pos;
	}
	tmp = pawint_vptradd( list, mem->block.prev );
	mem->taken.next = tmp->taken.next;
	tmp->taken.next = pos;
	mem->empty.prev = 0;
	mem->empty.next = 0;
	mem->size = take;
	mem->used = want;
	return pos;
}

void sysmem_free( sysmem *list, pawvu pos )
{
	/* Just assume the offset is correct, if it's not then it was given by
	 * either faulty or malicous code, probably the former. */
	sysmem *mem = pawint_vptradd( list, pos ), *tmp = NULL;
	/* Cannot release the page/pageset from here */
	if ( !pos )
		return;
	/* Remove this from linked list of taken blocks */
	tmp = pawint_vptradd( mem->taken.prev );
	tmp->taken.next = mem->taken.next;
	tmp = pawint_vptradd( mem->taken.prev );
	tmp->taken.prev = mem->taken.prev;
	/* Let the previous block absorb this one for simplicity, doesn't matter
	 * if it was allocated or not */
	if ( mem->block.prev == 0 )
	{
		mem->empty.prev = list->prev;
		if ( list->empty.prev )
		{
			tmp = pawint_vptradd( list, list->empty.prev );
			tmp->empty.next = pos;
		}
		list->empty.prev = pos;
		return;
	}
	if ( mem->block.next )
	{
		tmp = pawint_vptradd( list, mem->block->next );
		tmp->block.prev = mem->block.prev;
	}
	tmp = pawint_vptradd( list, mem->block->prev );
	tmp->block.next = mem->block.next;
	if ( tmp.block.size )
	{
		tmp->block.size = mem->next - mem->prev;
		return;
	}
}

pawvu sysmem_new( sysmem *list, pawvu want )
{
	sysmem *mem = list;
	pawvu max = list->prev / sizeof(sysmem);
	pawvu pos = 0, len = list->used / sizeof(sysmem);
	while ( mem->empty.next )
	{
		pos = mem->empty.next;
		mem = pawint_vptradd( list, pos );
		if ( mem->block.next - pos >= want )
			return sysmem_take( list, pos, want );
	}
	if ( len < max )
	{
		pos = sizeof(sysmem) * len;
		mem = pawint_vptradd( list, len );
		return sysmem_take( list, pos, want );
	}
	return 0;
}
