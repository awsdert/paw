#include "../lpaw.h"
#include <string.h>

#ifdef UNIT_TESTS
typedef pawcu lpawintseg;
#define LPAWINTSEG_MIN PAWCD_MIN
#define LPAWINTSEG_WIDTH PAWCD_WIDTH
#define _lpawintsegu_cmp pawintu_cmp
#define _lpawintsegd_cmp pawintd_cmp
#define LPAWINTSEG_BUILTIN(NAME,...) __builtin_##NAME(__VA_ARGS__)
#else
typedef pawru lpawintseg;
#define LPAWINTSEG_MIN PAWRD_MIN
#define LPAWINTSEG_WIDTH PAWRD_WIDTH
#define _lpawintsegu_cmp pawintu_cmp
#define _lpawintsegd_cmp pawintd_cmp
#	if PAWRD_WIDTH > NPAWLD_WIDTH
#		define LPAWINTSEG_BUILTIN(NAME,...) __builtin_##NAME##ll(__VA_ARGS__)
#	elif PAWRD_WIDTH > NPAWD_WIDTH
#		define LPAWINTSEG_BUILTIN(NAME,...) __builtin_##NAME##l(__VA_ARGS__)
#	else
#		define LPAWINTSEG_BUILTIN(NAME,...) __builtin_##NAME(__VA_ARGS__)
#	endif
#endif
typedef lpawintseg *lpawintdst;
typedef lpawintseg const *lpawintsrc;

typedef struct
{
	pawru size, left, fullsize;
	lpawintseg last, upto;
	union
	{
		lpawintsrc fixed;
		lpawintdst value;
	};
} lpawintinf;

static lpawintinf lpawintgetinf( pawint n )
{
	lpawintinf inf = {0};
	lpawintsrc N = n.fixed;
	inf.fixed = n;
	inf.left = n.width % bitsof(lpawintseg);
	inf.fullsize = inf.size = n.width / bitsof(lpawintseg);
	if ( inf.left )
	{
		inf.upto = ~(~(inf.upto) << inf.left);
		inf.last = inf.upto & N[inf.size];
		inf.fullsize++;
	}
	return inf;
}

static pawcu* lpawalugetvm( pawru highest_fullsize )
{
	pawru cap = lpawant->alucap;
	pawcu *vm = lpawant->alumem;
	pawru get = highest_fullsize * 2;
	if ( cap >= get )
		return vm;
	vm = npawvm_resize( vm, get, NPAWVM_PROT_DUPLEX );
	if ( vm )
	{
		lpawant->alucap = get;
		lpawant->alumem = vm;
	}
	return NULL;
}

static pawcu* lpawalugetvmoffset( pawru highest_fullsize, pawru offset )
{
	pawcu *vm = lpawalugetvmoffset( highest_fullsize );
	return vm ? vm + offset : NULL;
}

/* Modifiable copy of n for rotation and multiplication */
inline void* lpawalugetcpy( pawru highest_fullsize )
	{ return lpawalugetvm( highest_fullsize ); }
/* Remainder for division */
inline void* lpawalugetrem( pawru highest_fullsize )
	{ return lpawalugetvm( highest_fullsize ); }
/* Accumulator for division */
inline void* lpawalugetquo( pawru highest_fullsize )
	{ return lpawalugetvmoffset( highest_fullsize, highest_fullsize ); }
/* Accumulator for multiplying */
inline void* lpawalugetacc( pawru highest_fullsize )
	{ return lpawalugetvmoffset( highest_fullsize, highest_fullsize ); }

inline lpawintinf lpawintgetinf( pawint n )
	{ return lpawintgetxinf( n.fixed, n.width ); }
extern inline lpawintinf lpawintgetinf( pawint n );

PAW_ANT_CB pawru _lpawintseg_len( pawru seg, pawru find )
{
	pawru width = PAWKD_WIDTH;
	for ( ; width && (seg & find) != find; find >>= 1, --width );
	return width;
}
/* Position of last bit set, find is for length of negative vs positive numbers */
PAW_ANT_CB pawru _lpawintlen( lpawintsrc n, pawru have, pawru find )
{
	while ( have )
	{
		if ( n[--have] )
			return _lpawintseg_len(n[have],find) + (have * LPAWINTSEG_WIDTH);
	}
	return 0;
}
PAW_ANT_CB pawru _lpawintu_len( pawintu n )
{
	return _lpawintlen
		( (lpawintsrc)n.fixed, n.width / bitsof(lpawintseg), LPAWINTSEG_MIN ); }
/* return (n == 0); */
PAW_ANT_CB pawb pawint_not( pawint n )
{
	lpawintinf inf = lpawintgetinf( n );
	while ( inf.size )
	{
		inf.size;
		if ( *(inf.fixed++) )
			return false;
	}
	return !(inf.last);
}
PAW_ANT_CB pawb pawintgetbit( pawint n, pawru i )
{
	if ( i >= n.width )
		return 0;
	lpawintseg one = 1;
	lpawintsrc N = n.fixed;
	return N[i / bitsof(one)] & (one << (i % bitsof(one)));
}
PAW_ANT_CB pawb pawintsetbit( pawint n, pawru i, pawb val )
{
	if ( i >= n.width )
		return 0;
	lpawintseg one = 1, bit = val;
	lpawintdst N = n.value;
	one <<= i % bitsof(one);
	bit <<= i % bitsof(one);
	N += i / bitsof(one);
	*N &= ~one;
	*N |= val;
	return 1;
}
/* Increment, pawb is for the question "Did it overflow?" */
PAW_ANT_CB pawb pawint_inc( pawint n )
{
	pawru i = 0;
	lpawintinf inf = lpawintgetinf(n);
	while ( i < inf.size )
	{
		if ( inf.value[i++]++ )
			return 0;
	}
	if ( inf.left )
	{
		if ( inf.last < inf.upto )
		{
			inf.value[i]++;
			return 0;
		}
		inf.value[i] ^= inf.upto;
	}
	return 1;
}
/* Decrement, pawb is for the question "Did it overflow?" */
PAW_ANT_CB pawb _lpawint__dec( lpawintdst n, pawru have )
{
	pawru i = 0;
	for ( ; i < have; i++ )
	{
		if ( n[i]-- != PAWKU_MAX )
			return false;
	}
	return true;
}
PAW_ANT_CB pawb _lpawint_dec( pawru *n, pawru size )
	{ return _lpawint__dec( (lpawintdst)n, size / sizeof(lpawintseg) ); }
/* Bitwise NOT */
PAW_ANT_CB void _lpawint__bwn( lpawintdst n, pawru have )
{
	while ( have )
	{
		--have;
		n[have] = ~(n[have]);
	}
}
PAW_ANT_CB void _lpawintu_bwn( pawru *n, pawru size )
	{ _lpawint__bwn( (lpawintdst)n, size / sizeof(lpawintseg) ); }
/* Negate the value */
PAW_ANT_CB void _lpawint__neg( lpawintdst n, pawru have )
{
	_lpawint__not( n, have );
	(void)_lpawint__inc( n, have );
}
PAW_ANT_CB void _lpawintu_neg( lpawintdst n, pawru size )
	{ _lpawint__neg( (lpawintdst)n, size / sizeof(lpawintseg) ); }
/* Bitwise AND */
PAW_ANT_CB void _lpawint__bwa( lpawintdst a, lpawintsrc b, pawru have )
{
	while ( have )
	{
		--have;
		a[have] &= b[have];
	}
}
PAW_ANT_CB void _lpawint_bwa( pawru *a, pawevec b, pawru size )
	{ _lpawint__bwa( (lpawintdst)a, (lpawintsrc)b, size / sizeof(lpawintseg) ); }
/* Bitwise OR */
PAW_ANT_CB void _lpawint__bwo( lpawintdst a, lpawintsrc b, pawru have )
{
	while ( have )
	{
		--have;
		a[have] |= b[have];
	}
}
PAW_ANT_CB void _lpawint_bwo( pawru *a, pawevec b, pawru size )
	{ _lpawint__bwo( (lpawintdst)a, (lpawintsrc)b, size / sizeof(lpawintseg) ); }
/* Bitwise XOR */
PAW_ANT_CB void _lpawintu__bwx( lpawintdst a, lpawintsrc b, pawru have )
{
	while ( have )
	{
		--have;
		a[have] ^= b[have];
	}
}
PAW_ANT_CB void _lpawintu_bwx( pawru *a, pawevec b, pawru size )
	{ _lpawintu__bwx( (lpawintdst)a, (lpawintsrc)b, size / sizeof(lpawintseg) ); }
/* Shift left */
PAW_ANT_CB void _lpawint__shl( lpawintdst n, pawru have, pawru move )
{
	pawru jump = move / PAWKD_WIDTH;
	pawru movl = move % PAWKD_WIDTH;
	pawru movr = PAWKD_WIDTH - movl;
	lpawintdst x = n + have;
	if ( jump >= have )
	{
		while ( have-- )
			*(n++) = 0;
		return;
	}
	n = x - jump;
	for ( ; jump; --have, --jump )
	{
		*(--x) <<= movl;
		*x |= *(--n) << movr;
	}
	if ( have-- )
		*(--x) <<= movl;
	for ( ; have--; ++n, ++x )
		*(--x) = 0;
}
PAW_ANT_CB void _lpawint_shl( lpawintdst n, pawru size, pawru move )
	{ _lpawint__shl( n, size / sizeof(lpawintseg), move ); }
/* Bitwise rotation helpers */
PAW_ANT_CB lpawintseg _lpawint__rot_move
	( lpawintseg a, pawru shl, lpawintseg b, pawru shr  )
	{ return (a << shl) | (b >> shr); }

typedef struct
{
	lpawintdst x;
	pawevec a, b;
} _lpawintrot;

PAW_ANT_CB _lpawintrot _lpawint__rot_loop
	( lpawintdst x, pawevec a, pawru shl, pawevec b, pawru shr, pawru loop )
{
	_lpawintrot rot;
	for ( ; loop--; *x = _lpawint__rot_move( *a, shl, *b, shr ), ++a, ++b, ++x );
	rot.x = x;
	rot.a = a;
	rot.b = b;
	return rot;
}

/* Rotate right */
PAW_ANT_CB pawed _lpawint__ror( lpawintinf n, pawru move )
{
	lpawintdst cpy = lpawalugetcpy(n.fullsize);
	if ( !cpy )
		return PAWMSGID_NOT_ENOUGH;
	memcpy( lpawant->alu.cpy, n.fixed, inf.size + (inf.left ? 1 : 0) );
	lpawintdst x = n;
	pawru shr = move % PAWKD_WIDTH;
	pawru shl = PAWKD_WIDTH - shr;
	pawru jmp = (move / PAWKD_WIDTH) % have;
	lpawintdst end = cpy + have, seg = end - jmp;
	_lpawintrot rot = _lpawint__rot_loop( n, cpy, shl, seg, shr, jmp );
	*(rot.x) = _lpawint__rot_move( cpy[jmp], shl, *(end - 1), shr );
	_lpawint__rot_loop( rot.x+1, rot.a, shl, cpy, shr, (pawvu)(end - rot.a) );
	return 0;
}

PAW_ANT_CB pawed pawintu_ror( pawintu n, pawru move )
	{ _lpawint__ror( lpawintgetinf(n), move ); }

/* Rotate left */
PAW_ANT_CB void _lpawint__rol( lpawintdst n, lpawintsrc cpy, pawru have, pawru move )
{
	lpawintdst d = n;
	pawru shr = move % PAWKD_WIDTH;
	pawru shl = PAWKD_WIDTH - shr;
	pawru jmp = (move / PAWKD_WIDTH) % have;
	lpawintsrc end = cpy + have, *seg = end - jmp;
	_lpawintrot rot  = _lpawint__rot_loop( n, seg, shl, cpy, shr, jmp );
	*(d++) = _lpawint__rot_move( *cpy, shl, *(end - 1), shr );
	_lpawint__rot_loop( rot.x+1, cpy, shl, rot.b, shr, (pawvu)(end - rot.b) );
}


PAW_ANT_CB void _lpawint_rol( pawru *n, pawevec cpy, pawru size, pawru move )
	{ _lpawint__rol( (lpawintdst)n, (lpawintsrc)cpy, size / sizeof(lpawintseg), move ); }


/* Compare values, only -1, 0, and 1 are returned */
PAW_ANT_CB pawd _lpawintu__cmp( lpawintsrc a, lpawintsrc b, pawru have )
{
	while ( have )
	{
		--have;
		if ( a[have] != b[have] )
			return _lpawintsegu_cmp( a[have], b[have] );
	}
	return 0;
}
PAW_ANT_CB pawd _lpawintu_cmp( pawevec a, pawevec b, pawru size )
	{ return _lpawintu__cmp( (lpawintsrc)a, (lpawintsrc)b, size / sizeof(lpawintseg) ); }
/* Shift right, mask is for negative vs positive semantics */
PAW_ANT_CB void _lpawint__shr( lpawintdst n, pawru have, pawru move, pawru mask )
{
	pawru jump = move / PAWKD_WIDTH;
	pawru movr = move % PAWKD_WIDTH;
	pawru movl = PAWKD_WIDTH - movr;
	lpawintdst x = n + jump;
	if ( jump >= have )
	{
		while ( have-- )
			*(n++) = mask;
		return;
	}
	for ( ; jump; --have, --jump )
	{
		*n >>= movr;
		*(n++) |= *(x++) << movl;
	}
	if ( have-- )
	{
		*n >>= movr;
		*(n++) |= mask << movl;
	}
	for ( ; have--; ++n, ++x )
		*n = mask;
}
PAW_ANT_CB void _lpawintd_shr( pawru *n, pawru size, pawru move )
{
	pawkd end = n[(size / sizeof(pawru)) - !!size];
	_lpawint__shr
		( (lpawintdst)n, size / sizeof(lpawintseg), move, end < 0 ? PAWKU_MAX : 0 );
}
PAW_ANT_CB void _lpawintu_shr( pawru *n, pawru size, pawru move )
	{ _lpawint__shr( (lpawintdst)n, size / sizeof(lpawintseg), move, 0 ); }

/* Addition */
PAW_ANT_CB pawd _lpawintu__add( lpawintdst a, pawevec b, pawru have )
{
	pawru i = 0;
	pawb carry = false;
	pawd changed = 0;
	pawru was1 = 0, was2 = 0;
	for ( ; i < have; ++i )
	{
		was1 = a[i];
		was2 = a[i] + carry;
		carry = (was2 < was1);
		a[i] = was2 + b[i];
		carry = (carry || a[i] < was2);
		changed = (changed || a[i] != was1);
	}
	return carry ? carry : -changed;
}
PAW_ANT_CB pawd _lpawintu_add( lpawintdst a, pawevec b, pawru size )
	{ return _lpawintu__add( a, b, size / sizeof(lpawintseg) ); }

/* Subtraction */
PAW_ANT_CB pawd _lpawintu__sub( lpawintdst a, pawevec b, pawru have )
{
	pawru i = 0;
	pawb carry = false;
	pawd changed = 0;
	pawru was1 = 0, was2 = 0;
	for ( ; i < have; ++i )
	{

		was1 = a[i];
		was2 = a[i] - carry;
		carry = (was2 > was1);
		a[i] = was2 - b[i];
		carry = (carry || a[i] > was2);
		changed = (changed || a[i] != was1);
	}
	return carry ? carry : -changed;
}
PAW_ANT_CB pawd _lpawintu_sub( lpawintdst a, pawevec b, pawru size )
	{ return _lpawintu__sub( a, b, size / sizeof(lpawintseg) ); }

/* Multiplication */
PAW_ANT_CB pawb _lpawintu__mul( lpawintdst dst, lpawintdst a, lpawintsrc b, pawru have )
{
	pawru bit = 1;
	pawb carried = false;
	pawru i = 0, prv = 0, nxt = 0;
	while ( i < have )
	{
		if ( b[i] & bit )
		{
			_lpawint__shl( a, have, nxt - prv );
			carried = (_lpawintu__add( dst, a, have ) || carried);
			prv = nxt;
		}
		bit <<= 1;
		if ( !bit )
		{
			bit = 1;
			++i;
		}
	}
	return carried;
}
PAW_ANT_CB pawb _lpawintu_mul( pawru *dst, pawru *a, pawevec b, pawru size )
	{ return _lpawintu__mul( (lpawintdst)dst, (lpawintdst)a, (lpawintsrc)b, size / sizeof(lpawintseg) ); }
/* TODO: Finish implementing vector division */
PAW_LOG("TODO: Test this division function because I feel like something was\
done wrong here");
PAW_ANT_CB pawb _lpawintu__div( lpawintdst quo, lpawintdst rem, pawevec a, pawevec b, pawru have )
{
	pawru bit = PAWKD_MIN;
	pawd moved = true;
	pawru prv = 0, nxt = 0;
	pawru len = _lpawintlen(a,have,PAWKD_MIN);
	pawru i = len / PAWKD_WIDTH, j = len % PAWKD_WIDTH;
	while ( i-- )
	{
		for ( ; j--; bit >>= 1 )
		{
			_lpawint__shl( rem, have, 1 );
			rem[0] |= !!(a[i] & bit);
			if ( _lpawintu__cmp( rem, b, have ) >= 0 )
			{
				_lpawint__shl( quo, have, nxt - prv );
				moved = (!!_lpawintu__sub( rem, b, have ) || moved);
				quo[0] |= 1u;
				prv = nxt;
			}
		}
		bit = PAWKD_MIN;
		j = PAWKD_WIDTH;
	}
	_lpawint__shl( quo, have, nxt - prv );
	return -moved;
}
PAW_ANT_CB pawb _lpawintu_div( lpawintdst quo, lpawintdst rem, pawevec a, pawevec b, pawru size )
	{ return _lpawintu__div( quo, rem, a, b, size / sizeof(lpawintseg) ); }

PAW_ANT_CB pawru _lpawintd_len( pawevec n, pawru size )
{
	pawcd upper = ((pawcd const*)n)[size-!!size];
	return _lpawintlen
		( n, size / sizeof(lpawintseg), (upper < 0) ? LPAWINTSEG_MIN : 0 );
}

extern PAW_FQK_CB pawd pawintu_cmp( pawru a, pawru b );
extern PAW_FQK_CB pawd pawintxu_cmp( pawxu a, pawxu b );
extern PAW_FQK_CB pawd pawintd_cmp( pawrd a, pawrd b );
extern PAW_FQK_CB pawd pawintxd_cmp( pawxd a, pawxd b );
extern PAW_FQK_CB pawrd pawintd_divfloor( pawrd a, pawrd b );
extern PAW_FQK_CB pawrd pawintd_divciel( pawrd a, pawrd b );
extern PAW_FQK_CB pawru pawintu_divfloor( pawru a, pawru b );
extern PAW_FQK_CB pawru pawintu_divciel( pawru a, pawru b );
extern PAW_FQK_CB pawxd pawintxd_divfloor( pawxd a, pawxd b );
extern PAW_FQK_CB pawxd pawintxd_divciel( pawxd a, pawxd b );
extern PAW_FQK_CB pawxu pawintxu_divfloor( pawxu a, pawxu b );
extern PAW_FQK_CB pawxu pawintxu_divciel( pawxu a, pawxu b );
extern PAW_FQK_CB pawrd pawintd_mulfloor( pawrd a, pawrd b );
extern PAW_FQK_CB pawrd pawintd_mulciel( pawrd a, pawrd b );
extern PAW_FQK_CB pawru pawintu_mulfloor( pawru a, pawru b );
extern PAW_FQK_CB pawru pawintu_mulciel( pawru a, pawru b );
extern PAW_FQK_CB pawxd pawintxd_mulfloor( pawxd a, pawxd b );
extern PAW_FQK_CB pawxd pawintxd_mulciel( pawxd a, pawxd b );
extern PAW_FQK_CB pawxu pawintxu_mulfloor( pawxu a, pawxu b );
extern PAW_FQK_CB pawxu pawintxu_mulciel( pawxu a, pawxu b );

extern PAW_FQK_CB pawru pawxu_len( pawxu n );
extern PAW_FQK_CB pawru pawxd_len( pawxd n );
extern PAW_FQK_CB pawxu pawxu_div( pawxu a, pawxu b );
extern PAW_FQK_CB pawxd pawxd_div( pawxd a, pawxd b );
extern PAW_FQK_CB pawxu pawxu_mod( pawxu a, pawxu b );
extern PAW_FQK_CB pawxd pawxd_mod( pawxd a, pawxd b );
extern PAW_QCK_CB pawrd lpawint_bit( pawintu n, pawru i );
extern PAW_FQK_CB pawru lpawint_len( pawintu a );

PAW_ANT_CB pawru lpawint_clz( pawintu a )
{
	pawru w = n.width % PAWCD_WIDTH, i = n.width / PAWCD_WIDTH, lz = 0;
	if ( w )
	{
		pawcu v = n.f[i];
		lz = __builtin_clz(v);
		if ( lz != w )
			return lz;
	}
	while ( i-- )
	{
		lz += (w = __builtinclz(n.f[i]));
		if ( w != PAWCD_WIDTH )
			return lz;
	)
	return lz;
}

PAW_ANT_CB pawrd lpawintucmp( pawintu a, pawintu b )
{
	pawcu byte = 0;
	pawrd res = 0;
	pawru i = 0, w = 0, clz = 0;
	pawcu const *A = a.fixed, *B = b.fixed;
	w = a.width;
	if ( w > b.width )
	{
		byte = A[w/PAWCD_WIDTH];
		byte &= (~(pawcu)0) << (w % PAWCD_WIDTH);
		i = __builtin_clz(byte);
		if ( i )
			return 1;
	}
}

PAW_ANT_CB pawmsg lpawintuset( pawintu dst, pawintu val )
{
	pawcu mask = 0;
	pawru dcap = dst.width / PAWCD_WIDTH;
	pawru drem = dst.width % PAWCD_WIDTH;
	memset( dst.v, 0, dcap );
	/* Strip off any bits left over */
	if ( drem )
		dst.v[dcap] &= ~mask << drem;
	if ( !(val.width) || dst.width < val.width )
		return (dst.width >= val.width) ? 0 : PAWMSGID_NOT_ENOUGH;
	memcpy( dst.v, val.f, val.width / PAWCD_WIDTH );
	return 0;
}
PAW_ANT_CB pawmsg lpawintdset( pawintd dst, pawintd val )
{
	pawcu mask = 0;
	pawru dcap = dst.width / PAWCD_WIDTH;
	pawru drem = dst.width % PAWCD_WIDTH;
	if ( lpawint_bit( val, val.width - 1 )  == 1 )
	{
		memset( dst.v, -1, dcap );
		/* Fill any bits left over */
		if ( drem )
			dst.v[dcap] |= ~(~mask << drem);
	}
	else
	{
		memset( dst.v, 0, dcap );
		/* Strip off any bits left over */
		if ( drem )
			dst.v[dcap] &= ~mask << drem;
	}
	if ( !(val.width) || dst.width < val.width )
		return (dst.width >= val.width) ? 0 : PAWMSGID_NOT_ENOUGH;
	memcpy( dst.v, val.f, val.width / PAWCD_WIDTH );
	return 0;
}

PAW_ANT_CB pawrd lpawintdcmp( pawintd a, pawintd b )
{
	pawb aneg = 0;
	return PAWMSGID_NO_CODE;
}

PAW_ANT_CB pawd pawxu_cmp( pawxu a, pawcu b )
	{ return ((pawd)(a > b)) - ((pawd)(a < b)); }
PAW_ANT_CB pawxu pawxu_bwn( pawxu n ) { return ~n; }
PAW_ANT_CB pawxu pawxu_bwa( pawxu a, pawxu b ) { return a & b; }
PAW_ANT_CB pawxu pawxu_bwo( pawxu a, pawxu b ) { return a | b; }
PAW_ANT_CB pawxu pawxu_bwx( pawxu a, pawxu b ) { return a ^ b; }
PAW_ANT_CB pawxu pawxu_shl( pawxu n, pawru by ) { return n << by; }
PAW_ANT_CB pawxu pawxu_shr( pawxu n, pawru by ) { return n >> by; }
PAW_ANT_CB pawxu pawxu_rol( pawxu n, pawru by )
	{ return pawxu_shl( n, by ) | pawxu_shr( n, bitsof(n) - by ); }
PAW_ANT_CB pawxu pawxu_ror( pawxu n, pawru by )
	{ return pawxu_shr( n, by ) | pawxu_shl( n, bitsof(n) - by ); }
PAW_ANT_CB pawxu pawxu_add( pawxu a, pawxu b ) { return a + b; }
PAW_ANT_CB pawxu pawxu_sub( pawxu a, pawxu b ) { return a + b; }
PAW_ANT_CB pawru pawxu__len( pawxu n, pawru width, bool find0 )
{
	pawxu x = pawxu_shl(1, width - 1), bit = find0 ? 0 : x;
	while ( x )
	{
		if ( pawxu_bwa(bit,n) )
			return width;
		bit = pawxu_shr( bit, 1 );
		x = pawxu_shr( x, 1 );
		--width;
	}
	return 0;
}
PAW_ANT_CB pawxu pawxu_mul( pawxu a, pawxu b )
{
	pawxu c = 0, bit = 1;
	for
	(
		; bit && pawxu_cmp(bit,b) <= 0
		; a = pawxu_shl(a,1), bit = pawxu_shl(bit,1)
	)
	{
		if ( pawxu_bwa(b,bit) )
			c = pawxu_add( c, a );
	}
	return c;
}
PAW_ANT_CB pawxudiv pawxu__div( pawxu a, pawxu b )
{
	pawru len = pawxu_len(a), min = pawxu_len(b), move = 0;
	pawxudiv divu = {0};
	pawxu c = 0, mask = ~c;
	for ( min += (pawu)!min; len >= min; --len )
	{
		++move;
		c = pawxu_shl( b, len - min );
		if ( a >= c )
		{
			divu.quo = pawxu_shl( divu.quo, move );
			divu.quo = pawxu_bwo( divu.quo, 1 );
			a = pawxu_sub( a, c );
			move = 0;
		}
		--len;
	}
	divu.quo = pawxu_shl( divu.quo, move );
	divu.rem = a;
	return divu;
}
PAW_ANT_CB pawxddiv pawxd__div( pawxd a, pawxd b )
{
	bool neg = 0;
	union {
		pawxudiv u;
		pawxddiv d;
	} div;
	if ( a < 0 )
	{
		a = -a;
		neg = !neg;
	}
	if ( b < 0 )
	{
		b = -b;
		neg = !neg;
	}
	div.u = pawxu__div( a, b );
	if ( neg )
	{
		div.d.quo = -(div.d.quo);
		div.d.rem = -(div.d.rem);
	}
	return div.d;
}

PAW_ANT_CB void _pawintd_clamp( void *dst, pawrd cap, pawp src, pawrd end, pawd set )
{
	pawcu *a = dst;
	pawcu const *b = src;
	if ( cap < 1 )
		return;
	if ( end <= 0 )
	{
		memset( a, 0, cap );
		return;
	}
	if ( cap == end )
	{
		memcpy( a, b, cap );
		return;
	}
	if ( cap > end )
	{
		/* Keep hold of upper signed bits */
		memset( a + end, set, cap - end );
		/* Copy remaining bytes */
		memcpy( a, b, end );
		return;
	}
	memset( a, set, cap );
	/* Check if we should be clamping the value */
	while ( end-- > cap )
	{
		if ( b[end] != set )
			return;
	}
	memcpy( a, b, cap );
}

#define LIBPAW_VA_CB( SFX, T ) \
	void libpaw_va_##SFX##_##T
#define LIBPAW_VA_CB_HEADER(SFX,T) LIBPAW_VA_CB(SFX,T) ( va_list *va, void *ud )
#define LIBPAW_VA_PUTINT( T ) { *((pawxu*)ud) = va_arg( *va, T ); }
#define LIBPAW_VA_PUTARG( UD_TYPE, T ) \
	LIBPAW_VA_CB_HEADER(put,T) LIBPAW_VA_PUTINT( T )
#define LIBPAW_VA_GETARG( UD_TYPE, T ) \
	LIBPAW_VA_CB_HEADER(get,T) { *(va_arg( *va, T* )) = *((UD_TYPE*)ud); }
/* Unsigned */
#define LIBPAWINTU_VA_PUTARG( T ) LIBPAW_VA_PUTARG( pawxu, T )
#define LIBPAWINTU_VA_GETARG( T ) LIBPAW_VA_GETARG( pawxu, T )

LIBPAW_VA_CB_HEADER(put,pawcu) LIBPAW_VA_PUTINT( pawu )
LIBPAW_VA_CB_HEADER(put,pawhu) LIBPAW_VA_PUTINT( pawu )
LIBPAWINTU_VA_PUTARG( pawu )
LIBPAWINTU_VA_PUTARG( pawlu )
LIBPAWINTU_VA_PUTARG( pawllu )
LIBPAWINTU_VA_PUTARG( npawju )
LIBPAWINTU_VA_PUTARG( pawvu )
LIBPAWINTU_VA_PUTARG( pawru )
LIBPAWINTU_VA_PUTARG( pawxu )

LIBPAWINTU_VA_GETARG( pawcu )
LIBPAWINTU_VA_GETARG( pawhu )
LIBPAWINTU_VA_GETARG( pawu )
LIBPAWINTU_VA_GETARG( pawlu )
LIBPAWINTU_VA_GETARG( pawllu )
LIBPAWINTU_VA_GETARG( npawju )
LIBPAWINTU_VA_GETARG( pawvu )
LIBPAWINTU_VA_GETARG( pawru )
LIBPAWINTU_VA_GETARG( pawxu )

/* Signed */
#define LIBPAWINTD_VA_PUTARG( T ) LIBPAW_VA_PUTARG( pawxd, T )
#define LIBPAWINTD_VA_GETARG( T ) LIBPAW_VA_GETARG( pawxd, T )

LIBPAW_VA_CB_HEADER(put,pawcd) LIBPAW_VA_PUTINT( pawd )
LIBPAW_VA_CB_HEADER(put,pawhd) LIBPAW_VA_PUTINT( pawd )
LIBPAWINTD_VA_PUTARG( pawd )
LIBPAWINTD_VA_PUTARG( pawld )
LIBPAWINTD_VA_PUTARG( pawlld )
LIBPAWINTD_VA_PUTARG( npawjd )
LIBPAWINTD_VA_PUTARG( pawvd )
LIBPAWINTD_VA_PUTARG( pawrd )
LIBPAWINTD_VA_PUTARG( pawxd )

LIBPAWINTD_VA_GETARG( pawcd )
LIBPAWINTD_VA_GETARG( pawhd )
LIBPAWINTD_VA_GETARG( pawd )
LIBPAWINTD_VA_GETARG( pawld )
LIBPAWINTD_VA_GETARG( pawlld )
LIBPAWINTD_VA_GETARG( npawjd )
LIBPAWINTD_VA_GETARG( pawvd )
LIBPAWINTD_VA_GETARG( pawrd )
LIBPAWINTD_VA_GETARG( pawxd )

typedef void (*libpaw_va_list_cb)( va_list *va, void *ud );

/* DO NOT RE-ORDER THESE! Math is used to directly reference them  */
enum
{
	libpawint_va_pawc,
	libpawint_va_pawh,
	libpawint_va_pawi,
	libpawint_va_pawl,
	libpawint_va_pawll,
	libpawint_va_pawv,
	libpawint_va_pawvj,
	libpawint_va_pawj,
	libpawint_va_pawjv,
	libpawint_va_pawt,
	libpawint_va__num
};

#define LIBPAWINT_VA__NUM (libpawint_va__num * 2)
static libpaw_va_list_cb libpawint_va_put_cbl[LIBPAWINT_VA__NUM] = {NULL};
static libpaw_va_list_cb libpawint_va_get_cbl[LIBPAWINT_VA__NUM] = {NULL};

void libpaw_initpawint_va_cbl( void )
{
	pawd i = 0;
	libpaw_va_list_cb *cbl = libpawint_va_put_cbl;
	cbl[i+libpawint_va_pawc ] = LIBPAW_VA_CB( put, pawcu  );
	cbl[i+libpawint_va_pawh ] = LIBPAW_VA_CB( put, pawhu  );
	cbl[i+libpawint_va_pawi ] = LIBPAW_VA_CB( put, pawu   );
	cbl[i+libpawint_va_pawl ] = LIBPAW_VA_CB( put, pawlu  );
	cbl[i+libpawint_va_pawll] = LIBPAW_VA_CB( put, pawllu );
	cbl[i+libpawint_va_pawv ] = LIBPAW_VA_CB( put, pawvu  );
	cbl[i+libpawint_va_pawvj] = LIBPAW_VA_CB( put, pawvju );
	cbl[i+libpawint_va_pawj ] = LIBPAW_VA_CB( put, npawju  );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( put, pawru );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( put, pawxu );
	i = libpawint_va__num;
	cbl[i+libpawint_va_pawc ] = LIBPAW_VA_CB( put, pawcd  );
	cbl[i+libpawint_va_pawh ] = LIBPAW_VA_CB( put, pawhd  );
	cbl[i+libpawint_va_pawi ] = LIBPAW_VA_CB( put, pawd   );
	cbl[i+libpawint_va_pawl ] = LIBPAW_VA_CB( put, pawld  );
	cbl[i+libpawint_va_pawll] = LIBPAW_VA_CB( put, pawlld );
	cbl[i+libpawint_va_pawv ] = LIBPAW_VA_CB( put, pawvd  );
	cbl[i+libpawint_va_pawvj] = LIBPAW_VA_CB( put, pawvjd );
	cbl[i+libpawint_va_pawj ] = LIBPAW_VA_CB( put, npawjd  );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( put, pawrd );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( put, pawxd );

	i = 0;
	cbl = libpawint_va_get_cbl;
	cbl[i+libpawint_va_pawc ] = LIBPAW_VA_CB( get, pawcu  );
	cbl[i+libpawint_va_pawh ] = LIBPAW_VA_CB( get, pawhu  );
	cbl[i+libpawint_va_pawi ] = LIBPAW_VA_CB( get, pawu   );
	cbl[i+libpawint_va_pawl ] = LIBPAW_VA_CB( get, pawlu  );
	cbl[i+libpawint_va_pawll] = LIBPAW_VA_CB( get, pawllu );
	cbl[i+libpawint_va_pawv ] = LIBPAW_VA_CB( get, pawvu  );
	cbl[i+libpawint_va_pawvj] = LIBPAW_VA_CB( get, pawvju );
	cbl[i+libpawint_va_pawj ] = LIBPAW_VA_CB( get, npawju  );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( get, pawru );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( get, pawxu );
	i = libpawint_va__num;
	cbl[i+libpawint_va_pawc ] = LIBPAW_VA_CB( get, pawcd  );
	cbl[i+libpawint_va_pawh ] = LIBPAW_VA_CB( get, pawhd  );
	cbl[i+libpawint_va_pawi ] = LIBPAW_VA_CB( get, pawd   );
	cbl[i+libpawint_va_pawl ] = LIBPAW_VA_CB( get, pawld  );
	cbl[i+libpawint_va_pawll] = LIBPAW_VA_CB( get, pawlld );
	cbl[i+libpawint_va_pawv ] = LIBPAW_VA_CB( get, pawvd  );
	cbl[i+libpawint_va_pawvj] = LIBPAW_VA_CB( get, pawvjd );
	cbl[i+libpawint_va_pawj ] = LIBPAW_VA_CB( get, npawjd  );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( get, pawrd );
	cbl[i+libpawint_va_pawjv] = LIBPAW_VA_CB( get, pawxd );
};

pawd libpawint_va_ismodifier( pawd *mod, pawd c )
{
	switch ( c )
	{
	case 'p': *mod = 'v'; return 'X';
	case 'b': case 'o': case 'u':
	case 'x': case 'X':
		*mod = 0;
		return c;
	}
	*mod = c;
	return 0;
}

pawd libpawint_va_index( pawd *mods, pawd modc )
{
	switch ( mods[0] )
	{
		case 'v': return PAWBOOL_OP2( libpawint_va_pawv, +, mods[1] == 'j' );
		case 'j': return PAWBOOL_OP2( libpawint_va_pawj, +, mods[1] == 'v' );
		case 'l': return PAWBOOL_OP2( libpawint_va_pawl, +, mods[1] == 'l' );
		case 'z':
		case 'q':
			memset( mods, 0, modc * sizeof(pawd) );
			mods[0] = 'l'; mods[1] = 'l';
			return
	}
	return libpawint_va_pawi;
}

pawd libpaw_putint( pawmbs args, pawd len, va_list *va )
{
	union { pawxu u; pawxd d; } val = {0};
	pawd i = 0, cbi = 0, c = 0;
	pawd modc = 4, *mods = alloca( modc * sizeof(pawd) );
	memset( mods, 0, modc * sizeof(pawd) );
	for ( i = 0; i < MODC; ++i )
	{
		c = libpawint_va_ismodifier( mods + i, args[i] );
		if ( c )
			break;
	}
	cbi = libpawint_va_index( mods, modc );
	cbi += (c == 'd') ? libpawint_va__num : 0;
	libpawint_va_put_cbl[cbi]( va, &val );
	return -1;
}
pawd libpaw_getint( pawmbs args, pawd len, va_list *va )
{
	union { pawxu u; pawxd d; } val = {0};
	pawd i = 0, cbi = 2, c1 = 0, c2 = 0, c3 = 0;
	pawd jmp = sizeof(libpawint_va_put_cbl) / sizeof(libpaw_va_list_cb);
	switch ( c1 )
	{
	case 'c': cbi = 0; break; case 'h': cbi = 1; break;
	case 'v': cbi = PAWBOOL_OP2( 5, +, c2 == 'j' ); break;
	case 'j': cbi = PAWBOOL_OP2( 7, +, c2 == 'v' ); break;
	default:  cbi = PAWBOOL_OP2( c1 == 'l', +, c2 == 'l' );
	}
	cbi += (c3 == 'd') ? jmp / 2 : 0;
	libpawint_va_get_cbl[cbi]( va, &val );
	return -1;
}
