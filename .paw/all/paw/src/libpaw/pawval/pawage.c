#include "../lpaw.h"
extern PAW_FQK_CB pawru _pawageNSu_len( _pawageNSu n );
extern PAW_FQK_CB pawb  _pawageNSu_not( _pawageNSu n );
extern PAW_FQK_CB _pawageNSu _pawageNSu_bwn( _pawageNSu n );
extern PAW_FQK_CB _pawageNSu _pawageNSu_inc( _pawageNSu n );
extern PAW_FQK_CB _pawageNSu _pawageNSu_dec( _pawageNSu n );
extern PAW_FQK_CB _pawageNSu _pawageNSu_neg( _pawageNSu n );
extern PAW_FQK_CB _pawageNSu _pawageNSu_shl( _pawageNSu n, pawru move );
extern PAW_FQK_CB _pawageNSu _pawageNSu_shr( _pawageNSu n, pawru move );
extern PAW_FQK_CB _pawageNSu _pawageNSu_rol( _pawageNSu n, pawru move );
extern PAW_FQK_CB _pawageNSu _pawageNSu_ror( _pawageNSu n, pawru move );
extern PAW_FQK_CB pawd _pawageNSu_cmp( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_bwa( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_bwo( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_bwx( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_add( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_sub( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_mul( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSdivu _pawageNSu__div( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_div( _pawageNSu a, _pawageNSu b );
extern PAW_FQK_CB _pawageNSu _pawageNSu_mod( _pawageNSu a, _pawageNSu b );

extern PAW_FQK_CB pawru _pawageNSd_len( _pawageNSd n );
extern PAW_FQK_CB pawb  _pawageNSd_not( _pawageNSd n );
extern PAW_FQK_CB _pawageNSd _pawageNSd_bwn( _pawageNSd n );
extern PAW_FQK_CB _pawageNSd _pawageNSd_inc( _pawageNSd n );
extern PAW_FQK_CB _pawageNSd _pawageNSd_dec( _pawageNSd n );
extern PAW_FQK_CB _pawageNSd _pawageNSd_neg( _pawageNSd n );
extern PAW_FQK_CB _pawageNSd _pawageNSd_shl( _pawageNSd n, pawru move );
extern PAW_FQK_CB _pawageNSd _pawageNSd_shr( _pawageNSd n, pawru move );
extern PAW_FQK_CB _pawageNSd _pawageNSd_rol( _pawageNSd n, pawru move );
extern PAW_FQK_CB _pawageNSd _pawageNSd_ror( _pawageNSd n, pawru move );
extern PAW_FQK_CB pawd _pawageNSd_cmp( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_bwa( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_bwo( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_bwx( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_add( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_sub( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_mul( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSdivd _pawageNSd__div( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_div( _pawageNSd a, _pawageNSd b );
extern PAW_FQK_CB _pawageNSd _pawageNSd_mod( _pawageNSd a, _pawageNSd b );

extern PAW_FQK_CB pawru pawageNS_len( pawageNS n );
extern PAW_FQK_CB pawb  pawageNS_not( pawageNS n );
extern PAW_FQK_CB pawageNS pawageNS_bwn( pawageNS n );
extern PAW_FQK_CB pawageNS pawageNS_inc( pawageNS n );
extern PAW_FQK_CB pawageNS pawageNS_dec( pawageNS n );
extern PAW_FQK_CB pawageNS pawageNS_neg( pawageNS n );
extern PAW_FQK_CB pawageNS pawageNS_shl( pawageNS n, pawru move );
extern PAW_FQK_CB pawageNS pawageNS_shr( pawageNS n, pawru move );
extern PAW_FQK_CB pawageNS pawageNS_rol( pawageNS n, pawru move );
extern PAW_FQK_CB pawageNS pawageNS_ror( pawageNS n, pawru move );
extern PAW_FQK_CB pawd pawageNS_cmp( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_bwa( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_bwo( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_bwx( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_add( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_sub( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_mul( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNSdiv pawageNS__div( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_div( pawageNS a, pawageNS b );
extern PAW_FQK_CB pawageNS pawageNS_mod( pawageNS a, pawageNS b );

extern PAW_FQK_CB pawru _pawageQSu_len( _pawageQSu n );
extern PAW_FQK_CB pawb  _pawageQSu_not( _pawageQSu n );
extern PAW_FQK_CB _pawageQSu _pawageQSu_bwn( _pawageQSu n );
extern PAW_FQK_CB _pawageQSu _pawageQSu_inc( _pawageQSu n );
extern PAW_FQK_CB _pawageQSu _pawageQSu_dec( _pawageQSu n );
extern PAW_FQK_CB _pawageQSu _pawageQSu_neg( _pawageQSu n );
extern PAW_FQK_CB _pawageQSu _pawageQSu_shl( _pawageQSu n, pawru move );
extern PAW_FQK_CB _pawageQSu _pawageQSu_shr( _pawageQSu n, pawru move );
extern PAW_FQK_CB _pawageQSu _pawageQSu_rol( _pawageQSu n, pawru move );
extern PAW_FQK_CB _pawageQSu _pawageQSu_ror( _pawageQSu n, pawru move );
extern PAW_FQK_CB pawd _pawageQSu_cmp( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_bwa( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_bwo( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_bwx( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_add( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_sub( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_mul( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSdivu _pawageQSu__div( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_div( _pawageQSu a, _pawageQSu b );
extern PAW_FQK_CB _pawageQSu _pawageQSu_mod( _pawageQSu a, _pawageQSu b );

extern PAW_FQK_CB pawru _pawageQSd_len( _pawageQSd n );
extern PAW_FQK_CB pawb  _pawageQSd_not( _pawageQSd n );
extern PAW_FQK_CB _pawageQSd _pawageQSd_bwn( _pawageQSd n );
extern PAW_FQK_CB _pawageQSd _pawageQSd_inc( _pawageQSd n );
extern PAW_FQK_CB _pawageQSd _pawageQSd_dec( _pawageQSd n );
extern PAW_FQK_CB _pawageQSd _pawageQSd_neg( _pawageQSd n );
extern PAW_FQK_CB _pawageQSd _pawageQSd_shl( _pawageQSd n, pawru move );
extern PAW_FQK_CB _pawageQSd _pawageQSd_shr( _pawageQSd n, pawru move );
extern PAW_FQK_CB _pawageQSd _pawageQSd_rol( _pawageQSd n, pawru move );
extern PAW_FQK_CB _pawageQSd _pawageQSd_ror( _pawageQSd n, pawru move );
extern PAW_FQK_CB pawd _pawageQSd_cmp( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_bwa( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_bwo( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_bwx( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_add( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_sub( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_mul( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSdivd _pawageQSd__div( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_div( _pawageQSd a, _pawageQSd b );
extern PAW_FQK_CB _pawageQSd _pawageQSd_mod( _pawageQSd a, _pawageQSd b );

extern PAW_FQK_CB pawru pawageQS_len( pawageQS n );
extern PAW_FQK_CB pawb  pawageQS_not( pawageQS n );
extern PAW_FQK_CB pawageQS pawageQS_bwn( pawageQS n );
extern PAW_FQK_CB pawageQS pawageQS_inc( pawageQS n );
extern PAW_FQK_CB pawageQS pawageQS_dec( pawageQS n );
extern PAW_FQK_CB pawageQS pawageQS_neg( pawageQS n );
extern PAW_FQK_CB pawageQS pawageQS_shl( pawageQS n, pawru move );
extern PAW_FQK_CB pawageQS pawageQS_shr( pawageQS n, pawru move );
extern PAW_FQK_CB pawageQS pawageQS_rol( pawageQS n, pawru move );
extern PAW_FQK_CB pawageQS pawageQS_ror( pawageQS n, pawru move );
extern PAW_FQK_CB pawd pawageQS_cmp( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_bwa( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_bwo( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_bwx( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_add( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_sub( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_mul( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQSdiv pawageQS__div( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_div( pawageQS a, pawageQS b );
extern PAW_FQK_CB pawageQS pawageQS_mod( pawageQS a, pawageQS b );


void libpaw_init_pawage( libpaw_constants *page )
{
	pawd i = 0;
	pawageNS *ns = page->ageNS_units, nsmul = pawageNS_v(1000);
	pawageQS *qs = page->ageQS_units, qsmul = pawageQS_v(1000);

	/* Don't you just love metric? */
	page->ageNS_units[0] = pawageNS_v(1);
	for ( i = 1; i++ < pawageNS_metric_count; ns[1] = pawageNS_mul(*ns,nsmul), ++ns );

	page->ageQS_units[0] = pawageQS_v(1);
	for ( i = 1; i++ < pawageQS_metric_count; qs[1] = pawageQS_mul(*qs,qsmul), ++qs );

	/* Imperial units, shouldn't need these anymore but might as well */
	ns = page->ageNS_units;
	ns[ pawageNS_unit_sh ] = pawageNS_mul( *ns, pawageNS_v(10) );
	ns[ pawageNS_unit_Ej ] = pawageNS_mul( ns[pawageNS_unit_ms], pawageNS_v(3) );

	qs = page->ageQS_units;
	qs[ pawageQS_unit_Pj ] = pawageQS_mul( qs[pawageQS_unit_zs], pawageQS_v(3) );
	qs[ pawageQS_unit_sv ] = pawageQS_mul( qs[pawageQS_unit_fs], pawageQS_v(100) );
	qs[ pawageQS_unit_sh ] = pawageQS_mul( qs[pawageQS_unit_ns], pawageQS_v(10) );
	qs[ pawageQS_unit_Ej ] = pawageQS_mul( qs[pawageQS_unit_ms], pawageQS_v(3) );
}
