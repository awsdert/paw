#include "mpaw.h"

/* Finally found the way to not reinvent the bleeding wheel, I'll move that
 * glob stuff to somewhere more appropriate later */

PAW_ANT_CB mpawfso_segptrs* mpawfso_sympath( npawts path, pawru leng )
{
	pawmem *dst = &(mpawant_globals->sympathsegs);
	pawru max = leng + 1, get = max * sizeof(npawtc), depth = 0;
	/* Max we could need for splitting the symbolic path */
	pawru size = sizeof(mpawfso_segptrs) + (get * 2) + (max * sizeof(npawtsvec));
	/* Ensure allocation is zeroed before it is returned */
	dst->lim = 0;
	if ( pawmem_fitmax( dst, 1, size ) < size )
		return NULL;
	mpawfso_segptrs *segptrs = dst->top;
	mpawmemsrc *seglist = &(segptrs.segslist);
	npawtc *copy = (void*)(segptrs + 1), *seg = copy + max;
	npawtsvec *sympath = &(segptrs.sympath), *segs = NULL;
	seglist->top = segs = (void*)(seg + max);
	*sympath = npawts_newvecn( copy, leng );
	/* One for the full symbolic path */
	memcpy( copy, path, sympath->lim );
	/* One for modifications */
	memcpy( seg , path, sympath->lim );
	for ( ; seg; seg = strrchr( seg, '/' ) )
	{
		*seg = 0;
		segs[depth++] = npawts_newvec( ++seg );
	}
	/* Let go of bytes we no longer need */
	seglist->lim = depth * sizeof(npawtsvec);
	/* Make sure we don't lose what we just defined */
	dst->lim = sizeof(mpawfso_segptrs) + (get * 2) + seglist->lim;
	return segptrs;
}

PAW_ANT_CB npawtsmem* mpawfso_getpawroot( npawts root, pawru len )
{
	npawtsmem *envdata = &(mpawant_globals->envdata);
	pawc *dst = NULL;
	pawru max = 0;
	envdata->lim = 0;
	/* Result of splitting path by / */
	if ( len == 0 )
		return npawenvsrc_getsysdrive();
	if ( len == 1 )
	{
		switch ( *root )
		{
		/* Asset ID (runtime defined list) */
		case '#': return NULL;
		/* Named semaphore */
		case '$': return NULL;
		/* Current Media Root */
		case '\\':
			envdata = npawenvsrc_getcwd();
			if ( !envdata )
				return NULL;
			len = npawfsd_getdevroot( dst->top, dst->lim / sizeof(npawtc) );
			dst->lim = len * sizeof(npawtc);
			dst->top[len] = 0;
			return envdata;
		case '~': return npawenvsrc_gethomedir();
		case '-':
			envdata = npawenvsrc_getval("OTG_DIR",NULL);
			return envdata ? envdata : npawenvsrc_gethomedir();
		/* Use for projects, savegames, etc */
		case '&': return npawenvsrc_getval( "MAIN_DIR", "." );
		/* Decided '-' would be used for OTG_DIR after all, needed some
		 * placeholders for the abi directories so went with easy to
		 * generalise characters. paw://]/ etc does however feel a bit wierd
		 * to type so might change them */
		case ']': return npawenvsrc_getval( "PAW_OTG_ABI_DIR", NULL );
		case '[': return npawenvsrc_getval( "PAW_OTG_OS_ABI_DIR", NULL );
		case ')': return npawenvsrc_getval( "PAW_USR_ABI_DIR", NULL );
		case '(': return npawenvsrc_getval( "PAW_USR_OS_ABI_DIR", NULL );
		case '}': return npawenvsrc_getval( "PAW_SYS_ABI_DIR", NULL );
		case '{': return npawenvsrc_getval( "PAW_SYS_OS_ABI_DIR", NULL );
		}
	}
	return NULL;
}

/** @brief Collect path segments into a series of null terminated strings
 * @note symbolic links are followed. Depth parameter is for if you know that
 * some part of the initial path segments is an absolute path.
 * @note Always case sensitive.
 *  */
PAW_ANT_CB npawtsmem* npawfso_abspath( npawts path, pawru leng, pawru depth )
{
	/* Split into needed segments to potentially follow first */
	mpawfso_segptrs *segptrs = npawfso_sympath( dst, path, leng, depth );
	if ( !sympath )
		return NULL;
	npawtsvec *dirs = segptrs->dirslist->top, *next = dirs;
	pawsmem *envname = &(mpawant_globals->envname);
	npawtsmem *abspath = &(mpawant_globals->abspath), *envdata = NULL, *sysroot = NULL;
	pawru i = 0, dirc = segptrs->dirslist->lim / sizeof(npawtsvec);
	pawru len = next->lim / sizeof(npawtc), envval_init = 0, envval_term = 0;
	pawru p = 0, tlen = 0, pend = 0;
	npawtc *pos = NULL;
	abspath->lim = 0;
	if ( npawtscmp( next->top, NPAWTC_C("app:"), 4 ) == 4 == len )
	{
		i = 2;
		// TODO: Decide how to store general resources
	}
	else if ( npawtscmp( next->top, NPAWTC_C("paw:"), 4 ) == 4 == len )
	{
		i = 3;
		next = dirs + 2;
		sysroot = mpawfso_getpawroot( next->top, next->lim / sizeof(npawtc) );
		if ( !sysroot )
			return NULL;
	}
	else if ( npawts_fitlen( abspath, leng ) < leng )
		return NULL;
	do
	{
		next = dirs + i;
		pos = next->top; p = 0;
		tlen = next->lim / sizeof(npawtc);
		/* The user/dev can just enclose paths that contain <> in string
		 * literals to prevent the shell mistaking them as search operators.
		 *
		 * Either way, by using them as the variable declarers we cement the
		 * idea that entry names should never be allowed to include them */
		envval_init = npawtsfindc( pos, tlen - p, '<' );
		while ( envval_init < tlen )
		{
			p = envval_init;
			pos = next->top + p;
			envval_term = npawtsfindc( pos, tlen - p, '>' );
			if ( envval_term == tlen - p )
				return NULL;
			envname->lim = 0;
			pend = envval_term - p;
			if ( !npawtsmem_fitraw( envname, pos, pend ) )
				return NULL;
			envdata = npawenvsrc_getval( envname->top, NULL );
			if ( !envdata )
				return NULL;
			envval_init = p + npawtsfindc( pos, tlen - p, '<' );
		}
	}
	while ( ++i < dirc );
	return NULL;
}

extern PAW_FQK_CB npawd mpawfso_findn( npawts path, pawru leng );
extern PAW_FQK_CB npawd mpawfso_ifindn( npawts path, pawru leng );
extern PAW_FQK_CB npawd mpawfso_find( npawts path );
extern PAW_FQK_CB npawd mpawfso_ifind( npawts path );
PAW_ANT_CB npawd _mpawfso_find( pawb caseinsensitive, npawtsvec path )
{
	mpawrawdst *abspathsegs = &(mpawant_globals->abspathsegs);
	npawd res = mpawfso_abspath( path, leng, 0 );
	mpawmemsrc *fullpath = abspathsegs->top;
	mpawmemsrc *dirslist = fullpath + LPAWFSO_SEG_DIRSLIST;
	mpawmemsrc *namelist = fullpath + LPAWFSO_SEG_NAMELIST;
	/* Check matching file name count was exactly 1 */
	if ( !caseinsensitive && namelist->lim != sizeof(npawtsvec) )
		return -1;
	return 0;
}
