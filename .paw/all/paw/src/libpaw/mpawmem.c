#include "mpaw.h"

extern inline void* mpawva_ptrarg( va_list va );

pawmsg mpawmem_refcall( pawabs cls, pawe32d call, va_list va )
{
	pawmsg msg = 0;
	mpawrawdst *raw = (void*)(uintptr_t)cls;
	mpawmem_vt vt = raw->vtable;
	switch ( call )
	{
		case PAWCALL_GETRAW:
			void   *dstptr = mpawva_ptrarg( va );
			pawabs *getptr = mpawva_ptrarg( va );
			pawabs *absptr = mpawva_ptrarg( va );
			pawabs abs = absptr ? *absptr : raw->abs;
			if ( abs > raw->get )
				return PAWMSGID_NOT_RANGED;
			if ( !getptr )
				return 0;
			pawabs get = *getptr;
			if ( abs + get > raw->get )
				get = raw->get - abs;
			memcpy( dstptr, raw->buf + abs, get );
			if ( absptr ) *absptr = abs + get;
			*getptr = get;
			return 0;
		case PAWCALL_PUTRAW:
			pawp    srcptr = mpawva_ptrarg( va );
			pawabs *getptr = mpawva_ptrarg( va );
			pawabs *absptr = mpawva_ptrarg( va );
			pawabs abs = absptr ? *absptr : raw->abs;
			if ( abs > raw->cap )
				return PAWMSGID_NOT_RANGED;
			if ( !getptr )
				return 0;
			pawabs get = *getptr;
			pawabs end = abs + get;
			if ( end > raw->cap )
			{
				msg vt->growCB ?
					vt->growCB( (uintptr_t)(&raw->top), (uintptr_t)(&raw->cap), end )
					: PAWMSGID_NOT_ENOUGH;
				if ( msg )
					get = raw->cap - abs;
			}
			memcpy( raw->buf + abs, src, get );
			if ( absptr ) *absptr = abs + get;
			*getptr = get;
			return msg;
	}
}

PAW_QCK_CB mpawrawdst mpawrawdst_getmem( void )
{
	mpawrawdst *env = &(mpawthisant->envdata), tmp = {0};
	if ( env->end )
	{
		tmp = *(mpawrawdst*)(env->top);
		memset( env->top, 0, env->end );
		env->end = 0;
	}
	return tmp;
}
typedef signed _BitInt(64) pawclsdesc;
PAW_QCK_CB pawclsdesc mpawrawdst_ignite
	( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru cap )
{
	mpawrawdst mem = {0};
	mem._file = file;
	mem._line = line;
	mem.allocCB = cb ? cb : mpawrawdst_standardCB;
	pawru got = mpawrawdst_setcap( &mem, T, cap );
	if ( got == 0 )
	{
		mpawrawdst *env = &(mpawthisant->envdata);
		memcpy( env->top, &mem, sizeof(mem) );
		env->end = sizeof(mem);
	}
	return got;
}

PAW_ANT_CB pawed mpawrawdst_setcap( mpawrawdst *mem, pawru T, pawru cap )
{
	pawru lim = mem->end + T;
	if ( cap && cap < T )
		cap = T;
	pawru got = mem->allocCB( &(mem->top), &(mem->cap), mem->end, cap );
	if ( !got )
	{
		mem->end = 0;
		return 0;
	}
	if ( lim >= got )
	{
		uchar *top = mem->top;
		mem->end = lim = got - T;
		pawrawclr( top + lim, T );
	}
	return got;
}

PAW_QCK_CB pawru mpawrawdst_setcap( mpawrawdst *mem, pawru T, pawru cap )
{
	pawru lim = mem->end + T;
	if ( cap && cap < T )
		cap = T;
	pawru got = mem->newCB( &(mem->top), &(mem->cap), mem->end, cap );
	if ( !got )
	{
		mem->end = 0;
		return 0;
	}
	if ( lim >= got )
	{
		uchar *top = mem->top;
		mem->end = lim = got - T;
		pawrawclr( top + lim, T );
	}
	return got;
}
PAW_QCK_CB pawru mpawrawdst_setlim( mpawrawdst *mem, pawru T, pawru lim )
{
	pawru got = mpawrawdst_setcap( mem, T, lim + T );
	if ( got > lim )
		mem->end = lim;
	return mem->end;
}
PAW_FQK_CB pawru mpawrawdst_setmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_setcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_setlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_setlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_setraw( mpawrawdst *mem, pawru T, pawp raw, pawru lim )
{
	pawru got = mpawrawdst_setcap( mem, T, lim + T );
	if ( got < lim )
	{
		pawrawclr( mem->top, mem->cap );
		mem->end = 0;
	}
	else
	{
		pawrawclr( ((uchar*)(mem->top)) + lim, T );
		pawrawcpy( mem->top, raw, lim );
		mem->end = lim;
	}
	return mem->end;
}

/* Fit allocation to request if need be */
PAW_QCK_CB pawru mpawrawdst_fitcap( mpawrawdst *mem, pawru T, pawru cap )
{
	return (mem->cap < T || mem->cap < cap)
		? mpawrawdst_setcap( mem, T, cap ) : mem->cap;
}
PAW_FQK_CB pawru mpawrawdst_fitlim( mpawrawdst *mem, pawru T, pawru lim )
	{ return mpawrawdst_setlim( mem, T, lim ); }
PAW_FQK_CB pawru mpawrawdst_fitmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_fitcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_fitlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_fitlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_fitraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru got = mpawrawdst_fitlen( mem, T, len );
	if ( got >= len )
		pawrawcpy( mem->top, raw, mem->end );
	return got;
}

/* Cap allocation if request is bigger */
PAW_FQK_CB pawru mpawrawdst_capcap( mpawrawdst *mem, pawru T, pawru cap )
	{ return (mem->cap <= cap) ? mem->cap : mpawrawdst_setcap( mem, T, cap ); }
PAW_QCK_CB pawru mpawrawdst_caplim( mpawrawdst *mem, pawru T, pawru lim )
{
	if ( mem->end <= lim )
		return mem->end;
	mpawrawdst_setlim( mem, T, lim );
	pawrawclr( pawv_addbytes( mem->top, lim ), T );
	return mem->end;
}
PAW_FQK_CB pawru mpawrawdst_capmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_capcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_caplen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_caplim( mem, T, T * len ); }
PAW_QCK_CB pawru mpawrawdst_capraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru waz = mem->end;
	pawru lim = mpawrawdst_caplim( mem, T, T * len );
	return pawrawcpy( mem->top, raw, lim );
}

PAW_QCK_CB pawru mpawrawdst_catcap( mpawrawdst *mem, pawru T, pawru cap )
{
	cap += mem->cap;
	pawru got = mpawrawdst_fitcap( mem, T, cap );
	return (got >= cap) ? got : 0;
}
PAW_QCK_CB pawru mpawrawdst_catlim( mpawrawdst *mem, pawru T, pawru lim )
{
	lim += mem->end;
	pawru got = mpawrawdst_fitlim( mem, T, lim );
	return (got >= lim) ? got : 0;
}
PAW_FQK_CB pawru mpawrawdst_catmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_catcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_catlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_catlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_catraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru waz = mem->end;
	pawru got = mpawrawdst_catlen( mem, T, len );
	if ( got )
	{
		uchar *top = mem->top;
		pawrawcpy( pawv_addbytes( mem->top, waz ), raw, T * len );
	}
	return got;
}

PAW_ANT_CB pawru mpawrawdst_standardCB( void **old, pawru *was, pawru cap )
{
}

PAW_QCK_CB void mpawrawdst_expire( mpawrawdst *mem )
{
	(void)mem->newCB( &(mem->top), &(mem->cap), 0, 0, 0 );
	mem->end = 0;
}
PAW_QCK_CB void mpawrawdst_delete( mpawrawdst *mem )
{
	pawalloc_cb newCB = mem->newCB;
	pawru T = sizeof(*mem);
	mpawrawdst_expire( mem );
	newCB( (void**)&mem, &T, 0, 0 );
}
/* Get initialised memory */
PAW_ANT_CB mpawrawdst mpawgetmem(void);
/* looped copy */
PAW_ANT_CB npawzu npawrawlcpy
	( void *dst, npawzu cap, npawzu pos, void *cpy, npawzu len, npawzu end )
{
	uchar *top = dst;
	if ( pos > cap )
		return pos;
	npawzu stop = ( pos + len * end > cap ) ? (cap - pos) / len : end;
	if ( !cpy )
	{
		memset( top + pos, 0, stop < end ? cap - pos : len * end );
		return;
	}
	for ( ; stop--; pos += len )
		memcpy( top + pos, cpy, len );
	if ( stop < end )
	{
		memcpy( top + pos, cpy, cap - pos );
		return cap;
	}
	return pos;
}
PAW_ANT_CB pawmsg mpawrawlcpy
	( pawclsdesc clsdesc, pawru *pos, upawabs cpy, pawru len, pawru end )
{
#if 1
	return PAWMSGID_NO_CODE;
#else
	npawzu cap = 0;
	void *dst = NULL /* Call some function to grab the actual pointer */ ;
	npawzu did = npawrawlcpy( dst, cap, *pos, cpu, len, end );
	pawmsg msg = (did > cap || did < *pos + len * end) ? PAWMSGID_NOT_RANGED : 0;
	*pos = did;
	return msg;
#endif
}

PAW_ANT_CB void* npawrealloc
	( void **old, npawzu *cap, npawzu get, npawzu pos, npawzu len, npawzu end )
{
	uchar *top = *old;
	if ( !get )
	{
		if ( !top )
			return NULL;
		npawrawlcpy( top, *cap, pos, cpy, len, end );
		free( cap );
		return NULL;
	}
	top = top ? realloc( top, get ) : malloc( get );
	if ( !cap )
		return NULL;
	*cap = get;
	if ( npawrawlcpy( top, get, pos, cpy, len, end ) <= pos + len * end )
		pawmsgid( PAWMSGID_INVALIDOPT, 0 );
	return top;
}

PAW_QCK_CB mpawrawdst _mpawrawdst_new( pawmbs file, pawru line, pawalloc_cb cb )
{
	mpawrawdst mem = {0};
	mem.line = line;
	mem.file = file;
	mem.newCB = cb ? cb : mpawrawdst_standardCB;
	return mem;
}
PAW_QCK_CB mpawrawdst _mpawrawdst_newcap( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru cap )
{
	mpawrawdst mem = _mpawrawdst_new( file, line, cb );
	mpawrawdst_setcap( &mem, T, cap );
	return mem;
}
PAW_QCK_CB mpawrawdst _mpawrawdst_newlim( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru lim )
{
	mpawrawdst mem = _mpawrawdst_newcap( file, line, cb, T, lim + T );
	if ( mem.top )
		mem.end = lim;
	return mem;
}
PAW_FQK_CB mpawrawdst _mpawrawdst_newmax( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru max )
	{ return _mpawrawdst_newcap( file, line, cb, T, T * max ); }
PAW_FQK_CB mpawrawdst _mpawrawdst_newlen( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru len )
	{ return _mpawrawdst_newlim( file, line, cb, T, T * len ); }
PAW_QCK_CB mpawrawdst _mpawrawdst_newraw( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawp raw, pawru len )
{
	mpawrawdst mem = _mpawrawdst_newlen( file, line, cb, T, len );
	if ( mem.top )
		pawrawcpy( mem.top, raw, T * len );
	return mem;
}

#define PAWMEM_ERRSRC PAWMBS_ERRSRC, mpawrawdst_standardCB
#define mpawrawdst_new( T ) _mpawrawdst_new( PAWMEM_ERRSRC, T )
#define mpawrawdst_newcap( T, cap ) _mpawrawdst_newcap( PAWMEM_ERRSRC, sizeof(T), cap )
#define mpawrawdst_newlim( T, lim ) _mpawrawdst_newlim( PAWMEM_ERRSRC, sizeof(T), lim )
#define mpawrawdst_newmax( T, max ) _mpawrawdst_newmax( PAWMEM_ERRSRC, sizeof(T), max )
#define mpawrawdst_newlen( T, len ) _mpawrawdst_newlen( PAWMEM_ERRSRC, sizeof(T), len )
#define mpawrawdst_newraw( T, raw, len ) _mpawrawdst_newraw( PAWMEM_ERRSRC, sizeof(T), raw, len )


PAW_ANT_CB pawed mpawrawdst_mallocv( void *ud, pawed call, va_list va )
{
	switch ( call )
	{
	case PAWMM_CALL_SETCAP:
		return mpawrawdst_setcap( ud, va_arg( va, pawru ),  va_arg( va, pawru ) );
	}
}

PAW_QCK_CB pawru mpawrawdst_setcap( mpawrawdst *mem, pawru T, pawru cap )
{
	pawru lim = mem->end + T;
	if ( cap && cap < T )
		cap = T;
	pawru got = mem->newCB( &(mem->top), &(mem->cap), mem->end, cap );
	if ( !got )
	{
		mem->end = 0;
		return 0;
	}
	if ( lim >= got )
	{
		uchar *top = mem->top;
		mem->end = lim = got - T;
		pawrawclr( top + lim, T );
	}
	return got;
}
PAW_QCK_CB pawru mpawrawdst_setlim( mpawrawdst *mem, pawru T, pawru lim )
{
	pawru got = mpawrawdst_setcap( mem, T, lim + T );
	if ( got > lim )
		mem->end = lim;
	return mem->end;
}
PAW_FQK_CB pawru mpawrawdst_setmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_setcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_setlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_setlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_setraw( mpawrawdst *mem, pawru T, pawp raw, pawru lim )
{
	pawru got = mpawrawdst_setcap( mem, T, lim + T );
	if ( got < lim )
	{
		pawrawclr( mem->top, mem->cap );
		mem->end = 0;
	}
	else
	{
		pawrawclr( ((uchar*)(mem->top)) + lim, T );
		pawrawcpy( mem->top, raw, lim );
		mem->end = lim;
	}
	return mem->end;
}

/* Fit allocation to request if need be */
PAW_QCK_CB pawru mpawrawdst_fitcap( mpawrawdst *mem, pawru T, pawru cap )
{
	return (mem->cap < T || mem->cap < cap)
		? mpawrawdst_setcap( mem, T, cap ) : mem->cap;
}
PAW_FQK_CB pawru mpawrawdst_fitlim( mpawrawdst *mem, pawru T, pawru lim )
	{ return mpawrawdst_setlim( mem, T, lim ); }
PAW_FQK_CB pawru mpawrawdst_fitmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_fitcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_fitlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_fitlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_fitraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru got = mpawrawdst_fitlen( mem, T, len );
	if ( got >= len )
		pawrawcpy( mem->top, raw, mem->end );
	return got;
}

/* Cap allocation if request is bigger */
PAW_FQK_CB pawru mpawrawdst_capcap( mpawrawdst *mem, pawru T, pawru cap )
	{ return (mem->cap <= cap) ? mem->cap : mpawrawdst_setcap( mem, T, cap ); }
PAW_QCK_CB pawru mpawrawdst_caplim( mpawrawdst *mem, pawru T, pawru lim )
{
	if ( mem->end <= lim )
		return mem->end;
	mpawrawdst_setlim( mem, T, lim );
	pawrawclr( pawv_addbytes( mem->top, lim ), T );
	return mem->end;
}
PAW_FQK_CB pawru mpawrawdst_capmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_capcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_caplen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_caplim( mem, T, T * len ); }
PAW_QCK_CB pawru mpawrawdst_capraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru waz = mem->end;
	pawru lim = mpawrawdst_caplim( mem, T, T * len );
	return pawrawcpy( mem->top, raw, lim );
}

PAW_QCK_CB pawru mpawrawdst_catcap( mpawrawdst *mem, pawru T, pawru cap )
{
	cap += mem->cap;
	pawru got = mpawrawdst_fitcap( mem, T, cap );
	return (got >= cap) ? got : 0;
}
PAW_QCK_CB pawru mpawrawdst_catlim( mpawrawdst *mem, pawru T, pawru lim )
{
	lim += mem->end;
	pawru got = mpawrawdst_fitlim( mem, T, lim );
	return (got >= lim) ? got : 0;
}
PAW_FQK_CB pawru mpawrawdst_catmax( mpawrawdst *mem, pawru T, pawru max )
	{ return mpawrawdst_catcap( mem, T, T * max ) / T; }
PAW_FQK_CB pawru mpawrawdst_catlen( mpawrawdst *mem, pawru T, pawru len )
	{ return mpawrawdst_catlim( mem, T, T * len ) / T; }
PAW_QCK_CB pawru mpawrawdst_catraw( mpawrawdst *mem, pawru T, pawp raw, pawru len )
{
	pawru waz = mem->end;
	pawru got = mpawrawdst_catlen( mem, T, len );
	if ( got )
	{
		uchar *top = mem->top;
		pawrawcpy( pawv_addbytes( mem->top, waz ), raw, T * len );
	}
	return got;
}

/* malloc/realloc/free with memset( top + pos, 0, got - pos ); applied in all
 * but the get == 0 case. Checks for NULL pointers also to sort out which
 * function should be getting called. This prevents inconsistancies across
 * malloc.h implementations from rearing their ugly heads in the ABI.
 *
 * In other words a request for 0 memory will always give a NULL pointer, if
 * the given pointer was not NULL then free( *old ) will be called. */
PAW_ANT_CB pawru mpawrawdst_standardCB( void **old, pawru *was, pawru pos, pawru get );
PAW_QCK_CB void mpawrawdst_expire( mpawrawdst *mem );
PAW_QCK_CB void mpawrawdst_delete( mpawrawdst *mem );
/* Get initialised memory */
PAW_QCK_CB mpawrawdst _mpawrawdst_new( pawmbs file, pawru line );
PAW_QCK_CB mpawrawdst _mpawrawdst_newcap( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru cap );
PAW_QCK_CB mpawrawdst _mpawrawdst_newlim( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru lim );
PAW_FQK_CB mpawrawdst _mpawrawdst_newmax( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru max );
PAW_FQK_CB mpawrawdst _mpawrawdst_newlen( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawru len );
PAW_QCK_CB mpawrawdst _mpawrawdst_newraw( pawmbs file, pawru line, pawalloc_cb cb, pawru T, pawp raw, pawru len );

typedef struct _mpawmd mpawmd;

/* Thread/Ant VM */
typedef struct
{
	/* Cheap way to detect if position is in use, 0 is reserved for managing
	 * the vms itself */
	pawru   pos;
	pawru   cap;
	/* Memory descriptor objects */
	mpawmd *mds;
} mpawtvm;

/* Non-destructible VM object, can only be released by owning thread */
typedef struct
{
	/* So threads have a means of syncronising their increment/decrement of
	 * rdlocks in mpawmm objects. Also prevents lag caused by threads locking
	 * the same semaphore for accessing different thread allocations */
	npawsys  sem;
	pawexe   tid;
	mpawtvm *tvm;
} mpawvm;

struct _mpawmd
{
	pawrd allocid;
	pawrd nextact, nextclr;
	pawru octperm;
	/* When owner wants to write it will increment wrlocks and wait for rdlocks
	 * to clear. While not 0 any new rdlock increment will wait until wrlocks
	 * has been cleared. */
	pawru rdlocks;
	pawru wrlocks;
	/* Max size that ca be "reallocated" before contents need to be moved to
	 * a new allocation */
	pawru maxsize;
	/* Size that was requested */
	pawru defined;
	void *address;
	/* To hide win32 map handles from public API */
	npawsys fsm;
	/* Ensures we enable custom handlers for unknown allocaters */
	void (*mpawrelmemCB)( npawsys fsm, void *address );
};

__thread mpawtvm *antvm = NULL;
static mpawvm  *appvms = NULL;
static npawsys  appsem = -1;

PAW_APP_CB pawrd mpawinitappvms(void)
{
	if ( appvms )
		return 0;
	pawru pagesize = pawgetpagesize();
	npawsys sem = npawnewsem();
	pawrd semerr = npawgeterr(), memerr = -1;
	appvms = npawmap( NULL, pagesize, 0600, -1, 0 );
	memerr = npawgeterr();
	if ( !appvms || appsem < 0 )
	{
		if ( appvms )
		{
			npawmap( appvms, 0, 0, -1, 0 );
			appvms = NULL;
			return npawerr2msg( semerr );
		}
		intelsem( sem );
		return npawerr2msg( memerr );
	}
	appvms->tid = pawgetantid();
	appvms->sem = appsem = sem;
	appvms->cap = pagesize;
	return 0;
}

PAW_APP_CB void mpawtermappvms(void)
{
	if ( !appvms )
		return;
	intelsem( appsem );
	npawmap (void*)appvms, 0, 0, -1, 0 );
	appvms = NULL;
}

mpawmd* mpawgetmd( paweld node )
{
	paweu vmcount = appvms->cap / sizeof(mpawvm);
	paweu vmindex = node & PAWEU_MAX;
	if ( !vmindex || vmindex >= vmcount )
		return NULL;
	mpawvm *vm = appvms + vmindex;
	paweu mdcount = vm->cap / sizeof(mpawmd);
	paweu mdindex = node >> PAWED_WIDTH;
	if ( !mdindex || mdindex >= mdcount )
		return NULL;
	return vm->mds + mdindex;
}

pawrd pawantsharemem( pawb share )
{
	mpawvm *vm = mpawgetantvm();
	if ( !vm )
		return -1;
	if ( !share )
	{
		if ( vm->sem < 0 )
			return 0;
		npawsem_release( vm->sem );
	}
	if ( vm->sem >= 0 )
		return 0;
	vm->sem = npawsem_acquire();
	if ( vm->sem < 0 )
	{
		pawsetmsg( npawerr2msg( npawgeterr() ), 0 );
		return -1;
	}
	return 0;
}

mpawvm* mpawgetantvm( void )
{
	if ( !appvms )
	{
		pawsetmsg( PAWMSGID_INITIALISE, 0 );
		return NULL;
	}

	if  ( npawsem_infwait( appsem ) != 0 )
	{
		npawsem_release( sem );
		return NULL;
	}
	pawru pos = 1, count = appvms->cap / sizeof(mpawvm);
	npawerr err = 0;
	for ( ; pos < count; ++pos )
	{
		vm = appvms + pos;
		if ( vm->pos )
			continue;
		antpos = pos;
		vm->sem = sem;
		vm->tid = pawgetantid();
		break;
	}
	if ( pos == count )
	{
		pawru pagesize = pawgetpagesize();
		vm = npawmap( appvms, appvms->cap + pagesize, 0600, -1, 0 );
		if ( vms )
		{
			appvms = vm;
			antpos = pos;
			vm->cap += pagesize;
			memset( vm += pos, 0, paegsize );
			vm->tid = pawgetantid();
			vm->sem = sem;
			vm->pos = pos;
		}
	}
	npawsem_remlock( appsem );
}

mpawmd* mpawnewmd( paweld *node )
{
	mpawvm *vm = appvms + antpos;
	*node = -1;
	if ( !appvms )
	{
		pawsetmsg( PAWMSGID_INITIALISE, 0 );
		return NULL;
	}
	if ( vm == appvms )
	{

	}
	if ( !(vm->mds) )
	{
		pawru pagesize = pawgetpagesize();
		vm->mds = npawmap( NULL, pagesize, 0600, -1, 0 );
		if ( !(vm->mds) )
		{
			pawsetmsg( npawerr2msg( npawgeterr() ) );
			return NULL;
		}
		vm->cap = pagesize;
		pawru count = pagesize / sizeof(mpawmd);
		for ( paweu i = 0; i < count; ++i )
			vm->mds[i].nextclr = i + 1;
	}
	paweu mdcount = vm->cap / sizeof(mpawmd), mdindex = vm->mds->nextclr;
	mpawmd *md = vm->mds + mdindex;
	if ( mdindex >= mdcount )
	{
		pawru pagesize = pawgetpagesize();
		mpawmd *mds = npawmap( vm->mds, vm->cap + pagesize, 0600, -1, 0 );
		if ( !(vm->mds) )
		{
			pawsetmsg( npawerr2msg( npawgeterr() ) );
			return NULL;
		}
		vm->cap += pagesize;
		pawru count = vm->cap / sizeof(mpawmd);
		for ( paweu i = mdcount; i < count; ++i )
			vm->mds[i].nextclr = i + 1;
	}
	vm->mds->nextclr = md->nextclr;
	md->allocid = mdindex;
	return md;
}

PAW_ANT_CB paweld mpawrealloc( paweld node, pawrd each, pawrd leng )
{
	pawru size = each * leng;
	if ( perN < 0 )
		return PAWMSGID_INVALIDCAP;
	if ( getN < 0 )
		return PAWMSGID_INVALIDLEN;
	if ( (node & PAWEHU_MAX) != antpos )
		return PAWMSGID_NO_ACCESS;
	return 0;
}

PAW_ANT_CB upawabs mpawgetptr( pawrd node )
{
	upawabs ptr = {0};
	pawrd *N = malloc( mm.defined + sizeof(pawrd) );
	if ( !N )
		return ptr;
	memcpy( N + 1, mm->address, mm->defined );
	ptr->ptr = N + 1;
	return ptr;
}
PAW_ANT_CB void mpawrelptr( upawabs ptr )
{
	if ( !(ptr->cptr) )
		return;
	pawrd *N = ((pawrd*)(ptr->top)) - 1;
	/* Like this we prevent buffer overruns while allowing direct pointed
	 * access to code that needs to be faster than simply calling io methods */
	memcpy( mm->address, ptr->cptr, mm->defined );
	free( N );
}
