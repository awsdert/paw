#include "mpaw.h"

npawtsvec npawenv_getpool(void) { return *((npawtsvec*)&(mpawthisant->envpool.pool)); }
void     npawenv_clrpool(void)
{
	mpawenvbufs *envpool = &(mpawthisant->envpool);
	if ( envpool->top )
	{
		npawtsmem *bufs = NULL, *buf = NULL;
		pawru buff = 0;
		while ( buff < envpool->end )
		{
			buf = bufs + buff;
			if ( buf->top )
				pawstdend( buf->top );
		}
		pawstdend( envpool->top );
	}
	if ( envpool->pool.top )
		pawstdend( envpool->pool.top );
	memset( envpool, 0, sizeof(*envpool) );
}
npawd     npawenv_putpool( npawtsvec pool, pawb keep_buffers )
{
	pawru i = 0, l = 0, n = 0, keys = 0, buff = 0, pair = 0;
	npawtsvec left = {0};
	mpawrawdst *envpool = (mpawrawdst*)&(mpawthisant->envpool);
	npawtsmem *bufs = NULL, *buf = NULL;
	npawts *strs = NULL;
	pawru tspoolc = pool.end / sizeof(npawtc);
	while ( i < tspoolc )
	{
		left.end = tspoolc - i;
		left.top = pool.top + i;
		l = npawtslinelen( left.top, left.end );
		n = npawtsnewlinelen( left.top + l, left.end - l );
		i += l + n;
		++keys;
	}
	if ( mpawrawdst_fitmax( envpool, sizeof(npawtsmem), keys * 2 ) < keys * 2 )
		return -1;
	mpawrawdst_fitlen( envpool, sizeof(mpawrawdst), keys );
	if ( mpawrawdst_fitmax( envpool + 1, sizeof(npawtc), tspoolc ) < tspoolc )
		return -1;
	bufs = mpawthisant->envpool.top;
	strs = (npawts*)(bufs + keys);
	for ( i = 0; i < tspoolc; i += l + n, ++buff )
	{
		left.end = tspoolc - i;
		left.top = pool.top + i;
		l = npawtslinelen( left.top, left.end );
		n = npawtsnewlinelen( left.top + l, left.end - l );
		buf = bufs + buff;
		if ( npawtsmem_fitmax( buf, l ) < l )
			return -1;
		/* Ensure the existing list matches the pointers used */
		if ( buf->top[0] )
			strs[pair++] = buf->top;
	}
	/* List is supposed to end with NULL */
	strs[pair++] = NULL;

	/* Time to replace the pool */
	for ( i = 0, buff = 0; i < tspoolc; i += l + n, ++buff )
	{
		left.end = tspoolc - i;
		left.top = pool.top + i;
		l = npawtslinelen( left.top, left.end );
		n = npawtsnewlinelen( left.top + l, left.end - l );
		buf = bufs + buff;
		strs[buff] = buf->top;
		memcpy( buf->top, left.top, sizeof(npawtc) * l );
		buf->top[l] = 0;
	}
	strs[buff] = NULL;
	if ( keep_buffers )
	{
		for ( ; buff < keys; ++buff )
		{
			buf = bufs + buff;
			/* Keep the buffers but wipe their contents for security reasons */
			if ( buf->top )
			{
				memset( buf->top, 0, buf->end );
				buf->end = 0;
			}
		}
	}
	else
	{
		i = buff;
		for ( ; buff < keys; ++buff )
		{
			buf = bufs + buff;
			if ( buf->top )
				pawstdend( buf->top );
		}
		memset( bufs + i, 0, sizeof(npawtsmem) * (keys - i) );
	}
	return 0;
}

typedef struct
{
	pawru i, len, nl;
	npawtsvec key;
	npawtsvec val;
} npawenv;
npawenv _mpaw_getenv( npawtsvec key )
{
	npawts C = NULL;
	pawru i = 0, l = 0, n = 0;
	npawenv txt = {0};
	npawtsmem pool = mpawthisant->envpool.pool;
	pawru tspoolc = pool.end / sizeof(npawtc);
	pawru keylen = key.end / sizeof(npawtc);
	txt.key = key;
	if ( keylen > tspoolc )
		return txt;
	while ( tspoolc )
	{
		l = npawtslinelen( pool.top, tspoolc );
		n = npawtsnewlinelen( pool.top + l, tspoolc - l );
		i = npawtscmpn( pool.top, l, key.top, keylen );
		C = pool.top + i;
		if ( i == keylen && *C == '=' )
		{
			txt.i = (pawvu)(pool.top - pool.top);
			txt.len = l;
			txt.nl  = n;
			txt.val.top = ++C;
			txt.val.end = (l - i) * sizeof(npawtc);
			txt.key.top = pool.top;
			return txt;
		}
		pool.top += l + n;
		tspoolc -= l + n;
	}
	return txt;
}
npawtsvec mpaw_getenv( npawtsvec key )
	{ npawenv txt = _mpaw_getenv( key ); return txt.val; }
void _mpaw_remenv( npawenv txt )
{
	if ( was.len )
		pawrawset( was.key.top, ' ', sizeof(npawtc) * was.len );
}
void mpaw_remenv( npawtsvec key )
	{ _mpaw_remenv(_mpaw_getenv( key )); }
npawd   mpaw_catenv( pawru len )
{
	npawtsmem *pool = &mpaw_antpool;
	pawru is = sizeof(npawtc) * pool->len;
	pawru add = sizeof(npawtc) * ++len;
	npawtc *top = pool->top;
	pawru cap = is + add;
	if ( cap <= pool->cap )
	{
		pawrawset( top + pool->len, ' ', add );
		pool->len += --len;
		top[pool->len] = 0;
		return 0;
	}
	top = pawnilget( pool->top, is, cap );
	if ( !top )
		return -1;
	pawrawset( top + pool->len, ' ', add );
	pool->len += --len;
	top[pool->len] = 0;
	pool->top = top;
	pool->cap = cap;
	return 0;
}

npawd   _libaw_putenv( npawtsvec key, npawtsvec val, npawtsvec linesep )
{
	pawru i = 0, l = 0, n = 0, need = key.len + val.len + 1;
	npawtsmem pool = mpaw_antpool;
	npawtc *C = NULL;
	npawd res = 0;
	while ( pool.len )
	{
		C = pool.top;
		l = npawtslinelen( C, pool.len );
		n = npawtsnewlinelen( C + l, pool.len - l );
		if ( *C == ' ' && l == need || l >= need + linesep.len )
		{
			_mpawputenv_setline:
			memcpy( C, key.top, sizeof(npawtc) * key.len );
			C += key.len;
			*C = '=';
			memcpy( ++C, val.top, sizeof(npawtc) * val.len );
			C += val.len;
			if ( need == l )
				return 0;
			/* Ensure our ' ' characters are not misinterpreted as part of the
			 * value */
			pawrawset( C, linesep.top, sizeof(npawtc) * linesep.len );
			return 0;
		}
	}
	i = pool.len;
	res = mpaw_catenv( need + linesep.len );
	if ( res != 0 )
		return res;
	pool = mpaw_antpool;
	C = pool.top + i;
	n = linesep.len;
	l = need;
	/* Ensure we always end with a new line */
	memcpy( C + need, linesep.top, sizeof(npawtc) * n );
	goto _libaw_putenv_setline;
}

npawd   mpawputenv( npawtsvec key, npawtsvec val, pawb replace )
{
	npawenv was = _mpaw_getenv(key);
	npawtsmem *pool = &mpaw_antpool;
	npawtsvec linesep = {0};
	pawru need = key.len + 1;
	npawd res = 0;
	if ( !(val.top) || !(val.top[0]) )
	{
		/* A value is mandatory, if none given then assumed "1" is wanted */
		val.top = NPAWTC_C("1");
		val.len = 1;
	}
	need += val.len;
	linesep.top = NPAWTC_C(CAUGHT_LINE_SEP);
	linesep.len = npawtslen(linesep.top);
	need += linesep.len;
	if ( was.len )
	{
		if ( !replace )
			return -1;
		if ( was.len < need )
		{
			res = _mpawputenv(key,val,linesep);
			if ( res != 0 )
				return res;
			_mpaw_remenv( was );
		}
		return res;
	}
	return _mpawputenv(key,val,linesep);
}
