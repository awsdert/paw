#define NPAW_BUILD 1
#include <paw/mbetext.h>
#ifdef CAUGHT_OS_WAS_api_msw
pawllc u8tou32( pawp src, pawru lim )
{
	pawru  i = 1, use = 0, len = lim / sizeof(pawmbc), end = 0;
	pawmbs txt = src;
	pawmbc c = 0;
	pawllc llc = 0;
	c = *txt;
	*did = sizeof(pawmbc);
	if ( !(c & 0x80) )
		return c;
	for ( use = 0; c & 0x80; ++use, c <<= 1 );
	csc = c >> use; ++use;
	end = (len < use) ? len : use;
	for ( ; i < end; ++i )
	{
		c = txt[i];
		csc <<= 6;
		csc |= c & 0x3F;
	}
	*did = i * sizeof(pawmbc);
	return llc;
}

inline npawzd WideCharToMultiByteCB
	( npawzu cp, DWORD opts, npawls src, npawzu len, void *dst, npawzu cap )
{
	return WideCharToMultiByte
		( cp, opts | WC_NO_BEST_FIT_CHARS, src, len, dst, cap, NULL, NULL );
}
inline npawzd MultiByteToWideCharCB
	( npawzu cp, DWORD opts, pawp src, npawzu end, npawlc *dst, npawzu max )
{
	return MultiByteToWideChar( cp, opts | MB_PRECOMPOSED, src, end, dst, max );
}
inline pawmsg convert( pawb srcls, void *dst, npawzu *cap, pawp src, npawzu end )
{
	npawzd got = 0;
	if ( !dst )
	{
		/* We're just calculating here so no need to concern ourselves with
		 * invalid sequences. Callers will expect that at worst it's a simple
		 * length * 4 being returned to them */
		got = srcls
			? WideCharToMultiByteCB( 12000, 0, src, end / sizeof(npawlc), NULL, 0 )
			: MultiByteToWideCharCB( 12000, 0, src, end, NULL, 0 );
		if ( got >= 0 )
		{
			if ( srcls ) got *= sizeof(npawlc);
			*cap = got ? got : 1;
			return 0;
		}
		switch ( GetLastError() )
		{
		case ERROR_INVALID_PARAMETER:      return PAWMSGID_INVALIDSEQ;
		case ERROR_NO_UNICODE_TRANSLATION: return PAWMSGID_NO_MATCH;
		}
		return PAWMSGID_FAILED;
	}
	pawru did = 0, done = 0;
	pawmbc C[PAWMBC_MAX_ENCODED_CHARS+1] = {0};
	if ( srcls )
	{
		pawcsc csc = 0;
		npawls spos = src;
		npawzu max = *cap / sizeof(pawllc);
		pawllc *dpos = dst, *dend = dpos + max;
		end /= sizeof(npawlc);
		while ( end )
		{
			if ( dpos == dend )
			{
				*cap -= max * sizeof(pawllc);
				return PAWMSGID_NOT_ENOUGH;
			}

			pawrawclr( C, sizeof(C) );
			got = WideCharToMultiByteCB
				( CP_UTF8, WC_ERR_INVALID_CHARS, spos, 1, C, sizeof(C) );
			if ( got >= 0 )
			{
				end--;
				spos++;
				*dpos++ = u8tou32( C, sizeof(C) );
				continue;
			}

			switch ( GetLastError() )
			{
			case ERROR_INVALID_PARAMETER:
				if ( end > 1 )
					break;
				return PAWMSGID_INCOMPLETE;
			case ERROR_NO_UNICODE_TRANSLATION: return PAWMSGID_NO_MATCH;
			}

			pawrawclr( C, sizeof(C) );
			got = WideCharToMultiByteCB
				( CP_UTF8, WC_ERR_INVALID_CHARS, spos, 2, C, sizeof(C) );
			if ( got >= 0 )
			{
				end -= 2;
				spos += 2;
				*dpos++ = u8tou32( C, sizeof(C) );
				continue;
			}

			switch ( GetLastError() )
			{
			case ERROR_INVALID_PARAMETER:      return PAWMSGID_INVALIDSEQ;
			case ERROR_NO_UNICODE_TRANSLATION: return PAWMSGID_NO_MATCH;
			}

			break;
		}
	else
	{
		npawzu max = *cap / sizeof(npawlc);
		npawlc *pos = dst;
		while ( end )
		{
			got = MultiByteToWideCharCB
				( CP_UTF8, MB_ERR_INVALID_CHARS, src, end, pos, max );
		}
	}

	return end ? PAWMSGID_FAILED : 0;
}
inline pawmsg llstols( npawlc *dst, npawzu *cap, pawp  src, npawzu end )
{
	return convert(0,dst,cap,src,end);
}
inline pawmsg lstolls( void   *dst, npawzu *cap, npawls src, npawzu end )
{
	return convert(1,dst,cap,src,end);
}
#else
inline pawmsg convert
	( iconv_t cd, void *dst, npawzu *cap, pawp  src, npawzu end )
{
	if ( dst )
	{
		if ( iconv( cd, &src, &end, &dst, cap ) != (size_t)-1 )
			return llc;
		switch ( errno )
		{
		case E2BIG:  return PAWMSGID_NOT_ENOUGH;
		case EINVAL: return PAWMSGID_INCOMPLETE;
		case EILSEQ: return PAWMSGID_INVALIDSEQ;
		}
		return -1;
	}
	pawe64u c = 0;
	npawzu did = 0, cap2;
	do
	{
		dst = &c;
		cap2 = sizeof(c);
		if ( iconv( cd, &src, end, &dst, &cap2 ) != (size_t)-1 )
		{
			did += sizeof(c) - cap2;
			break;
		}
		switch ( errno )
		{
		case E2BIG:  did += sizeof(c) - cap2; break;
		case EINVAL: *cap = did; return PAWMSGID_INCOMPLETE;
		case EILSEQ: *cap = did; return PAWMSGID_INVALIDSEQ;
		default:     *cap = did; return PAWMSGID_FAILED;
		}
	}
	while ( end );
	*cap = did;
	return 0;
}
inline pawmsg llstols( npawlc *dst, npawzu *cap, pawp   src, npawzu end )
{
	npawmbecd *cd = lpawget_npawmbecd();
	return convert( cd->llstolscd, dst, cap, src, end );
}
inline pawmsg lstolls( void   *dst, npawzu *cap, npawls src, npawzu end )
{
	npawmbecd *cd = lpawget_npawmbecd();
	return convert( cd->llstolscd, dst, cap, src, end );
}
extern inline pawmsg convert( iconv_t cd, void *dst, npawzu *cap, pawp  src, npawzu end );
#endif

extern inline pawrd llstols( npawlc *dst, npawzu *cap, pawp   src, npawzu end );
extern inline pawrd lstolls( void   *dst, npawzu *cap, npawls src, npawzu end );

pawcsc mbe_wctou32( pawp src, pawru lim, pawru *val, pawb calc )
{
	if ( !lim )
		return 0;
	if ( lim < sizeof(npawlc) )
		return -PAWMSGID_INCOMPLETE;
#ifdef CAUGHT_OS_WAS_api_msw
	char8_t u8[MB_LEN_MAX+1] = {0};
	pawru end = sizeof(npawlc) * 2;
	pawed got = 0;
	if ( end > lim ) end = lim;
	do
	{
		/* I'd love to go to UTF32 directly but microsoft in their
		 * "infinite wisdom" decided the WC_ERR_INVALID_CHARS was only acceptable
		 * for UTF-8 as though there couldn't possibly be an attempt to convert
		 * an invalid character to utf-32. */
		got = lstollc( calc ? NULL : u8, calc ? 0 : sizeof(u8), src, end );
		if ( got > 0 )
		{
			if ( calc )
			{
				*val = got * 2;
				return 0;
			}
			*val = end;
			return u8tou32( u8, got, &end );
		}
		end -= sizeof(npawlc);
		switch ( GetLastError() )
		{
		case ERROR_INSUFFICIENT_BUFFER: continue;
		case ERROR_INVALID_PARAMETER:   return -PAWMSGID_INVALIDSEQ;
		case ERROR_NO_UNICODE_TRANSLATION:
			return (lim < sizeof(npawlc) * 2)
				? -PAWMSGID_INCOMPLETE : -PAWMSGID_NO_MATCH;
		}
	} while ( end && end <= lim );
#endif
	return -1;
}

pawmsg mbe_u32towc( void *dst, pawru cap, pawru *val, pawlls src, pawru end )
{
#ifdef CAUGHT_OS_WAS_api_msw
	pawed got = llctols( dst, cap, src, end );
	if ( got >= 0 )
	{
		*val = got ? got : 1;
		*val *= sizeof(npawlc);
		return 0;
	}
	switch ( GetLastError() )
	{
	case ERROR_INSUFFICIENT_BUFFER:    return PAWMSGID_NOT_ENOUGH;
	case ERROR_INVALID_PARAMETER:      return PAWMSGID_INVALIDSEQ;
	case ERROR_NO_UNICODE_TRANSLATION: return PAWMSGID_NO_MATCH;
	}
	return 1;
#endif
	return -1;
}
/* libpaw's multi-byte encoder will look for these to convert characters
 * to/from encodings */
pawcsc libpawmbe_minget( pawp src, pawru lim, pawru *min )
	{ return mbe_wctou32( src, lim, min, 1 ); }

pawmsg libpawmbe_minput( pawp src, pawru lim, pawru *min )
	{ return mbe_u32towc( NULL, 0, min, src, lim ); }

pawcsc libpawmbe_getllc( pawp  src, pawru lim, pawru *did )
	{ return mbe_wctou32( src, lim, did, 0 ); }

pawmsg libpawmbe_putllc( void *dst, pawru cap, pawru *did, pawllc llc )
	{ return mbe_u32towc( dst, cap, did, &llc, sizeof(pawllc) ); }
