include $(TOP_DIR)path.mak
PRJ:=libpaw-checks
INC_DIR:=$(TOP_DIR)include $(TOP_DIR)src/libpaw
C_INCS:=$(SRC_DIR)/libpaw.h $(call seekall,$(INC_DIR)/paw,*.h)
INC_DIRS:=$(INC_DIR)$(OS_PATH_SEP)$(INC_DIRS))
OBJ_DIR:=libpaw/objs
SRC_DIRS:=$(wildcard $(SRC_DIR)/*/)
SRC_DIRS:=$(filter-out $(wildcard $(SRC_DIR)/*/*/),$(SRC_DIRS))
SRC_DIRS+=$(wildcard $(SRC_DIR)/*/$(PAW_OS)/)
SRC_DIRS:=$(SRC_DIRS:$(SRC_DIR)/%/=%)
OBJ_DIRS:=$(OBJ_DIR) $(SRC_DIRS:%=$(OBJ_DIR)/%)
SUB_DIRS:=libpaw $(OBJ_DIRS)
DST_DIRS:=$(foreach m,$(PAW_MODES),$(foreach d,$(SUB_DIRS),$(PAW_OS_DIR)/$m/$d))
C_SRCS:=$(wildcard $(SRC_DIR)/*.c $(SRC_DIR)/pawval/*.c)
C_OBJS:=$(C_SRCS:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)
$(info C_OBJS=$(C_OBJS))
LIBS:=cglm

$(call mkdirs,$(DST_DIRS))

all: build

build: $(foreach m,$(PAW_MODES),$(PAW_OS_DIR)/$m/libpaw-checks.elf)

clean: $(PAW_MODES:%=$(PAW_OS_DIR)/%)
	rm $(SUB_DIRS:%=$</%/*.*)
	rm $</$(SUB_DIRS)
	rm $</libpaw.elf

$(PAW_OS_DIR)/all/libpaw$(LIB_EXT): $(C_OBJS:%=$(PAW_OS_DIR)/all/%)
	$(CC) $(PAW_OS_ABI_OPTS) -shared -o $@ $^ $(LIBS:%=-l %)
$(PAW_OS_DIR)/debug/libpaw$(LIB_EXT): $(C_OBJS:%=$(PAW_OS_DIR)/debug/%)
	$(CC) $(PAW_OS_ABI_OPTS) -shared -ggdb -o $@ $^ $(LIBS:%=-l %)
$(PAW_OS_DIR)/timed/libpaw$(LIB_EXT): $(C_OBJS:%=$(PAW_OS_DIR)/timed/%)
	$(CC) $(PAW_OS_ABI_OPTS) -shared -o $@ $^ $(LIBS:%=-l %)
$(PAW_OS_DIR)/tests/libpaw$(LIB_EXT): $(C_OBJS:%=$(PAW_OS_DIR)/tests/%)
	$(CC) $(PAW_OS_ABI_OPTS) -shared -ggdb -o $@ $^ $(LIBS:%=-l %)

$(PAW_OS_DIR)/all/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(PAW_OS_ABI_OPTS) $(INC_DIR:%=-I"%") -std=gnu11 -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(PAW_OS_DIR)/debug/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(PAW_OS_ABI_OPTS) $(INC_DIR:%=-I"%") -std=gnu11 -ggdb -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(PAW_OS_DIR)/timed/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(PAW_OS_ABI_OPTS) $(INC_DIR:%=-I"%") -std=gnu11 -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(PAW_OS_DIR)/tests/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(PAW_OS_ABI_OPTS) $(INC_DIR:%=-I"%") -std=gnu11 -ggdb -o $@ -c $(CAUGHT_SHELL_CWD)/$<

$(SRC_DIR)/%.c: $(C_INCS)

.PHONY: all build clean

