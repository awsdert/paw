#include <libpaw.h>

typedef struct
{
	pawref fd;
	pawzu size;
	void  *list;
	void  *data;
} chkvm;

pawd libpaw_checks( pawd argc, npawts *argv )
{
	chkvm vm = {0};
	pawd did = npawsys_open
	(
		&(vm.fd),
		NPAWTC_C("chkvm.pawvm"),
		NPAWFSO_BIAS_OPEN_OR_MAKE, false,
		NPAWFSO_MODE_ANY_DUPLEX,
		NPAWFSO_PERM_ANY_DUPLEX
	);
	if ( did != 0 )
		return -1;
}
pawd main( pawd argc, npawts *argv )
	{ return libpaw_checks( argc, argv ) ? EXIT_FAILURE : EXIT_SUCCESS; }
