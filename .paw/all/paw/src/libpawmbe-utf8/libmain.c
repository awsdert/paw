#include <paw/mbetext.h>

/* libpaw's multi-byte encoder will look for these to convert characters
 * to/from encodings */
pawcsc libpawmbe_getllc( pawp src, pawru lim, pawru *did )
{
	pawru  i = 1, use = 0, len = lim / sizeof(pawmbc), end = 0;
	pawmbs txt = src;
	pawmbc c = 0;
	pawcsc csc = 0;
	if ( !len )
		return 0;
	c = *txt;
	*did = sizeof(pawmbc);
	if ( !(c & 0x80) )
		return c;
	for ( use = 0; c & 0x80; ++use, c <<= 1 );
	csc = c >> use; ++use;
	if ( use > PAWMBC_MAX_ENCODED_CHARS )
		return -PAWMSGID_INVALIDSEQ;
	if ( len < use )
		return -PAWMSGID_INCOMPLETE;
	end = (len < use) ? len : use;
	for ( ; i < end; ++i )
	{
		c = txt[i];
		if ( (c & 0xC0) != 0x80 )
		{
			csc <<= (use - i) * 6;
			*did = i * sizeof(pawmbc);
			return -PAWMSGID_INVALIDSEQ;
		}
		csc <<= 6;
		csc |= c & 0x3F;
	}
	*did = i * sizeof(pawmbc);
	return csc;
}

pawmsg libpawmbe_putllc( void *dst, pawru cap, pawru *did, pawllc csc )
{
	pawru   len = 0, lz = __builtin_clz(csc), width = PAWLLC_WIDTH - lz;
	pawmbc  C[PAWMBC_MAX_ENCODED_CHARS] = {0};
	if ( width > PAWMBC_MAX_ENCODED_WIDTH )
		return PAWMSGID_NOT_RANGED;
	if ( csc < 0x80 )
	{
		/* Regular ASCII */
		++len;
		C[0] = csc;
	}
	else
	{
		pawru i = PAWMBC_MAX_ENCODED_CHARS;
		for ( ; width >= 6; C[--i] = 0x80 | (csc & 0x3F), csc >>= 6 );
		C[i] = (((pawmbc)0xFC) << (width + 1)) | csc;
	}
	len *= sizeof(pawmbc);
	if ( len > cap )
		return PAWMSGID_NOT_ENOUGH;
	pawrawcpy( dst, C, len );
	*did = len;
	return 0;
}
