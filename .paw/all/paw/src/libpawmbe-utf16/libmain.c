#include <paw/mbetext.h>

/* libpaw's multi-byte encoder will look for these to convert characters
 * to/from encodings */
pawcsc libpawmbe_getllc( pawp src, pawru lim, pawru *did )
{
	pawhs txt = src;
	pawhc c = txt[0];
	if ( lim < sizeof(pawhc) )
		return -PAWMSGID_INCOMPLETE;
	if ( PAWINTU_BEWTEEN(0xDC00,c,0xDFFF) )
		return -PAWMSGID_INVALIDPOS;
	if ( PAWINTU_BEWTEEN(0xD800,c,0xDBFF) )
	{
		if ( lim < sizeof(pawllc) )
			return -PAWMSGID_INCOMPLETE;
		*did = sizeof(pawllc);
		return ((pawllc)(c & 0x3FF) << 10) | (txt[1] & 0x3FF);
	}
	*did = sizeof(pawhc);
	return  c;
}

pawmsg libpawmbe_putllc( void *dst, pawru cap, pawru *did, pawllc csc )
{
	pawhc C[PAWHC_MAX_ENCODED_CHARS+1] = {0};
	pawru len = 0;
	if ( csc > 0x10FFFF )
		return PAWMSGID_INVALIDSEQ;
	else if ( csc < 0xD800 )
	{
		len = 1;
		C[0] = csc;
	}
	else if ( csc < 0xE000 )
		return PAWMSGID_INVALIDSEQ;
	else if ( csc < 0x10000 )
	{
		len = 1;
		C[0] = csc;
	}
	else
	{
		len = 2;
		C[0] = 0xD800 | (csc >> 10);
		C[1] = 0xDC00 | (csc & 0x3F);
	}
	len *= sizeof(pawhc);
	*did = len;
	if ( len > cap )
		return PAWMSGID_NOT_ENOUGH;
	pawrawcpy( dst, C, len );
	return 0;
}
