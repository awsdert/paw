#define NPAW_BUILD
#include <paw/libexec.h>

#ifdef CAUGHT_OS_WAS_api_msw
#	include <windows.h>
BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
	switch ( fdwReason ) {
	case DLL_PROCESS_ATTACH: return (pawlib_pid_attach() == 0) ? TRUE : FALSE;
	case DLL_PROCESS_DETACH: pawlib_pid_detach(); break;
	case DLL_THREAD_ATTACH:  return (pawlib_tid_attach() == 0) ? TRUE : FALSE;
	case DLL_THREAD_DETACH:  pawlib_tid_detach(); break;
	default: return FALSE;
	}
	return TRUE;
}
#else
/* I'll check later if this is right */
static void libdetach(void) __attribute__((destructor))
{
	if ( getpid() == gettid() )
		pawlib_pid_detach();
	else
		pawlib_tid_detach();
}
#if 0
/* Not sure if this would work out */
static void libsig( int sig )
{
	switch ( sig )
	{
	case SIGABRT: pawlib_tid_detach(); break;
	}
}
#endif
static void libattach(void) __attribute__((constructor))
{
	//signal(SIGABRT,libsig);
	if ( getpid() == gettid() )
	{
		if ( pawlib_pid_attach() < 0 )
			exit(EXIT_FAILURE);
	}
	else
	{
		if ( pawlib_tid_attach() < 0 )
			exit(EXIT_FAILURE);
	}
}
#endif
