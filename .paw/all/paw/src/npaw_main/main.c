#include <libpaw.h>
pawb _pawshoulappterm = 0, *pawshouldappterm = &_pawshoulappterm;
__thread pawb _pawshoulantterm = 0, *pawshouldantterm = &_pawshoulantterm;
static void appsig( int sig )
{
	switch ( sig )
	{
	case SIGABRT: exit(); break;
	case SIGTERM: _pawshouldappterm = 1; break;
	}
}
pawd libpawmbe_ts2mbs( pawmem *dst, npawts txt, pawru len )
{
	return -1;
}

pawd tmain( pawd argc, npawts **argv )
{
	signal(SIGABRT,appsig);
	signal(SIGTERM,appsig);
	pawmem tmp = {0};
	pawd i = 0, max = argc + 1;
	pawmbc *args = NULL, *arg = NULL;
	pawru size = sizeof(pawtc*) * max, used = 0;
	pawru *lengths = alloca( sizeof(pawru) * argc );
	for ( i = 0; i < argc; i++ )
	{
		lengths[i] += pawlslen( argv[i] );
		size += lengths[i] + 1;
	}
	size *= sizeof(pawmbc);
	args = alloca( size );
	memset( args, 0, size );
	for ( i = 0; i < argc; i++ )
	{
		arg = args + argc + 1 + used;
		if ( libpawmbe_ts2mbs( &tmp, argv[i], lengths[i] ) < 0 )
		{
			pawmem_fitcap( &tmp, 0 );
			return -1;
		}
		memcpy( arg, tmp->top, tmp->lim );
		used += lengths[i] + 1;
	}
	pawmem_fitcap( &tmp, 0 );
	return pawmain( argc, (pawmbc**)args, lengths );
}

#ifdef PAW_ON_MSW
#include <windows.h>
#include <wchar.h>
#include <stdio.h>

int __cdecl wWinMain
	( HINSTANCE hInst, HINSTANCE hPrev, LPWSTR lpCmdLine, int nShowCmd )
{
	pawd i = 0, argc = 0;
	(void)hInst; (void)hPrev; (void)lpCmdLine; (void)nShowCmd;
	LPWSTR *argv = CommandLineToArgvW( GetCommandLineW(), &nArgs );
	if ( !argv )
	{
		wprintf( L"CommandLineToArgvW failed\n" );
		return 0;
	}
	pawd res = tmain( argc, argv );
	LocalFree((void*)argv);
	return (res < 0) ? 0 : 1;
}
#else
int main( int argc, char *argv )
	{ return (tmain( argc, argv ) != 0) ? EXIT_FAILURE : EXIT_SUCCESS; }
#endif
