include $(TOP_DIR)abi.mak
PRJ:=libpawmbe-utf32
OUT:=$(PRJ)$(LIB_EXT)
INC_DIR:=$(TOP_DIR)include
INC_FILES:=$(wildcard $(INC_DIR)/*.h)
INC_FILES:=$(filter-out $(INC_DIR)/template% $(INC_DIR)/inherit%,$(INC_FILES))
C_INCS:=$(call seekall,$(INC_DIR)/paw,*.h)

CPATH:=$(SRC_DIR)$(OS_PATH_SEP).$(OS_PATH_SEP)$(INC_DIR)
SRC_DIRS:=$(wildcard $(SRC_DIR)/*/)
SRC_DIRS:=$(filter-out $(wildcard $(SRC_DIR)/*/*/),$(SRC_DIRS))
SRC_DIRS+=$(wildcard $(SRC_DIR)/*/$(NPAW)/)
SRC_DIRS:=$(SRC_DIRS:$(SRC_DIR)/%/=%)

OBJ_DIRS:=$(call obj_dirs_are,paw,$(PRJ),$(SRC_DIRS))
PAW_DIRS:=$(ALL_PAW_DIRS) $(call mode_dirs_are,$(NPAW_DIR),paw,$(OBJ_DIRS))

C_SRCS:=$(wildcard $(SRC_DIR)/*.c)
C_OBJS:=$(C_SRCS:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)
LIBS:=cglm

all: build

build: $(PAW_DIRS:%=%/) $(foreach m,$(PAW_MODES),$(NPAW_DIR)/$m/paw/$(OUT))

clean: $(PAW_MODES:%=$(NPAW_DIR)/%)
	rm $(SUB_DIRS:%=$</%/*.*)
	rm $</$(SUB_DIRS)
	$(if $(wildcard $</$(PRJ).*),rm $(wildcard $</$(PRJ).*))

BFLAGS:=$(NPAW_ABI_OPTS) -shared
$(NPAW_DIR)/all/$(OUT): $(C_OBJS:%=$(NPAW_DIR)/all/%)
	$(CC) $(BFLAGS)       -o $@ $^ $(LIBS:%=-l %)
$(NPAW_DIR)/debug/$(OUT): $(C_OBJS:%=$(NPAW_DIR)/debug/%)
	$(CC) $(BFLAGS) -ggdb -o $@ $^ $(LIBS:%=-l %)
$(NPAW_DIR)/timed/$(OUT): $(C_OBJS:%=$(NPAW_DIR)/timed/%)
	$(CC) $(BFLAGS)       -o $@ $^ $(LIBS:%=-l %)
$(NPAW_DIR)/tests/$(OUT): $(C_OBJS:%=$(NPAW_DIR)/tests/%)
	$(CC) $(BFLAGS) -ggdb -o $@ $^ $(LIBS:%=-l %)

CFLAGS+=$(NPAW_ABI_OPTS) $(INC_DIR:%=-I"$(CAUGHT_SHELL_CWD)/%")
$(NPAW_DIR)/all/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS)       -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(NPAW_DIR)/debug/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -ggdb -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(NPAW_DIR)/timed/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS)       -o $@ -c $(CAUGHT_SHELL_CWD)/$<
$(NPAW_DIR)/tests/$(OBJ_DIR)/%.c.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -ggdb -o $@ -c $(CAUGHT_SHELL_CWD)/$<

$(SRC_DIR)/%.c: $(C_INCS)

.PHONY: all build clean

