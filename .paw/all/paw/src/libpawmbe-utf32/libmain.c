#include <paw/mbetext.h>

/* libpaw's multi-byte encoder will look for these to convert characters
 * to/from encodings */
pawcsc libpawmbe_getllc( pawp src, pawru lim, pawru *did )
{
	if ( lim < sizeof(pawllc) )
		return -PAWMSGID_INCOMPLETE;
	*did = sizeof(pawllc);
	return *(pawlls)src;
}

pawmsg libpawmbe_putllc( void *dst, pawru cap, pawru *did, pawllc csc )
{
	if ( lim < sizeof(pawllc) )
		return PAWMSGID_NOT_ENOUGH;
	*did = sizeof(pawllc);
	*(pawllc)dst = csc;
	return 0;
}
