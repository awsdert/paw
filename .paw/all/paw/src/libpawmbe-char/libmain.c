#define NPAW_BUILD 1
#include <paw/mbetext.h>
#include <errno.h>
#include <uchar.h>

/* This is a hackfix module for supporting local encoding. I may implement
 * something better in the future but for now this will suffice as it does
 * not require me to explicitly check the locale. */

/* libpaw's multi-byte encoder will look for these to convert characters
 * to/from encodings */
pawcsc libpawmbe_getllc( pawp src, pawru lim, pawru *did )
{
	/* Must provide this to make thread safe, despite the fact the function
	 * should NEVER have been checking any kind of previous state. Only needed
	 * to know where to begin and where to cut off which src and lim provide.
	 * It's also a native object which is why we define NPAW_BUILD at the top,
	 * luckily this is of the only 2 encodings that must be supported natively,
	 * all others should be do-able via the public ABI. */
	mbstate_t mb = {0};
	pawcsc csc = 0;
	pawru res = mbrtoc32( &csc, src, lim, &mb );
	switch ( res )
	{
	case 0: *did = 1; return csc;
	case (pawru)-1: return -PAWMSGID_INVALIDSEQ;
	case (pawru)-2: return -PAWMSGID_INCOMPLETE;
	case (pawru)-3: return 0;
	}
	did = res;
	return csc;
}

pawmsg libpawmbe_putllc( void *dst, pawru cap, pawru *did, pawllc csc )
{
	mbstate_t mb = {0};
	pawc C[MB_LEN_MAX+1] = {0};
	pawrd res = c32rtomb( C, csc, &mb ), end = (res < 0) ? 1 : res;
	if ( end >= cap )
	{
		*did = cap;
		return PAWMSGID_NOT_ENOUGH;
	}
	*did = end;
	memcpy( dst, C, end );
	return (res < 0) ? PAWMSGID_TERMINAL : 0;
}
