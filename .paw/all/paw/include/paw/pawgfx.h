#include "pawval.h"

typedef enum
{
	PAWGFX_COMPUTE_SHADER,
	PAWGFX_VERTICE_SHADER,
	PAWGFX_SIMPLEX_SHADER,
	PAWGFX_TESCTRL_SHADER,
	PAWGFX_TESEVAL_SHADER,
	PAWGFX_COLOURS_SHADER
} PAWGFX_STYPE;

typedef enum
{
	PAWGFX_COUNT_OF_1 = 0,
	PAWGFX_COUNT_OF_2,
	PAWGFX_COUNT_OF_3,
	PAWGFX_COUNT_OF_4,
	PAWGFX_COUNT_AS_BGRA
} PAWGFX_COUNT;

typedef enum
{
	/* pawe8d */
	PAWGFX_VTYPE_CHAR = 0,
	/* pawe8u */
	PAWGFX_VTYPE_UCHAR,
	PAWGFX_VTYPE_BOOL = PAWGFX_VTYPE_UCHAR,
	/* pawe16d */
	PAWGFX_VTYPE_SHORT,
	/* pawe16u */
	PAWGFX_VTYPE_USHORT,
	/* pawe32d */
	PAWGFX_VTYPE_INT,
	/* pawe32u */
	PAWGFX_VTYPE_UINT,
	/* I neither know nor care what these are about at the moment, just
	 * defining them to match up GL_INT_2_10_10_10_REV,
	 * GL_UNSIGNED_INT_2_10_10_10_REV, etc */
	PAWGFX_VTYPE_INT_2_10_10_10_REV,
	PAWGFX_VTYPE_UINT_2_10_10_10_REV,
	PAWGFX_VTYPE_INT_10F_11F_11F_REV,
	PAWGFX_VTYPE_UINT_10F_11F_11F_REV,
	/* pawe32d[2] */
	PAWGFX_VTYPE_IVEC2,
	/* pawe32d[3] */
	PAWGFX_VTYPE_IVEC3,
	/* pawe32d[4] */
	PAWGFX_VTYPE_IVEC4,
	/* pawe32u[2] */
	PAWGFX_VTYPE_UVEC2,
	/* pawe32u[3] */
	PAWGFX_VTYPE_UVEC3,
	/* pawe32u[4] */
	PAWGFX_VTYPE_UVEC4,
	/* pawf */
	PAWGFX_FLOAT,
	/* pawf[2] */
	PAWGFX_VTYPE_VEC2,
	/* pawf[3] */
	PAWGFX_VTYPE_VEC3,
	/* pawf[4] */
	PAWGFX_VTYPE_VEC4,
	/* pawlf */
	PAWGFX_DOUBLE,
	/* pawlf[2] */
	PAWGFX_VTYPE_DVEC2,
	/* pawlf[3] */
	PAWGFX_VTYPE_DVEC3,
	/* pawlf[4] */
	PAWGFX_VTYPE_DVEC4
} PAWGFX_VTYPE;

/* glGenProgram semantics */
PAW_APP_CB pawe32u pawgfxnewbinary( void );
typedef struct
{
	paws code;
	pawe32u id;
	PAWGFX_STYPE stype;
} pawgfxshader;
/* glGenShader semantics */
PAW_APP_CB void    pawgfxnewshaders( pawgfxshader *shaders, pawru len );
PAW_QCK_CB pawe32u pawgfxnewshader( PAWGFX_STYPE stype, paws code )
{
	pawgfxshader shader = { code, -1, stype };
	pawgfxnewshaders( &shader, 1 );
	return shader.id;
}
/* For glGenBuffer semantics */
PAW_APP_CB void    pawgfxnewbuffers( pawe32u *bufs, pawe32u count );
PAW_QCK_CB pawe32u pawgfxnewbuffer( void )
	{ pawe32u id = -1; pawgfxnewbuffers( &id, 1 ); return id; }

/* For glGenVertexArrays semantics - stores bindings given for symbols
 * These links helped me understand them better:
 * https://stackoverflow.com/questions/11821336/what-are-vertex-array-objects
 * https://web.archive.org/web/20150225192608/http://www.arcsynthesis.org/gltut/Positioning/Tutorial%2005.html
 * */
PAW_APP_CB void    pawgfxnewdesigns( pawe32u *designs, pawru len );
PAW_QCK_CB pawe32u pawgfxnewdesign( void )
	{ pawe32u design = -1; pawgfxnewdesigns( &design, 1 ); return design; }
/* For glBindVertexArray(ID) semantics */
PAW_APP_CB void    pawgfxexpectdesign( pawe32u design );
/* For glBindVertexArray(0) semantics */
PAW_APP_CB void    pawgfxignoredesign( void );
typedef struct
{
	paws    name;
	pawe32u index;
	/* I don't see any reason for these to potentially be different during
	 * the course of the app's lifetime so just putting them here as a
	 * convenience location */
	PAWGFX_VTYPE vtype;
	PAWGFX_COUNT count;
} pawgfxsymbol;
/* For glGetUniformLocation semantics */
PAW_ANT_CB void    pawgfxfindglobals( pawe32u binary, pawgfxsymbol *syms, pawru len );
PAW_QCK_CB pawe32u pawgfxfindglobal( pawe32u binary, paws name )
{
	pawgfxsymbol sym = { name, -1, 0, 1 };
	pawgfxfindglobals( binary, &sym, 1 );
	return sym.index;
}
/* For glGetAttribLocation semantics */
PAW_APP_CB pawe32u pawgfxfindstatics( pawe32u binary, pawgfxsymbol *syms, pawru len );
PAW_QCK_CB pawe32u pawgfxfindstatic( pawe32u binary, paws name )
{
	pawgfxsymbol sym = { name, -1, 0, 1 };
	pawgfxfindstatics( binary, &sym, 1 );
	return sym.index;
}
/* For glEnableVertexAttribArray semantics */
PAW_APP_CB void    pawgfxexpectsymbols( pawe32u const *indices, pawru len );
PAW_FQK_CB void    pawgfxexpectsymbol( pawe32u index )
	{ pawgfxexpectsymbols( &loc, 1 ); }
/* For glDisableVertexAttribArray semantics */
PAW_APP_CB void    pawgfxignoresymbols( pawe32u *indices, pawru len );
PAW_FQK_CB void    pawgfxignoresymbol( pawe32u index );
	{ pawgfxignoresymbols( &index, 1 ); }
typedef struct
{
	pawgfxsymbol const *sym;
	pawb normalise;
	pawe32d stride;
	void   *offset;
} pawgfxsymcfg;
/* glVertexAttribPointer semantics */
PAW_APP_CB void    pawgfxsetsymptrs( pawgfxsymcfg const *symcfgs, pawru len );
PAW_QCK_CB void    pawgfxsetsymptr
	( pawgfxsymbol const *symbol, pawb normalise, pawe32d stride, void *offset )
{
	pawgfxsymcfg symcfg = { sym, normalise, stride, offset };
	pawgfxsetsymptrs( &symcfg, 1 );
}
