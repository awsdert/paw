#include "pawval.h"
PAW_C_API_OPEN
typedef void (*pawatexit_cb)( void );
extern pawb *pawshouldappterm;
__thread extern pawb *pawshouldantterm;
PAW_ANT_API pawed pawant_atexit( pawatexit_cb cb );
PAW_ANT_API void pawantexit( pawmsg msg );
PAW_ANT_API void pawantabort( pawmsg msg );
PAW_APP_API pawed pawapp_atexit( pawatexit_cb cb );
PAW_ANT_API void pawappexit( pawmsg msg );
PAW_ANT_API void pawappabort( pawmsg msg );
PAW_C_API_SHUT
