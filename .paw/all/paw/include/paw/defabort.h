/* ONLY include this file where you quickly include <paw/mayabort.h> (or
 * similar if the standard libraries decide to include such headers */
#ifndef ABORT
#	define ABORT 1
#endif

#if ABORT != 1
#	undef ABORT
#	define ABORT 1
#endif
