#pragma once
#include "antexec.h"
PAW_C_API_OPEN
extern pawb *pawshouldlibterm;
extern pawmsg pawlibinit( void );
extern pawmsg pawlibexec( void );
extern void   pawlibterm( void );
PAW_C_API_SHUT
