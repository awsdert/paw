#pragma once
#include "pawalltypes.h"

#ifdef NPAW_BUILD
PAW_QCK_CB pawmsg npawerrno2msg( pawrd err )
{
	switch ( err )
	{
	case 0: return 0;
	case EBUSY:		return PAWMSGID_BUSY;
	case EAGAIN:	return PAWMSGID_RETRY;
	case EBADFD:	return PAWMSGID_WSA_EBADF;
	case EACCES:	return PAWMSGID_WSA_EACCES;
	case EINVAL:	return PAWMSGID_WSA_EINVAL;
	case EALREADY:	return PAWMSGID_WSA_EALREADY;
	case ENOMEM:	return PAWMSGID_NOT_ENOUGH_MEMORY;
	}
	return -1;
}
PAW_QCK_CB pawrd npawmsg2errno( pawmsg msg )
{
	switch ( msg )
	{
	case 0: return 0;
	case PAWMSGID_BUSY:			return EBUSY;
	case PAWMSGID_RETRY:		return EAGAIN;
	case PAWMSGID_WSA_EINVAL:	return EINVAL;
	case PAWMSGID_WSA_EBADF:	return EBADFD;
	case PAWMSGID_WSA_EACCES:	return EACCES;
	case PAWMSGID_WSA_EALREADY:	return EALREADY;
	case PAWMSGID_NOT_ENOUGH_MEMORY: return ENOMEM;
	}
	return -1;
}
#ifdef COMPILE_MPAW_LOCALS
extern PAW_QCK_CB pawmsg npawerrno2msg( pawrd err );
extern PAW_QCK_CB pawrd npawmsg2errno( pawmsg msg );
#endif
#endif
