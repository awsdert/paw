#pragma once
#include "pawraw.h"

PAW_C_API_OPEN

/* Intentionally done the same as original codes on linux. Win32 compatibiliry
 * is great but I will not put optimising for it before optimising for linux. */
typedef enum
{
	/* The 1st 5 MUST NOT move as they map directly to linux codes */
	PAWALTER_ABSAT = 0, /* abs = n; n = abs */
	PAWALTER_UPABS = 1, /* abs += n; n = abs */
	PAWALTER_BYEND = 2, /* abs = end - n; n = abs */
	PAWALTER_SKIP0 = 3, /* 1st abs >= n where byte values > 0; n = abs */
	PAWALTER_NEXT0 = 4, /* 1st abs >= n where byte values = 0; n = abs */
	/* Custom functionality from here */
	PAWALTER_ORGAT = 5, /* align at/by n, default is sizeof(npawjd) */
	PAWALTER_ENDAT = 6, /* end = (n <= cap) ? n : cap; n = end */
	PAWALTER_CAPAT = 7, /* cap = aligned n; n = cap */
	PAWALTER_UPEND = 8, /* end += n; if ( end > cap ) end = cap; n = end */
	PAWALTER_UPCAP = 9  /* cap += aligned n; n = cap */
} pawalter;
#define PAWALTER_MAX (PAWALTER_UPCAP + 1)

PAW_FQK_CB npawsys dpaw_retdid( npawsys msg, pawru did )
	{ return (msg << PAWSYS_SHIFT_MSG_BY) | did; }

PAW_FQK_CB dpaw dpawnew( pawmbs file, pawru line, pawmm const *mm )
	{ return (dpaw){ line, file, mm }; }
#define DPAWNEW(MM) dpawnew( PAWMBS_ERRSRC, MM )

PAW_QCK_CB void dpaw_expire( dpaw *data )
	{ (void)data->mm->alterCB( data, NULL, PAWALTER_CAPAT ); }

#define T pawrd
#define NAME dpaw
#include "template_dpaw.h"

PAW_FQK_CB npawsys dpaw_useval( dpaw *data, pawalter alter, pawrd *val )
	{ return _dpaw_useval( data, alter, val ); }
PAW_FQK_CB npawsys dpaw_cpyraw( dpaw *data, pawrd abs, void *dst, pawru end )
	{ return _dpaw_cpyraw( data, abs, dst, end ); }
PAW_FQK_CB npawsys dpaw_getraw( dpaw *data, void *dst, pawru end )
	{ return _dpaw_getraw( data, dst, end ); }
PAW_FQK_CB npawsys dpaw_setraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw_setraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw_putraw( dpaw *data, pawp src, pawru end )
	{ return _dpaw_putraw( data, src, end ); }
PAW_FQK_CB npawsys dpaw_fitraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw_fitraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw_capraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw_capraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw_insraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw_insraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw_catraw( dpaw *data, pawp src, pawru end )
	{ return _dpaw_catraw( data, src, end ); }

extern pawmm const *pawmm_mini;
extern pawmm const *pawmm_page;
#ifdef NPAW_BUILD
extern pawmm const *pawmm_mpaw;
#endif



PAW_QCK_CB mpawrawdst mpawrawdstnew( pawmbs file, pawru line, pawmm const *mm )
	{ return (mpawrawdst){ dpawnew(file,line,mm), 0, NULL, 0, 0 }; }

extern pawmm *pawmm_file;
typedef struct
{
	dpaw data;
	npawsys fso;
	npawsys fsm;
	pawraw map;
	pawrd end;
	pawrd cap;
	pawrd abs;
} mpawfso;
PAW_QCK_CB mpawfso mpawfsonew( pawmbs file, pawru line )
	{ return (mpawfso){ dpawnew(file,line,pawmm_file), 0, 0, {0,NULL}, 0, 0 }; }

#ifdef _LARGEFILE64_SOURCE
#define T pawe64d
#define NAME dpaw64
#include "template_dpaw.h"

PAW_FQK_CB npawsys dpaw64_useval( dpaw *data, pawalter alter, pawe64d *val )
	{ return _dpaw64_useval( data, alter, val ); }
PAW_FQK_CB npawsys dpaw64_cpyraw( dpaw *data, pawrd abs, void *dst, pawru end )
	{ return _dpaw64_cpyraw( data, abs, dst, end ); }
PAW_FQK_CB npawsys dpaw64_getraw( dpaw *data, void *dst, pawru end )
	{ return _dpaw64_getraw( data, dst, end ); }
PAW_FQK_CB npawsys dpaw64_setraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw64_setraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw64_putraw( dpaw *data, pawp src, pawru end )
	{ return _dpaw64_putraw( data, src, end ); }
PAW_FQK_CB npawsys dpaw64_fitraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw64_fitraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw64_capraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw64_capraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw64_insraw( dpaw *data, pawrd abs, pawp src, pawru end )
	{ return _dpaw64_insraw( data, abs, src, end ); }
PAW_FQK_CB npawsys dpaw64_catraw( dpaw *data, pawp src, pawru end )
	{ return _dpaw64_catraw( data, src, end ); }

PAW_QCK_CB mpawraw mpawfso64new( pawmbs file, pawru line )
	{ return (mpawfso64){ dpawnew(file,line,pawmm_file64), 0, 0, {0,NULL}, 0, 0 }; }
#endif

PAW_C_API_SHUT
