#pragma once

/* The IO here is for interaction with the terminal, not a spawning thread */

enum
{
	PAWIO_INVALID = 0,
	PAWIO_STDGET,
	PAWIO_STDPUT,
	PAWIO_STDERR
};
