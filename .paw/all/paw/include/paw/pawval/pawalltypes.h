#pragma once

/*
#ifndef __GNUC_EXECUTION_CHARSET_NAME
#	error "Need __GNUC_EXECUTION_CHARSET_NAME! Must set -fexec-charset=utf-#"
#	include <paw/defabort.h>
#endif
*/
/*
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmultichar"
#if '£' != 0xC2A3
#   error "Must set -fexec-charset=utf-8 and -finput-charset=utf-8"
#	include <paw/defabort.h>
#endif
#pragma GCC diagnostic pop
*/

#ifndef __GNUC__
#	error "Paw ABIs REQUIRE a GNU ABI compatible compiler"
#	include <paw/defabort.h>
#endif

#ifndef __has_include
#	error "This compiler does not support __has_include()"
#	include <paw/defabort.h>
#endif

#if (__SIZEOF_POINTER__ * __CHAR_BIT__) < AUTOC_ARCH_GREG_WIDTH
#	error "Pointers must be of equal width the main registers!"
#	include <paw/defabort.h>
#endif

#if !__has_include(<stdbit.h>)
#	error "This compiler does not support _BitInt"
#	include <paw/defabort.h>
#endif

#include <paw/mayabort.h>

/** @brief Give cleanup attribute to an allocation
 * @note Callback must be in form of void foo(void *ptr);
 * @example
 * void autofree( void *ptr )
 * {
 *   void **ref = (void**)ptr;
 *   if ( *ref )
 *      free( *ref );
 *    *ref = NULL;
 * }
 * int foo(void)
 * {
 *    PAWAUTOTERM(autofree) void *tmp = malloc(BUFSIZ);
 *    return tmp ? 0 : -1;
 * }*/
#define PAWAUTOTERM(CB) [[gnu::cleanup(CB)]]

#ifdef BUILD_PAW
/* Not expected to be thread safe */
#	define PAW_APP_CB
/* Expected to be thread safe */
#	define PAW_ANT_CB
#else
/* Not expected to be thread safe */
#	define PAW_APP_CB	extern
/* Expected to be thread safe */
#	define PAW_ANT_CB	extern
#endif
/* Exists for template usage */
#define PAW_TYPEDEF typedef

#define _PAW_PRAGMA(CONTENT) _Pragma(#CONTENT)
#define PAW_PRAGMA(CONTENT) _PAW_PRAGMA(CONTENT)
#define _PAW_LOG(CONTENT) PAW_PRAGMA(GCC message #CONTENT)
#define PAW_LOG(CONTENT) _PAW_LOG(CONTENT)

#ifdef NPAW_BUILD
/* Treating these as mandatory */
#define _GNU_SOURCE 1
#if AUTOC_ARCH_GREG_WIDTH >= 32
#define _FILE_OFFSET_BITS 64
#define _LARGEFILE64_SOURCE 1
#endif
#endif

/* As an idea for languages that get compiled down to assembly, C or some
 * other low level language, they should compile the names in their language's
 * scripts to follow these schemes:
 *
 * stdnamingscheme_<language>_<type>_<name>
 * // Human readable version for UTF compiled name
 * utfnamingscheme_<language_hex_seq>_<type>_name_<name_hex_seq>
 *
 * For example lua compile functions would look like this:
 *
 * stdnamingscheme_Lua_function_print
 * // Lua function print
 * utfnamingscheme_4C7561_function_7072696E74
 *
 * The reason the names must use hex in the utf versions is just so that it's
 * accessible in ANSI C. There's always some nutter trying rando combinations,
 * may as well ease the process just a bit. Also makes it easy to offload
 * compiling to assembly or to machine code to gcc/clang and just focus on the
 * language fetaures/support/etc. It'll be the debugger's job to understand
 * where to redirect those objects to for expanding it's abilities.
 *
 * For example a developer could right a rule like "Lua & 4C7561 >
 * lua_debug_plugin" for the debugger to know where to filter watched values
 * etc through when it comes across those prefixes so that it gets told how
 * to display such stuf in it's UI */

#ifdef _DEBUG
#	define PAW_BUILD_DEBUG 1
#else
#	define PAW_BUILD_DEBUG 0
#endif

#ifdef _TIMED
#	define PAW_BUILD_TIMED 1
#else
#	define PAW_BUILD_TIMED 0
#endif

#ifdef _TESTS
#	define PAW_BUILD_TESTS 1
#else
#	define PAW_BUILD_TESTS 0
#endif

/* Unlike "release" the word "default" brings to mind a different purpose for
 * the same build, in other words if no other variant takes priority or can be
 * found then default to this one */
#define PAW_BUILD_DEFAULT !(PAW_BUILD_DEBUG | PAW_BUILD_TIMED | PAW_BUILD_TIMED)

/* Triggers autocomplete since __attribute by itself usually doesn't. Plus it's
 * shorter :)  */
#define PAWATTR(ATTR) __attribute__(ATTR)
#define PAWATTR_HOT 	PAWATTR((hot))
#define PAWATTR_COLD	PAWATTR((cold))
#define PAWATTR_PURE	PAWATTR((pure))
#define PAWATTR_CONST	PAWATTR((const))
#define PAWATTR_FLATTEN	PAWATTR((flatten))
#define PAWATTR_FINLINE PAWATTR((always_inline))
/* Useful for pawinit() & mpawinit() */
#define PAWATTR_UNUSED	PAWATTR((warn_unused_result))
#define PAWATTR_TUNION	PAWATTR((__transparent_union__))

#define PAWATTR_ZONEVIS(ZONE) PAWATTR((visibility(ZONE))
/* Insists that the parameter at PARAM_POS be ... and when called ... must end
 * with NULL */
#define PAWATTR_SENTINAL(PARAM_POS) PAWATTR((sentinel(PARAM_POS)))

#define PAW_QCK_CB __inline __cdecl
/* Expression Callback */
#define PAW_EXP_CB PAWATTR_PURE PAW_QCK_CB
#define PAW_FTN_CB PAWATTR_FLATTEN PAW_QCK_CB
#define PAW_FQK_CB PAWATTR_FINLINE PAW_QCK_CB

#ifdef _LIBPAW
#	define PAW_LOCAL(LOCAL) lm##LOCAL
#else
#	define PAW_LOCAL(LOCAL) l##LOCAL
#endif

#define PAW_INDIRECT( TXT ) TXT
#define PAW_INDIRECT2( TXT ) PAW_INDIRECT( TXT )
#define PAW_INDIRECT3( TXT ) PAW_INDIRECT2( TXT )
#define PAW_INDIRECT4( TXT ) PAW_INDIRECT3( TXT )
#define PAW_INDIRECT5( TXT ) PAW_INDIRECT4( TXT )
#define PAW_INDIRECT6( TXT ) PAW_INDIRECT5( TXT )
#define PAW_INDIRECT7( TXT ) PAW_INDIRECT6( TXT )
#define PAW_INDIRECT8( TXT ) PAW_INDIRECT7( TXT )

#define PAW_TOSTRING( TXT ) #TXT
#define PAW_TOSTRING2( TXT ) PAW_TOSTRING( TXT )
#define PAW_TOSTRING3( TXT ) PAW_TOSTRING2( TXT )
#define PAW_TOSTRING4( TXT ) PAW_TOSTRING3( TXT )
#define PAW_TOSTRING5( TXT ) PAW_TOSTRING4( TXT )
#define PAW_TOSTRING6( TXT ) PAW_TOSTRING5( TXT )
#define PAW_TOSTRING7( TXT ) PAW_TOSTRING6( TXT )
#define PAW_TOSTRING8( TXT ) PAW_TOSTRING7( TXT )

#define PAW_APPENDTO( TO, TXT ) TO##TXT
#define PAW_APPENDTO2( TO, TXT ) PAW_APPENDTO( TO, TXT )
#define PAW_APPENDTO3( TO, TXT ) PAW_APPENDTO2( TO, TXT )
#define PAW_APPENDTO4( TO, TXT ) PAW_APPENDTO3( TO, TXT )
#define PAW_APPENDTO5( TO, TXT ) PAW_APPENDTO4( TO, TXT )
#define PAW_APPENDTO6( TO, TXT ) PAW_APPENDTO5( TO, TXT )
#define PAW_APPENDTO7( TO, TXT ) PAW_APPENDTO6( TO, TXT )
#define PAW_APPENDTO8( TO, TXT ) PAW_APPENDTO7( TO, TXT )

#define PAWSYM( P1,P2,P3,P4 ) P1##P2##P3##P4
#define PAWSYM2( P1,P2,P3,P4 ) PAWSYM( P1,P2,P3,P4 )
#define PAWSYM3( P1,P2,P3,P4 ) PAWSYM2( P1,P2,P3,P4 )
#define PAWSYM4( P1,P2,P3,P4 ) PAWSYM3( P1,P2,P3,P4 )
#define PAWSYM5( P1,P2,P3,P4 ) PAWSYM4( P1,P2,P3,P4 )
#define PAWSYM6( P1,P2,P3,P4 ) PAWSYM5( P1,P2,P3,P4 )
#define PAWSYM7( P1,P2,P3,P4 ) PAWSYM6( P1,P2,P3,P4 )
#define PAWSYM8( P1,P2,P3,P4 ) PAWSYM7( P1,P2,P3,P4 )

#include <autoc/cc.h>
#include <autoc/arch.h>
#include <autoc/syntax.h>
#ifdef NPAW_BUILD
#	include <autoc/sys.h>
#endif
#include <autoc/floats.h>
#include <caught.h>

#define PAW_C_API_LINK AUTOC_C_LINK
#define PAW_C_API_OPEN AUTOC_C_OPEN
#define PAW_C_API_SHUT AUTOC_C_SHUT

/* This looks a bit neater in the resulting code */
#ifndef CAST
#	define CAST(T,X) ((T)(X))
#endif

#ifdef __cplusplus
#	include <climits>
#	include <cstddef>
#	include <cstdbool>
#	include <cstdint>
#	include <cfloat>
#	include <cstdarg>
/* Who really wants to use that pointlessly long reinterpret_cast<T>(X)? */
#	ifndef CASTXX
#		define CASTXX(T,X) (reinterpret_cast<T>(X))
#	endif
#else
#	include <limits.h>
#	include <stddef.h>
#	include <stdbool.h>
#	include <stdint.h>
#	include <float.h>
#	include <stdarg.h>
#	ifdef COMPILE_MPAW_LOCALS
#		include <string.h>
#		include <wchar.h>
#		include <uchar.h>
#		include <stdlib.h>
#		include <stdio.h>
#	endif
#endif

/* BitWise Sign bit */
#define TEMPLATE_FORMULA_PAWINT_BWS(N) \
	({(__typeof__(N)) X = 1; X << (bitsof(X) - 1);})

#define PAWINT_BWS(N) _Generic((N), \
	int: INT_MIN, \
	unsigned int: INT_MIN, \
	long: LONG_MIN, \
	unsigned long: LONG_MIN, \
	long long: LLONG_MIN, \
	unsigned long long: LLONG_MIN, \
	default: TEMPLATE_FORMULA_PAWINT_BWS )

/* Count Leading Zeros */
#define TEMPLATE_FORMULA_PAWINT_CLZ(N) \
	({ \
		pawru num = 0; \
		__typeof__(N) X = N; \
		const __typeof__(N) L = TEMPLATE_FORMULA_PAWINT_BWS(N); \
		for ( __typeof__(N) X = N; X && !(X & L); X <<= 1, ++num ); \
		num; \
	})

#define PAWINT_CLZ(N) _Generic((N), \
	int: __builtin_clz, \
	unsigned int: __builtin_clz, \
	long: __builtin_clzl, \
	unsigned long: __builtin_clzl, \
	default: __builtin_clzll )

/* Count Trailing Zeros */
#define TEMPLATE_FORMULA_PAWINT_CTZ(N) \
	({ \
		pawru num = 0; \
		__typeof__(N) X = N; \
		for ( ; X && !(X & 1); X >>= 1, --num ); \
		num; \
	})

#define PAWINT_CTZ(N) _Generic((N), \
	int: __builtin_ctz, \
	unsigned int: __builtin_ctz, \
	long: __builtin_ctzl, \
	unsigned long: __builtin_ctzl, \
	long long: __builtin_ctzll, \
	unsigned long long: __builtin_ctzll, \
	default: TEMPLATE_FORMULA_PAWINT_CTZ )

/* Find First Set bit */
#define TEMPLATE_FORMULA_PAWINT_FFS(N) \
	({ \
		pawru pos = 0; \
		__typeof__(N) X = N; \
		for ( ; X && !(X & 1); X >>= 1, ++pos ); \
		pos; \
	})

#define PAWINT_FFS(N) _Generic((N), \
	int: __builtin_ffs, \
	unsigned int: __builtin_ffs, \
	long: __builtin_ffsl, \
	unsigned long: __builtin_ffsl, \
	long long: __builtin_ffsll, \
	unsigned long long: __builtin_ffsll, \
	default: TEMPLATE_FORMULA_PAWINT_FFS )

/* Find Last Set bit */
#define TEMPLATE_FORMULA_PAWINT_FLS(N) \
	({ \
		__typeof__(N) X = N; \
		pawru pos = bitsof(X); \
		const __typeof__(N) L = TEMPLATE_FORMULA_PAWINT_BWS(N); \
		for ( ; X && !(X & L); X <<= 1, ++pos ); \
		pos; \
	})

#define PAWINT_FLS(N) _Generic((N), \
	int: __builtin_fls, \
	unsigned int: __builtin_fls, \
	long: __builtin_flsl, \
	unsigned long: __builtin_flsl, \
	long long: __builtin_flsll, \
	unsigned long long: __builtin_flsll, \
	default: TEMPLATE_FORMULA_PAWINT_FLS )

/* Convenience macros.
 *
 * Division involves movement and when you move nothing you naturally should
 * expect nothing in your target that started empty, hence the boolean to 0 */

#define PAWINT_DIVFLOOR(A,B) ((B) ? (A) / (B) : 0)
#define PAWINT_DIVCIEL(A,B) (PAWINT_DIVFLOOR(A,B) + (((A) % (B)) ? 1 : 0))
#define PAWINT_MULFLOOR(A,B) (PAWINT_DIVFLOOR(A,B) * (B))
#define PAWINT_MULCIEL(A,B) (PAWINT_DIVCIEL(A,B) * (B))
#define PAWINT_DIVPAIR(A,B,Q,R) \
	do { Q = 0; R = A; if (B) { Q = R / (B); R %= (B); } } while (0)

#if -1 == ~0
#	define _PAWINT1 1
#else
#	define _PAWINT1 0
#endif

#if BITINT_MAXWIDTH > 4096
#	define PAWINT_CAPBITS 4096
#else
#	define PAWINT_CAPBITS BITINT_MAXWIDTH
#endif

#ifndef ATTR_LE
#define ATTR_LE __attribute__((scalar_storage_order ("little-endian"))
#define ATTR_BE __attribute__((scalar_storage_order ("big-endian"))
#endif

/* C++ should be using nullptr anyways */
#ifndef NULL
#	define NULL (void*)0
#endif

#define PAWPTR_SIZE	 __SIZEOF_INTPTR__
#define PAWPTR_WIDTH __INTPTR_WIDTH__
typedef void const *pawp;

/* Less likely to have ABI differences than pawb */
typedef unsigned _BitInt(1) pawb;

/* For less error prone casting to math safe types */
#define PAWBOOL_OP0( OP, N ) OP((pawb)(N))
#define PAWBOOL_OP1( N, OP ) ((pawb)(N))OP
#define PAWBOOL_OP2( A, OP, B ) ((pawb)(A)) OP ((pawb)(B))

/* Header was getting unmanagable so moved everything to dedicated headers
 * for this header to include */
#include "pawint/pawmbc.h"
#include "pawint/pawhc.h"
#include "pawint/pawllc.h"
#include "pawint/pawrd.h"
/* Half Word */
#include "pawint/pawehd.h"
/* Word */
#include "pawint/pawed.h"
/* Double Word */
#include "pawint/paweld.h"
/* Tetra Word */
#include "pawint/pawetd.h"
/* Octa Word */
#include "pawint/paweod.h"
/* Hexadeca Word */
#include "pawint/pawexd.h"
#include "pawint/pawejd.h"
#include "pawint/pawage.h"
#include "pawint/pawageNS.h"
#include "pawint/pawageQS.h"
#include "pawint/npawtc.h"
#include "pawint/npawlc.h"
#include "pawint/pawe2d.h"
#include "pawint/pawe4d.h"
#include "pawint/pawe8d.h"
#include "pawint/pawe16d.h"
#include "pawint/pawe32d.h"
#include "pawint/pawe64d.h"
#include "pawint/pawe128d.h"
#include "pawint/pawe256d.h"
#include "pawint/pawe256d.h"
#include "pawint/pawe512d.h"
#include "pawint/pawe1024d.h"
#include "pawint/pawe2048d.h"
#include "pawint/pawe4096d.h"
#include "pawint/pawreg.h"

PAW_C_API_OPEN

#include <stdbit.h>

typedef signed char schar;
typedef long long llong;

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ullong;

typedef char const *paws;

/* These names should be less confusing */
typedef uintptr_t tpawstackforkbuffer[5];
#define PAW_EXEC_STACK_FORK __builtin_setjmp
#define PAW_EXIT_STACK_FORK __builtin_longjmp

#define PAWFLT_STOREDASBASE	FLT_RADIX
#define PAWFLT_ROUNDING_DIR	FLT_ROUNDS
#define PAWFLT_MIN_DEC4TEXT	DECIMAL_DIG
#define PAWFLT_MIN_VAL_TYPE	FLT_EVAL_METHOD

#if CAUGHT_FLT_ROUNDS == 0
#	define PAW_ROUND(SIGN,N) AUTOC_ROUND_2WARDS0((SIGN),(N))
#elif CAUGHT_FLT_ROUNDS == 1
#	define PAW_ROUND(SIGN,N) AUTOC_ROUND_NEAREST((SIGN),(N))
#elif CAUGHT_FLT_ROUNDS == 2
#	define PAW_ROUND(SIGN,N) AUTOC_ROUND_2POSINF((SIGN),(N))
#elif CAUGHT_FLT_ROUNDS == 3
#	define PAW_ROUND(SIGN,N) AUTOC_ROUND_2NEGINF((SIGN),(N))
#else
#	define PAW_ROUND(SIGN,N) AUTOC_ROUND_NEARESTFROM0((SIGN),(N))
#endif

#define PAWFLT_M_ROUNDING_NOT_DEF -1
#define PAWFLT_M_ROUNDING_DIR_NIL 0
#define PAWFLT_M_ROUNDING_DIR_ONE 1
#define PAWFLT_M_ROUNDING_POS_INF 2
#define PAWFLT_M_ROUNDING_NEG_INF 3

#define PAWFLT_M_EVALUATE_NOT_DEF -1
#define PAWFLT_M_EVALUATE_AS_TYPE 0
#define PAWFLT_M_EVALUATE_MIN_DBL 1
#define PAWFLT_M_EVALUATE_MIN_LDB 2

#if 0
/* Fixed point float
 * https://en.wikipedia.org/wiki/Decimal32_floating-point_format
 *  */
typedef _Decimal32 pawefpf;
typedef _Decimal64 pawelfpf;
typedef _Decimal128 pawellfpf;
/* Geometric/Imaginary point number */
typedef _Complex32 paweipf;
typedef _Complex64 pawelipf;
typedef _Complex128 pawellipf;
#endif

typedef float AUTOC_FLT_MODE(HF) pawehspf;
typedef float AUTOC_FLT_MODE(SF) pawespf;
typedef float AUTOC_FLT_MODE(DF) pawelspf;
typedef float AUTOC_FLT_MODE(XF) pawexspf;

#if 0
typedef pawespf pawev2f __attribute__((vector_size ( 8)));
typedef pawespf pawev3f __attribute__((vector_size (12)));
typedef pawespf pawev4f __attribute__((vector_size (16)));
#endif

/* Shifting point float */
typedef float pawspf;
/* Max recoverable digits */
#define PAWSPF_MAX_REC	FLT_DIG
#define PAWSPF_MAN_DIG	FLT_MANT_DIG
#define PAWSPF_MAX		FLT_MAX
#ifdef __FLT_HAS_INFINITY__
#define PAWSPF_INF		((pawf)(INFINITY))
#endif
/* For normalised values (base 2) */
#define PAWSPF_EXP_MAX	FLT_MAX_EXP
/* For original values (base 10) */
#define PAWSPF_E10_MAX	FLT_MAX_10_EXP
#define PAWSPF_MIN		FLT_MIN
#define PAWSPF_EXP_MIN	FLT_MIN_EXP
#define PAWSPF_E10_MIN	FLT_MIN_10_EXP
#define PAWSPF_EPSILON	FLT_EPSILON

typedef double pawlspf;
#define PAWLSPF_MAX_REC	DBL_DIG
#define PAWLSPF_MAN_DIG	DBL_MANT_DIG
#define PAWLSPF_MAX		DBL_MAX
#ifdef __DBL_HAS_INFINITY__
#define PAWLSPF_INF		((pawlf)(INFINITY))
#endif
#define PAWLSPF_EXP_MAX	DBL_MAX_EXP
#define PAWLSPF_E10_MAX	DBL_MAX_10_EXP
#define PAWLSPF_MIN		DBL_MIN
#define PAWLSPF_EXP_MIN	DBL_MIN_EXP
#define PAWLSPF_E10_MIN	DBL_MIN_10_EXP
#define PAWLSPF_EPSILON	DBL_EPSILON

typedef long double pawllspf;
#define PAWLLSPF_MAX_REC	LDBL_DIG
#define PAWLLSPF_MAN_DIG	LDBL_MANT_DIG
#define PAWLLSPF_MAX		LDBL_MAX
#ifdef __LDBL_HAS_INFINITY__
#define PAWLLSPF_INF		((pawllspf)(INFINITY))
#endif
#define PAWLLSPF_EXP_MAX	LDBL_MAX_EXP
#define PAWLLSPF_E10_MAX	LDBL_MAX_10_EXP
#define PAWLLSPF_MIN		LDBL_MIN
#define PAWLLSPF_EXP_MIN	LDBL_MIN_EXP
#define PAWLLSPF_E10_MIN	LDBL_MIN_10_EXP
#define PAWLLSPF_EPSILON	LDBL_EPSILON

typedef pawllspf pawjspf;
#define PAWJSPF_MAX_REC	PAWLLSPF_MAX_REC
#define PAWJSPF_MAN_DIG	PAWLLSPF_MAN_DIG
#define PAWJSPF_MAX		PAWLLSPF_MAX
#ifdef PAWLLSPF_INF
#	define PAWJSPF_INF	PAWLLSPF_INF
#endif
#define PAWJSPF_EXP_MAX	PAWLLSPF_EXP_MAX
#define PAWJSPF_E10_MAX	PAWLLSPF_E10_MAX
#define PAWJSPF_MIN		PAWLLSPF_MIN
#define PAWJSPF_EXP_MIN	PAWLLSPF_EXP_MIN
#define PAWJSPF_E10_MIN	PAWLLSPF_E10_MIN
#define PAWJSPF_EPSILON	PAWLLSPF_EPSILON

#if PAWRD_WIDTH > 64
#	define PAWABS_WIDTH PAWRD_WIDTH
#else
#	define PAWABS_WIDTH 64
#endif

/* File system position */
typedef   signed _BitInt(PAWABS_WIDTH) pawfsp;
/* Library memory pointer - Useful for mpawraw*()/mpawmemsrc/mpawrawdst to use
 * one unconstrained ABI instead of multiple doing the same thing */
typedef unsigned _BitInt(PAWABS_WIDTH) pawabs;

enum
{
	/* "Open Files Per User" under worst defaults (includes stdin, stdout
	 * & stderr) */
	PAWMAX_OFPU_DOS = 15,
	PAWMAX_OFPU_LINUX = 1024,
	PAWMAX_OFPU_WIN32 = 2048
};

typedef pawrd   pawexe;
#define PAWEXE_WIDTH PAWRD_WIDTH

#if PAWEXE_WIDTH >= 64
#define PAWMSG_WIDTH PAWEXE_WIDTH
#else
#define PAWMSG_WIDTH PAWINT_MULCIEL(64,CHAR_BIT)
#endif
#define PAWMSG_TYPE unsigned _BitInt(PAWMSG_WIDTH)
typedef PAWMSG_TYPE pawmsg;
#define PAWMSG_MAX (~CAST(PAWMSG_TYPE,0))
#include "enum_pawmsgid.h"
#include "enum_pawsubid.h"

/* sign (1 bit)
 * + thread index (16bits)
 * + memory index (16bits)
 * + unsigned offset (PAWRD_WIDTH-1 bits) */
typedef   signed _BitInt(PAWABS_WIDTH+32) pawid;
PAW_QCK_CB pawrd   mpawid_getoffset( pawid id ) { return id & PAWRD_MAX; }
PAW_QCK_CB pawe16u mpawid_getmempos( pawid id )
	{ return (id >> (PAWRD_WIDTH - 1)) & 0xFFFF; }
PAW_QCK_CB pawe16u mpawid_getantpos( pawid id )
	{ return (id >> (PAWRD_WIDTH + 15)) & 0xFFFF; }
PAW_QCK_CB pawb    mpawid_seemsvalid( pawid id )
	{ return ( id >= 0 && mpawid_getantpos(id) && mpawid_getmempos(id) ); }

#define PAWP_SLOTDIFF( TOP, N )  ((pawvd)((TOP) - (N)))

typedef pawmsg (*mpawid_cb)( pawid id, pawe32d func, va_list va );

struct _mpawraw
{
#define VTABLE void
#include "inherit_mpawraw.h"
};
typedef struct _mpawraw mpawrawdst;
typedef const struct _mpawraw mpawrawsrc;

struct _mpawmem
{
#define T void
#include "inherit_mpawmem.h"
};
typedef struct _mpawmem mpawmemdst;
typedef const struct _mpawmem mpawmemsrc;

typedef pawe64u  pawfsomask;
#define _PAWGLOBDEF(BIT) (((pawfsomask)1u) << (32 + BIT))
#define PAWGLOB_O_STOP_ON_RD_ERROR = _PAWGLOBDEF(0)
#define PAWGLOB_O_APPEND_FWSLASHES = _PAWGLOBDEF(1)
#define PAWGLOB_O_NO_SORTING_PATHS = _PAWGLOBDEF(2)
#define PAWGLOB_O_RESERVE_1ST4NULL = _PAWGLOBDEF(3)
#define PAWGLOB_O_DEFAULT_TO_GIVEN = _PAWGLOBDEF(4)
#define PAWGLOB_O_APPENDENUMERATED = _PAWGLOBDEF(5)
#define PAWGLOB_O_NO_ESC_CHARACTER = _PAWGLOBDEF(6)

#ifdef NPAW_BUILD
#include <npaw_types.h>
/* Pointers are less expensive than the actual integer so we just use those
 * instead */
PAW_ANT_CB pawmsg npawfso_setmask( npawfsomask *dst, pawfsomask const *src );
PAW_ANT_CB pawmsg npawfso_getmask( pawfsomask *dst, npawfsomask const *src );
#endif

/* Allows partial function overloading */
typedef union PAWATTR_TUNION
{
	/* This one is purely for ensuring any npawsys type will not change the
	 * size of this union */
	pawrd rd;
	pawfsp fsp;
	pawabs abs;
	void  *ptr;
	void const *cptr;
	uchar *top;
	uchar const *ctop;
	/* This is a conevience member to avoid casting every time we need a
	 * member like this in the handles paw gives out */
	void  **ptr2ptr;
#ifdef NPAW_BUILD
	npawsys sys;
#endif
} upawabs;

enum
{
	PAWDATAMDOEL_WIDTHOF_CHAR = 0, // Makes more sense to have this one 1st
	PAWDATAMODEL_SIZEOF_FUNCTION,
	PAWDATAMODEL_SIZEOF_POINTER,
	PAWDATAMODEL_SIZEOF_CHAR8_T,
	PAWDATAMODEL_SIZEOF_WCHAR_T,
	PAWDATAMODEL_SIZEOF_CHAR16_T,
	PAWDATAMODEL_SIZEOF_CHAR32_T,
	PAWDATAMODEL_SIZEOF_SHORT,
	PAWDATAMODEL_SIZEOF_INT,
	PAWDATAMODEL_SIZEOF_LONG,
	PAWDATAMODEL_SIZEOF_LLONG,
	PAWDATAMODEL_SIZEOF_SIZE_T,
	PAWDATAMODEL_SIZEOF_INT_LEAST8_T,
	PAWDATAMODEL_SIZEOF_INT_LEAST16_T,
	PAWDATAMODEL_SIZEOF_INT_LEAST32_T,
	PAWDATAMODEL_SIZEOF_INT_LEAST64_T,
	PAWDATAMODEL_SIZEOF_INTMAX_T,
	PAWDATAMODEL_TYPE_COUNT
};

#define PAWDATAMODEL_BITSPERSIZE 4
#define PAWDATAMODEL_SIZEMASK \
	(~((~((pawdatamodel)0)) << PAWDATAMODEL_BITSPERSIZE))

typedef union {
	unsigned _BitInt
	(
		PAWDATAMODEL_BITSPERSIZE
		+ (PAWDATAMODEL_TYPE_COUNT*PAWDATAMODEL_BITSPERSIZE)
	) mask;
	uchar list[PAWDATAMODEL_TYPE_COUNT];
} pawdatamodel;
#define PAWDATAMODEL_RDVALUE(MASK,POS) \
	(((MASK) >> (PAWDATAMODEL_BITSPERSIZE * POS)) & 0xF)
#define PAWDATAMODEL_WRVALUE(SIZE,POS) \
	(((pawdatamodel)(SIZE)) << (PAWDATAMODEL_BITSPERSIZE * POS))

typedef struct
{
	upawabs      charset;
	upawabs      wcharset;
	pawdatamodel modelmask;
} pawcompiletimeinfo;

PAW_FQK_CB paweld pawintu_inrange( pawru width, pawru value )
{
	pawru lz = value ? __builtin_clzll( value ) : PAWRD_WIDTH;
	if ((PAWRD_WIDTH - lz) <= width)
		return 1;
	pawsetmsg( PAWMSGID_ARITHMETIC_OVERFLOW, 0 );
	return 0;
}

PAW_FQK_CB paweld pawintd_inrange( pawru width, pawrd value )
	{ return pawintu_inrange(width,(value >= 0) ? value : ~value); }

#ifndef _LIBPAW
extern pawcompiletimeinfo const *pawcti;
#	ifdef COMPILE_PAW_LOCALS
pawcompiletimeinfo _pawcti =
{
	.charset.cptr = PAWS_CHARSET,
	.wcharset.cptr = NPAWLS_CHARSET,
	.modelmask = PAWDATAMODEL_WRVALUE(CHAR_BIT,PAWDATAMODEL_WIDTHOF_CHAR)
	| PAWDATAMODEL_WRVALUE(sizeof(void(*)()),PAWDATAMODEL_SIZEOF_FUNCTION)
	| PAWDATAMODEL_WRVALUE(sizeof(void*),PAWDATAMODEL_SIZEOF_POINTER)

	| PAWDATAMODEL_WRVALUE(sizeof(char8_t),PAWDATAMODEL_SIZEOF_CHAR8_T)
	| PAWDATAMODEL_WRVALUE(sizeof(wchar_t),PAWDATAMODEL_SIZEOF_WCHAR_T)
	| PAWDATAMODEL_WRVALUE(sizeof(char16_t),PAWDATAMODEL_SIZEOF_CHAR16_T)
	| PAWDATAMODEL_WRVALUE(sizeof(char32_t),PAWDATAMODEL_SIZEOF_CHAR32_T)

	| PAWDATAMODEL_WRVALUE(sizeof(short),PAWDATAMODEL_SIZEOF_SHORT)
	| PAWDATAMODEL_WRVALUE(sizeof(int),PAWDATAMODEL_SIZEOF_INT)
	| PAWDATAMODEL_WRVALUE(sizeof(long),PAWDATAMODEL_SIZEOF_LONG)
	| PAWDATAMODEL_WRVALUE(sizeof(long long),PAWDATAMODEL_SIZEOF_LLONG)
	| PAWDATAMODEL_WRVALUE(sizeof(size_t),PAWDATAMODEL_SIZEOF_SIZE_T)
	| PAWDATAMODEL_WRVALUE(sizeof(int_least8_t),PAWDATAMODEL_SIZEOF_INT_LEAST8_T)
	| PAWDATAMODEL_WRVALUE(sizeof(int_least16_t),PAWDATAMODEL_SIZEOF_INT_LEAST16_T)
	| PAWDATAMODEL_WRVALUE(sizeof(int_least32_t),PAWDATAMODEL_SIZEOF_INT_LEAST32_T)
	| PAWDATAMODEL_WRVALUE(sizeof(int_least64_t),PAWDATAMODEL_SIZEOF_INT_LEAST64_T)
	| PAWDATAMODEL_WRVALUE(sizeof(intmax_t),PAWDATAMODEL_SIZEOF_INTMAX_T)
};
pawcompiletimeinfo const *pawcti = &_pawcti;
PAW_ANT_CB pawintu
#	endif
#endif

#if 0
typedef pawed pawev2d __attribute__((vector_size ( 8)));
typedef pawed pawev3d __attribute__((vector_size (12)));
typedef pawed pawev4d __attribute__((vector_size (16)));

typedef paweu pawev2u __attribute__((vector_size ( 8)));
typedef paweu pawev3u __attribute__((vector_size (12)));
typedef paweu pawev4u __attribute__((vector_size (16)));
#endif

typedef struct
{
	pawru   width;
	upawabs value;
} pawintd, pawintu, pawint;

PAW_ANT_CB pawru mpawint_clz( pawintu a );
PAW_EXP_CB pawintu mpawintunew( upawabs value, pawru width )
	{ return (pawintu){ width, value }; }
PAW_EXP_CB pawintd mpawintdnew( upawabs value, pawru width )
	{ return (pawintd){ width, value }; }
PAW_EXP_CB pawru mpawint_len( pawintu a ) { return a.width - mpawint_clz(a); }
PAW_QCK_CB pawrd mpawint_bit( pawintu n, pawru i )
{
	pawru mov = i % CHAR_BIT;
	uchar bit = 1u << mov;
	return (i >= n.width) ? -1 : (n.value.ctop[i / CHAR_BIT] & bit) >> mov;
}

#ifdef COMPILE_MPAW_LOCALS
extern PAW_EXP_CB pawintu mpawintunew( upawabs value, pawru width );
extern PAW_EXP_CB pawintd mpawintdnew( upawabs value, pawru width );
extern PAW_EXP_CB pawru mpawint_len( pawintu a );
extern PAW_QCK_CB pawrd mpawint_bit( pawintu n, pawru i );
#endif

/* Allocate enough thread local memory for math of at least width - needed for
 * multiplication, division and rotation, possibly others too */
PAW_ANT_CB pawmsg mpawintsetmaxwidth( pawru width );
/* Akin to a CPU register, whole page is allocated at thread start for this */
PAW_ANT_CB pawmsg mpawintset2res( pawint dst );
/* Akin to a CPU register, whole page is allocated at thread start for this,
 * exclusive to the remainder of division */
PAW_ANT_CB pawmsg mpawintset2rem( pawint dst );
PAW_ANT_CB pawb   mpawintset2uvalue( pawint n, pawintu val, pawb clamp );
PAW_ANT_CB pawb   mpawintset2dvalue( pawint n, pawintd val, pawb clamp );
/* You don't need me to tell you what these do */
PAW_ANT_CB pawb   mpawintgetbit( pawint n, pawru i );
/* Returns 0 if i is out of range */
PAW_ANT_CB pawb   mpawintsetbit( pawint n, pawru i, pawb val );

/* Is n 0?; */
PAW_ANT_CB pawb  mpawint_not( pawint n );
/* Bitwise NOT */
PAW_ANT_CB void  mpawint_bwn( pawint n );
/* Increment, pawb is for the question "Did it overflow?" */
PAW_ANT_CB pawb  mpawint_inc( pawint n );
/* Decrement, pawb is for the question "Did it overflow?" */
PAW_ANT_CB pawb  mpawint_dec( pawint n );
/* Negate the value */
PAW_ANT_CB void  mpawint_neg( pawint n );
/* Bitwise AND */
PAW_ANT_CB void  mpawint_bwa( pawint a, upawabs b );
/* Bitwise OR */
PAW_ANT_CB void  mpawint_bwo( pawint a, upawabs b );
/* Bitwise XOR */
PAW_ANT_CB void  mpawint_bwx( pawint a, upawabs b );
/* Shift left */
PAW_ANT_CB void  mpawint_shl( pawint n, pawru move );
/* Rotate bits right */
PAW_ANT_CB void  mpawint_ror( pawint n, pawru move );
/* Rotate bits left */
PAW_ANT_CB void  mpawint_rol( pawint n, pawru move );
/* Addition */
PAW_ANT_CB pawrd mpawint_add( pawint a, upawabs b );
/* Subtraction */
PAW_ANT_CB pawrd mpawint_sub( pawint a, upawabs b );


PAW_QCK_CB pawb pawintu_v( pawintu n, upawabs val )
{
	pawintu x = {0};
	x.value.cptr = &val;
	x.width = bitsof(val);
	return mpawintset2uvalue( n, x, 0 );
}
/* Unsigned bit length */
PAW_ANT_CB pawru mpawintu_len( pawintu n );
/* Unsigned comparison, only -1, 0, and 1 are returned */
PAW_ANT_CB pawrd mpawintu_cmp( pawintu a, pawintu b );
/* Unsigned shift right */
PAW_ANT_CB void mpawintu_shr( pawint n, pawru move );
/* Unsigned multiplication - a & b are treated floor(dst.width / 2) */
PAW_ANT_CB pawb mpawintu_mul( pawintu dst, pawintu a, pawintu b );
/* Unsigned division (and modulation) */
PAW_ANT_CB pawb mpawintu_div( pawintu quo, void *a, pawp b );

/* Set n to val */
PAW_ANT_CB pawb pawintd_set( pawintd n, pawintd val, pawb clamp );
PAW_QCK_CB pawb pawintd_v( pawintu n, upawabs val )
{
	pawintd x = {0};
	x.value.cptr = &val;
	x.width = bitsof(val);
	return mpawintset2dvalue( n, x, 0 );
}
/* Signed bit length (negative numbers search for end 0 instead of end 1) */
PAW_ANT_CB pawru mpawintd_len( pawintd n );
/* Signed comparison, only -1, 0, and 1 are returned */
PAW_ANT_CB pawrd mpawintd_cmp( pawintd a, pawintd b );
/* Signed shift right */
PAW_ANT_CB void  mpawintd_shr( pawintd n, pawru move );

/* Signed multiplication - returns 1 if overflowed */
PAW_ANT_CB pawb mpawintd_mul( pawintd dst, void *a, pawp b );
/* Signed division (and modulation) - returns 1 if nothing to done */
PAW_ANT_CB pawb mpawintd_div( pawintd quo, void *a, pawp b );

#define PAWINTDNEW( PTR ) pawintdnew( PTR, bitsof(*(PTR)) )
#define PAWINTUNEW( PTR ) pawintunew( PTR, bitsof(*(PTR)) )

typedef struct
{
	pawru softlim;
	pawru hardlim;
} pawlim;

/* Sometimes you just want to add bytes rather than element size, these
 * decomplicates code that needs to do so */
PAW_EXP_CB upawabs  mpawabs_addbytes( upawabs a, pawfsp  b )
	{ a.abs += b;     return a; }
PAW_EXP_CB upawabs  mpawabs_bytediff( upawabs a, upawabs b )
	{ a.abs -= b.abs; return a; }

#ifdef COMPILE_MPAW_LOCALS
extern PAW_EXP_CB upawabs  mpawabs_addbytes( upawabs a, pawfsp  b );
extern PAW_EXP_CB upawabs  mpawabs_bytediff( upawabs a, upawabs b );
#endif

#ifndef _LIBPAW
PAW_FQK_CB void*  pawv_addbytes( pawp a, pawvd b )
	{ return mpawabs_addbytes( a, b ); }
PAW_FQK_CB pawp   pawp_addbytes( pawp a, pawvd b )
	{ return mpawabs_addbytes( a, b ); }
PAW_FQK_CB pawrd  pawp_bytediff( pawp a, pawp  b )
	{ return mpawabs_bytediff( a, b ); }
#	ifdef COMPILE_PAW_LOCALS
extern PAW_FQK_CB void*  pawv_addbytes( pawp a, pawvd b );
extern PAW_FQK_CB pawp   pawp_addbytes( pawp a, pawvd b );
extern PAW_FQK_CB pawrd  pawp_bytediff( pawp a, pawp  b );
#	endif
#endif



/* Get offset */
PAW_QCK_CB upawabs mpawmemsrc_getoff( mpawmemsrc mem, pawfsp o )
{
	return (o >= 0 && mem.end > o)
		? mpawabs_addbytes( mem.buf, o ) : mpawabs_addbytes(0,0);
}
/* Get member */
PAW_QCK_CB upawabs mpawmemsrc_getmem( mpawmemsrc mem, pawru T, pawfsp i )
{
	i *= T;
	return (i >= 0 && mem.end > i + T)
		? mpawabs_addbytes( mem.buf, i ) : mpawabs_addbytes(0,0);
}
#ifdef COMPILE_MPAW_LOCALS
extern PAW_QCK_CB upawabs mpawmemsrc_getoff( mpawmemsrc mem, pawfsp o );
extern PAW_QCK_CB upawabs mpawmemsrc_getmem( mpawmemsrc mem, pawru T, pawfsp i );
#endif

#ifndef _LIBPAW
PAW_FQK_CB pawp pawmemsrc_getoff( mpawmemsrc mem, pawfsp o )
	{ return mpawmemsrc_getoff( mem, o ).cptr; }
PAW_FQK_CB pawp pawmemsrc_getmem( mpawmemsrc mem, pawru T, pawfsp i )
	{ return mpawmemsrc_getmem( mem, T, i ).cptr; }
#	ifdef COMPILE_PAW_LOCALS
PAW_FQK_CB pawp pawmemsrc_getoff( mpawmemsrc mem, pawfsp o );
PAW_FQK_CB pawp pawmemsrc_getmem( mpawmemsrc mem, pawru T, pawfsp i );
#	endif
#endif

PAW_QCK_CB pawru pawru__len( pawru n, pawru width, pawb find0 )
{
	pawru x = ((pawru)1) << (width - 1), bit = find0 ? 0 : x;
	while ( x )
	{
		if ( (bit & n) == bit )
			return width;
		bit >>= 1;
		x >>= 1;
		--width;
	}
	return 0;
}

PAW_ANT_CB pawru pawemaxu__len( paweju n, pawru width, pawb find0 );
PAW_FQK_CB pawru pawemaxu_len( paweju n ) { return pawemaxu__len( n, bitsof(n), 0 ); }
PAW_FQK_CB pawru pawemaxd_len( pawejd n ) { return pawemaxu__len( n, bitsof(n), n < 0 ); }
/* _BitInt seems to have issues with looped operations and limiting the
 * generated instructions so we shift the instruction issue to fixed calls and
 * fix the loop issue with custom implementations where appropriate. */
PAW_ANT_CB pawrd pawemaxu_cmp( paweju a, uchar b );
PAW_ANT_CB paweju pawemaxu_bwn( paweju n );
PAW_ANT_CB paweju pawemaxu_bwa( paweju a, paweju b );
PAW_ANT_CB paweju pawemaxu_bwo( paweju a, paweju b );
PAW_ANT_CB paweju pawemaxu_bwx( paweju a, paweju b );
PAW_ANT_CB paweju pawemaxu_shl( paweju n, pawru by );
PAW_ANT_CB paweju pawemaxu_shr( paweju n, pawru by );
PAW_ANT_CB paweju pawemaxu_rol( paweju n, pawru by );
PAW_ANT_CB paweju pawemaxu_ror( paweju n, pawru by );
PAW_ANT_CB paweju pawemaxu_add( paweju a, paweju b );
PAW_ANT_CB paweju pawemaxu_sub( paweju a, paweju b );
PAW_ANT_CB paweju pawemaxu_mul( paweju a, paweju b );
typedef struct { paweju quo, rem; } pawemaxudiv;
typedef struct { pawejd quo, rem; } pawemaxddiv;
PAW_ANT_CB pawemaxudiv pawemaxu__div( paweju a, paweju b );
PAW_ANT_CB pawemaxddiv pawemaxd__div( pawejd a, pawejd b );
PAW_FQK_CB paweju pawemaxu_div( paweju a, paweju b ) { return pawemaxu__div(a,b).quo; }
PAW_FQK_CB pawejd pawemaxd_div( pawejd a, pawejd b ) { return pawemaxd__div(a,b).quo; }
PAW_FQK_CB paweju pawemaxu_mod( paweju a, paweju b ) { return pawemaxu__div(a,b).rem; }
PAW_FQK_CB pawejd pawemaxd_mod( pawejd a, pawejd b ) { return pawemaxd__div(a,b).rem; }

/* msg = (csc < 0) ? -csc : 0; character = (csc < 0) ? 0 : csc */
typedef signed _BitInt(PAWMSG_WIDTH) pawcsc;

typedef pawcsc (*mpawcs_toc_cb)( pawcsc c );
typedef pawcsc (*mpawcs_getc_cb)( pawp  src, pawru i );
typedef pawru   (*mpawcs_putc_cb)( void *src, pawru i, pawcsc c );
typedef pawcsc (*pawmbe_getllc_cb)( pawp  src, pawru *size );
typedef pawcsc (*pawmbe_putllc_cb)( void *dst, pawru *size, pawcsc c );

PAW_QCK_CB pawcsc pawcsc_tolower( pawcsc c )
	{ pawru x = c - 'A'; return (x > 26) ? c : x + 'a'; }
PAW_QCK_CB pawcsc pawcsc_toupper( pawcsc c )
	{ pawru x = c - 'a'; return (x > 26) ? c : x + 'A'; }
/* For pawcsc_toc_cb */
PAW_QCK_CB pawcsc pawcsc_asgiven( pawcsc c ) { return c; }

/* Write the bytes consumed into size once done, even if it's 0 bytes */
PAW_ANT_CB pawcsc paws_getcsc( pawp src, pawru *size );
/* Success should return parameter c, otherwise one of the codes above */
PAW_ANT_CB pawcsc paws_putcsc( void *dst, pawru *size, pawcsc c );
PAW_ANT_CB pawcsc pawmbs_getcsc( pawp src, pawru *size );
PAW_ANT_CB pawcsc pawmbs_putcsc( void *dst, pawru *size, pawcsc c );
#ifdef NPAW_BUILD
PAW_ANT_CB pawcsc npawts_getcsc( pawp src, pawru *size );
PAW_ANT_CB pawcsc npawts_putcsc( void *dst, pawru *size, pawcsc c );
PAW_ANT_CB pawcsc npawls_getcsc( pawp src, pawru *size );
PAW_ANT_CB pawcsc npawls_putcsc( void *dst, pawru *size, pawcsc c );
#endif
PAW_ANT_CB pawcsc pawhs_getcsc( pawp src, pawru *size );
PAW_ANT_CB pawcsc pawhs_putcsc( void *dst, pawru *size, pawcsc c );
/* So that can avoid checking what type of text converting to/src by just
 * letting the given callback produce the expected result based on what it
 * assumes is the type */
PAW_ANT_CB pawcsc pawlls_getcsc( pawp src, pawru *size );
PAW_ANT_CB pawcsc pawlls_putcsc( void *dst, pawru *size, pawcsc c );

/* Force comparison to result in -1, 0, or 1 */
PAW_EXP_CB pawabs pawintu_cmp( upawabs a, upawabs b )
	{ return ((pawfsp)(a.abs > b.abs)) - ((pawfsp)(a.abs < b.abs)); }
PAW_EXP_CB pawfsp pawintd_cmp( upawabs a, upawabs b )
	{ return ((pawfsp)(a.fsp > b.fsp)) - ((pawfsp)(a.fsp < b.fsp)); }

#ifdef MCOMPILE_PAW_LOCALS
extern PAW_EXP_CB pawabs pawintu_cmp( upawabs a, upawabs b );
extern PAW_EXP_CB pawfsp pawintd_cmp( upawabs a, upawabs b );
#endif

#define PAWINTD_BEWTEEN( LOW, VAL, HIGH ) \
	((pawb)(((VAL) >= (LOW)) && ((VAL) <= (HIGH))))

#define PAWINTU_BEWTEEN( LOW, VAL, HIGH ) \
	((pawb)(((VAL)  - (LOW)) <= ((HIGH) -  (LOW))))

PAW_QCK_CB pawesc pawint_parsec62( pawesc c, pawru base, pawllc A, pawllc a )
{
	pawesc v = c - '0';
	if ( v < 10 )
		return (v < base) ? v : -1;
	v = c - 'A';
	if ( v < 26 )
	{
		v += A;
		return (v < base) ? v : -1;
	}
	v = c - 'a';
	if ( v < 26 )
	{
		v += a;
		return (a < base) ? a : -1;
	}
	return -1;
}

typedef pawesc (*pawparsec_cb)( pawesc c, pawru base );

PAW_ANT_CB pawesc pawint_parsec36( pawesc c, pawru base );
PAW_ANT_CB pawesc pawint_parsec10( pawesc c, pawru base );

#ifdef MCOMPILE_PAW_LOCALS
PAW_QCK_CB pawesc pawint_parsec62( pawesc c, pawru base, pawllc A, pawllc a );
PAW_ANT_CB pawesc pawint_parsec36( pawesc c, pawru base )
	{ return pawint_parsec62( c, base, 10, 10 ); }
PAW_ANT_CB pawesc pawint_parsec10( pawesc c, pawru base )
	{ pawesc v = c - '0'; return (v < 10u && v < base) ? v : -1; }
#endif

typedef pawesc (*pawint_getchar_cb)( upawabs ud );
typedef struct
{
	pawru  used;
	pawesc prvc, gotc, putc;
} pawstreamc;
/** @brief Converts an escape sequence into expected raw value
 * @note Expects start with the character that immeadiatly follows the
 * escape character. This allows for custom escape characters instead of \ */
PAW_QCK_CB pawstreamc pawint_stresc( pawb ms, upawabs ud, pawint_getchar_cb cb )
{
	pawstreamc esc = {0};
	pawparsec_cb parsec = pawint_parsec36, v = 0;
	pawru *i = &(esc.used), max = 0, base = 16, mov = 4;
	pawesc *p = &(esc.prvc), *c = &(esc.gotc), *u = &(esc.putc), tmp = 0;
	*p = *c;
	*c = cb( ud );
	if ( *c < 0 )
	{
		*i = 0;
		return esc;
	}
	*i = 1;
	switch ( *c )
	{
	default:
		tmp = pawint_parsec10(*c,8);
		if ( tmp < 0 )
		{
			/* Not a pre-recognised character, treat like '"?\ if expecting
			 * ms behaviour, otherwise treat */
			*u = ms ? *c : -1;
			if ( !ms )
				*u = '\\';
			else
			{
				++(*i);
				*u = *c;
			}
			return esc;
		}
		*u = tmp;
		/* Octal character */
		max = 2;
		mov = 3;
		base = 8;
		parsec = pawint_parsec10;
		break;
	case 'a': *u = 0x07; return esc;
	case 'b': *u = 0x08; return esc;
	case 't': *u = 0x09; return esc;
	case 'n': *u = 0x0A; return esc;
	case 'v': *u = 0x0B; return esc;
	case 'f': *u = 0x0C; return esc;
	case 'r': *u = 0x0D; return esc;
	case 'e': *u = 0x1B; return esc;
	/* UTF-16 character*/
	case 'u': max = 4; break;
	/* Unicode Character */
	case 'U': max = 8; break;
	case 'o':
		max = 3;
		mov = 3;
		base = 8;
		parsec = pawint_parsec10;
		break;
	/* Exact Nibbles/Octals */
	case 'x': max = ~max; break;
	}
	for ( ; max--; ++(*i) )
	{
		*p = *c;
		*c = cb( ud );
		if ( *c < 0 )
			return esc;
		tmp = parsec(*c,base);
		if ( v >= 0 )
			*u = (*u << mov) | tmp;
		else
			return esc;
	}
	*i += 1;
	return esc;
}

PAW_QCK_CB pawstreamc pawint_ttyesc( upawabs ud, pawint_getchar_cb cb )
{
	pawstreamc esc = {0};
	pawru *i = &(esc.used);
	pawesc *p = &(esc.prvc), *c = &(esc.gotc), *u = &(esc.putc);
	*p = *c;
	*c = cb( ud );
	if ( *c != '^' )
	{
		*i = 0;
		return esc;
	}
	*i = 1;
	*p = '^';
	*c = cb( ud );
	if ( *c < 0 )
	{
		*i = 1;
		return esc;
	}
	*i = 2;
	switch ( *c )
	{
	case 'G': *u = 0x07; return esc;
	case 'H': *u = 0x08; return esc;
	case 'I': *u = 0x09; return esc;
	case 'J': *u = 0x0A; return esc;
	case 'K': *u = 0x0B; return esc;
	case 'L': *u = 0x0C; return esc;
	case 'M': *u = 0x0D; return esc;
	case '[': *u = 0x1B; return esc;
	}
	/* Not a pre-recognised character, treat like '"?\ if expecting
	 * ms behaviour, otherwise treat */
	*u = -1;
	return esc;
}

/** @brief Copy integer value of src into dst, setting upper bytes as needed
 * @param set Value to set each byte above len in dst
 * @return true on clamped value, false if no clamping needed
 * @note Mainly exists to avoid issues with signed emulated integers */
PAW_ANT_CB pawb _pawintd_clamp
	( void *dst, pawrd cap, pawp src, pawrd len, schar set );
PAW_QCK_CB pawb pawintd_clamp( void *dst, pawrd cap, pawp src, pawrd len )
{
	pawrd end = len - 1;
	schar set = (end < 0) ? 0 : ((schar const*)src)[end];
	return _pawintd_clamp( dst, cap, src, len, set );
}
PAW_QCK_CB pawb pawintu_clamp( void *dst, pawrd cap, pawp src, pawrd len )
	{ return _pawintd_clamp( dst, cap, src, len, 0 );	}

/* The floor functions can still be useful if the parameters are the result
of expressions */

PAW_FQK_CB pawrd pawintd_divfloor( pawrd a, pawrd b ) { return a / b; }
PAW_FQK_CB pawrd pawintd_divciel( pawrd a, pawrd b ) { return (a / b) + !!(a % b); }

PAW_FQK_CB pawru pawintu_divfloor( pawru a, pawru b ) { return a / b; }
PAW_FQK_CB pawru pawintu_divciel( pawru a, pawru b ) { return (a / b) + !!(a % b); }

PAW_FQK_CB pawejd pawintxd_divfloor( pawejd a, pawejd b ) { return a / b; }
PAW_FQK_CB pawejd pawintxd_divciel( pawejd a, pawejd b ) { return (a / b) + !!(a % b); }

PAW_FQK_CB paweju pawintxu_divfloor( paweju a, paweju b ) { return a / b; }
PAW_FQK_CB paweju pawintxu_divciel( paweju a, paweju b ) { return (a / b) + !!(a % b); }

/* a multiple of b */
PAW_FQK_CB pawrd pawintd_mulfloor( pawrd a, pawrd b )
	{ return pawintd_divfloor(a,b) * b; }
PAW_FQK_CB pawrd pawintd_mulciel( pawrd a, pawrd b )
	{ return pawintd_divciel(a,b) * b; }

PAW_FQK_CB pawru pawintu_mulfloor( pawru a, pawru b )
	{ return pawintu_divfloor(a,b) * b; }
PAW_FQK_CB pawru pawintu_mulciel( pawru a, pawru b )
	{ return pawintu_divciel(a,b) * b; }

PAW_FQK_CB pawrd pawintxd_mulfloor( pawrd a, pawrd b )
	{ return pawintxd_divfloor(a,b) * b; }
PAW_FQK_CB pawrd pawintxd_mulciel( pawrd a, pawrd b )
	{ return pawintxd_divciel(a,b) * b; }

PAW_FQK_CB pawru pawintxu_mulfloor( pawru a, pawru b )
	{ return pawintxu_divfloor(a,b) * b; }
PAW_FQK_CB pawru pawintxu_mulciel( pawru a, pawru b )
	{ return pawintxu_divciel(a,b) * b; }

PAW_QCK_CB pawru pawint_ptrv_count( pawp ud, va_list va )
{
	va_list cp;
	pawru i = 0;
	va_copy( cp, va );
	for ( ; ud; ++i, ud = va_arg( cp, pawp ) );
	va_end( cp );
	return i;
}

/* Linked lists done by pointer are slow to locate items in large lists. This
 * can be circumvented by just storing the linked list in a dedicated buffer or
 * array and using the indices as links instead. This carries the advantage of
 * not only being able to quickly access the desired link in a large array but
 * also be able to quickly sort large values/objects/strings without actually
 * sorting them until you go to write them to file */
typedef union
{
	struct { pawed base, here; }; // parent & self
	struct { pawed last, init; }; // end children
	struct { pawed prev, next; };
} pawties;

typedef struct
{
	upawabs tied;
	pawties location; // parent & self
	pawties released, assigned, siblings;
	pawties children; // end children
} pawtie;

PAW_C_API_SHUT
