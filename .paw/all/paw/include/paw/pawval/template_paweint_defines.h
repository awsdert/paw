#define EINTDIVD EINTNAME(_,divd)
#define EINTDIVU EINTNAME(_,divu)

/* Return max value */
#define EINTU_MAX EINTNAME(_,u_max)
/* Convert value n to EINTVALU */
#define EINTU_V EINTNAME(_,u_v)
/* Study programming more if you don't what get these 2 do */
#define EINTU_GETBIT EINTNAME(_,u_getbit)
#define EINTU_SETBIT EINTNAME(_,u_setbit)
/* Is N not anything but 0? */
#define EINTU_NOT EINTNAME(_,u_not)
/* Increment N */
#define EINTU_INC EINTNAME(_,u_inc)
/* Decrement N */
#define EINTU_DEC EINTNAME(_,u_dec)
/* Negate N */
#define EINTU_NEG EINTNAME(_,u_neg)
/* Bit count of actually used bits, 0 to MAX checks for ending 1, MIN to -1
 * checks for 0 */
#define EINTU_LEN EINTNAME(_,u_len)
/* Unsigned comparison */
#define EINTU_CMP EINTNAME(_,u_cmp)
/* Bitwise NOT (flip all bits) */
#define EINTU_BWN EINTNAME(_,u_bwn)
/* Bitwise AND (keep all bits from A that matched with those of B) */
#define EINTU_BWA EINTNAME(_,u_bwa)
/* Bitwise OR (keep all bits from both A and B) */
#define EINTU_BWO EINTNAME(_,u_bwo)
/* Bitwise XOR (keep all bits from A that did not match with those of B) */
#define EINTU_BWX EINTNAME(_,u_bwx)
/* Signed Shift Right */
#define EINTU_SSR EINTNAME(_,u_ssr)
/* Unsigned Shift Right */
#define EINTU_USR EINTNAME(_,u_usr)
/* Default Shift Left */
#define EINTU_SHL EINTNAME(_,u_shl)
/* Default Shift Right */
#define EINTU_SHR EINTNAME(_,u_shr)
/* Rotate bits left */
#define EINTU_ROL EINTNAME(_,u_rol)
/* Rotate bits right */
#define EINTU_ROR EINTNAME(_,u_ror)
/* Addition */
#define EINTU_ADD EINTNAME(_,u_add)
/* Subtraction */
#define EINTU_SUB EINTNAME(_,u_sub)
/* Multiplication */
#define EINTU_MUL EINTNAME(_,u_mul)
/* Combined Division & Modulo, N / 0 = 0 (quo), N (rem).
 * Nothing is taken from N to the quotient therefore the result is always 0
 * just as with multiplication where nothing is duplicated into the result
 * when multiplied by 0 */
#define EINTU__DIV EINTNAME(_,u__div)
/* d__div(a,b).quo */
#define EINTU_DIV EINTNAME(_,u_div)
/* d__div(a,b).rem */
#define EINTU_MOD EINTNAME(_,u_mod)

/* Return max value */
#define EINTD_MAX EINTNAME(_,d_max)
/* Return min value */
#define EINTD_MIN EINTNAME(_,d_min)
/* Convert value n to EINTVALD */
#define EINTD_V EINTNAME(_,d_v)
/* Is N not anything but 0? */
#define EINTD_NOT EINTNAME(_,d_not)
/* Increment N */
#define EINTD_INC EINTNAME(_,d_inc)
/* Decrement N */
#define EINTD_DEC EINTNAME(_,d_dec)
/* Negate N */
#define EINTD_NEG EINTNAME(_,d_neg)
/* Bit count of actually used bits, 0 to MAX checks for ending 1, MIN to -1
 * checks for 0 */
#define EINTD_LEN EINTNAME(_,d_len)
/* Signed comparison */
#define EINTD_CMP EINTNAME(_,d_cmp)
/* Bitwise NOT (flip all bits) */
#define EINTD_BWN EINTNAME(_,d_bwn)
/* Bitwise AND (keep all bits from A that matched with those of B) */
#define EINTD_BWA EINTNAME(_,d_bwa)
/* Bitwise OR (keep all bits from both A and B) */
#define EINTD_BWO EINTNAME(_,d_bwo)
/* Bitwise XOR (keep all bits from A that did not match with those of B) */
#define EINTD_BWX EINTNAME(_,d_bwx)
/* Signed Shift Right */
#define EINTD_SSR EINTNAME(_,d_ssr)
/* Unsigned Shift Right */
#define EINTD_USR EINTNAME(_,d_usr)
/* Default Shift Left */
#define EINTD_SHL EINTNAME(_,d_shl)
/* Default Shift Right */
#define EINTD_SHR EINTNAME(_,d_shr)
/* Rotate bits left */
#define EINTD_ROL EINTNAME(_,d_rol)
/* Rotate bits right */
#define EINTD_ROR EINTNAME(_,d_ror)
/* Addition */
#define EINTD_ADD EINTNAME(_,d_add)
/* Subtraction */
#define EINTD_SUB EINTNAME(_,d_sub)
/* Multiplication */
#define EINTD_MUL EINTNAME(_,d_mul)
/* Combined Division & Modulo, N / 0 = 0 (quo), N (rem).
 * Nothing is taken from N to the quotient therefore the result is always 0
 * just as with multiplication where nothing is duplicated into the result
 * when multiplied by 0 */
#define EINTD__DIV EINTNAME(_,d__div)
/* d__div(a,b).quo */
#define EINTD_DIV EINTNAME(_,d_div)
/* d__div(a,b).rem */
#define EINTD_MOD EINTNAME(_,d_mod)
