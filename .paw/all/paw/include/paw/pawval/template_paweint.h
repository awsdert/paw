#include "template_paweint_predefs.h"
#if EINTSEGW >= MIN_WIDTH
#	include "template_paweint_pureint.h"
#else
#	include "template_paweint_vecbase.h"
#endif
#ifdef EINT_EXTERNS
#	include "template_paweint_externs.h"
#	undef EINT_EXTERNS
#endif
#include "template_paweint_undefs.h"
#undef EINTSEGD
#undef EINTSEGD_MAX
#undef EINTSEGD_MIN
#undef EINTSEGD_CMP
#undef EINTSEGU
#undef EINTSEGU_MAX
#undef EINTSEGU_CMP
#undef EINTSEGW
#undef EINTNAME
#undef MIN_WIDTH
#ifdef EINTBASE
#	undef EINTBASE
#endif
#ifdef EINTSEG_IS_BITINT
#	undef EINTSEG_IS_BITINT
#endif
#undef QCK
