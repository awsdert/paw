#pragma once
#include "pawalltypes.h"

/* This is for identifying the implementation of libpaw attached. Refer to
 * headers you find in the vendor directory as the universal codes for the
 * individual vendors will be defined there */
pawb pawisvnd( pawmbs name );
/* This is for implementation specific extensions. Provided so that if paw
 * doesn't yet provide an API for a common need of an implementation they can
 * just shove it in this and implement what paw expects in later versions.
 * At no point should an implementation provide it's extensions directly in the
 * paw headers, instead they should have their own dedicated header for
 * software to include in addition to the paw headers, for example:
 * paw/vendor/unistd/cinnamon.h (doesn't exist at the time of writing this
 * comment, just an example of where the header might go) */
PAW_ANT_CB pawrd pawvndv( pawrd code, va_list va );

PAW_QCK_CB pawrd PAW_LOCAL(pawvndf)( pawrd code, ... )
{
	va_list va;
	va_start( va, code );
	pawrd res = pawvndv( code, va );
	va_end( va );
	return res;
}
#ifdef COMPILE_PAW_LOCALS
extern PAW_QCK_CB pawrd PAW_LOCAL(pawvndf)( pawrd code, ... );
#endif

PAW_FQK_CB void* PAW_LOCAL(pawva_ptrarg)( va_list va )
	{ return (void*)((uintptr_t)(va_arg( va, pawabs ))); }

#ifdef COMPILE_PAW_LOCALS
extern PAW_FQK_CB void* PAW_LOCAL(pawva_ptrarg)( va_list va );
#endif

 /** @brief Set each byte in dst to 0
 * @param clr: How many bytes in dst to set */
PAW_ANT_CB pawabs mpawrawclr( upawabs dst, upawabs clr );
PAW_QCK_CB pawru pawrawclr( void *dst, pawru clr )
	{ return mpawrawclr( dst, clr ); }
/** @brief Set each byte in dst to val
 * @param set: How many bytes in dst to set */
PAW_ANT_CB pawru pawrawset( void *dst, uchar val, pawru set );
 /** @brief Like pawrawclr but with memset_explicit(dst,0,set) semantics */
PAW_ANT_CB pawru pawsecclr( void *dst, pawru clr );
/** @brief Like pawrawset but with memset_explicit(dst,val,set) semantics */
PAW_ANT_CB pawru pawsecset( void *dst, uchar val, pawru set );
/** @brief Copy from start of memfer/s
 * @param cpy: Bytes to copy from src to dst
 * @example pawrawcpy( dst, src, cap );
 * @note Use pawrcpyn instead, it allows the compiler to decide for you if
 * the capacity check is safe to skip or not */
PAW_ANT_CB pawru pawrawcpy(  upawabs dst, upawabs src, pawru cpy );
PAW_QCK_CB pawru pawrawcpyn( upawabs dst, pawru cap, upawabs src, pawru cpy )
	{ return pawrawcpy( dst, src, (cap < cpy) ? cap : cpy ); }
#if defined _LIBPAW && defined COMPILE_PAW_LOCALS
#endif
/** @brief Reverse copy from end of memfer/s
 * @param cpy: Bytes to copy from src to dst
 * @example pawrawrcp( dst + cpy, src + cpy, cpy ); */
PAW_ANT_CB pawru pawrawrcp( void *dst, pawp src, pawru cpy );
PAW_QCK_CB pawru pawrawrcpn( void *dst, pawru cap, pawp src, pawru cpy )
	{ return pawrawrcp( dst, src, (cap < cpy) ? cap : cpy ); }
/** @brief Compare same sized arrays
 * @example pos = pawrawcmp( a, b, cap ); if ( pos != (pawru)-1 ) {...} */
PAW_ANT_CB pawru pawrawcmp( pawp a, pawp b, pawru cap );
PAW_QCK_CB pawru pawrawcmpn( pawp a, pawru acap, pawp b, pawru bcap )
{
	pawru cap = (acap < bcap) ? acap : bcap;
	pawru pos = pawrawcmp( a, b, cap );
	return (pos == (pawru)-1) ? cap : pos;
}
/** @brief Compare same sized arrays
 * @example pos = pawrawcmp( a + cap, b + cap, cap ); if ( pos != (pawru)-1 ) {...} */
PAW_ANT_CB pawru pawrawrcmp( pawp a, pawp b, pawru cap );
PAW_QCK_CB pawru pawrawrcmpn( pawp a, pawru acap, pawp b, pawru bcap )
{
	pawru cap = (acap < bcap) ? acap : bcap;
	pawru pos = pawrawrcmp( a, b, cap );
	return (pos == (pawru)-1) ? cap : pos;
}



