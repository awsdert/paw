#ifndef EINTSEGD
#	error "paweint templates require EINTSEGD (signed segment) to be defined!"
#	include <abort_compilation>
#endif
#ifndef EINTSEGU
#	error "paweint templates require EINTSEGU (unsigned segment) to be defined!"
#	include <abort_compilation>
#endif
#ifndef EINTSEGW
#	error "paweint templates require EINTSEGW (SIZEOF_SEG * CHAR_BIT) to be defined!"
#	include <abort_compilation>
#endif
#ifndef EINTNAME
#	error "paweint templates require EINTNAME(PFX,SFX) to be defined!"
#	include <abort_compilation>
#endif
