#ifndef SRC
#	error "template_paweint_wrapper.h require SRC(PFX,SFX) to be defined!"
#	include <abort_compilation>
#endif
#ifndef DST
#	error "template_paweint_wrapper.h require DST(PFX,SFX) to be defined!"
#	include <abort_compilation>
#endif
#define EINTVALU DST(_,u)
#define EINTVALD DST(_,d)
#define EINTNAME DST
typedef SRC(,u) EINTVALU;
typedef SRC(,d) EINTVALD;
#include "template_paweint_defines.h"
typedef SRC(_,divd) EINTDIVD;
typedef SRC(_,divu) EINTDIVU;
/* This file exists for ease of switching underlying pawe#d, further wrappers
 * are needed for some intellisense implementations, this file just takes the
 * work out of changing the underlying emulated (potentially) integer */
PAW_FQK_CB EINTVALU EINTU_V( pawru n ) { return SRC(_,u_v)(n); }
PAW_FQK_CB pawru EINTU_LEN( EINTVALU n ) { return SRC(_,u_len)(n); }
PAW_FQK_CB bool EINTU_NOT( EINTVALU n ) { return SRC(_,u_not)(n); }
PAW_FQK_CB bool EINTU_GETBIT( EINTVALU n, pawru i )
	{ return SRC(_,u_getbit)(n,i); }
PAW_FQK_CB EINTVALU EINTU_SETBIT( EINTVALU n, pawru i, pawb val )
	{ return SRC(_,u_setbit)(n,i,val); }
PAW_FQK_CB EINTVALU EINTU_INC( EINTVALU n ) { return SRC(_,u_inc)(n); }
PAW_FQK_CB EINTVALU EINTU_DEC( EINTVALU n ) { return SRC(_,u_dec)(n); }
PAW_FQK_CB EINTVALU EINTU_NEG( EINTVALU n ) { return SRC(_,u_neg)(n); }
PAW_FQK_CB EINTVALU EINTU_BWN( EINTVALU n ) { return SRC(_,u_bwn)(n); }
PAW_FQK_CB pawrd EINTU_CMP( EINTVALU a, EINTVALU b ) { return SRC(_,u_cmp)(a,b); }
PAW_FQK_CB EINTVALU EINTU_BWA( EINTVALU a, EINTVALU b ) { return SRC(_,u_bwa)(a,b); }
PAW_FQK_CB EINTVALU EINTU_BWO( EINTVALU a, EINTVALU b ) { return SRC(_,u_bwo)(a,b); }
PAW_FQK_CB EINTVALU EINTU_BWX( EINTVALU a, EINTVALU b ) { return SRC(_,u_bwx)(a,b); }
PAW_FQK_CB EINTVALU EINTU_ROL( EINTVALU n, pawru move ) { return SRC(_,u_rol)(n,move); }
PAW_FQK_CB EINTVALU EINTU_ROR( EINTVALU n, pawru move ) { return SRC(_,u_ror)(n,move); }
PAW_FQK_CB EINTVALU EINTU_SHL( EINTVALU n, pawru move ) { return SRC(_,u_shl)(n,move); }
PAW_FQK_CB EINTVALU EINTU_SHR( EINTVALU n, pawru move ) { return SRC(_,u_shr)(n,move); }
PAW_FQK_CB EINTVALU EINTU_SSR( EINTVALU n, pawru move ) { return SRC(_,u_ssr)(n,move); }
PAW_FQK_CB EINTVALU EINTU_USR( EINTVALU n, pawru move ) { return SRC(_,u_usr)(n,move); }
PAW_FQK_CB EINTVALU EINTU_ADD( EINTVALU a, EINTVALU b ) { return SRC(_,u_add)(a,b); }
PAW_FQK_CB EINTVALU EINTU_SUB( EINTVALU a, EINTVALU b ) { return SRC(_,u_sub)(a,b); }
PAW_FQK_CB EINTVALU EINTU_MUL( EINTVALU a, EINTVALU b ) { return SRC(_,u_mul)(a,b); }
PAW_FQK_CB EINTDIVU EINTU__DIV( EINTVALU a, EINTVALU b ) { return SRC(_,u__div)(a,b); }
PAW_FQK_CB EINTVALU EINTU_DIV( EINTVALU a, EINTVALU b ) { return SRC(_,u_div)(a,b); }
PAW_FQK_CB EINTVALU EINTU_MOD( EINTVALU a, EINTVALU b ) { return SRC(_,u_mod)(a,b); }

PAW_FQK_CB EINTVALD EINTD_V( pawrd n ) { return SRC(_,d_v)(n); }
PAW_FQK_CB pawru EINTD_LEN( EINTVALD n ) { return SRC(_,d_len)(n); }
PAW_FQK_CB bool EINTD_NOT( EINTVALD n ) { return SRC(_,d_not)(n); }
PAW_FQK_CB EINTVALD EINTD_INC( EINTVALD n ) { return SRC(_,d_inc)(n); }
PAW_FQK_CB EINTVALD EINTD_DEC( EINTVALD n ) { return SRC(_,d_dec)(n); }
PAW_FQK_CB EINTVALD EINTD_NEG( EINTVALD n ) { return SRC(_,d_neg)(n); }
PAW_FQK_CB EINTVALD EINTD_BWN( EINTVALD n ) { return SRC(_,d_bwn)(n); }
PAW_FQK_CB pawrd EINTD_CMP( EINTVALD a, EINTVALD b ) { return SRC(_,d_cmp)(a,b); }
PAW_FQK_CB EINTVALD EINTD_BWA( EINTVALD a, EINTVALD b ) { return SRC(_,d_bwa)(a,b); }
PAW_FQK_CB EINTVALD EINTD_BWO( EINTVALD a, EINTVALD b ) { return SRC(_,d_bwo)(a,b); }
PAW_FQK_CB EINTVALD EINTD_BWX( EINTVALD a, EINTVALD b ) { return SRC(_,d_bwx)(a,b); }
PAW_FQK_CB EINTVALD EINTD_ROL( EINTVALD n, pawru move ) { return SRC(_,d_rol)(n,move); }
PAW_FQK_CB EINTVALD EINTD_ROR( EINTVALD n, pawru move ) { return SRC(_,d_ror)(n,move); }
PAW_FQK_CB EINTVALD EINTD_SHL( EINTVALD n, pawru move ) { return SRC(_,d_shl)(n,move); }
PAW_FQK_CB EINTVALD EINTD_SHR( EINTVALD n, pawru move ) { return SRC(_,d_shr)(n,move); }
PAW_FQK_CB EINTVALD EINTD_SSR( EINTVALD n, pawru move ) { return SRC(_,d_ssr)(n,move); }
PAW_FQK_CB EINTVALD EINTD_USR( EINTVALD n, pawru move ) { return SRC(_,d_usr)(n,move); }
PAW_FQK_CB EINTVALD EINTD_ADD( EINTVALD a, EINTVALD b ) { return SRC(_,d_add)(a,b); }
PAW_FQK_CB EINTVALD EINTD_SUB( EINTVALD a, EINTVALD b ) { return SRC(_,d_sub)(a,b); }
PAW_FQK_CB EINTVALD EINTD_MUL( EINTVALD a, EINTVALD b ) { return SRC(_,d_mul)(a,b); }
PAW_FQK_CB EINTDIVD EINTD__DIV( EINTVALD a, EINTVALD b ) { return SRC(_,d__div)(a,b); }
PAW_FQK_CB EINTVALD EINTD_DIV( EINTVALD a, EINTVALD b ) { return SRC(_,d_div)(a,b); }
PAW_FQK_CB EINTVALD EINTD_MOD( EINTVALD a, EINTVALD b ) { return SRC(_,d_mod)(a,b); }

#include "template_paweint_undefs.h"
#undef SRC
#undef DST
