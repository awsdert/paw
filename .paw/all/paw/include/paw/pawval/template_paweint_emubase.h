#include "template_paweint_checks.h"
#define QCK PAW_QCK_CB
#define EINTPP(TXT) TXT
#define EINTRAW EINTNAME(_,raw)
#define EINTEMU EINTNAME(_,e)
#define EINTVALD EINTNAME(_,d)
#define EINTVALU EINTNAME(_,u)
#if !defined(EINTBASE)
#	error "EINTBASE(PFX,SFX) must be defined for template_paweint_emubase.h!"
#	include <abort_compilation>
#endif
#include "template_paweint_emudefs.h"
PAW_TYPEDEF union
{
	EINTEVALU raw[2];
	struct { EINTEVALU lower; union { EINTEVALD d; EINTEVALU u; } upper; };
} EINTEMU;
/* Separate unions are to ensure the compiler catches comparison mismatches */
PAW_TYPEDEF union { EINTEVALU raw[2]; EINTEMU emu; } EINTVALU;
PAW_TYPEDEF union { EINTEVALU raw[2]; EINTEMU emu; } EINTVALD;
#include "template_paweint_defines.h"
PAW_TYPEDEF struct { EINTVALD quo, rem; } EINTDIVD;
PAW_TYPEDEF struct { EINTVALU quo, rem; } EINTDIVU;

/* Internal usage functions */
PAW_QCK_CB pawjvu EINTE_LEN( EINTEMU n, pawjvu width, bool find0 )
{
	pawjvu len = 0, max = bitsof(n.raw[0]);
	if ( width <= max )
		return EINTEU__LEN( n.raw[0].emu, width, find0 );
	len = EINTEU__LEN( n.raw[1].emu, width - max, find0 );
	return len ? (len + max) : EINTEU__LEN( n.raw[0].emu, max, find0 );
}
PAW_QCK_CB bool EINTE_NOT( EINTEMU n )
	{ return (EINTEU_NOT( n.raw[0] ) && EINTEU_NOT(n.raw[1])); }
PAW_QCK_CB EINTEMU EINTE_BWN( EINTEMU n )
{
	n.raw[0] = EINTEU_BWN(n.raw[0]);
	n.raw[1] = EINTEU_BWN(n.raw[1]);
	return n;
}
PAW_QCK_CB EINTEMU EINTE_INC( EINTEMU n )
{
	EINTEVALU x = n.raw[0];
	n.raw[0] = EINTEU_INC(x);
	/* We'll worry about the overflow messages later */
	if ( EINTEU_CMP( x, n.raw[0] ) < 0 )
		n.raw[1] = EINTEU_INC(n.raw[1]);
	return n;
}
PAW_QCK_CB EINTEMU EINTE_DEC( EINTEMU n )
{
	EINTEVALU x = n.raw[0];
	n.raw[0] = EINTEU_DEC(x);
	if ( EINTEU_CMP( n.raw[0], x ) > 0 )
		n.raw[1] = EINTEU_DEC( n.raw[1] );
	return n;
}
PAW_QCK_CB EINTEMU EINTE_NEG( EINTEMU n ) { return EINTE_INC( EINTE_BWN( n ) ); }
PAW_QCK_CB EINTEMU EINTE_USR( EINTEMU n, pawjvu move )
{
	EINTEVALU u = n.raw[1], l = n.raw[0];
	pawjvu bits = bitsof(n.raw[0]), mov = move % bits;
	n.raw[0] = EINTEU_BWO(EINTEU_USR(l,mov),EINTEU_SHL(u,bits - mov));
	n.raw[1] = EINTEU_USR(u,mov);
	return n;
}
PAW_QCK_CB EINTEMU EINTE_SSR( EINTEMU n, pawjvu move )
{
	EINTEVALU u = n.raw[1], l = n.raw[0];
	pawjvu bits = bitsof(u), mov = move % bits;
	EINTEVALU x = EINTEU_SSR(u,mov);
	if ( move > bits )
	{
		n.raw[0] = x;
		n.raw[1] = EINTEU_MAX();
	}
	else
	{
		n.raw[0] = EINTEU_BWO(EINTEU_USR(n.raw[0],mov),EINTEU_SHL(u,bits - mov));
		n.raw[1] = x;
	}
	return n;
}
PAW_QCK_CB EINTEMU EINTE_SHL( EINTEMU n, pawjvu move )
{
	EINTEVALU u = n.raw[1], l = n.raw[0];
	pawjvu bits = bitsof(n.raw[0]), mov = move % bits;
	n.raw[1] = EINTEU_BWO(EINTEU_SHL(u,mov),EINTEU_USR(l,bits - mov));
	n.raw[0] = EINTEU_SHL(l,mov);
	return n;
}
PAW_QCK_CB EINTEMU EINTE_ROR( EINTEMU n, pawjvu move )
{
	EINTEVALU u = n.raw[1], l = n.raw[0];
	pawjvu bits = bitsof(n.raw[0]), shr = move % bits, shl = bits - shr;
	n.raw[0] = EINTEU_BWO(EINTEU_USR(l,shr),EINTEU_SHL(u,shl));
	n.raw[1] = EINTEU_BWO(EINTEU_USR(u,shr),EINTEU_SHL(l,shl));
	return n;
}
PAW_QCK_CB EINTEMU EINTE_ROL( EINTEMU n, pawjvu move )
{
	EINTEVALU u = n.raw[1], l = n.raw[0];
	pawjvu bits = bitsof(n.raw[0]), shl = move % bits, shr = bits - shl;
	n.raw[1] = EINTEU_BWO(EINTEU_SHL(l,shl),EINTEU_USR(u,shr));
	n.raw[0] = EINTEU_BWO(EINTEU_SHL(u,shl),EINTEU_USR(l,shr));
	return n;
}
PAW_QCK_CB EINTEMU EINTE_BWA( EINTEMU a, EINTEMU b )
{
	a.raw[0] = EINTEU_BWA(a.raw[0],b.raw[0]);
	a.raw[1] = EINTEU_BWA(a.raw[1],b.raw[1]);
	return a;
}
PAW_QCK_CB EINTEMU EINTE_BWO( EINTEMU a, EINTEMU b )
{
	a.raw[0] = EINTEU_BWO(a.raw[0],b.raw[0]);
	a.raw[1] = EINTEU_BWO(a.raw[1],b.raw[1]);
	return a;
}
PAW_QCK_CB EINTEMU EINTE_BWX( EINTEMU a, EINTEMU b )
{
	a.raw[0] = EINTEU_BWX(a.raw[0],b.raw[0]);
	a.raw[1] = EINTEU_BWX(a.raw[1],b.raw[1]);
	return a;
}
PAW_QCK_CB pawd EINTE_CMP( EINTEMU a, EINTEMU b )
{
	pawd cmp = EINTEU_CMP( a.raw[1], b.raw[1] );
	return cmp ? cmp : EINTEU_CMP( a.raw[0], b.raw[0] );
}
PAW_QCK_CB EINTEMU EINTE_ADD( EINTEMU a, EINTEMU b )
{
	pawd cmp = 0;
	EINTEVALU x = a.raw[0];
	a.raw[0] = EINTEU_ADD(a.raw[0],b.raw[0]);
	cmp = EINTEU_CMP( a.raw[0], x );
	if ( cmp < 0 || (cmp == 0 && !EINTEU_NOT(b.raw[0])) )
		a.raw[0] = EINTEU_INC(a.raw[0]);
	a.raw[1] = EINTEU_ADD(a.raw[1],b.raw[1]);
	return a;
}
PAW_QCK_CB EINTEMU EINTE_SUB( EINTEMU a, EINTEMU b )
{
	pawd cmp = 0;
	EINTEVALU x = a.raw[0];
	a.raw[0] = EINTEU_SUB(a.raw[0],b.raw[0]);
	cmp = EINTEU_CMP( a.raw[0], x );
	if ( cmp > 0 || (cmp == 0 && !EINTEU_NOT(b.raw[0])) )
		a.raw[0] = EINTEU_DEC(a.raw[0]);
	a.raw[1] = EINTEU_SUB(a.raw[1],b.raw[1]);
	return a;
}
PAW_QCK_CB EINTEMU EINTE_MUL( EINTEMU a, EINTEMU b )
{
	pawjvu move = 0;
	EINTEVALU bit = EINTEU_V(1);
	EINTEMU c = {{0}};
	while ( !EINTEU_NOT(bit) )
	{
		if ( !EINTEU_NOT( EINTEU_BWA(b.raw[0],bit) ) )
			c = EINTE_ADD( c, EINTE_SHL( a, move ) );
		if ( !EINTEU_NOT( EINTEU_BWA(b.raw[1],bit) ) )
			c = EINTE_ADD( c, EINTE_SHL( a, move + bitsof(bit) ) );
		EINTEU_SHL(bit,1);
		move++;
	}
	return c;
}
PAW_QCK_CB EINTDIVU EINTE__DIV( EINTEMU a, EINTEMU b )
{
	pawjvu max = bitsof(a.raw[0]);
	pawjvu alen = EINTE_LEN(a,bitsof(a),0);
	pawjvu blen = EINTE_LEN(b,bitsof(b),0), len = alen;
	EINTDIVU c = {{{0}}};
	EINTSEGU bit = 1;
	EINTEMU div = b;
	c.rem.emu = a;
	/* In division by 0 you take nothing from the remainder 0 times so 0 is
	 * the answer */
	if ( !alen || !blen || blen > alen )
		return c;
	b = EINTE_SHL( b, alen - blen );
	while ( EINTE_CMP( b, div ) >= 0 )
	{
		if ( EINTE_CMP( c.rem.emu, b ) < 0 )
		{
			b = EINTE_USR( b, 1 );
			--len;
		}
		else
		{
			c.quo.emu = EINTE_SHL( c.quo.emu, alen - len );
			c.quo.raw[0].raw[0] |= 1;
			EINTE_SUB( c.rem.emu, b );
			len = EINTE_LEN(c.rem.emu,alen,0);
			b = EINTE_USR( b, alen - len );
			alen = len;
		}
	}
	return c;
}
/* Unsigned operations */
QCK EINTVALU EINTU_V( pawjvu n )
	{ EINTVALU x = {{EINTEU_V(n),EINTEU_V(0)}}; return x; }
QCK EINTVALU EINTU_MAX(void)
	{ EINTVALU n = {{EINTEU_MAX(),EINTEU_MAX()}}; return n; }
QCK pawjvu EINTU_LEN( EINTVALU n ) { return EINTE_LEN( n.emu, bitsof(n), 0 ); }
QCK bool  EINTU_NOT( EINTVALU n ) { return EINTE_NOT( n.emu ); }
QCK EINTVALU EINTU_BWN( EINTVALU n )
	{ n.emu = EINTE_BWN( n.emu ); return n; }
QCK EINTVALU EINTU_INC( EINTVALU n )
	{ n.emu = EINTE_INC( n.emu ); return n; }
QCK EINTVALU EINTU_DEC( EINTVALU n )
	{ n.emu = EINTE_DEC( n.emu ); return n; }
QCK EINTVALU EINTU_NEG( EINTVALU n )
	{ n.emu = EINTE_NEG( n.emu ); return n; }
QCK EINTVALU EINTU_USR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALU EINTU_SSR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALU EINTU_SHR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALU EINTU_SHL( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_SHL( n.emu, move ); return n; }
QCK EINTVALU EINTU_ROR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_ROR( n.emu, move ); return n; }
QCK EINTVALU EINTU_ROL( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_ROL( n.emu, move ); return n; }
QCK pawd EINTU_CMP( EINTVALU a, EINTVALU b )
{
	pawd cmp = EINTEU_CMP( a.raw[1], b.raw[1] );
	return cmp ? cmp : EINTEU_CMP( a.raw[0], b.raw[0] );
}
QCK EINTVALU EINTU_BWA( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWA( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_BWO( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWO( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_BWX( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWX( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_ADD( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_ADD( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_SUB( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_SUB( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_MUL( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_MUL( a.emu, b.emu ); return a; }
QCK EINTDIVU EINTU__DIV( EINTVALU a, EINTVALU b )
	{ return EINTE__DIV( a.emu, b.emu ); }
QCK EINTVALU EINTU_DIV( EINTVALU a, EINTVALU b )
	{ return EINTE__DIV( a.emu, b.emu ).quo; }
QCK EINTVALU EINTU_MOD( EINTVALU a, EINTVALU b )
	{ return EINTE__DIV( a.emu, b.emu ).quo; }

/* Signed operations */
QCK EINTVALD EINTD_V( pawjvu n )
{
	EINTVALD x = {{EINTEU_V(0)}};
	x.raw[0].emu = EINTED_V(n).emu;
	return x;
}
QCK EINTVALD EINTD_MAX(void)
{
	EINTVALD n = {{EINTEU_V(0)}};
	n.raw[1].emu = EINTED_MAX().emu;
	n.raw[0] = EINTEU_MAX();
	return n;
}
QCK EINTVALD EINTD_MIN(void)
{
	EINTVALD n = {{EINTEU_V(0)}};
	n.raw[1].emu = EINTED_MIN().emu;
	return n;
}
QCK pawjvu EINTD_LEN( EINTVALD n )
	{ return EINTE_LEN( n.emu, bitsof(n), (EINTED_CMP(n.emu.upper.d, EINTED_V(0)) < 0) ); }
QCK bool  EINTD_NOT( EINTVALD n ) { return EINTE_NOT( n.emu ); }
QCK EINTVALD EINTD_BWN( EINTVALD n )
	{ n.emu = EINTE_BWN( n.emu ); return n; }
QCK EINTVALD EINTD_INC( EINTVALD n )
	{ n.emu = EINTE_INC( n.emu ); return n; }
QCK EINTVALD EINTD_DEC( EINTVALD n )
	{ n.emu = EINTE_DEC( n.emu ); return n; }
QCK EINTVALD EINTD_NEG( EINTVALD n )
	{ n.emu = EINTE_NEG( n.emu ); return n; }
QCK EINTVALD EINTD_USR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALD EINTD_SSR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALD EINTD_SHR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALD EINTD_SHL( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SHL( n.emu, move ); return n; }
QCK EINTVALD EINTD_ROR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_ROR( n.emu, move ); return n; }
QCK EINTVALD EINTD_ROL( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_ROL( n.emu, move ); return n; }
QCK pawd EINTD_CMP( EINTVALD a, EINTVALD b )
{
	EINTEVALD *A = ((void*)(a.raw+1)), *B = ((void*)(b.raw+1));
	pawd cmp = EINTED_CMP( *A, *B );
	return cmp ? cmp : EINTEU_CMP( a.raw[0], b.raw[0] );
}
QCK EINTVALD EINTD_BWA( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWA( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_BWO( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWO( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_BWX( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWX( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_ADD( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_ADD( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_SUB( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_SUB( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_MUL( EINTVALD a, EINTVALD b )
{
	EINTEVALD nil = EINTED_V(0);
	pawd acmp = EINTED_CMP( *((EINTEVALD*)(a.raw+1)), nil );
	pawd bcmp = EINTED_CMP( *((EINTEVALD*)(b.raw+1)), nil );
	EINTEMU x = (bcmp < 0) ? EINTE_NEG(b.emu) : b.emu;
	if ( acmp >= 0 )
	{
		a.emu = EINTE_MUL( a.emu, x );
		if ( bcmp < 0 )
			return EINTD_NEG(a);
	}
	a.emu = EINTE_MUL( EINTE_NEG( a.emu ), x );
	return (bcmp < 0) ? a : EINTD_NEG( a );
}
QCK EINTDIVD EINTD__DIV( EINTVALD a, EINTVALD b )
{
	EINTDIVU n = {{0}};
	EINTDIVD N = {{0}};
	EINTEVALD nil = EINTED_V(0);
	pawd acmp = EINTED_CMP( *((EINTEVALD*)(a.raw+1)), nil );
	pawd bcmp = EINTED_CMP( *((EINTEVALD*)(b.raw+1)), nil );
	EINTEMU x = (bcmp < 0) ? EINTE_NEG(b.emu) : b.emu;
	if ( acmp >= 0 )
	{
		n = EINTE__DIV( a.emu, x );
		N.rem.emu = n.rem.emu;
		N.quo.emu = (bcmp < 0) ? EINTE_NEG(n.quo.emu) : n.quo.emu;
		return N;
	}
	n = EINTE__DIV( EINTE_NEG( a.emu ), x );
	N.quo.emu = (bcmp < 0) ? n.quo.emu : EINTE_NEG(n.quo.emu);
	N.rem.emu = EINTE_NEG(n.rem.emu);
	return N;
}
QCK EINTVALD EINTD_DIV( EINTVALD a, EINTVALD b )
	{ return EINTD__DIV( a, b ).quo; }
QCK EINTVALD EINTD_MOD( EINTVALD a, EINTVALD b )
	{ return EINTD__DIV( a, b ).rem; }
#undef EINTRAW
