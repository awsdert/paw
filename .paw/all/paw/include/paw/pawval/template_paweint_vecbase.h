#include "paweint_vector_functions.h"
#include "template_paweint_checks.h"
#define EINTPP(TXT) TXT
#define EINTEMU EINTNAME(_,e)
#define EINTVALD EINTNAME(_,d)
#define EINTVALU EINTNAME(_,u)
#define EINTVEC_COUNT	PAWINT_DIVCIEL(MIN_WIDTH,PAWTD_WIDTH)
#define EINTVEC_LSEGS	(EINTVEC_COUNT - 1)
#define EINTVEC_WIDTH	(EINTVEC_COUNT * PAWTD_WIDTH)
#define EINTVEC_SIZE	(EINTVEC_WIDTH / PAWC_WIDTH)

typedef union
{
	pawtu raw[EINTVEC_COUNT];
	struct { pawtu lower[EINTVEC_LSEGS]; union { pawtd d; pawtu u; } upper; };
} EINTEMU;
/* Separate unions are to ensure the compiler catches comparison mismatches */
typedef union { pawtu raw[EINTVEC_COUNT]; EINTEMU emu; } EINTVALD;
typedef union { pawtu raw[EINTVEC_COUNT]; EINTEMU emu; } EINTVALU;
#include "template_paweint_defines.h"
typedef struct { EINTVALD quo, rem; } EINTDIVD;
typedef struct { EINTVALU quo, rem; } EINTDIVU;

/* We cast so that the unit tests don't need to know the type, they just need
 * to give it's integers the types pawtd/pawtu */

PAW_QCK_CB EINTVALU EINTU_V( pawru n ) { EINTVALU x = {{0}}; x.raw[0] = n; return x; }
PAW_QCK_CB pawru EINTU_LEN( EINTVALU n )
	{ return _paweintu_len( (pawevec)&n, sizeof(n) ); }
PAW_QCK_CB pawb EINTU_NOT( EINTVALU n )
	{ return _paweint_not( (pawevec)&n, sizeof(n) ); }
PAW_QCK_CB pawb EINTU_GETBIT( EINTVALU n, pawru i )
	{ return _paweint_getbit( (pawevec)&n, sizeof(n), i ); }
PAW_QCK_CB EINTVALU EINTU_SETBIT( EINTVALU n, pawru i, pawb val )
	{ _paweint_setbit( (pawevec)&n, sizeof(n), i, val ); return n; }
PAW_QCK_CB EINTVALU EINTU_BWN( EINTVALU n )
	{ _paweint_bwn( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALU EINTU_INC( EINTVALU n )
	{ _paweint_inc( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALU EINTU_DEC( EINTVALU n )
	{ _paweint_dec( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALU EINTU_NEG( EINTVALU n )
	{ _paweint_neg( n.raw, sizeof(n) ); return n; }

PAW_QCK_CB pawd EINTU_CMP( EINTVALU a, EINTVALU b )
	{ return _paweintu_cmp( a.raw, b.raw, sizeof(a) ); }

PAW_QCK_CB EINTVALU EINTU_BWA( EINTVALU a, EINTVALU b )
	{ _paweint_bwa( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALU EINTU_BWO( EINTVALU a, EINTVALU b )
	{ _paweint_bwo( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALU EINTU_BWX( EINTVALU a, EINTVALU b )
	{ _paweint_bwx( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALU EINTU_SSR( EINTVALU n, pawru move )
	{ _paweintd_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALU EINTU_USR( EINTVALU n, pawru move )
	{ _paweintu_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALU EINTU_SHR( EINTVALU n, pawru move )
	{ _paweintu_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALU EINTU_SHL( EINTVALU n, pawru move )
	{ _paweint_shl( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALU EINTU_ROR( EINTVALU n, pawru move )
{
	EINTVALU cpy = n;
	_paweint_ror( n.raw, (pawevec)&cpy, sizeof(n), move );
	return n;
}
PAW_QCK_CB EINTVALU EINTU_ROL( EINTVALU n, pawru move )
{
	EINTVALU cpy = n;
	_paweint_rol( n.raw, (pawevec)&cpy, sizeof(n), move );
	return n;
}

PAW_QCK_CB EINTVALU EINTU_ADD( EINTVALU a, EINTVALU b )
	{ _paweint_add( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALU EINTU_SUB( EINTVALU a, EINTVALU b )
	{ _paweint_sub( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALU EINTU_MUL( EINTVALU a, EINTVALU b )
{
	EINTVALU n = {0};
	_paweintu_mul( n.raw, b.raw, a.raw, sizeof(a) );
	return n;
}
PAW_QCK_CB EINTDIVU EINTU__DIV( EINTVALU a, EINTVALU b )
{
	EINTDIVU div = {{0}};
	_paweintu_div( div.quo.raw, div.rem.raw, a.raw, b.raw, sizeof(a) );
	return div;
}
PAW_QCK_CB EINTVALU EINTU_DIV( EINTVALU a, EINTVALU b ) { return EINTU__DIV( a, b ).quo; }
PAW_QCK_CB EINTVALU EINTU_MOD( EINTVALU a, EINTVALU b ) { return EINTU__DIV( a, b ).rem; }

PAW_QCK_CB EINTVALD EINTD_V( pawru n ) { EINTVALD x = {{0}}; x.raw[0] = n; return x; }
PAW_QCK_CB pawru EINTD_LEN( EINTVALD n )
	{ return _paweintd_len( (pawevec)&n, sizeof(n) ); }
PAW_QCK_CB pawb EINTD_NOT( EINTVALD n )
	{ return _paweint_not( (pawevec)&n, sizeof(n) ); }
PAW_QCK_CB EINTVALD EINTD_BWN( EINTVALD n )
	{ _paweint_bwn( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALD EINTD_INC( EINTVALD n )
	{ _paweint_inc( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALD EINTD_DEC( EINTVALD n )
	{ _paweint_dec( n.raw, sizeof(n) ); return n; }
PAW_QCK_CB EINTVALD EINTD_NEG( EINTVALD n )
	{ _paweint_neg( n.raw, sizeof(n) ); return n; }

PAW_QCK_CB pawd EINTD_CMP( EINTVALD a, EINTVALD b )
	{ return _paweintd_cmp( a.raw, b.raw, sizeof(a) ); }

PAW_QCK_CB EINTVALD EINTD_BWA( EINTVALD a, EINTVALD b )
	{ _paweint_bwa( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALD EINTD_BWO( EINTVALD a, EINTVALD b )
	{ _paweint_bwo( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALD EINTD_BWX( EINTVALD a, EINTVALD b )
	{ _paweint_bwx( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALD EINTD_SSR( EINTVALD n, pawru move )
	{ _paweintd_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALD EINTD_USR( EINTVALD n, pawru move )
	{ _paweintd_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALD EINTD_SHR( EINTVALD n, pawru move )
	{ _paweintd_shr( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALD EINTD_SHL( EINTVALD n, pawru move )
	{ _paweint_shl( n.raw, sizeof(n), move ); return n; }
PAW_QCK_CB EINTVALD EINTD_ROR( EINTVALD n, pawru move )
{
	EINTVALD cpy = n;
	_paweint_ror( n.raw, (pawevec)&cpy, sizeof(n), move );
	return n;
}
PAW_QCK_CB EINTVALD EINTD_ROL( EINTVALD n, pawru move )
{
	EINTVALD cpy = n;
	_paweint_rol( n.raw, (pawevec)&cpy, sizeof(n), move );
	return n;
}

PAW_QCK_CB EINTVALD EINTD_ADD( EINTVALD a, EINTVALD b )
	{ _paweint_add( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALD EINTD_SUB( EINTVALD a, EINTVALD b )
	{ _paweint_sub( a.raw, b.raw, sizeof(a) ); return a; }
PAW_QCK_CB EINTVALD EINTD_MUL( EINTVALD a, EINTVALD b )
{
	EINTVALD n = {{0}};
	_paweintd_mul( n.raw, a.raw, b.raw, sizeof(n) );
	return n;
}
PAW_QCK_CB EINTDIVD EINTD__DIV( EINTVALD a, EINTVALD b )
{
	EINTDIVD n = {{0}};
	_paweintd_div( n.quo.raw, n.rem.raw, a.raw, b.raw, sizeof(a) );
	return n;
}
PAW_QCK_CB EINTVALD EINTD_DIV( EINTVALD a, EINTVALD b ) { return EINTD__DIV( a, b ).quo; }
PAW_QCK_CB EINTVALD EINTD_MOD( EINTVALD a, EINTVALD b ) { return EINTD__DIV( a, b ).rem; }
#undef EINTVEC_SIZE
#undef EINTVEC_WIDTH
#undef EINTVEC_LSEGS
#undef EINTVEC_COUNT
