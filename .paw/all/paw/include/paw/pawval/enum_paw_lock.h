#pragma once
typedef enum
{
	PAW_REMLOCK = 0,
	PAW_TRYLOCK_ONETIME,
	PAW_TRYLOCK_N_TIMES,
	PAW_W84LOCK_FOREVER,
	PAW_W84LOCK_UPTO_MS,
	PAW_W84LOCK_UPTO_NS
} PAW_LOCK_CALL;
