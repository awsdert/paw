/* Maintain out sanity with easy access names */
#define EINTEVALU	EINTBASE(_,u)
#define EINTEVALD	EINTBASE(_,d)
#define EINTEDIVU	EINTBASE(_,divu)
#define EINTEDIVD	EINTBASE(_,divd)

/* Return max value */
#define EINTE_MAX EINTNAME(_,e_max)
/* Convert value n to EINTVALD */
#define EINTE_V EINTNAME(_,e_v)
/* Is N not anything but 0? */
#define EINTE_NOT EINTNAME(_,e_not)
/* Increment N */
#define EINTE_INC EINTNAME(_,e_inc)
/* Decrement N */
#define EINTE_DEC EINTNAME(_,e_dec)
/* Negate N */
#define EINTE_NEG EINTNAME(_,e_neg)
/* Bit count of actually used bits, 0 to MAX checks for ending 1, MIN to -1
 * checks for 0 */
#define EINTE_LEN EINTNAME(_,e_len)
/* Unsigned comparison */
#define EINTE_CMP EINTNAME(_,e_cmp)
/* Bitwise NOT (flip all bits) */
#define EINTE_BWN EINTNAME(_,e_bwn)
/* Bitwise AND (keep all bits from A that matched with those of B) */
#define EINTE_BWA EINTNAME(_,e_bwa)
/* Bitwise OR (keep all bits from both A and B) */
#define EINTE_BWO EINTNAME(_,e_bwo)
/* Bitwise XOR (keep all bits from A that did not match with those of B) */
#define EINTE_BWX EINTNAME(_,e_bwx)
/* Signed Shift Right */
#define EINTE_SSR EINTNAME(_,e_ssr)
/* Unsigned Shift Right */
#define EINTE_USR EINTNAME(_,e_usr)
/* Shift bits Left */
#define EINTE_SHL EINTNAME(_,e_shl)
/* Rotate bits left */
#define EINTE_ROL EINTNAME(_,e_rol)
/* Rotate bits right */
#define EINTE_ROR EINTNAME(_,e_ror)
/* Addition */
#define EINTE_ADD EINTNAME(_,e_add)
/* Subtraction */
#define EINTE_SUB EINTNAME(_,e_sub)
/* Multiplication */
#define EINTE_MUL EINTNAME(_,e_mul)
/* Combined Division & Modulo, N / 0 = 0 (quo), N (rem).
 * Nothing is taken from N to the quotient therefore the result is always 0
 * just as with multiplication where nothing is duplicated into the result
 * when multiplied by 0 */
#define EINTE__DIV EINTNAME(_,e__div)
/* d__div(a,b).quo */
#define EINTE_DIV EINTNAME(_,e_div)
/* d__div(a,b).rem */
#define EINTE_MOD EINTNAME(_,e_mod)

/* Return max value */
#define EINTEU_MAX EINTBASE(_,u_max)
/* Convert value n to EINTVALD */
#define EINTEU_V EINTBASE(_,u_v)
/* Is N not anything but 0? */
#define EINTEU_NOT EINTBASE(_,u_not)
/* Increment N */
#define EINTEU_INC EINTBASE(_,u_inc)
/* Decrement N */
#define EINTEU_DEC EINTBASE(_,u_dec)
/* Negate N */
#define EINTEU_NEG EINTBASE(_,u_neg)
/* Specifically for division usage */
#define EINTEU__LEN EINTBASE(_,e_len)
/* Bit count of actually used bits, 0 to MAX checks for ending 1, MIN to -1
 * checks for 0 */
#define EINTEU_LEN EINTBASE(_,u_len)
/* Unsigned comparison */
#define EINTEU_CMP EINTBASE(_,u_cmp)
/* Bitwise NOT (flip all bits) */
#define EINTEU_BWN EINTBASE(_,u_bwn)
/* Bitwise AND (keep all bits from A that matched with those of B) */
#define EINTEU_BWA EINTBASE(_,u_bwa)
/* Bitwise OR (keep all bits from both A and B) */
#define EINTEU_BWO EINTBASE(_,u_bwo)
/* Bitwise XOR (keep all bits from A that did not match with those of B) */
#define EINTEU_BWX EINTBASE(_,u_bwx)
/* Signed Shift Right */
#define EINTEU_SSR EINTBASE(_,u_ssr)
/* Unsigned Shift Right */
#define EINTEU_USR EINTBASE(_,u_usr)
/* Default Shift Left */
#define EINTEU_SHL EINTBASE(_,u_shl)
/* Default Shift Right */
#define EINTEU_SHR EINTBASE(_,u_shr)
/* Rotate bits left */
#define EINTEU_ROL EINTBASE(_,u_rol)
/* Rotate bits right */
#define EINTEU_ROR EINTBASE(_,u_ror)
/* Addition */
#define EINTEU_ADD EINTBASE(_,u_add)
/* Subtraction */
#define EINTEU_SUB EINTBASE(_,u_sub)
/* Multiplication */
#define EINTEU_MUL EINTBASE(_,u_mul)
/* Combined Division & Modulo, N / 0 = 0 (quo), N (rem).
 * Nothing is taken from N to the quotient therefore the result is always 0
 * just as with multiplication where nothing is duplicated into the result
 * when multiplied by 0 */
#define EINTEU__DIV EINTBASE(_,u__div)
/* d__div(a,b).quo */
#define EINTEU_DIV EINTBASE(_,u_div)
/* d__div(a,b).rem */
#define EINTEU_MOD EINTBASE(_,u_mod)

/* Return max value */
#define EINTED_MAX EINTBASE(_,d_max)
/* Return min value */
#define EINTED_MIN EINTBASE(_,d_min)
/* Convert value n to EINTVALD */
#define EINTED_V EINTBASE(_,d_v)
/* Is N not anything but 0? */
#define EINTED_NOT EINTBASE(_,d_not)
/* Increment N */
#define EINTED_INC EINTBASE(_,d_inc)
/* Decrement N */
#define EINTED_DEC EINTBASE(_,d_dec)
/* Negate N */
#define EINTED_NEG EINTBASE(_,d_neg)
/* Bit count of actually used bits, 0 to MAX checks for ending 1, MIN to -1
 * checks for 0 */
#define EINTED_LEN EINTBASE(_,d_len)
/* Signed comparison */
#define EINTED_CMP EINTBASE(_,d_cmp)
/* Bitwise NOT (flip all bits) */
#define EINTED_BWN EINTBASE(_,d_bwn)
/* Bitwise AND (keep all bits from A that matched with those of B) */
#define EINTED_BWA EINTBASE(_,d_bwa)
/* Bitwise OR (keep all bits from both A and B) */
#define EINTED_BWO EINTBASE(_,d_bwo)
/* Bitwise XOR (keep all bits from A that did not match with those of B) */
#define EINTED_BWX EINTBASE(_,d_bwx)
/* Signed Shift Right */
#define EINTED_SSR EINTBASE(_,d_ssr)
/* Unsigned Shift Right */
#define EINTED_USR EINTBASE(_,d_usr)
/* Default Shift Left */
#define EINTED_SHL EINTBASE(_,d_shl)
/* Default Shift Right */
#define EINTED_SHR EINTBASE(_,d_shr)
/* Rotate bits left */
#define EINTED_ROL EINTBASE(_,d_rol)
/* Rotate bits right */
#define EINTED_ROR EINTBASE(_,d_ror)
/* Addition */
#define EINTED_ADD EINTBASE(_,d_add)
/* Subtraction */
#define EINTED_SUB EINTBASE(_,d_sub)
/* Multiplication */
#define EINTED_MUL EINTBASE(_,d_mul)
/* Combined Division & Modulo, N / 0 = 0 (quo), N (rem).
 * Nothing is taken from N to the quotient therefore the result is always 0
 * just as with multiplication where nothing is duplicated into the result
 * when multiplied by 0 */
#define EINTED__DIV EINTBASE(_,d__div)
/* d__div(a,b).quo */
#define EINTED_DIV EINTBASE(_,d_div)
/* d__div(a,b).rem */
#define EINTED_MOD EINTBASE(_,d_mod)
