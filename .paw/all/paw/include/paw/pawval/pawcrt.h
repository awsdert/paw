#pragma once
#include "mpawcrt.h"

#ifndef NPAW_BUILD

#if WCHAR_MAX > USHRT_MAX
#	define PAWCRT_MAPWCSPFX(SFX) mpawcrt_lcs##SFX
#	define PAWCRT_MAPWCSSFX(PFX) mpawcrt_##PFX##lcs
#	define PAWCRT_MAPWCHPFX(SFX) mpawcrt_lch##SFX
#	define PAWCRT_MAPWCHSFX(PFX) mpawcrt_##PFX##lch
#else
#	define PAWCRT_MAPWCSPFX(SFX) mpawcrt_wcs##SFX
#	define PAWCRT_MAPWCSSFX(PFX) mpawcrt_##PFX##wcs
#	define PAWCRT_MAPWCHPFX(SFX) mpawcrt_wch##SFX
#	define PAWCRT_MAPWCHSFX(PFX) mpawcrt_##PFX##wch
#endif

PAW_FQK_CB size_t pawcrt_strlen( char const *txt )
{
	pawru len = mpawcrt_strlen( txt );
	return pawintu_inrange( bitsof(len), len ) ? len : SIZE_MAX;
}
PAW_FQK_CB size_t pawcrt_wcslen( wchar_t const *txt )
{
	pawru len = PAWCRT_MAPWCSPFX(len)( txt );
	return pawintu_inrange( bitsof(len), len ) ? len : SIZE_MAX;
}
PAW_FQK_CB size_t pawcrt_chrchk( char c, char const *reject  )
	{ return mpawcrt_chrchk( c, reject ); }
PAW_FQK_CB size_t pawcrt_wchchk( wchar_t c, wchar_t const *reject )
	{ return PAWCRT_MAPWCHPFX(cspn)( c, reject ); }

PAW_FQK_CB size_t pawcrt_strspn( char const *txt, char const *reject  )
{
	pawru pos = mpawcrt_strspn( txt, reject );
	return pawintu_inrange( bitsof(pos), pos ) ? pos : SIZE_MAX;
}
PAW_FQK_CB size_t pawcrt_wcsspn( wchar_t const *txt, wchar_t const *reject )
{
	pawru pos = PAWCRT_MAPWCSPFX(spn)( txt, reject );
	return pawintu_inrange( bitsof(pos), pos ) ? pos : SIZE_MAX;
}

PAW_FQK_CB size_t pawcrt_strcspn( char const *txt, char const *reject  )
{
	pawru pos = mpawcrt_strcspn( txt, reject );
	return pawintu_inrange( bitsof(pos), pos ) ? pos : SIZE_MAX;
}
PAW_FQK_CB size_t pawcrt_wcscspn( wchar_t const *txt, wchar_t const *reject )
{
	pawru pos = PAWCRT_MAPWCSPFX(cspn)( txt, reject );
	return pawintu_inrange( bitsof(pos), pos ) ? pos : SIZE_MAX;
}

PAW_FQK_CB char* pawcrt_strncpy( char *dest, char const *src, size_t n  )
	{ return mpawcrt_strncat( dest, src, n ).ptr; }
PAW_FQK_CB wchar_t* pawcrt_wcsncpy( wchar_t *dest, wchar_t const *src, size_t n )
	{ return PAWCRT_MAPWCSPFX(ncpy)( dest, src, n ).ptr; }

PAW_FQK_CB char* pawcrt_strncat( char *dest, char const *src, size_t n  )
	{ return mpawcrt_strncat( dest, src, n ).ptr; }
PAW_FQK_CB wchar_t* pawcrt_wcsncat( wchar_t *dest, wchar_t const *src, size_t n )
	{ return PAWCRT_MAPWCSPFX(ncat)( dest, src, n ).ptr; }
#ifdef COMPILE_PAW_LOCALS
extern PAW_FQK_CB size_t   pawcrt_strlen( char const *txt );
extern PAW_FQK_CB size_t   pawcrt_wcslen( char const *txt );
/* Instead of searching strings these 2 scan just a single character, don't
 * know why the standard library doesn't define these except maybe the name
 * hasn't been agreed upon. Keep an eye out for a version check here whenever
 * a new major version of paw is released (so 2.X onwards). */
extern PAW_FQK_CB size_t   pawcrt_chrchk( char c, char const *reject );
extern PAW_FQK_CB size_t   pawcrt_wchchk( wchar_t c, wchar_t const *reject );
extern PAW_FQK_CB size_t   pawcrt_strspn(  char const *txt, char const *reject );
extern PAW_FQK_CB size_t   pawcrt_wcsspn(  wchar_t const *txt, wchar_t const *reject );
extern PAW_FQK_CB size_t   pawcrt_strcspn( char const *txt, char const *reject );
extern PAW_FQK_CB size_t   pawcrt_wcscspn( wchar_t const *txt, wchar_t const *reject );
extern PAW_FQK_CB char*    pawcrt_strncpy( char *dest, char const *src, size_t n  );
extern PAW_FQK_CB wchar_t* pawcrt_wcsncpy( wchar_t *dest, wchar_t const *src, size_t n );
extern PAW_FQK_CB char*    pawcrt_strncat( char *dest, char const *src, size_t n  );
extern PAW_FQK_CB wchar_t* pawcrt_wcsncat( wchar_t *dest, wchar_t const *src, size_t n );
#endif
#define strlen pawcrt_strlen
#define wcslen pawcrt_wcslen
#define chrchk pawcrt_chrchk
#define wchchk pawcrt_wchchk
#define strspn pawcrt_strspn
#define wcsspn pawcrt_wcsspn
#define strcspn pawcrt_strcspn
#define wcscspn pawcrt_wcscspn
#define strncpy pawcrt_strncpy
#define wcsncpy pawcrt_wcsncpy
#define strncat pawcrt_strncat
#define wcsncat pawcrt_wcsncat
#endif
