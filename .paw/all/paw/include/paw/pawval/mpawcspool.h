#pragma once
#include "mpawcs.h"

typedef struct
{
	mpawcsimp imp;
	mpawrawdst list;
	mpawrawdst pool;
} mpawcspool;

PAW_QCK_CB npawd mpawcs_ignite( mpawcspool *cspool )
{
	mpawrawdst_ignite( &cspool );
}
