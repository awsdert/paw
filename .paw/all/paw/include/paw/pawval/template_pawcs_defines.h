#ifndef CH
#	error "template_pawcs.h requires CH (pawc, pawmbc, etc) to be defined"
#	include <paw/defabort.h>
#endif
#ifndef CS
#	error "template_pawcs.h requires CS (paws, pawmbs, etc) to be defined"
#	include <paw/defabort.h>
#endif
#ifndef CSVEC
#	error "template_pawcs.h requires CS (pawsvec, pawmbsvec, etc) to be defined"
#	include <paw/defabort.h>
#endif
#ifndef CSMEM
#	error "template_pawcs.h requires CS (pawsmem, pawmbsmem, etc) to be defined"
#	include <paw/defabort.h>
#endif
#ifndef CS_C
#	error "template_pawcs.h requires CS_C(LIT) to be defined"
#	include <paw/defabort.h>
#endif
#ifndef CS_NAME
#	error "template_pawcs.h requires CS_NAME(PFX,SFX) to be defined"
#	include <paw/defabort.h>
#endif
#include <paw/mayabort.h>

#define CZ sizeof(CH)

/* Start from 0 */
#define CS_GETLLC 	CS_NAME(,_getllc)
#define CS_GETC 	CS_NAME(_,getc)
#define _CSIGET CS_NAME(__,iget)
#define _CSGET CS_NAME(__,get)
#define CSGET CS_NAME(_,get)
/* Convert to uppercase always */
#define CS_PUTLLC 	CS_NAME(,_putllc)
#define CS_PUTC 	CS_NAME(_,putc)
#define _CSPUT CS_NAME(__,put)
#define CSPUT CS_NAME(_,put)

#define CS_LENN CS_NAME(_,lenn)
#define CS_PARSEC10 CS_NAME(_,parsec10)
#define CS_PARSEC36 CS_NAME(_,parsec36)
#define CS_PARSEC64 CS_NAME(_,parsec64)
#define CSVEC_GETC_FUNC CS_NAME(_,vec_getcCB)
#define CS_ESCAPE CS_NAME(_,escape)
#define CS_NEWVEC CS_NAME(_,newvec)
#define CS_NEWVECN CS_NAME(_,newvecn)
#define CS_LINELEN CS_NAME(_,linelen)
#define CS_NEWLINELEN CS_NAME(_,newlinelen)


#define CS_LEN  	CS_NAME(_,len)
#define CS_CPY  	CS_NAME(_,cpy)
#define CS_CPYN 	CS_NAME(_,cpyn)
#define CS_CMP  	CS_NAME(_,cmp)
#define CS_CMPN 	CS_NAME(_,cmpn)
#define CS_ICMP 	CS_NAME(_,icmp)
#define CS_ICMPN	CS_NAME(_,icmpn)
#define CS_FINDC	CS_NAME(_,findc)
#define CS_IFINDC	CS_NAME(_,ifindc)
#define CS_FINDN 	CS_NAME(_,findn)
#define CS_IFINDN	CS_NAME(_,ifindn)
#define CS_FINDS	CS_NAME(_,finds)
#define CS_IFINDS	CS_NAME(_,ifinds)

/* Start from end */
#define CS_RLEN 	CS_NAME(_,rlen)
#define CS_RCPY 	CS_NAME(_,rcpy)
#define CS_RCPYN	CS_NAME(_,rcpyn)
#define CS_RCMP 	CS_NAME(_,rcmp)
#define CS_RCMPN	CS_NAME(_,rcmpn)
#define CS_RICMP	CS_NAME(_,ricmp)
#define CS_RICMPN	CS_NAME(_,ricmpn)
#define CS_RFINDC	CS_NAME(_,rfindc)
#define CS_RIFINDC	CS_NAME(_,rifindc)
#define CS_RFINDN 	CS_NAME(_,rfindn)
#define CS_RIFINDN	CS_NAME(_,rifindn)
#define CS_RFINDS	CS_NAME(_,rfinds)
#define CS_RIFINDS	CS_NAME(_,rifinds)

/* pawref related */
#define CSIO_NEWCAP CS_NAME(_,io_newcap)
#define CSIO_NEWMAX CS_NAME(_,io_newmax)
#define CSIO_NEWLEN CS_NAME(_,io_newlen)
#define CSIO_FITCAP CS_NAME(_,io_fitcap)
#define CSIO_FITMAX CS_NAME(_,io_fitmax)
#define CSIO_SETLEN CS_NAME(_,io_setlen)
#define CSIO_FITLEN CS_NAME(_,io_fitlen)

/* Intended for internal purposes, try to use pawref instead */
#define CSMEM_SETCAP	CS_NAME(_,mem_setcap)
#define CSMEM_SETLIM	CS_NAME(_,mem_setlim)
#define CSMEM_SETMAX	CS_NAME(_,mem_setmax)
#define CSMEM_SETLEN	CS_NAME(_,mem_setlen)
#define CSMEM_SETCSN	CS_NAME(_,mem_setcsn)
#define CSMEM_SETCSV	CS_NAME(_,mem_setcsv)
#define CSMEM_SETCS		CS_NAME(_,mem_setcs)

#define CSMEM_FITCAP	CS_NAME(_,mem_fitcap)
#define CSMEM_FITLIM	CS_NAME(_,mem_fitlim)
#define CSMEM_FITMAX	CS_NAME(_,mem_fitmax)
#define CSMEM_FITLEN	CS_NAME(_,mem_fitlen)
#define CSMEM_FITCSN	CS_NAME(_,mem_fitcsn)
#define CSMEM_FITCSV	CS_NAME(_,mem_fitcsv)
#define CSMEM_FITCS		CS_NAME(_,mem_fitcs)

#define CSMEM_CAPCAP	CS_NAME(_,mem_capcap)
#define CSMEM_CAPLIM	CS_NAME(_,mem_caplim)
#define CSMEM_CAPMAX	CS_NAME(_,mem_capmax)
#define CSMEM_CAPLEN	CS_NAME(_,mem_caplen)
#define CSMEM_CAPCSN	CS_NAME(_,mem_capcsn)
#define CSMEM_CAPCSV	CS_NAME(_,mem_capcsv)
#define CSMEM_CAPCS		CS_NAME(_,mem_capcs)

#define CSMEM_CATCAP	CS_NAME(_,mem_catcap)
#define CSMEM_CATLIM	CS_NAME(_,mem_catlim)
#define CSMEM_CATMAX	CS_NAME(_,mem_catmax)
#define CSMEM_CATLEN	CS_NAME(_,mem_catlen)
#define CSMEM_CATCSN	CS_NAME(_,mem_catcsn)
#define CSMEM_CATCSV	CS_NAME(_,mem_catcsv)
#define CSMEM_CATCS		CS_NAME(_,mem_catcs)

#define CSMEM_NEWCAP	CS_NAME(_,mem_newcap)
#define CSMEM_NEWLIM	CS_NAME(_,mem_newlim)
#define CSMEM_NEWMAX	CS_NAME(_,mem_newmax)
#define CSMEM_NEWLEN	CS_NAME(_,mem_newlen)
#define CSMEM_NEWCSN	CS_NAME(_,mem_newcsn)
#define CSMEM_NEWCSV	CS_NAME(_,mem_newcsv)
#define CSMEM_NEWCS		CS_NAME(_,mem_newcs)

#define CSMEM_EXPIRE	CS_NAME(_,mem_expire)
#define CSMEM_DELETE	CS_NAME(_,mem_delete)
#define CSMEM_CREATE	CS_NAME(_,mem_create)
