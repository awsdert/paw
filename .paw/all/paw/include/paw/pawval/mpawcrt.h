#pragma once
#include "pawref.h"

PAW_APP_CB upawabs mpawcrt_calloc( pawru count, pawru Tsize );
PAW_APP_CB upawabs mpawcrt_malloc( pawru size );
PAW_APP_CB upawabs mpawcrt_realloc( upawabs mem, pawru size );
/* Always returns NULL, convenient for 1 liners */
PAW_APP_CB upawabs mpawcrt_free( upawabs mem );

/* Since wchar_t* can have a different size depending on target system it is
 * easier to support both half int and int based ABIs thus for mpawcrt these
 * are the used casts (ignoring the const side of things):
 *
 * str = char*, wcs = pawehu*, lcs = paweu*
 *
 * inlines of the original functions are provided for non-native builds, aka
 * the builds meant exclusivly for paw ecosystem
 * */

/* Copying */

/* These 3  hold the same behaviour as the standard library ones with one
 * exception. Instead of every remaining character in dest being set to 0
 * only 1 is done to terminate the string. Any more would waste cycles.
 * dest[n] MUST be available to set to 0 */
PAW_ANT_CB upawabs mpawcrt_strncpy( upawabs dest, upawabs src, pawru n );
PAW_ANT_CB upawabs mpawcrt_wcsncpy( upawabs dest, upawabs src, pawru n );
PAW_ANT_CB upawabs mpawcrt_lcsncpy( upawabs dest, upawabs src, pawru n );
/* These 3  hold the same behaviour as the standard library ones with one
 * exception. The amount concated to dest from src is clamped to at most
 * n - lengthof(dest), dest[n] MUST be available to set to 0 */
PAW_ANT_CB upawabs mpawcrt_strncat( upawabs dest, upawabs src, pawru n );
PAW_ANT_CB upawabs mpawcrt_wcsncat( upawabs dest, upawabs src, pawru n );
PAW_ANT_CB upawabs mpawcrt_lcsncat( upawabs dest, upawabs src, pawru n );

/* Comparison */
PAW_ANT_CB pawru mpawcrt_strcmp( upawabs A, upawabs B );
PAW_ANT_CB pawru mpawcrt_wcscmp( upawabs A, upawabs B );
PAW_ANT_CB pawru mpawcrt_lcscmp( upawabs A, upawabs B );
/* Instead of searching strings these 3 scan just a single character, don't
 * know why the standard library doesn't define these except maybe the name
 * hasn't been agreed upon. Keep an eye out for a version check here whenever
 * a new major version of paw is released (so 2.X onwards). */
PAW_ANT_CB pawru mpawcrt_chrchk( pawru c, upawabs match );
PAW_ANT_CB pawru mpawcrt_wchchk( pawru c, upawabs match );
PAW_ANT_CB pawru mpawcrt_lchchk( pawru c, upawabs match );


/* Searching */
PAW_ANT_CB upawabs mpawcrt_strchr( upawabs txt, paweu c );
PAW_ANT_CB upawabs mpawcrt_wcschr( upawabs txt, paweu c );
PAW_ANT_CB upawabs mpawcrt_lcschr( upawabs txt, paweu c );
PAW_ANT_CB pawru mpawcrt_strcspn( upawabs txt, upawabs reject );
PAW_ANT_CB pawru mpawcrt_wcscspn( upawabs txt, upawabs reject );
PAW_ANT_CB pawru mpawcrt_lcscspn( upawabs txt, upawabs reject );
PAW_ANT_CB upawabs mpawcrt_strpbrk( upawabs txt, upawabs find );
PAW_ANT_CB upawabs mpawcrt_wcspbrk( upawabs txt, upawabs find );
PAW_ANT_CB upawabs mpawcrt_lcspbrk( upawabs txt, upawabs find );
PAW_ANT_CB upawabs mpawcrt_strrchr( upawabs txt, paweu c );
PAW_ANT_CB upawabs mpawcrt_wcsrchr( upawabs txt, paweu c );
PAW_ANT_CB upawabs mpawcrt_lcsrchr( upawabs txt, paweu c );
PAW_ANT_CB pawru mpawcrt_strspn( upawabs txt, upawabs reject );
PAW_ANT_CB pawru mpawcrt_wcsspn( upawabs txt, upawabs reject );
PAW_ANT_CB pawru mpawcrt_lcsspn( upawabs txt, upawabs reject );

/* Other */
PAW_ANT_CB pawru mpawcrt_strlen( upawabs txt );
PAW_ANT_CB pawru mpawcrt_wcslen( upawabs txt );
PAW_ANT_CB pawru mpawcrt_lcslen( upawabs txt );

#ifdef COMPILE_MPAW_LOCALS

#ifdef _WIN32
#	ifdef __MINGW__
#		include <fltkernel.h.h>
#		include <ntifs.h>
#	else
#		include <winternl.h>
#	endif
#	include <windows.h>
#endif

/* Pseudo-random sequence generation */
PAW_ANT_CB pawru mpawcrt_rand( upawabs seed )
{
	pawru rng = 0;
	/* Since pawru will never be mapped to anything smaller than an int and
	 * integer sizes are done by doubling (or quadrupling in one particular
	 * case) this should be safe at all times */
	uint *list = seed.ptr;
	/* O3 should compile this down to just 1, 2 or 4 copies of the statement
	 * in most cases, anything else would likely expect slower speed anyways
	 * as it would be expecting an older CPU */
	size_t const count = sizeof(pawru) / sizeof(uint);
#ifdef _WIN32
#	define TMP bitsof(ULONG)
#else
#	define TMP bitsof(uint)
#endif
	for ( size_t i = 0; i < count; ++i, rng <<= TMP )
	{
#ifdef _WIN32
		ULONG node = list[i];
		rng |= RtlRandom( &node );
		list[i] = node;
#else
		rng |= rand_r( list + i );
#endif
	}
#undef TMP
	return rng;
}

/* Dynamic memory management */
PAW_APP_CB upawabs mpawcrt_calloc( pawru count, pawru Tsize )
{
	/* I've read online at some point that malloc can still return a valid
	 * pointer on size 0 in some implementations so just assume calloc can
	 * have the same problem */
	upawabs mem = { .ptr = (count && Tsize) ? calloc( count, Tsize ) : NULL };
	return mem;
}
PAW_APP_CB upawabs mpawcrt_malloc( pawru size )
{
	/* I've read online at some point that malloc can still return a valid
	 * pointer on size 0 in some implementations so just force NULL as the
	 * only possible return value size 0 */
	upawabs mem = { .ptr = size ? malloc( size ) : NULL };
	return mem;
}
PAW_APP_CB upawabs mpawcrt_realloc( upawabs mem, pawru size )
{
	if ( size )
	{
		/* realloc can vary in how it treats either parameter when they're 0 so
		* just assume worst case scenario by default and tailor later */
		mem.ptr = mem.ptr ? realloc( mem.ptr, size ) : malloc( size );
	}
	else
	{
		if ( mem.ptr )
			free( mem.ptr );
		mem.ptr = NULL;
	}
	return mem;
}
PAW_APP_CB upawabs mpawcrt_free( upawabs mem )
{
	if ( mem.ptr )
		free( mem.ptr );
	/* NULL is returned for the sake of 1 liners that both clear the pointer
	 * and release it at the same time. Makes for tidier code */
	mem.ptr = NULL;
	return mem;
}

/* Copying */
PAW_ANT_CB upawabs mpawcrt_memcpy( upawabs dest, upawabs src, pawru n )
	{ dest.ptr = memcpy( dest.ptr, src.cptr, n ); return dest; }
PAW_ANT_CB upawabs mpawcrt_memmove( upawabs dest, upawabs src, pawru n )
	{ dest.ptr = memmove( dest.ptr, src.cptr, n ); return dest; }

PAW_ANT_CB upawabs mpawcrt_strncpy( upawabs dest, upawabs src, pawru n )
{
	uchar *dst = dest.ptr;
	pawru cpy = mpawcrt_wcslen( src );
	if ( cpy > n )
		cpy = n;
	memmove( dst, src.cptr, cpy );
	dst[cpy] = 0;
	return dest;
}
PAW_ANT_CB upawabs mpawcrt_wcsncpy( upawabs dest, upawabs src, pawru n )
{
	pawehu *dst = dest.ptr;
	pawru cpy = mpawcrt_wcslen( src );
	if ( cpy > n )
		cpy = n;
	memmove( dst, src.cptr, cpy * sizeof(pawehu) );
	dst[cpy] = 0;
	return dest;
}
PAW_ANT_CB upawabs mpawcrt_lcsncpy( upawabs dest, upawabs src, pawru n )
{
	paweu *dst = dest.ptr;
	pawru cpy = mpawcrt_wcslen( src );
	if ( cpy > n )
		cpy = n;
	memmove( dst, src.cptr, cpy * sizeof(paweu) );
	dst[cpy] = 0;
	return dest;
}
PAW_ANT_CB upawabs mpawcrt_strncat( upawabs dest, upawabs src, pawru n )
{
	uchar *dst = dest.ptr;
	pawru len = mpawcrt_lcslen( dest ), cpy = mpawcrt_wcslen( src ), max = n - len;
	dst += len;
	if ( cpy > max )
		cpy = max;
	memmove( dst, src.cptr, cpy );
	dst[cpy] = 0;
	return dest;
}
PAW_ANT_CB upawabs mpawcrt_wcsncat( upawabs dest, upawabs src, pawru n )
{
	pawehu *dst = dest.ptr;
	pawru len = mpawcrt_lcslen( dest ), cpy = mpawcrt_wcslen( src ), max = n - len;
	dst += len;
	if ( cpy > max )
		cpy = max;
	memmove( dst, src.cptr, cpy * sizeof(pawehu) );
	dst[cpy] = 0;
	return dest;
}
PAW_ANT_CB upawabs mpawcrt_lcsncat( upawabs dest, upawabs src, pawru n )
{
	paweu *dst = dest.ptr;
	pawru len = mpawcrt_lcslen( dest ), cpy = mpawcrt_wcslen( src ), max = n - len;
	dst += len;
	if ( cpy > max )
		cpy = max;
	memmove( dst, src.cptr, cpy * sizeof(paweu) );
	dst[cpy] = 0;
	return dest;
}

/* Comparison */
PAW_ANT_CB pawru mpawcrt_strcmp( upawabs A, upawabs B )
{
	uchar const *a = A.cptr, *b = B.cptr;
	while ( *a )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs;
}
PAW_ANT_CB pawru mpawcrt_wcscmp( upawabs A, upawabs B )
{
	pawehu const *a = A.cptr, *b = B.cptr;
	while ( *a )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs / sizeof(pawehu);
}
PAW_ANT_CB pawru mpawcrt_lcscmp( upawabs A, upawabs B )
{
	paweu const *a = A.cptr, *b = B.cptr;
	while ( *a )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs / sizeof(paweu);
}

PAW_ANT_CB pawru mpawcrt_strncmp( upawabs A, upawabs B, paweu max )
{
	uchar const *a = A.cptr, *b = B.cptr;
	for ( pawru i = 0; i < max && *a; ++i )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs;
}
PAW_ANT_CB pawru mpawcrt_wcsncmp( upawabs A, upawabs B, paweu max )
{
	pawehu const *a = A.cptr, *b = B.cptr;
	for ( pawru i = 0; i < max && *a; ++i )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs / sizeof(pawehu);
}
PAW_ANT_CB pawru mpawcrt_lcsncmp( upawabs A, upawabs B, paweu max )
{
	paweu const *a = A.cptr, *b = B.cptr;
	for ( pawru i = 0; i < max && *a; ++i )
	{
		if ( *a != *b )
			break;
		++a; ++b;
	}
	return mpawabs_bytediff( a, A.cptr ).abs / sizeof(paweu);
}

PAW_ANT_CB pawru mpawcrt_chrchk( pawru c, upawabs match )
{
	uchar const *loc = match.cptr;
	while ( *loc )
	{
		if ( c == *loc )
			return mpawabs_bytediff( loc, match ).abs;
		++loc;
	}
	return 0;
}
PAW_ANT_CB pawru mpawcrt_wchchk( pawru c, upawabs match )
{
	pawehu const *loc = match.cptr;
	while ( *loc )
	{
		if ( c == *loc )
			return mpawabs_bytediff( loc, match ).abs / sizeof(pawehu);
		++loc;
	}
	return 0;
}
PAW_ANT_CB pawru mpawcrt_lchchk( pawru c, upawabs match )
{
	paweu const *loc = match.cptr;
	while ( *loc )
	{
		if ( c == *loc )
			return mpawabs_bytediff( loc, match ).abs / sizeof(paweu);
		++loc;
	}
	return 0;
}

/* Searching */
PAW_ANT_CB upawabs mpawcrt_strchr( upawabs txt, paweu c )
{
	uchar const *end = txt.cptr;
	while ( *end )
	{
		if ( *end == c )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_wcschr( upawabs txt, paweu c )
{
	pawehu const *end = txt.cptr;
	while ( *end )
	{
		if ( *end == c )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_lcschr( upawabs txt, paweu c )
{
	paweu const *end = txt.cptr;
	while ( *end )
	{
		if ( *end == c )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}

PAW_ANT_CB pawru mpawcrt_strcspn( upawabs txt, upawabs reject )
{
	uchar const *end = txt.cptr;
	while ( *end && !mpawcrt_chrchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB pawru mpawcrt_wcscspn( upawabs txt, upawabs reject )
{
	pawehu const *end = txt.cptr;
	while ( *end && !mpawcrt_wchchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB pawru mpawcrt_lcscspn( upawabs txt, upawabs reject )
{
	pawehu const *end = txt.cptr;
	while ( *end && !mpawcrt_lchchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB upawabs mpawcrt_strpbrk( upawabs txt, upawabs find )
{
	uchar const *end = txt.cptr;
	while ( *end )
	{
		if ( mpawcrt_chrchk(*end,find) )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_wcspbrk( upawabs txt, upawabs find )
{
	pawehu const *end = txt.cptr;
	while ( *end )
	{
		if ( mpawcrt_wchchk(*end,find) )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_lcspbrk( upawabs txt, upawabs find )
{
	paweu const *end = txt.cptr;
	while ( *end )
	{
		if ( mpawcrt_lchchk(*end,find) )
		{
			txt.cptr = end;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_strrchr( upawabs txt, paweu c )
{
	uchar const *end = txt.cptr, *loc = NULL;
	while ( *end )
	{
		if ( *end == c )
			loc = end;
		++end;
	}
	txt.cptr = loc;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_wcsrchr( upawabs txt, paweu c )
{
	pawehu const *end = txt.cptr, *loc = NULL;
	while ( *end )
	{
		if ( *end == c )
			loc = end;
		++end;
	}
	txt.cptr = loc;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_lcsrchr( upawabs txt, paweu c )
{
	paweu const *end = txt.cptr, *loc = NULL;
	while ( *end )
	{
		if ( *end == c )
			loc = end;
		++end;
	}
	txt.cptr = loc;
	return txt;
}
PAW_ANT_CB pawru mpawcrt_strspn( upawabs txt, upawabs reject )
{
	uchar const *end = txt.cptr;
	while ( *end && mpawcrt_wchchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB pawru mpawcrt_wcsspn( upawabs txt, upawabs reject )
{
	pawehu const *end = txt.cptr;
	while ( *end && mpawcrt_wchchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB pawru mpawcrt_lcsspn( upawabs txt, upawabs reject )
{
	pawehu const *end = txt.cptr;
	while ( *end && mpawcrt_lchchk(*end,reject) ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}

PAW_ANT_CB upawabs mpawcrt_strtok( upawabs txt, upawabs find, upawabs rest )
{
	uchar *end = txt.ptr;
	while ( *end )
	{
		if ( mpawcrt_chrchk(*end,find) )
		{
			*(rest.ptr2ptr) = end + 1;
			*end = 0;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_wcstok( upawabs txt, upawabs find, upawabs rest )
{
	pawehu *end = txt.ptr;
	while ( *end )
	{
		if ( mpawcrt_wchchk(*end,find) )
		{
			*(rest.ptr2ptr) = end + 1;
			*end = 0;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}
PAW_ANT_CB upawabs mpawcrt_lcstok( upawabs txt, upawabs find, upawabs rest )
{
	paweu *end = txt.ptr;
	while ( *end )
	{
		if ( mpawcrt_lchchk(*end,find) )
		{
			*(rest.ptr2ptr) = end + 1;
			*end = 0;
			return txt;
		}
		++end;
	}
	txt.cptr = NULL;
	return txt;
}

/* Other */
PAW_ANT_CB pawru mpawcrt_strlen( upawabs txt )
{
	uchar const *end = txt.cptr;
	while ( *end ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
PAW_ANT_CB pawru mpawcrt_wcslen( upawabs txt )
{
	pawehu const *end = txt.cptr;
	while ( *end ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}

PAW_ANT_CB pawru mpawcrt_lcslen( upawabs txt )
{
	paweu const *end = txt.cptr;
	while ( *end ) ++end;
	return mpawabs_bytediff( end, txt ).abs;
}
#endif
