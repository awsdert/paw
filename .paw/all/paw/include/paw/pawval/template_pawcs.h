#include "template_pawcs_defines.h"
#include "mpawcs.h"
#include "pawref.h"
#include "paweint.h"

PAW_QCK_CB pawcsc CS_GETC( pawp raw, pawru i ) { return ((CS)raw)[i]; }
PAW_QCK_CB pawru CS_PUTC( void *raw, pawru i, pawcsc c )
	{ ((CH*)raw)[i] = c; return 1; }
PAW_QCK_CB mpawcsget _CSGET( void )
	{ mpawcsget imp = { CZ, pawcsc_asgiven, CS_GETC, CS_GETLLC }; return imp; }
PAW_QCK_CB mpawcsget _CSIGET( void )
	{ mpawcsget imp = _CSGET(); imp.tocCB = pawcsc_toupper; return imp; }
PAW_QCK_CB mpawcsput _CSPUT( void )
{
	mpawcsput imp = { CZ, (32 / bitsof(CH)) + 1, pawcsc_asgiven, CS_PUTC, CS_PUTLLC };
	return imp;
}

PAW_FQK_CB pawru CS_LEN( CS raw ) { return pawcslen( _CSGET(), raw ); }
PAW_FQK_CB pawru CS_LENN( CS raw, pawru max )
	{ return pawcslenn( _CSGET(), raw, max ); }
PAW_QCK_CB CSVEC CS_NEWVECN( CS raw, pawru len )
{
	union { mpawmemsrc main; CSVEC text; }
		vec = { pawcsnewvecn(_CSGET(),raw,len) };
	return vec.text;
}
PAW_FQK_CB CSVEC CS_NEWVEC( CS raw ) { return CS_NEWVECN( raw, CS_LEN(raw) ); }
PAW_FQK_CB pawru CS_LINELEN( CS raw, pawru len )
	{ return pawcslinelen( _CSGET(), raw, len ); }
PAW_FQK_CB pawru CS_NEWLINELEN( CS raw, pawru len )
	{ return pawcsnewlinelen( _CSGET(), raw, len ); }

PAW_QCK_CB pawrd CSVEC_GETC_FUNC( pawllc *C, void *ud )
{
	CSVEC *vec = ud;
	if ( !(vec->end) )
		return 0;
	*C = *(vec->top);
	vec->end -= CZ;
	vec->top++;
	return 1;
}
PAW_QCK_CB pawint_escaped CS_ESCAPE( CSVEC vec )
	{ return pawint_escchar( &vec, CSVEC_GETC_FUNC ); }
PAW_FQK_CB pawru CS_CPY( CH *dst, CS src, pawru len )
	{ return pawrawcpy( dst, src, CZ * len ) / CZ; }
PAW_QCK_CB pawru CS_CPYN( CH *dst, pawru max, CS src, pawru len )
	{ return pawrawcpyn( dst, CZ * max, src, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_RCPY( CH *dst, CS src, pawru len )
	{ return pawrawrcp( dst, src, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_RCPYN( CH *dst, pawru max, CS src, pawru len )
	{ return pawrawrcpn( dst, CZ * max, src, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_CMP( CS a, CS b, pawru len )
	{ return pawrawcmp( a, b, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_CMPN( CS a, pawru end, CS b, pawru len )
	{ return pawrawcmpn( a, CZ * end, b, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_RCMP( CS a, CS b, pawru len )
	{ return pawrawrcmp( a, b, CZ * len ) / CZ; }
PAW_FQK_CB pawru CS_RCMPN( CS a, pawru lenA, CS b, pawru lenB )
	{ return pawrawrcmpn( a, CZ * lenA, b, CZ * lenB ) / CZ; }
PAW_QCK_CB pawru CS_ICMP( CS a, CS b, pawru len )
{
	pawcsget imp =  _CSIGET();
	return pawcscmp
		( imp, pawcsnewvecn( imp, a, len ), pawcsnewvecn( imp, b, len ) );
}
PAW_QCK_CB pawru CS_ICMPN( CS a, pawru end, CS b, pawru len )
{
	pawcsget imp =  _CSIGET();
	return pawcscmp
		( imp, pawcsnewvecn( imp, a, end ), pawcsnewvecn( imp, b, len ) );
}

PAW_QCK_CB pawru CS_RICMP( CS a, CS b, pawru len )
{
	pawru i = 0;
	CS ae = a + len;
	CS be = b + len;
	for ( ; i < len && pawcsc_toupper(*ae) == pawcsc_toupper(*be); --a, --b );
	return len - i;
}

PAW_QCK_CB pawru CS_RICMPN( CS a, pawru lenA, CS b, pawru lenB )
{
	pawru dif = 0, pos = 0;
	if ( lenA < lenB )
	{
		pos = lenB - lenA;
		dif = CS_RICMP( a, b + pos, lenA );
	}
	else
	{
		pos = lenA - lenB;
		dif = CS_RICMP( a + pos, b, lenB );
	}
	return pos + dif;
}

PAW_FQK_CB pawru CS_FINDC( CS qry, pawru end, CH loc )
{
	pawcsget imp = _CSGET();
	return pawcsfindc( imp, pawcsnewvecn( imp, qry, end ), loc );
}

PAW_FQK_CB pawru CS_IFINDC( CS qry, pawru end, CH loc )
{
	pawcsget imp = _CSIGET();
	return pawcsfindc
		( imp, pawcsnewvecn( imp, qry, end ), pawcsc_toupper(loc) );
}

PAW_FQK_CB pawru CS_FINDN( CS qry, pawru end, CS loc, pawru len  )
{
	pawcsget imp = _CSGET();
	return pawcsfindn
		( imp, pawcsnewvecn( imp, qry, end ), pawcsnewvecn( imp, loc, len ) );
}
PAW_FQK_CB pawru CS_FINDS( CS qry, pawru end, CS loc )
	{ return CS_FINDN( qry, end, loc, CS_LEN( loc ) ); }
PAW_FQK_CB pawru CS_IFINDN( CS qry, pawru end, CS loc, pawru len )
{
	pawcsget imp = _CSIGET();
	return pawcsfindn
		( imp, pawcsnewvecn( imp, qry, end ), pawcsnewvecn( imp, loc, len ) );
}
PAW_QCK_CB pawru CS_IFINDS( CS qry, pawru end, CS loc )
{
	pawcsget imp = _CSGET();
	return pawcsfinds( imp, pawcsnewvecn( imp, qry, end ), loc );
}

PAW_FQK_CB void  CSMEM_EXPIRE( CSMEM *csmem ) { mpawrawdst_expire( (mpawrawdst*)csmem ); }
PAW_FQK_CB void  CSMEM_DELETE( CSMEM *csmem ) { mpawrawdst_delete( (mpawrawdst*)csmem ); }

PAW_FQK_CB CSMEM* CSMEM_CREATE( void ) { return pawstdnew(sizeof(CSMEM)); }

PAW_QCK_CB CSMEM CSMEM_NEWCAP( pawru cap )
	{ CSMEM mem = {0}; mem.asmem = mpawrawdst_newcap( CH, cap ); return mem; }
PAW_QCK_CB CSMEM CSMEM_NEWLIM( pawru lim )
	{ CSMEM mem = {0}; mem.asmem = mpawrawdst_newlim( CH, lim ); return mem; }
PAW_QCK_CB CSMEM CSMEM_NEWMAX( pawru max )
	{ CSMEM mem = {0}; mem.asmem = mpawrawdst_newmax( CH, max ); return mem; }
PAW_QCK_CB CSMEM CSMEM_NEWLEN( pawru len )
	{ CSMEM mem = {0}; mem.asmem = mpawrawdst_newlen( CH, len ); return mem; }
PAW_FQK_CB CSMEM CSMEM_NEWCSN( CS raw, pawru len )
	{ CSMEM mem = {0}; mem.asmem = mpawrawdst_newraw( CH, raw, len ); return mem; }
PAW_FQK_CB CSMEM CSMEM_NEWCS( CS raw )
	{ return CSMEM_NEWCSN( raw, CS_LEN(raw) ); }
PAW_QCK_CB CSMEM CSMEM_NEWCSV( va_list va )
{
	union { mpawrawdst mem; CSMEM txt; } x =
		{ pawcsmem_newcsv( _CSPUT(), NULL, va ) };
	return x.txt;
}

PAW_FQK_CB pawru CSMEM_SETCAP( CSMEM *mem, pawru cap )
	{ return mpawrawdst_setcap( (mpawrawdst*)mem, CZ, cap ); }
PAW_FQK_CB pawru CSMEM_SETLIM( CSMEM *mem, pawru lim )
	{ return mpawrawdst_setlim( (mpawrawdst*)mem, CZ, lim ); }
PAW_FQK_CB pawru CSMEM_SETMAX( CSMEM *mem, pawru max )
	{ return mpawrawdst_setmax( (mpawrawdst*)mem, CZ, max ); }
PAW_FQK_CB pawru CSMEM_SETLEN( CSMEM *mem, pawru len )
	{ return mpawrawdst_setlen( (mpawrawdst*)mem, CZ, len ); }
PAW_FQK_CB pawru CSMEM_SETCSN( CSMEM *mem, CS raw, pawru len )
	{ return mpawrawdst_setraw( (mpawrawdst*)mem, CZ, raw, len ); }
PAW_FQK_CB pawru CSMEM_SETCS( CSMEM *mem, CS raw )
	{ return CSMEM_SETCSN( mem, raw, CS_LEN(raw) ); }
PAW_QCK_CB pawcsc CSMEM_SETCSV( CSMEM *mem, va_list va )
	{ return pawcsmem_setcsv( (mpawrawdst*)mem, _CSPUT(), va ); }

PAW_FQK_CB pawru CSMEM_CAPCAP( CSMEM *mem, pawru cap )
	{ return mpawrawdst_capcap( (mpawrawdst*)mem, CZ, cap ); }
PAW_FQK_CB pawru CSMEM_CAPLIM( CSMEM *mem, pawru lim )
	{ return mpawrawdst_caplim( (mpawrawdst*)mem, CZ, lim ); }
PAW_FQK_CB pawru CSMEM_CAPMAX( CSMEM *mem, pawru max )
	{ return mpawrawdst_capmax( (mpawrawdst*)mem, CZ, max ); }
PAW_FQK_CB pawru CSMEM_CAPLEN( CSMEM *mem, pawru len )
	{ return mpawrawdst_caplen( (mpawrawdst*)mem, CZ, len ); }
PAW_FQK_CB pawru CSMEM_CAPCSN( CSMEM *mem, CS raw, pawru len )
	{ return mpawrawdst_capraw( (mpawrawdst*)mem, CZ, raw, len ); }
PAW_FQK_CB pawru CSMEM_CAPCS( CSMEM *mem, CS raw )
	{ return CSMEM_CAPCSN( mem, raw, CS_LEN(raw) ); }
PAW_QCK_CB pawcsc CSMEM_CAPCSV( CSMEM *mem, va_list va )
	{ return pawcsmem_capcsv( (mpawrawdst*)mem, _CSPUT(), va ); }

PAW_FQK_CB pawru CSMEM_FITCAP( CSMEM *mem, pawru cap )
	{ return mpawrawdst_fitcap( (mpawrawdst*)mem, CZ, cap ); }
PAW_FQK_CB pawru CSMEM_FITLIM( CSMEM *mem, pawru lim )
	{ return mpawrawdst_fitlim( (mpawrawdst*)mem, CZ, lim ); }
PAW_FQK_CB pawru CSMEM_FITMAX( CSMEM *mem, pawru max )
	{ return mpawrawdst_fitmax( (mpawrawdst*)mem, CZ, max ); }
PAW_FQK_CB pawru CSMEM_FITLEN( CSMEM *mem, pawru len )
	{ return mpawrawdst_fitlen( (mpawrawdst*)mem, CZ, len ); }
PAW_FQK_CB pawru CSMEM_FITCSN( CSMEM *mem, CS raw, pawru len )
	{ return mpawrawdst_fitraw( (mpawrawdst*)mem, CZ, raw, len ); }
PAW_FQK_CB pawru CSMEM_FITCS( CSMEM *mem, CS raw )
	{ return CSMEM_FITCSN( mem, raw, CS_LEN(raw) ); }
PAW_FQK_CB pawcsc CSMEM_FITCSV( CSMEM *mem, va_list va )
	{ return pawcsmem_fitcsv( (mpawrawdst*)mem, _CSPUT(), va ); }

PAW_FQK_CB pawru CSMEM_CATCAP( CSMEM *mem, pawru cap )
	{ return mpawrawdst_catcap( (mpawrawdst*)mem, CZ, cap ); }
PAW_FQK_CB pawru CSMEM_CATLIM( CSMEM *mem, pawru lim )
	{ return mpawrawdst_catlim( (mpawrawdst*)mem, CZ, lim ); }
PAW_FQK_CB pawru CSMEM_CATMAX( CSMEM *mem, pawru max )
	{ return mpawrawdst_catmax( (mpawrawdst*)mem, CZ, max ); }
PAW_FQK_CB pawru CSMEM_CATLEN( CSMEM *mem, pawru len )
	{ return mpawrawdst_catlen( (mpawrawdst*)mem, CZ, len ); }
PAW_FQK_CB pawru CSMEM_CATCSN( CSMEM *mem, CS raw, pawru len )
	{ return mpawrawdst_catraw( (mpawrawdst*)mem, CZ, raw, len ); }
PAW_FQK_CB pawru CSMEM_CATCS( CSMEM *mem, CS raw )
	{ return CSMEM_CATCSN( mem, raw, CS_LEN(raw) ); }
PAW_FQK_CB pawcsc CSMEM_CATCSV( CSMEM *mem, va_list va )
	{ return pawcsmem_catcsv( (mpawrawdst*)mem, _CSPUT(), va ); }

#include "template_pawcs_undefs.h"
