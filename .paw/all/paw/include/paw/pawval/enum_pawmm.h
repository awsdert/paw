#pragma once
enum
{
	PAWMM_CALL_NONE = 0

	PAWMM_CALL_ALLOC, /* pawint *cap */
	PAWMM_CALL_FETCH, /* pawint *abs, void *dst, pawint *cap */
	PAWMM_CALL_WRITE, /* pawint *abs, pawp  src, pawint *cap */

	PAWMM_CALL_GETCAP, /* pawint *cap */
	PAWMM_CALL_GETEND, /* pawint *end */
	PAWMM_CALL_GETABS, /* pawint *abs, void *dst, pawru cap */
	PAWMM_CALL_GETRAW, /* pawint *got, void *dst, pawru cap */

	PAWMM_CALL_SETCAP, /* */
	PAWMM_CALL_SETEND, /**/
	PAWMM_CALL_SETMAX, /**/
	PAWMM_CALL_SETLEN, /**/
	PAWMM_CALL_SETABS, /**/
	PAWMM_CALL_SETPOS, /**/
	PAWMM_CALL_SETRAW, /**/

	PAWMM_CALL_FITCAP, /**/
	PAWMM_CALL_FITEND, /**/
	PAWMM_CALL_FITMAX, /**/
	PAWMM_CALL_FITLEN, /**/
	PAWMM_CALL_FITABS, /**/
	PAWMM_CALL_FITRAW, /**/

	PAWMM_CALL_CATCAP, /**/
	PAWMM_CALL_CATEND, /**/
	PAWMM_CALL_CATMAX, /**/
	PAWMM_CALL_CATLEN, /**/
	PAWMM_CALL_CATABS, /**/
	PAWMM_CALL_CATRAW, /**/

	PAWMM_CALL_INSCAP, /**/
	PAWMM_CALL_INSEND, /**/
	PAWMM_CALL_INSMAX, /**/
	PAWMM_CALL_INSLEN, /**/
	PAWMM_CALL_INSABS, /**/
	PAWMM_CALL_INSRAW, /**/

	PAWMM_COUNTOF_CALLS,
};
