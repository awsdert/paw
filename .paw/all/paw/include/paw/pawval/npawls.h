#pragma once
#if !defined(NPAW_BUILD)
#	error "npawls.h is EXCLUSIVELY for native builds!"
#	include <abort_compilation>
#endif
#include "pawref.h"

typedef struct
{
#define RAW npawls
#include "inherit_pawmemsrc_as_union.h"
} npawlsvec;
typedef struct
{
#define T npawlc
#include "inherit_mpawrawdst_as_union.h"
} npawlsmem;

#define CH npawlc
#define CS npawls
#define CSVEC npawlsvec
#define CSMEM npawlsmem
#define CS_C(LIT) u8##LIT
#define CS_NAME(PFX,SFX) PFX##npawls##SFX
#include "template_pawcs.h"

PAW_FQK_CB pawru npawlslen( npawls str ) { return _npawlslen( str ); }
PAW_FQK_CB pawru npawlslinelen( npawls str, pawru len ) { return _npawlslinelen( str, len ); }
PAW_FQK_CB pawru npawlsnewlinelen( npawls str, pawru len ) { return _npawlsnewlinelen( str, len ); }

PAW_FQK_CB npawlsvec npawlsnewvecn( npawls txt, pawru len )
	{ return _npawlsnewvecn( txt, len ); }
PAW_FQK_CB npawlsvec npawlsnewvec( npawls txt )
	{ return _npawlsnewvec( txt ); }
PAW_FQK_CB pawru npawlscpy( npawlc *dst, npawls src, pawru cpy )
	{ return _npawlscpy( dst, src, cpy ); }
PAW_FQK_CB pawru npawlscpyn( npawlc *dst, pawru max, npawls src, pawru cpy )
	{ return _npawlscpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru npawlscmp( npawls a, npawls b, pawru len )
	{ return _npawlscmp( a, b, len ); }
PAW_FQK_CB pawru npawlscmpn( npawls a, pawru lenA, npawls b, pawru lenB )
	{ return _npawlscmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawlsicmp( npawls a, npawls b, pawru len )
	{ return _npawlsicmp( a, b, len ); }
PAW_FQK_CB pawru npawlsicmpn( npawls a, pawru lenA, npawls b, pawru lenB )
	{ return _npawlsicmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawlsfindc( npawls qry, pawru end, npawlc loc )
	{ return _npawlsfindc( qry, end, loc ); }
PAW_FQK_CB pawru npawlsifindc( npawls qry, pawru end, npawlc loc )
	{ return _npawlsifindc( qry, end, loc ); }
PAW_FQK_CB pawru npawlsfindn( npawls qry, pawru end, npawls loc, pawru len )
	{ return _npawlsfindn( qry, end, loc, len ); }
PAW_FQK_CB pawru npawlsifindn( npawls qry, pawru end, npawls loc, pawru len )
	{ return _npawlsifindn( qry, end, loc, len ); }
PAW_FQK_CB pawru npawlsfinds( npawls qry, pawru end, npawls loc )
	{ return _npawlsfinds( qry, end, loc ); }
PAW_FQK_CB pawru npawlsifinds( npawls qry, pawru end, npawls loc )
	{ return _npawlsifinds( qry, end, loc ); }

PAW_FQK_CB pawru npawlsrcpy( npawlc *dst, npawls src, pawru cpy )
	{ return _npawlsrcpy( dst, src, cpy ); }
PAW_FQK_CB pawru npawlsrcpyn( npawlc *dst, pawru max, npawls src, pawru cpy )
	{ return _npawlsrcpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru npawlsrcmp( npawls a, npawls b, pawru len )
	{ return _npawlsrcmp( a, b, len ); }
PAW_FQK_CB pawru npawlsrcmpn( npawls a, pawru lenA, npawls b, pawru lenB )
	{ return _npawlsrcmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawlsricmp( npawls a, npawls b, pawru len )
	{ return _npawlsricmp( a, b, len ); }
PAW_FQK_CB pawru npawlsricmpn( npawls a, pawru lenA, npawls b, pawru lenB )
	{ return _npawlsricmpn( a, lenA, b, lenB ); }

/* Buffer Functions */
PAW_FQK_CB pawru npawlsmem_setcap( npawlsmem *buf, pawru cap )
	{ return _npawlsmem_setcap( buf, cap ); }
PAW_FQK_CB pawru npawlsmem_setlim( npawlsmem *buf, pawru lim )
	{ return _npawlsmem_setlim( buf, lim ); }
PAW_FQK_CB pawru npawlsmem_setmax( npawlsmem *buf, pawru max )
	{ return _npawlsmem_setmax( buf, max ); }
PAW_FQK_CB pawru npawlsmem_setlen( npawlsmem *buf, pawru len )
	{ return _npawlsmem_setlen( buf, len ); }
PAW_FQK_CB pawru npawlsmem_setcsn( npawlsmem *buf, npawls txt, pawru len )
	{ return _npawlsmem_setcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawlsmem_setcs( npawlsmem *buf, npawls txt )
	{ return _npawlsmem_setcs( buf, txt ); }
PAW_FQK_CB pawcsc npawlsmem_setcsv( npawlsmem *buf, va_list va )
	{ return _npawlsmem_setcsv( buf, va ); }
PAW_QCK_CB pawcsc npawlsmem_setcsf( npawlsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawlsmem_setcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru npawlsmem_fitcap( npawlsmem *buf, pawru cap )
	{ return _npawlsmem_fitcap( buf, cap ); }
PAW_FQK_CB pawru npawlsmem_fitlim( npawlsmem *buf, pawru lim )
	{ return _npawlsmem_fitlim( buf, lim ); }
PAW_FQK_CB pawru npawlsmem_fitmax( npawlsmem *buf, pawru max )
	{ return _npawlsmem_fitmax( buf, max ); }
PAW_FQK_CB pawru npawlsmem_fitlen( npawlsmem *buf, pawru len )
	{ return _npawlsmem_fitlen( buf, len ); }
PAW_FQK_CB pawru npawlsmem_fitcsn( npawlsmem *buf, npawls txt, pawru len )
	{ return _npawlsmem_fitcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawlsmem_fitcs( npawlsmem *buf, npawls txt )
	{ return _npawlsmem_fitcs( buf, txt ); }
PAW_FQK_CB pawcsc npawlsmem_fitcsv( npawlsmem *buf, va_list va )
	{ return _npawlsmem_fitcsv( buf, va ); }
PAW_QCK_CB pawcsc npawlsmem_fitcsf( npawlsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawlsmem_fitcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru npawlsmem_catcap( npawlsmem *buf, pawru cap )
	{ return _npawlsmem_catcap( buf, cap ); }
PAW_FQK_CB pawru npawlsmem_catlim( npawlsmem *buf, pawru lim )
	{ return _npawlsmem_catlim( buf, lim ); }
PAW_FQK_CB pawru npawlsmem_catmax( npawlsmem *buf, pawru max )
	{ return _npawlsmem_catmax( buf, max ); }
PAW_FQK_CB pawru npawlsmem_catlen( npawlsmem *buf, pawru len )
	{ return _npawlsmem_catlen( buf, len ); }
PAW_FQK_CB pawru npawlsmem_catcsn( npawlsmem *buf, npawls txt, pawru len )
	{ return _npawlsmem_catcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawlsmem_catcs( npawlsmem *buf, npawls txt )
	{ return _npawlsmem_catcs( buf, txt ); }
PAW_FQK_CB pawcsc npawlsmem_catcsv( npawlsmem *buf, va_list va )
	{ return _npawlsmem_catcsv( buf, va ); }
PAW_QCK_CB pawcsc npawlsmem_catcsf( npawlsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawlsmem_catcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB npawlsmem npawlsmem_newcap( pawru cap )
	{ return _npawlsmem_newcap( cap ); }
PAW_FQK_CB npawlsmem npawlsmem_newlim( pawru lim )
	{ return _npawlsmem_newlim( lim ); }
PAW_FQK_CB npawlsmem npawlsmem_newmax( pawru max )
	{ return _npawlsmem_newmax( max ); }
PAW_FQK_CB npawlsmem npawlsmem_newlen( pawru len )
	{ return _npawlsmem_newlen( len ); }
PAW_FQK_CB npawlsmem npawlsmem_newcsn( npawls txt, pawru len )
	{ return _npawlsmem_newcsn( txt, len ); }
PAW_FQK_CB npawlsmem npawlsmem_newcs( npawls txt )
	{ return _npawlsmem_newcs( txt ); }
PAW_FQK_CB npawlsmem npawlsmem_newcsv( va_list va )
	{ return _npawlsmem_newcsv( va ); }
PAW_QCK_CB npawlsmem npawlsmem_newcsf( ... )
{
	va_list va;
	va_start( va, ... );
	npawlsmem csmem = _npawlsmem_newcsv( va );
	va_end( va );
	return csmem;
}
