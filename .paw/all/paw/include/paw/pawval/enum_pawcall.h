#pragma once

typedef enum
{
	PAWCALL_INVALID = 0,
	PAWCALL_REMOVE,
	PAWCALL_ASSIGN,
	PAWCALL_NEWFSO,
	PAWCALL_NEWABS,
	PAWCALL_NEWRAW,
	PAWCALL_DUPREF,
	PAWCALL_DUPABS,
	PAWCALL_DUPRAW,
	PAWCALL_RDLOCK,
	PAWCALL_WRLOCK,
	PAWCALL_GETCAP,
	PAWCALL_GETEND,
	PAWCALL_GETMAX,
	PAWCALL_GETLEN,
	PAWCALL_SETCAP,
	PAWCALL_SETEND,
	PAWCALL_SETMAX,
	PAWCALL_SETLEN,
	/* Streams and sockets will ignore the position variable, insisting on the
	 * variable being present simplifies the back end so that the object is
	 * smaller. Also means actual file positions will never be touched, instead
	 * pread/pwrite type calls are always used under the hood where possible
	 * while anything else just ignores the position value altogether. The
	 * pointed position value will have the amount read/written added to it
	 * to give the "move position" behaviour so for streams this can be used
	 * to indicate the total read/written altogether if you make sure it
	 * starts at 0 */
	PAWCALL_GETRAW,
	PAWCALL_PUTRAW,
	PAWCALL_COUNT
} PAWCALL;
