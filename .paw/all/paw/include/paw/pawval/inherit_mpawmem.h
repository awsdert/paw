#ifndef T
#	error "T must be defined!"
#	include <abort_compilation>
#endif

#include "inherit_mpawraw_as_union.h"
union
{
	pawabs top;
#if INTPTR_WIDTH < PAWRD_WIDTH
#	warning "Needs to be limited to your address space for buf to be correct"
#endif
	T *buf;
};

#undef T
