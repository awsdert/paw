#ifndef T
#	error "T must be defined!"
#	include <abort_compilation>
#endif

union
{
	pawabs dst;
	T* ptr;
};
