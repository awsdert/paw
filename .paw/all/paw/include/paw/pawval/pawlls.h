#pragma once
#include "pawref.h"

typedef struct
{
#define RAW pawlls
#include "inherit_pawmemsrc_as_union.h"
} pawllsvec;
typedef struct
{
#define T pawllc
#include "inherit_mpawrawdst_as_union.h"
} pawllsmem;

#define CH pawllc
#define CS pawlls
#define CSVEC pawllsvec
#define CSMEM pawllsmem
#define CS_C(LIT) U##LIT
#define CS_NAME(PFX,SFX) PFX##pawlls##SFX
#include "template_pawcs.h"

PAW_FQK_CB pawru pawllslen( pawlls str ) { return _pawllslen( str ); }
PAW_FQK_CB pawru pawllslinelen( pawlls str, pawru len ) { return _pawllslinelen( str, len ); }
PAW_FQK_CB pawru pawllsnewlinelen( pawlls str, pawru len ) { return _pawllsnewlinelen( str, len ); }

PAW_FQK_CB pawllsvec pawllsnewvecn( pawlls txt, pawru len )
	{ return _pawllsnewvecn( txt, len ); }
PAW_FQK_CB pawllsvec pawllsnewvec( pawlls txt ) { return _pawllsnewvec( txt ); }
PAW_FQK_CB pawru pawllscpy( pawllc *dst, pawlls src, pawru cpy )
	{ return _pawllscpy( dst, src, cpy ); }
PAW_FQK_CB pawru pawllscpyn( pawllc *dst, pawru max, pawlls src, pawru cpy )
	{ return _pawllscpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru pawllscmp( pawlls a, pawlls b, pawru len )
	{ return _pawllscmp( a, b, len ); }
PAW_FQK_CB pawru pawllscmpn( pawlls a, pawru lenA, pawlls b, pawru lenB )
	{ return _pawllscmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawllsicmp( pawlls a, pawlls b, pawru len )
	{ return _pawllsicmp( a, b, len ); }
PAW_FQK_CB pawru pawllsicmpn( pawlls a, pawru lenA, pawlls b, pawru lenB )
	{ return _pawllsicmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawllsfindc( pawlls qry, pawru end, pawllc loc )
	{ return _pawllsfindc( qry, end, loc ); }
PAW_FQK_CB pawru pawllsifindc( pawlls qry, pawru end, pawllc loc )
	{ return _pawllsifindc( qry, end, loc ); }
PAW_FQK_CB pawru pawllsfindn( pawlls qry, pawru end, pawlls loc, pawru len )
	{ return _pawllsfindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawllsifindn( pawlls qry, pawru end, pawlls loc, pawru len )
	{ return _pawllsifindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawllsfinds( pawlls qry, pawru end, pawlls loc )
	{ return _pawllsfinds( qry, end, loc ); }
PAW_FQK_CB pawru pawllsifinds( pawlls qry, pawru end, pawlls loc )
	{ return _pawllsifinds( qry, end, loc ); }

PAW_FQK_CB pawru pawllsrcpy( pawllc *dst, pawlls src, pawru cpy )
	{ return _pawllsrcpy( dst, src, cpy ); }
PAW_FQK_CB pawru pawllsrcpyn( pawllc *dst, pawru max, pawlls src, pawru cpy )
	{ return _pawllsrcpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru pawllsrcmp( pawlls a, pawlls b, pawru len )
	{ return _pawllsrcmp( a, b, len ); }
PAW_FQK_CB pawru pawllsrcmpn( pawlls a, pawru lenA, pawlls b, pawru lenB )
	{ return _pawllsrcmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawllsricmp( pawlls a, pawlls b, pawru len )
	{ return _pawllsricmp( a, b, len ); }
PAW_FQK_CB pawru pawllsricmpn( pawlls a, pawru lenA, pawlls b, pawru lenB )
	{ return _pawllsricmpn( a, lenA, b, lenB ); }

/* Buffer Functions */
PAW_FQK_CB pawru pawllsmem_setcap( pawllsmem *buf, pawru cap )
	{ return _pawllsmem_setcap( buf, cap ); }
PAW_FQK_CB pawru pawllsmem_setlim( pawllsmem *buf, pawru lim )
	{ return _pawllsmem_setlim( buf, lim ); }
PAW_FQK_CB pawru pawllsmem_setmax( pawllsmem *buf, pawru max )
	{ return _pawllsmem_setmax( buf, max ); }
PAW_FQK_CB pawru pawllsmem_setlen( pawllsmem *buf, pawru len )
	{ return _pawllsmem_setlen( buf, len ); }
PAW_FQK_CB pawru pawllsmem_setcsn( pawllsmem *buf, pawlls txt, pawru len )
	{ return _pawllsmem_setcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawllsmem_setcs( pawllsmem *buf, pawlls txt )
	{ return _pawllsmem_setcs( buf, txt ); }
PAW_FQK_CB pawcsc pawllsmem_setcsv( pawllsmem *buf, va_list va )
	{ return _pawllsmem_setcsv( buf, va ); }
PAW_QCK_CB pawcsc pawllsmem_setcsf( pawllsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawllsmem_setcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru pawllsmem_fitcap( pawllsmem *buf, pawru cap )
	{ return _pawllsmem_fitcap( buf, cap ); }
PAW_FQK_CB pawru pawllsmem_fitlim( pawllsmem *buf, pawru lim )
	{ return _pawllsmem_fitlim( buf, lim ); }
PAW_FQK_CB pawru pawllsmem_fitmax( pawllsmem *buf, pawru max )
	{ return _pawllsmem_fitmax( buf, max ); }
PAW_FQK_CB pawru pawllsmem_fitlen( pawllsmem *buf, pawru len )
	{ return _pawllsmem_fitlen( buf, len ); }
PAW_FQK_CB pawru pawllsmem_fitcsn( pawllsmem *buf, pawlls txt, pawru len )
	{ return _pawllsmem_fitcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawllsmem_fitcs( pawllsmem *buf, pawlls txt )
	{ return _pawllsmem_fitcs( buf, txt ); }
PAW_FQK_CB pawcsc pawllsmem_fitcsv( pawllsmem *buf, va_list va )
	{ return _pawllsmem_fitcsv( buf, va ); }
PAW_QCK_CB pawcsc pawllsmem_fitcsf( pawllsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawllsmem_fitcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru pawllsmem_catcap( pawllsmem *buf, pawru cap )
	{ return _pawllsmem_catcap( buf, cap ); }
PAW_FQK_CB pawru pawllsmem_catlim( pawllsmem *buf, pawru lim )
	{ return _pawllsmem_catlim( buf, lim ); }
PAW_FQK_CB pawru pawllsmem_catmax( pawllsmem *buf, pawru max )
	{ return _pawllsmem_catmax( buf, max ); }
PAW_FQK_CB pawru pawllsmem_catlen( pawllsmem *buf, pawru len )
	{ return _pawllsmem_catlen( buf, len ); }
PAW_FQK_CB pawru pawllsmem_catcsn( pawllsmem *buf, pawlls txt, pawru len )
	{ return _pawllsmem_catcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawllsmem_catcs( pawllsmem *buf, pawlls txt )
	{ return _pawllsmem_catcs( buf, txt ); }
PAW_FQK_CB pawcsc pawllsmem_catcsv( pawllsmem *buf, va_list va )
	{ return _pawllsmem_catcsv( buf, va ); }
PAW_QCK_CB pawcsc pawllsmem_catcsf( pawllsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawllsmem_catcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawllsmem pawllsmem_newcap( pawru cap )
	{ return _pawllsmem_newcap( cap ); }
PAW_FQK_CB pawllsmem pawllsmem_newlim( pawru lim )
	{ return _pawllsmem_newlim( lim ); }
PAW_FQK_CB pawllsmem pawllsmem_newmax( pawru max )
	{ return _pawllsmem_newmax( max ); }
PAW_FQK_CB pawllsmem pawllsmem_newlen( pawru len )
	{ return _pawllsmem_newlen( len ); }
PAW_FQK_CB pawllsmem pawllsmem_newcsn( pawlls txt, pawru len )
	{ return _pawllsmem_newcsn( txt, len ); }
PAW_FQK_CB pawllsmem pawllsmem_newcs( pawlls txt )
	{ return _pawllsmem_newcs( txt ); }
PAW_FQK_CB pawllsmem pawllsmem_newcsv( va_list va )
	{ return _pawllsmem_newcsv( va ); }
PAW_QCK_CB pawllsmem pawllsmem_newcsf( ... )
{
	va_list va;
	va_start( va, ... );
	pawllsmem csmem = _pawllsmem_newcsv( va );
	va_end( va );
	return csmem;
}
