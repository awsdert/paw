#include "template_paweint_checks.h"
#define EINTPP(TXT) TXT
#define EINTVALD EINTNAME(_,d)
#define EINTVALU EINTNAME(_,u)
typedef EINTSEGD EINTVALD;
typedef EINTSEGU EINTVALU;
#define EINTDIVD EINTNAME(_,divd)
#define EINTDIVU EINTNAME(_,divu)
#include "template_paweint_defines.h"
typedef struct { EINTVALD quo, rem; } EINTDIVD;
typedef struct { EINTVALU quo, rem; } EINTDIVU;
#define EINTE_LEN EINTNAME(_,e_len)

PAW_QCK_CB EINTVALU EINTU_MAX(void) { return EINTSEGU_MAX; }
PAW_QCK_CB EINTVALD EINTD_MAX(void) { return EINTSEGD_MAX; }
PAW_QCK_CB EINTVALD EINTD_MIN(void) { return EINTSEGD_MIN; }

PAW_QCK_CB EINTVALU EINTU_V( pawru n ) { return n; }
PAW_QCK_CB pawb EINTU_GETBIT( EINTVALU n, pawru i )
	{ return n & (((EINTVALU)1u) << i); }
PAW_QCK_CB EINTVALU EINTU_SETBIT( EINTVALU n, pawru i, pawb val )
{
	n &= ~(((EINTVALU)1u) << i);
	n |= (((EINTVALU)val) << i);
	return n;
}
PAW_QCK_CB pawb EINTU_NOT( EINTVALU n ) { return !n; }
PAW_QCK_CB EINTVALU EINTU_INC( EINTVALU n ) { return ++n; }
PAW_QCK_CB EINTVALU EINTU_DEC( EINTVALU n ) { return --n; }
PAW_QCK_CB EINTVALU EINTU_NEG( EINTVALU n ) { return -n; }
PAW_QCK_CB EINTVALU EINTU_BWN( EINTVALU n ) { return ~n; }
PAW_QCK_CB pawru EINTU_LEN( EINTVALU n )
{
#ifdef EINTSEG_IS_BITINT
	return pawemaxu__len( n, bitsof(n), 0 );
#else
	return pawru__len( n, bitsof(n), 0 );
#endif
}
PAW_QCK_CB pawrd EINTU_CMP( EINTVALU a, EINTVALU b )
	{ return ((pawrd)(a > b)) - ((pawrd)(a < b)); }
PAW_QCK_CB EINTVALU EINTU_BWA( EINTVALU a, EINTVALU b ) { return a & b; }
PAW_QCK_CB EINTVALU EINTU_BWO( EINTVALU a, EINTVALU b ) { return a | b; }
PAW_QCK_CB EINTVALU EINTU_BWX( EINTVALU a, EINTVALU b ) { return a ^ b; }
PAW_QCK_CB EINTVALU EINTU_SSR( EINTVALU n, pawru move ) { return ((EINTVALD)n) >> move; }
PAW_QCK_CB EINTVALU EINTU_USR( EINTVALU n, pawru move ) { return n >> move; }
PAW_QCK_CB EINTVALU EINTU_SHL( EINTVALU n, pawru move ) { return n << move; }
PAW_QCK_CB EINTVALU EINTU_SHR( EINTVALU n, pawru move ) { return n >> move; }
PAW_QCK_CB EINTVALU EINTU_ROL( EINTVALU n, pawru move )
	{ return (n << move) | (n >> (EINTSEGW - move)); }
PAW_QCK_CB EINTVALU EINTU_ROR( EINTVALU n, pawru move )
	{ return (n >> move) | (n << (EINTSEGW - move)); }
PAW_QCK_CB EINTVALU EINTU_ADD( EINTVALU a, EINTVALU b ) { return a + b; }
PAW_QCK_CB EINTVALU EINTU_SUB( EINTVALU a, EINTVALU b ) { return a - b; }
PAW_QCK_CB EINTVALU EINTU_MUL( EINTVALU a, EINTVALU b ) { return a * b; }
PAW_QCK_CB EINTDIVU EINTU__DIV( EINTVALU a, EINTVALU b )
{
#ifndef EINTSEG_IS_BITINT
	EINTDIVU divu = { a / b, a % b }; return divu;
#else
	pawemaxudiv xdiv = pawemaxu__div( a, b );
	EINTDIVU divu = { xdiv.quo, xdiv.rem };
	return divu;
#endif
}
PAW_QCK_CB EINTVALU EINTU_DIV( EINTVALD a, EINTVALD b )
	{ EINTDIVU divu = EINTU__DIV(a,b); return divu.quo; }
PAW_QCK_CB EINTVALU EINTU_MOD( EINTVALD a, EINTVALD b )
	{ EINTDIVU divu = EINTU__DIV(a,b); return divu.rem; }

PAW_QCK_CB EINTVALD EINTD_V( pawrd n ) { return n; }
PAW_QCK_CB pawb EINTD_NOT( EINTVALD n ) { return !n; }
PAW_QCK_CB EINTVALD EINTD_INC( EINTVALD n ) { return ++n; }
PAW_QCK_CB EINTVALD EINTD_DEC( EINTVALD n ) { return --n; }
PAW_QCK_CB EINTVALD EINTD_NEG( EINTVALD n ) { return -n; }
PAW_QCK_CB EINTVALD EINTD_BWN( EINTVALD n ) { return ~n; }
PAW_QCK_CB pawru EINTD_LEN( EINTVALD n )
	{ return pawemaxu__len( n, bitsof(n), n < 0 ); }
PAW_QCK_CB pawrd EINTD_CMP( EINTVALD a, EINTVALD b )
	{ return ((pawrd)(a > b)) - ((pawrd)(a < b)); }
PAW_QCK_CB EINTVALD EINTD_BWA( EINTVALD a, EINTVALD b ) { return a & b; }
PAW_QCK_CB EINTVALD EINTD_BWO( EINTVALD a, EINTVALD b ) { return a | b; }
PAW_QCK_CB EINTVALD EINTD_BWX( EINTVALD a, EINTVALD b ) { return a ^ b; }
PAW_QCK_CB EINTVALD EINTD_SSR( EINTVALD a, EINTVALD b ) { return a >> b; }
PAW_QCK_CB EINTVALD EINTD_USR( EINTVALD a, EINTVALD b ) { return ((EINTVALU)a) >> b; }
PAW_QCK_CB EINTVALD EINTD_SHL( EINTVALD a, EINTVALD b ) { return a << b; }
PAW_QCK_CB EINTVALD EINTD_SHR( EINTVALD a, EINTVALD b ) { return a >> b; }
PAW_QCK_CB EINTVALU EINTD_ROL( EINTVALU n, pawru move ) { return EINTU_ROL( n, move ); }
PAW_QCK_CB EINTVALU EINTD_ROR( EINTVALU n, pawru move ) { return EINTU_ROR( n, move ); }
PAW_QCK_CB EINTVALD EINTD_ADD( EINTVALD a, EINTVALD b ) { return a + b; }
PAW_QCK_CB EINTVALD EINTD_SUB( EINTVALD a, EINTVALD b ) { return a - b; }
PAW_QCK_CB EINTVALD EINTD_MUL( EINTVALD a, EINTVALD b ) { return a * b; }
PAW_QCK_CB EINTDIVD EINTD__DIV( EINTVALD a, EINTVALD b )
{
#ifndef EINTSEG_IS_BITINT
	EINTDIVD divd = { a / b, a % b }; return divd;
#else
	pawemaxddiv xdiv = pawemaxd__div( a, b );
	EINTDIVD divd = {xdiv.quo,xdiv.rem};
	return divd;
#endif
}
PAW_QCK_CB EINTVALD EINTD_DIV( EINTVALD a, EINTVALD b )
	{ EINTDIVD divd = EINTD__DIV(a,b); return divd.quo; }
PAW_QCK_CB EINTVALD EINTD_MOD( EINTVALD a, EINTVALD b )
	{ EINTDIVD divd = EINTD__DIV(a,b); return divd.rem; }
