#pragma once
#include "pawref.h"

typedef struct { pawru len; paws str; pawru shl; } pawstrctx;
PAW_QCK_CB pawrd pawc_next_mbc( char *str, pawstrctx *ctx )
{
	pawrd c = 0, i = 0;
	for
	(
		c = ctx->str[i] << ctx->shl
		, ctx->shl += CHAR_WIDTH
		; ctx->shl < CHAR_WIDTH
		; c <<= PAWMBC_WIDTH
		, c |= ctx->str[++i]
		, ctx->shl += CHAR_WIDTH
	);
	ctx->str += i;
	ctx->len -= i;
	ctx->shl %= 8;
	return ctx->shl ? ((pawru)c) >> ctx->shl : c;
}

typedef struct
{
#define RAW pawmbs
#include "inherit_pawmemsrc_as_union.h"
} pawmbsvec;
typedef struct
{
#define T pawmbc
#include "inherit_mpawrawdst_as_union.h"
} pawmbsmem;

#define CH pawmbc
#define CS pawmbs
#define CSVEC pawmbsvec
#define CSMEM pawmbsmem
#define CS_C(LIT) u8##LIT
#define CS_NAME(PFX,SFX) PFX##pawmbs##SFX
#include "template_pawcs.h"

PAW_FQK_CB pawru pawmbslen( pawmbs str ) { return _pawmbslen( str ); }
PAW_FQK_CB pawru pawmbslinelen( pawmbs str, pawru len ) { return _pawmbslinelen( str, len ); }
PAW_FQK_CB pawru pawmbsnewlinelen( pawmbs str, pawru len ) { return _pawmbsnewlinelen( str, len ); }

PAW_FQK_CB pawmbsvec pawmbsnewvecn( pawmbs txt, pawru len )
	{ return _pawmbsnewvecn( txt, len ); }
PAW_FQK_CB pawmbsvec pawmbsnewvec( pawmbs txt ) { return _pawmbsnewvec( txt ); }
PAW_FQK_CB pawru pawmbscmp( pawmbs a, pawmbs b, pawru len )
	{ return _pawmbscmp( a, b, len ); }
PAW_FQK_CB pawru pawmbscmpn( pawmbs a, pawru lenA, pawmbs b, pawru lenB )
	{ return _pawmbscmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawmbsicmp( pawmbs a, pawmbs b, pawru len )
	{ return _pawmbsicmp( a, b, len ); }
PAW_FQK_CB pawru pawmbsicmpn( pawmbs a, pawru lenA, pawmbs b, pawru lenB )
	{ return _pawmbsicmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawmbsfindc( pawmbs qry, pawru end, pawmbc loc )
	{ return _pawmbsfindc( qry, end, loc ); }
PAW_FQK_CB pawru pawmbsifindc( pawmbs qry, pawru end, pawmbc loc )
	{ return _pawmbsifindc( qry, end, loc ); }
PAW_FQK_CB pawru pawmbsfindn( pawmbs qry, pawru end, pawmbs loc, pawru len )
	{ return _pawmbsfindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawmbsifindn( pawmbs qry, pawru end, pawmbs loc, pawru len )
	{ return _pawmbsifindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawmbsfinds( pawmbs qry, pawru end, pawmbs loc )
	{ return _pawmbsfinds( qry, end, loc ); }
PAW_FQK_CB pawru pawmbsifinds( pawmbs qry, pawru end, pawmbs loc )
	{ return _pawmbsifinds( qry, end, loc ); }

/* Buffer Functions */
PAW_FQK_CB pawru pawmbsmem_setcap( pawmbsmem *buf, pawru cap )
	{ return _pawmbsmem_setcap( buf, cap ); }
PAW_FQK_CB pawru pawmbsmem_setlim( pawmbsmem *buf, pawru lim )
	{ return _pawmbsmem_setlim( buf, lim ); }
PAW_FQK_CB pawru pawmbsmem_setmax( pawmbsmem *buf, pawru max )
	{ return _pawmbsmem_setmax( buf, max ); }
PAW_FQK_CB pawru pawmbsmem_setlen( pawmbsmem *buf, pawru len )
	{ return _pawmbsmem_setlen( buf, len ); }
PAW_FQK_CB pawru pawmbsmem_setcsn( pawmbsmem *buf, pawmbs txt, pawru len )
	{ return _pawmbsmem_setcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawmbsmem_setcs( pawmbsmem *buf, pawmbs txt )
	{ return _pawmbsmem_setcs( buf, txt ); }
PAW_FQK_CB pawcsc pawmbsmem_setcsv( pawmbsmem *buf, va_list va )
	{ return _pawmbsmem_setcsv( buf, va ); }
PAW_QCK_CB pawcsc pawmbsmem_setcsf( pawmbsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawmbsmem_setcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru pawmbsmem_fitcap( pawmbsmem *buf, pawru cap )
	{ return _pawmbsmem_fitcap( buf, cap ); }
PAW_FQK_CB pawru pawmbsmem_fitlim( pawmbsmem *buf, pawru lim )
	{ return _pawmbsmem_fitlim( buf, lim ); }
PAW_FQK_CB pawru pawmbsmem_fitmax( pawmbsmem *buf, pawru max )
	{ return _pawmbsmem_fitmax( buf, max ); }
PAW_FQK_CB pawru pawmbsmem_fitlen( pawmbsmem *buf, pawru len )
	{ return _pawmbsmem_fitlen( buf, len ); }
PAW_FQK_CB pawru pawmbsmem_fitcsn( pawmbsmem *buf, pawmbs txt, pawru len )
	{ return _pawmbsmem_fitcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawmbsmem_fitcs( pawmbsmem *buf, pawmbs txt )
	{ return _pawmbsmem_fitcs( buf, txt ); }
PAW_FQK_CB pawcsc pawmbsmem_fitcsv( pawmbsmem *buf, va_list va )
	{ return _pawmbsmem_fitcsv( buf, va ); }
PAW_QCK_CB pawcsc pawmbsmem_fitcsf( pawmbsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawmbsmem_fitcsv( buf, va );
	va_end( va );
	return c;
}
PAW_FQK_CB pawru pawmbsmem_catcap( pawmbsmem *buf, pawru cap )
	{ return _pawmbsmem_catcap( buf, cap ); }
PAW_FQK_CB pawru pawmbsmem_catlim( pawmbsmem *buf, pawru lim )
	{ return _pawmbsmem_catlim( buf, lim ); }
PAW_FQK_CB pawru pawmbsmem_catmax( pawmbsmem *buf, pawru max )
	{ return _pawmbsmem_catmax( buf, max ); }
PAW_FQK_CB pawru pawmbsmem_catlen( pawmbsmem *buf, pawru len )
	{ return _pawmbsmem_catlen( buf, len ); }
PAW_FQK_CB pawru pawmbsmem_catcsn( pawmbsmem *buf, pawmbs txt, pawru len )
	{ return _pawmbsmem_catcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawmbsmem_catcs( pawmbsmem *buf, pawmbs txt )
	{ return _pawmbsmem_catcs( buf, txt ); }
PAW_FQK_CB pawcsc pawmbsmem_catcsv( pawmbsmem *buf, va_list va )
	{ return _pawmbsmem_catcsv( buf, va ); }
PAW_QCK_CB pawcsc pawmbsmem_catcsf( pawmbsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawmbsmem_catcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawmbsmem pawmbsmem_newcap( pawru cap )
	{ return _pawmbsmem_newcap( cap ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newlim( pawru lim )
	{ return _pawmbsmem_newlim( lim ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newmax( pawru max )
	{ return _pawmbsmem_newmax( max ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newlen( pawru len )
	{ return _pawmbsmem_newlen( len ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newcsn( pawmbs txt, pawru len )
	{ return _pawmbsmem_newcsn( txt, len ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newcs( pawmbs txt )
	{ return _pawmbsmem_newcs( txt ); }
PAW_FQK_CB pawmbsmem pawmbsmem_newcsv( va_list va )
	{ return _pawmbsmem_newcsv( va ); }
PAW_QCK_CB pawmbsmem pawmbsmem_newcsf( ... )
{
	va_list va;
	va_start( va, ... );
	pawmbsmem csmem = _pawmbsmem_newcsv( va );
	va_end( va );
	return csmem;
}
