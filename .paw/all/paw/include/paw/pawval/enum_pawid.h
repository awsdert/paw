#pragma once

/* All IDs here are for the current thread only, the STDGET/STDPUT/STDERR
 * reference here are for this thread to comunicate to it's spawner, not the
 * owning terminal, look to enum_pawio.h for that */

enum
{
	PAWID_INVALID = 0,
	PAWID_STDGET,
	PAWID_STDPUT,
	PAWID_STDERR,
	PAWID_MSGQUE
};
