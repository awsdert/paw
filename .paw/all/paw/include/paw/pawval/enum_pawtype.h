#pragma once
typedef enum
{
	PAWTYPE_INVALID = 0,
	/* Memory/Buffer */
	PAWTYPE_MEM,
	/* Thread */
	PAWTYPE_ANT,
	/* File System Object */
	PAWTYPE_FSO,
	/* File System Directory */
	PAWTYPE_FSD,
} PAWTYPE;
