#include "template_paweint_checks.h"
#define QCK PAW_FQK_CB
#define EINTPP(TXT) TXT
#define EINTEMU EINTNAME(_,e)
#define EINTVALD EINTNAME(_,d)
#define EINTVALU EINTNAME(_,u)
PAW_TYPEDEF struct
{
	EINTSEGU raw[2];
	struct { EINTSEGU lower; union { EINTSEGD d; EINTSEGU u; } upper; };
} EINTEMU;
/* Separate unions are to ensure the compiler catches comparison mismatches */
PAW_TYPEDEF union { EINTSEGU raw[2]; EINTEMU emu; } EINTVALD;
PAW_TYPEDEF union { EINTSEGU raw[2]; EINTEMU emu; } EINTVALU;
#define EINTDIVD EINTNAME(_,divd)
#define EINTDIVU EINTNAME(_,divu)
#include "template_paweint_defines.h"
#include "template_paweint_emudefs.h"
PAW_TYPEDEF struct { EINTVALD quo, rem; } EINTDIVD;
PAW_TYPEDEF struct { EINTVALU quo, rem; } EINTDIVU;

/* Internal usage functions */
PAW_QCK_CB pawjvu EINTE_LEN( EINTEMU n, pawjvu width, bool find0 )
{
	pawjvu max = bitsof(n), shl = (width - 1);
	EINTSEGU x = 1, bit = find0;
	if ( width > max )
	{
		pawjvu len = 0;
		shl -= max;
		x <<= shl;
		bit <<= shl;
		for ( ; x; --width, bit >>= 1, x >>= 1 )
		{
			if ( (n.raw[1] & bit) == bit )
				return width;
			if ( (n.raw[0] & bit) == bit )
				len = len ? len : width - max;
		}
		return len;
	}
	x <<= shl;
	bit <<= shl;
	for ( ; x; --width, bit >>= 1, x >>= 1 )
	{
		if ( (n.raw[0] & bit) == bit )
			return width;
	}
	return 0;
}

PAW_QCK_CB EINTEMU EINTE_BWN( EINTEMU n )
	{ n.raw[1] = ~(n.raw[1]); n.raw[0] = ~(n.raw[0]); return n; }
PAW_QCK_CB EINTEMU EINTE_INC( EINTEMU n )
	{ n.raw[0]++; n.raw[1] += !(n.raw[0]); return n; }
PAW_QCK_CB EINTEMU EINTE_DEC( EINTEMU n )
	{ n.raw[0]--; n.raw[1] += (n.raw[0] == EINTSEGU_MAX); return n; }
PAW_QCK_CB EINTEMU EINTE_NEG( EINTEMU n ) { return EINTE_INC(EINTE_BWN(n)); }
PAW_QCK_CB pawd EINTE_CMP( EINTEMU a, EINTEMU b )
{
	return ( a.raw[1] != b.raw[0] )
		? EINTSEGU_CMP( a.raw[1], b.raw[1] )
		: EINTSEGU_CMP( a.raw[0], b.raw[0] );
}
PAW_QCK_CB EINTEMU EINTE_ROR( EINTEMU n, pawjvu move )
{
	EINTEMU c = {{0}};
	pawjvu shr = move % bitsof(c), max = bitsof(c.raw[0]), shl = 0;
	if ( shr >= max )
	{
		EINTSEGU x = n.raw[0];
		n.raw[0] = n.raw[1];
		n.raw[1] = x;
		shr -= max;
	}
	shl = max - shr;
	c.raw[0] = (n.raw[0] >> shr) | (n.raw[1] << shl);
	c.raw[1] = (n.raw[1] >> shr) | (n.raw[0] << shl);
	return c;
}
PAW_QCK_CB EINTEMU EINTE_ROL( EINTEMU n, pawjvu move )
{
	EINTEMU c = {{0}};
	pawjvu shl = move % bitsof(c), max = bitsof(c.raw[0]), shr = 0;
	if ( shl >= max )
	{
		EINTSEGU x = n.raw[0];
		n.raw[0] = n.raw[1];
		n.raw[1] = x;
		shl -= max;
	}
	shr = max - shl;
	c.raw[0] = (n.raw[1] << shr) | (n.raw[0] >> shr);
	c.raw[1] = (n.raw[0] << shr) | (n.raw[1] >> shr);
	return c;
}
PAW_QCK_CB EINTEMU EINTE_SHL( EINTEMU n, pawjvu move )
{
	pawjvu max = bitsof(n.raw[0]);
	if ( move >= bitsof(n) )
	{
		n.raw[0] = 0;
		n.raw[0] = 0;
		return n;
	}
	if ( move >= max )
	{
		move -= max;
		n.raw[1] = n.raw[0] << move;
		n.raw[0] = 0;
		return n;
	}
	n.raw[1] = (n.raw[1] << move) | (n.raw[0] >> (max - move));
	n.raw[0] <<= move;
	return n;
}
PAW_QCK_CB EINTEMU EINTE_USR( EINTEMU n, pawjvu move )
{
	pawjvu max = bitsof(n.raw[0]);
	if ( move >= bitsof(n) )
	{
		n.raw[0] = 0;
		n.raw[0] = 0;
		return n;
	}
	if ( move >= max )
	{
		move -= max;
		n.raw[0] = n.raw[1] >> move;
		n.raw[1] = 0;
		return n;
	}
	n.raw[0] = (n.raw[0] >> move) | (n.raw[1] << (max - move));
	n.raw[1] >>= move;
	return n;
}
PAW_QCK_CB EINTEMU EINTE_SSR( EINTEMU n, pawjvu move )
{
	pawjvu max = bitsof(n.raw[0]);
	EINTSEGU nil = (n.raw[1] > EINTSEGD_MAX) ? EINTSEGU_MAX : 0;
	if ( move >= bitsof(n) )
	{
		n.raw[0] = nil;
		n.raw[0] = nil;
		return n;
	}
	if ( move >= max )
	{
		move -= max;
		n.raw[0] = (n.raw[1] >> move) | (nil << (max - move));
		n.raw[1] = nil;
		return n;
	}
	n.raw[0] = (n.raw[0] >> move) | (n.raw[1] << (max - move));
	n.raw[1] = (n.raw[1] >> move) | (nil << (max - move));
	return n;
}
PAW_QCK_CB EINTEMU EINTE_BWA( EINTEMU a, EINTEMU b )
	{ a.raw[1] &= b.raw[0]; a.raw[0] &= b.raw[0]; return a; }
PAW_QCK_CB EINTEMU EINTE_BWO( EINTEMU a, EINTEMU b )
	{ a.raw[1] |= b.raw[0]; a.raw[0] |= b.raw[0]; return a; }
PAW_QCK_CB EINTEMU EINTE_BWX( EINTEMU a, EINTEMU b )
	{ a.raw[1] ^= b.raw[0]; a.raw[0] ^= b.raw[0]; return a; }
PAW_QCK_CB EINTEMU EINTE_ADD( EINTEMU a, EINTEMU b )
{
	EINTEMU c;
	c.raw[0] = a.raw[0] + b.raw[0];
	c.raw[1] = (a.raw[1] + b.raw[1]) + (pawu)(c.raw[0] < a.raw[0]);
	return c;
}
PAW_QCK_CB EINTEMU EINTE_SUB( EINTEMU a, EINTEMU b )
{
	EINTEMU c;
	c.raw[0] = a.raw[0] - b.raw[0];
	c.raw[1] = (a.raw[1] - b.raw[1]) - (pawu)(c.raw[0] > a.raw[0]);
	return c;
}
PAW_QCK_CB EINTEMU EINTE_MUL( EINTEMU a, EINTEMU b )
{
	pawjvu move = 0;
	EINTSEGU bit = 1;
	EINTEMU c = {{0}};
	while ( bit )
	{
		if ( b.raw[0] & bit )
			c = EINTE_ADD( c, EINTE_SHL( a, move ) );
		if ( b.raw[1] & bit )
			c = EINTE_ADD( c, EINTE_SHL( a, move + bitsof(bit) ) );
		bit <<= 1;
		move++;
	}
	return c;
}
PAW_QCK_CB EINTDIVU EINTE__DIV( EINTEMU a, EINTEMU b )
{
	pawjvu max = bitsof(a.raw[0]);
	pawjvu alen = EINTE_LEN(a,bitsof(a),EINTSEGD_MIN);
	pawjvu blen = EINTE_LEN(b,bitsof(b),EINTSEGD_MIN), len = alen;
	EINTDIVU c = {{{0}}};
	EINTSEGU bit = 1;
	EINTEMU div = b;
	c.rem.emu = a;
	/* In division by 0 you take nothing from the remainder 0 times so 0 is
	 * the answer */
	if ( !alen || !blen || blen > alen )
		return c;
	b = EINTE_SHL( b, alen - blen );
	while ( EINTE_CMP( b, div ) >= 0 )
	{
		if ( EINTE_CMP( c.rem.emu, b ) < 0 )
		{
			b = EINTE_USR( b, 1 );
			--len;
		}
		else
		{
			c.quo.emu = EINTE_SHL( c.quo.emu, alen - len );
			c.quo.emu.raw[0] |= 1;
			EINTE_SUB( c.rem.emu, b );
			len = EINTE_LEN(c.rem.emu,alen,bit <<= ((alen % max) - 1));
			b = EINTE_USR( b, alen - len );
			alen = len;
		}
	}
	return c;
}

/* Unsigned operations */
QCK EINTVALU EINTU_V( pawjvu n ) { EINTVALU x = {{0}}; x.raw[0] = n; return x; }
QCK EINTVALU EINTU_MAX(void) { EINTVALU n = {{EINTSEGU_MAX,EINTSEGU_MAX}}; return n; }
QCK pawjvu EINTU_LEN( EINTVALU n )
	{ return EINTE_LEN( n.emu, bitsof(n), EINTSEGD_MIN ); }
QCK bool EINTU_NOT( EINTVALU n ) { return !(n.raw[1] || n.raw[0]); }
QCK EINTVALU EINTU_BWN( EINTVALU n )
	{ n.raw[1] = ~(n.raw[1]); n.raw[0] = ~(n.raw[0]); return n; }
QCK EINTVALU EINTU_INC( EINTVALU n )
	{ n.raw[0]++; n.raw[1] += !(n.raw[0]); return n; }
QCK EINTVALU EINTU_DEC( EINTVALU n )
	{ n.raw[0]--; n.raw[1] += (n.raw[0] == EINTSEGU_MAX); return n; }
QCK EINTVALU EINTU_NEG( EINTVALU n ) { return EINTU_INC(EINTU_BWN(n)); }
QCK pawd EINTU_CMP( EINTVALU a, EINTVALU b )
{
	return ( a.raw[1] != b.raw[0] )
		? EINTSEGU_CMP( a.raw[1], b.raw[1] )
		: EINTSEGU_CMP( a.raw[0], b.raw[0] );
}
QCK EINTVALU EINTU_SHL( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_SHL( n.emu, move ); return n; }
QCK EINTVALU EINTU_USR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALU EINTU_SSR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALU EINTU_SHR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALU EINTU_ROL( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_ROL( n.emu, move ); return n; }
QCK EINTVALU EINTU_ROR( EINTVALU n, pawjvu move )
	{ n.emu = EINTE_ROR( n.emu, move ); return n; }
QCK EINTVALU EINTU_BWA( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWA( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_BWO( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWO( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_BWX( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_BWX( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_ADD( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_ADD( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_SUB( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_SUB( a.emu, b.emu ); return a; }
QCK EINTVALU EINTU_MUL( EINTVALU a, EINTVALU b )
	{ a.emu = EINTE_MUL( a.emu, b.emu ); return a; }
QCK EINTDIVU EINTU__DIV( EINTVALU a, EINTVALU b )
	{ return EINTE__DIV( a.emu, b.emu ); }
QCK EINTVALU EINTU_DIV( EINTVALU a, EINTVALU b ) { return EINTU__DIV( a, b ).quo; }
QCK EINTVALU EINTU_MOD( EINTVALU a, EINTVALU b ) { return EINTU__DIV( a, b ).rem; }
/* Signed operations */
QCK EINTVALD EINTD_V( pawjvd n ) { EINTVALD x = {{0}}; x.raw[0] = n; return x; }
QCK EINTVALD EINTD_MAX(void) { EINTVALD n = {{EINTSEGU_MAX,EINTSEGD_MAX}}; return n; }
QCK EINTVALD EINTD_MIN(void) { EINTVALD n = {{0,EINTSEGD_MIN}}; return n; }
QCK pawjvu EINTD_LEN( EINTVALD n )
	{ return EINTE_LEN( n.emu, bitsof(n), (n.raw[1] < 0) ? 0 : EINTSEGD_MIN ); }
QCK bool EINTD_NOT( EINTVALD n ) { return !(n.raw[1] || n.raw[0]); }
QCK EINTVALD EINTD_BWN( EINTVALD n ) { n.emu = EINTE_BWN(n.emu); return n; }
QCK EINTVALD EINTD_INC( EINTVALD n ) { n.emu = EINTE_INC(n.emu); return n; }
QCK EINTVALD EINTD_DEC( EINTVALD n ) { n.emu = EINTE_DEC(n.emu); return n; }
QCK EINTVALD EINTD_NEG( EINTVALD n ) { n.emu = EINTE_NEG(n.emu); return n; }
QCK pawd EINTD_CMP( EINTVALD a, EINTVALD b )
{
	return (a.raw[1] != b.raw[1])
		? EINTSEGD_CMP( a.raw[1], b.raw[1] )
		: EINTSEGD_CMP( a.raw[0], b.raw[0] );
}
QCK EINTVALD EINTD_SHL( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SHL( n.emu, move ); return n; }
QCK EINTVALD EINTD_USR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_USR( n.emu, move ); return n; }
QCK EINTVALD EINTD_SSR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALD EINTD_SHR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_SSR( n.emu, move ); return n; }
QCK EINTVALD EINTD_ROL( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_ROL( n.emu, move ); return n; }
QCK EINTVALD EINTD_ROR( EINTVALD n, pawjvu move )
	{ n.emu = EINTE_ROR( n.emu, move ); return n; }
QCK EINTVALD EINTD_BWA( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWA( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_BWO( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWO( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_BWX( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_BWX( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_ADD( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_ADD( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_SUB( EINTVALD a, EINTVALD b )
	{ a.emu = EINTE_SUB( a.emu, b.emu ); return a; }
QCK EINTVALD EINTD_MUL( EINTVALD a, EINTVALD b )
{
	EINTEMU x = (b.raw[1] < 0) ? EINTE_NEG(b.emu) : b.emu;
	if ( a.raw[1] >= 0 )
	{
		a.emu = EINTE_MUL( a.emu, x );
		if ( b.raw[1] < 0 )
			return EINTD_NEG(a);
	}
	a.emu = EINTE_MUL( EINTE_NEG( a.emu ), x );
	return (b.raw[1] < 0) ? a : EINTD_NEG( a );
}
QCK EINTDIVD EINTD__DIV( EINTVALD a, EINTVALD b )
{
	EINTDIVU n = {{0}};
	EINTDIVD N = {{0}};
	EINTEMU x = (b.raw[1] < 0) ? EINTE_NEG(b.emu) : b.emu;
	if ( a.raw[1] >= 0 )
	{
		n = EINTE__DIV( a.emu, x );
		N.rem.emu = n.rem.emu;
		N.quo.emu = (b.raw[1] < 0) ? EINTE_NEG(n.quo.emu) : n.quo.emu;
		return N;
	}
	n = EINTE__DIV( EINTE_NEG( a.emu ), x );
	N.quo.emu = (b.raw[1] < 0) ? n.quo.emu : EINTE_NEG(n.quo.emu);
	N.rem.emu = EINTE_NEG(n.rem.emu);
	return N;
}
QCK EINTVALD EINTD_DIV( EINTVALD a, EINTVALD b )
	{ return EINTD__DIV( a, b ).quo; }
QCK EINTVALD EINTD_MOD( EINTVALD a, EINTVALD b )
	{ return EINTD__DIV( a, b ).rem; }
#undef EINTEMU
#include "template_paweint_emu_undefs.h"
