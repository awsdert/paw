#pragma once
#include "pawraw.h"

/* 1 = hardware tick: Can be as small as 32bit which is too small
 * for general dates but big enough for session time, timers, etc */
PAW_ANT_CB pawage pawage_get_tickcount( pawage *dst );
PAW_ANT_CB pawage pawage_get_monotonic( pawage *dst );
PAW_ANT_CB pawage pawage_get_timestamp( pawage *dst );
/* Ticks spent executing app code */
PAW_ANT_CB pawage pawage_get_apphasexec( pawage *dst );
/* Ticks spent executing thread code */
PAW_ANT_CB pawage pawage_get_anthasexec( pawage *dst );
/* Ticks not spent executing app code */
PAW_ANT_CB pawage pawage_get_appnotexec( pawage *dst );
/* Ticks not spent executing thread code */
PAW_ANT_CB pawage pawage_get_antnotexec( pawage *dst );

/* 1 = 1 nanosecond: common to 2000+ operating systems, might be as small as
 * 64bit which is too small to include quectoseconds but big enough for
 * general dates... at least for a few thousand years anyways. Changed back to
 * 64bit after I released that 2035 bug I vaguely rembered was specific to
 * 32bit variants built into systems */
typedef enum
{
	pawageNS_unit_ns,
	pawageNS_unit_us,
	pawageNS_unit_ms,
	pawageNS_unit_s,
	pawageNS_metric_count,
	pawageNS_unit_sh = pawageNS_metric_count,
	pawageNS_unit_Ej,
	pawageNS_unit_count
} pawageNS_unit;

PAW_ANT_CB pawageNS pawageNS_get_tickcount( pawageNS *dst );
PAW_ANT_CB pawageNS pawageNS_get_monotonic( pawageNS *dst );
PAW_ANT_CB pawageNS pawageNS_get_timestamp( pawageNS *dst );
/* Ticks spent executing app code */
PAW_ANT_CB pawageNS pawageNS_get_apphasexec( pawageNS *dst );
/* Ticks spent executing thread code */
PAW_ANT_CB pawageNS pawageNS_get_anthasexec( pawageNS *dst );
/* Ticks not spent executing app code */
PAW_ANT_CB pawageNS pawageNS_get_appnotexec( pawageNS *dst );
/* Ticks not spent executing thread code */
PAW_ANT_CB pawageNS pawageNS_get_antnotexec( pawageNS *dst );

/* 1 = 1 quectosecond: Not usually available on user hardware, nor do the
 * operating system APIs support it directly, you'll have include dedicated
 * libraries to read out such detailed timestamps and you'll also have to
 * convert them but otherwise you can use these for related calculations */

typedef enum
{
	/* Quectoseconds */
	pawageQS_unit_qs,
	/* Rontoseconds */
	pawageQS_unit_rs,
	/* Yoctoseconds */
	pawageQS_unit_ys,
	/* Zeptoseconds */
	pawageQS_unit_zs,
	/* Attoseconds */
	pawageQS_unit_as,
	/* Femtoseconds */
	pawageQS_unit_fs,
	/* Picoseconds */
	pawageQS_unit_ps,
	/* Nanoseconds */
	pawageQS_unit_ns,
	/* Microseconds */
	pawageQS_unit_us,
	/* Milliseconds */
	pawageQS_unit_ms,
	/* Seconds */
	pawageQS_unit_s,
	pawageQS_metric_count,
	/* Physics jiffy */
	pawageQS_unit_Pj,
	/* Svedberg */
	pawageQS_unit_sv,
	/* Shake */
	pawageQS_unit_sh,
	/* Electronics jiffy */
	pawageQS_unit_Ej,
	pawageQS_unit_count
} pawageQS_unit;

/* Changed back to 256bit after releasing that 2035 bug I vaguely remembered
 * was already accounted for with a 64bit pawageNS */
typedef pawe256d pawageQS;
