#ifndef T
#	error "T must be defined!"
#	include <abort_compilation>
#endif

union
{
	pawabs src;
	T* ptr;
};
