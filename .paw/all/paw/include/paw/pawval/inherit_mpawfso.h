/* Turns out HANDLEs are just indices, makes things so much simpler */
pawrd  fso;
/* Found a way to emulate mremap on windows so no longer need to store the
 * map handle here, instead it will be one page behind the map base with the
 * file handle in front of it and the size after it */
pawabs map;
