#pragma once

enum
{
	/* These follow the numbers given by the related defines for CreateFileA's
	 * dwCreationDisposition parameter */
	PAWFSO_BIAS_MAKEORFAIL = 1, /* CREATE_NEW */
	PAWFSO_BIAS_TRIMALWAYS = 2, /* CREATE_ALWAYS */
	PAWFSO_BIAS_OPENIFREAL = 3, /* OPEN_EXISTING */
	PAWFSO_BIAS_OPENALWAYS = 4, /* OPEN_ALWAYS */
	PAWFSO_BIAS_TRIMIFREAL = 5, /* TRUNCATE_EXISTING */
	LPAWFSO_BIAS_COUNT = 6
};

/* Note:
 * https://devblogs.microsoft.com/commandline/per-directory-case-sensitivity-and-wsl/
 * */
#define PAWFSO_O_PRECISECASE	((pawe64u)0x8)
#define PAWFSO_O_SHARE_ENTRY	((pawe64u)0x100)
/* Permit entry to be renamed/deleted */
#define PAWFSO_O_SHARE_TITLE	((pawe64u)0x200)
#define PAWFSO_O_OPEN_SYMLNK	((pawe64u)0x400)
