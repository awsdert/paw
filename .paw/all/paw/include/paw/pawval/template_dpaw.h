#ifndef T
#	error "T must be defined as the signed offset type!"
#	include <paw/defabort.h>
#endif
#ifndef NAME
#	error "NAME must be defined as NAME CUSTOMNAME"
#	include <paw/defabort.h>
#endif
#include <paw/mayabort.h>
#define SYM(SFX) PAWSYM2(_,NAME,_,SFX)
#define DPAW_USEVAL SYM(useval)
#define DPAW_CPYRAW SYM(cpyraw)
#define DPAW_GETRAW SYM(getraw)
#define DPAW_SETRAW SYM(setraw)
#define DPAW_PUTRAW SYM(putraw)
#define DPAW_FITRAW SYM(fitraw)
#define DPAW_CAPRAW SYM(capraw)
#define DPAW_INSRAW SYM(insraw)
#define DPAW_CATRAW SYM(catraw)

PAW_QCK_CB pawmsg DPAW_USEVAL( dpaw *data, pawalter alter, T *val )
{
	pawintd Val = PAWINTDNEW( val );
	switch ( alter )
	{
	case PAWALTER_ABSAT: case PAWALTER_ENDAT: case PAWALTER_CAPAT:
		return PAWMSGID_INVALIDOPT;
	default:
	}
	return data->mm->alterCB( data, &Val, alter );
}
PAW_QCK_CB npawsys DPAW_CPYRAW( dpaw *data, T abs, void *dst, pawru end )
{
	pawintd Abs = PAWINTDNEW(&abs);
	return dpaw_retdid( data->mm->writeCB( data, &Abs, dst, &end ), end );
}
PAW_QCK_CB npawsys DPAW_GETRAW( dpaw *data, void *dst, pawru end )
	{ return dpaw_retdid( data->mm->fetchCB( data, NULL, dst, &end ), end ); }
PAW_QCK_CB npawsys DPAW_SETRAW( dpaw *data, T abs, pawp src, pawru end )
{
	pawintd Abs = PAWINTDNEW(&abs);
	return dpaw_retdid( data->mm->writeCB( data, &Abs, src, &end ), end );
}
PAW_QCK_CB npawsys DPAW_PUTRAW( dpaw *data, pawp src, pawru end )
	{ return dpaw_retdid( data->mm->writeCB( data, NULL, src, &end ), end ); }
PAW_QCK_CB pawmsg DPAW_CAPRAW( dpaw *data, T abs, pawp src, pawru end )
{
	T cut = 0;
	pawintd Cut = PAWINTDNEW(&cut);
	npawsys msg = data->mm->alterCB( data, &Cut, PAWALTER_UPEND );
	if ( msg )
		return dpaw_retdid( msg, 0 );
	if ( cut < abs + end )
	{
		if ( cut <= abs )
			return dpaw_retdid( PAWMSGID_INVALIDPOS, 0 );
		return DPAW_SETRAW( data, abs, src, cut - abs );
	}
	msg = data->mm->alterCB( data, &Cut, PAWALTER_ENDAT );
	return msg ? dpaw_retdid( msg, 0 ) : DPAW_SETRAW( data, abs, src, end );
}
PAW_QCK_CB npawsys DPAW_INSRAW( dpaw *data, T abs, pawp src, pawru end )
{
	T mov = end;
	pawintd Mov = PAWINTDNEW(&mov);
	pawmsg msg = data->mm->shiftCB( data, &Mov );
	return msg ? dpaw_retdid( msg, 0 ) : DPAW_SETRAW( data, abs, src, end );
}
PAW_QCK_CB npawsys DPAW_CATRAW( dpaw *data, pawp src, pawru end )
{
	T abs = 0;
	pawintd Abs = PAWINTDNEW(&abs);
	pawmsg msg = data->mm->alterCB( data, &Abs, PAWALTER_UPEND );
	return msg ? dpaw_retdid( msg, 0 ) : DPAW_SETRAW( data, abs, src, end );
}
PAW_QCK_CB npawsys DPAW_FITRAW( dpaw *data, T abs, pawp src, pawru end )
{
	T fit = 0;
	pawintd Fit = PAWINTDNEW(&fit);
	pawmsg msg = data->mm->alterCB( data, &Fit, PAWALTER_UPEND );
	if ( msg )
		return dpaw_retdid( msg, 0 );
	fit = (fit < abs + end) ? abs + end : fit;
	msg = data->mm->alterCB( data, &Fit, PAWALTER_ENDAT );
	return msg ? dpaw_retdid( msg, 0 ) : DPAW_SETRAW( data, abs, src, end );
}

#undef DPAW_CATRAW
#undef DPAW_INSRAW
#undef DPAW_CAPRAW
#undef DPAW_FITRAW
#undef DPAW_SETRAW
#undef DPAW_USEVAL
#undef SYM
#undef NAME
#undef T
