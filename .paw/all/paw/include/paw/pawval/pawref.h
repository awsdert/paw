#pragma once
#include "pawalltypes.h"
#include "pawage.h"
#include "pawraw.h"

PAW_C_API_OPEN
#include "enum_pawfso_bias.h"
#include "enum_pawfso_hint.h"
#include "enum_pawcall.h"
#include "enum_paw_lock.h"
#include "enum_pawid.h"
#include "enum_pawio.h"


/* Callback list purposes */
typedef void (*pawvoid_cb)( void );

/** @brief Yield execution to another thread/application */
PAW_ANT_CB void 	pawyield( void );
/** @brief Get thread's id as win32/pthreads/etc report it */
PAW_ANT_CB pawexe	pawgetid( void );
/** @brief Get application's id as getpid()/etc report it */
PAW_ANT_CB pawexe	pawgetpid( void );
/** @brief Get parent application's id as getppid()/etc report it */
PAW_ANT_CB pawexe	pawgetppid( void );

/** @brief Total allocated space page space in thread */
PAW_ANT_CB uintptr_t pawgetspace( void );
/** @brief Allocate at least size page bytes for later allocations */
PAW_ANT_CB pawmsg pawprealloc( uintptr_t size );

PAW_ANT_CB pawexe pawgetanttid( pawid ant );
PAW_ANT_CB pawid  pawthisantis( void );
PAW_ANT_CB pawid  pawsrcpipeis( void );
PAW_ANT_CB pawid  pawdstpipeis( void );
PAW_ANT_CB pawid  pawlogpipeis( void );

/* This is used to indicate the minimum output buffer length before it's put
 * through pawio_putraw() DURING the argument processing loop. At the end of
 * the loop any left over is printed anyways. The input buffer will just take
 * as much as it can via pawio_getraw() before contiuing with argument
 * processing. Custom amounts are possible, these are just for standards
 * purposes */
enum
{
	PAWIO_MIN_INST = 0,
	/* Default min write */
	PAWIO_MIN_FREQ = 512,
	/* Default min flush */
	PAWIO_MIN_RARE = 8192
};

PAW_ANT_CB void pawio_setminwrite( pawid id, pawru min );
PAW_ANT_CB void pawio_setminflush( pawid id, pawru min );

typedef pawmsg (*pawant_cb)( pawid ant, pawed argc, pawmbc **argv );

#ifdef NPAW_BUILD
/** @brief Setup the globals needed for libpaw to work correctly
 * @note ONLY native software should consider using this, if at all
 * @note If you do use it offload initialisation stuff to the thread that paw
 * creates for you, leave the main thread to paw for signals etc
 * @example
 * int pawmain( mpawref *id, pawrd argsref, pawp ud ) { ... }
 * int main( int argc, char **argv )
 *   { return pawsinit(argc,argv,pawmain); } */
PAW_ANT_CB int 	pawsinit( int argc, paws *argv, pawant_cb mainCB );
/** @brief Setup the globals needed for libpaw to work correctly
 * @note ONLY native software should consider using this, if at all
 * @note If you do use it offload initialisation stuff to the thread that paw
 * creates for you, leave the main thread to paw for signals etc
 * @example
 * int pawmain( mpawref *id, pawrd argsref, pawp ud ) { ... }
 * int wWinMain( ..., DWORD argc, WCHAR **argv, ... )
 *   { return npawlsinit(argc,argv,pawmain); } */
PAW_ANT_CB int 	npawlsinit( int argc, npawls *argv, pawant_cb mainCB );
/** @brief Setup the globals needed for libpaw to work correctly
 * @note ONLY native software should consider using this, if at all
 * @note If you do use it offload initialisation stuff to the thread that paw
 * creates for you, leave the main thread to paw for signals etc
 * @example
 * int pawmain( mpawref *id, pawrd argsref, pawp ud ) { ... }
 * int tWinMain( ..., DWORD argc, TCHAR **argv, ... )
 *   { return npawlsinit(argc,argv,pawmain); } */
PAW_ANT_CB int 	npawtsinit( int argc, npawts *argv, pawant_cb mainCB );
#endif


/** @brief Wait for thread to end
 * @return Exit code given by thread, 0 means no issues, -1 an error
 * @note Tells the thread it can truly end AFTER copying the message state
 * and exit code. Internal Pointer will be cleared to prevent undefined
 * behaviour thereafter */
PAW_ANT_CB pawmsg pawant_w84end( npawsys ant );
/** @brief Get last known message state of ant */
PAW_ANT_CB pawmsg pawant_getmsg( npawsys ant );
/** @brief Tell thread it can exit whenever and clear internal pointer */
PAW_ANT_CB void   pawant_detach( npawsys ant );


PAW_C_API_SHUT
