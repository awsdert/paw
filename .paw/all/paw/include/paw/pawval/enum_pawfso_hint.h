#pragma once
/* Each option can override another however in most cases they cannot be
 * changed after the function returns. I decided this was the cleanest way of
 * supporting all forms of options, whether or not they're bitwise options
 * on the host platform. I just need to add new enums for any not covered here */
enum
{
	PAWFSO_VA_LIST_END = 0,
	/* PAWIO_MIN_FREQ by default, takes pawru */
	PAWFSO_HINT_MIN_TO_WRITE,
	/* PAWIO_MIN_RARE by default, takes pawru */
	PAWFSO_HINT_MIN_TO_FLUSH,
	/* Default share hint */
	PAWFSO_HINT_SHARE_ACCESS,
	PAWFSO_HINT_DO_NOT_SHARE,
	/* Default symlink handling */
	PAWFSO_HINT_OPEN_LINKED,
	PAWFSO_HINT_OPEN_SYMLNK,
	/* Default lifespan mode */
	PAWFSO_HINT_IS_PERMENANT,
	PAWFSO_HINT_DEL_ONLOGOUT,
	PAWFSO_HINT_DEL_ON_CLOSE,
	/* Default attitude to on execute */
	PAWFSO_HINT_IGNOREONEXEC,
	PAWFSO_HINT_REMOVEONEXEC,
	/* Default system flush mode */
	PAWFSO_HINT_MANUAL_FLUSH,
	PAWFSO_HINT_DIRECT_FLUSH,
	/* Default system cache mode */
	PAWFSO_HINT_BUFFERED_OPS,
	PAWFSO_HINT_NOT_BUFFERED,
	/* Default position hint */
	PAWFSO_HINT_RANDOM_IOPOS,
	PAWFSO_HINT_LINEAR_IOPOS,
	/* If a hint conflicting with native abilities is impossible then before
	 * the attempt to open/make a file is even made paw will just return -1 to
	 * indicate failure */
	/* Default offset mode */
	PAWFSO_HINT_BASIC_OFFSET,
	PAWFSO_HINT_64BIT_OFFSET,
	/* Default read mode */
	PAWFSO_HINT_PERMIT_FETCH,
	PAWFSO_HINT_REFUSE_FETCH,
	/* Default write mode */
	PAWFSO_HINT_PERMIT_WRITE,
	PAWFSO_HINT_REFUSE_WRITE,
	PAWFSO_HINT_PERMIT_RUNEX,
	/* Default run execution mode */
	PAWFSO_HINT_REFUSE_RUNEX,
	/* Default fetch DAC mode (msw only) */
	PAWFSO_HINT_PERMIT_R_DAC,
	PAWFSO_HINT_REFUSE_R_DAC,
	/* Default fetch attributes/info mode (msw only for now) */
	PAWFSO_HINT_PERMIT_R_INF,
	PAWFSO_HINT_REFUSE_R_INF,
	/* Default fetch extended attributes/info mode (msw only for now) */
	PAWFSO_HINT_PERMIT_RXINF,
	PAWFSO_HINT_REFUSE_RXINF,
	/* Default write DAC hint (msw only for now) */
	PAWFSO_HINT_PERMIT_W_DAC,
	PAWFSO_HINT_REFUSE_W_DAC,
	/* Default owner write attributes/info mode (msw only for now) */
	PAWFSO_HINT_PERMIT_W_INF,
	PAWFSO_HINT_REFUSE_W_INF,
	/* Default owner write extended attributes/info mode (msw only for now) */
	PAWFSO_HINT_PERMIT_WXINF,
	PAWFSO_HINT_REFUSE_WXINF,
	/* Default unix octal permissions hint (creation only), takes pawu.
	 * On msw an attempt will be made to duplicate those permissions in native
	 * format (presumably via the DAC list). Unless you have good reason to
	 * alter this it is better to leave at the default of matching the target
	 * directory's permissions */
	PAWFSO_HINT_PERMIT_OCTAL,
	PAWFSO_HINT_REFUSE_OCTAL,
	PAWFSO_VA_HINTS_COUNT
};
