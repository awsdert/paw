#undef CSMEM_FITCSF
#undef CSMEM_FITCSV
#undef CSMEM_FITRAW
#undef CSMEM_FITLEN
#undef CSMEM_FITMAX
#undef CSMEM_FITCAP

#undef CSMEM_INSCSF
#undef CSMEM_INSCSV
#undef CSMEM_INSRAW
#undef CSMEM_INSLEN
#undef CSMEM_INSMAX
#undef CSMEM_INSCAP

#undef CSMEM_CATCSF
#undef CSMEM_CATCSV
#undef CSMEM_CATRAW
#undef CSMEM_CATLEN
#undef CSMEM_CATMAX
#undef CSMEM_CATCAP

#undef CSMEM_CAPCSF
#undef CSMEM_CAPCSV
#undef CSMEM_CAPRAW
#undef CSMEM_CAPLEN
#undef CSMEM_CAPMAX
#undef CSMEM_CAPCAP

#undef CSMEM_SETCSF
#undef CSMEM_SETCSV
#undef CSMEM_SETRAW
#undef CSMEM_SETLEN
#undef CSMEM_SETMAX
#undef CSMEM_SETCAP

#undef CSMEM_NEWCSF
#undef CSMEM_NEWCSV
#undef CSMEM_NEWRAW
#undef CSMEM_NEWLEN
#undef CSMEM_NEWMAX
#undef CSMEM_NEWCAP

#undef CSIO_FITLEN
#undef CSIO_SETLEN
#undef CSIO_FITMAX
#undef CSIO_FITCAP
#undef CSIO_NEWLEN
#undef CSIO_NEWMAX
#undef CSIO_NEWCAP

#undef CS_RICMPN
#undef CS_RICMP
#undef CS_RCMPN
#undef CS_RCMP
#undef CS_RLEN

#undef CS_ICMPN
#undef CS_ICMP
#undef CS_CMPN
#undef CS_CMP
#undef CS_NEWLINELEN
#undef CS_LINELEN
#undef CS_NEWVECN
#undef CS_NEWVEC
#undef CS_LENN
#undef CS_LEN

#undef _CSIGET
#undef CSIGET
#undef _CSGET
#undef CSGET
#undef _CSPUT
#undef CSPUT

#undef CSMEM
#undef CSVEC

#undef CS_NAME
#undef CS_C
#undef CS
#undef CH
