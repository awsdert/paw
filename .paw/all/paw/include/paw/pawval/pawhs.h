#pragma once
#include "pawref.h"

typedef struct
{
#define RAW pawhs
#include "inherit_pawmemsrc_as_union.h"
} pawhsvec;
typedef struct
{
#define T pawhc
#include "inherit_mpawrawdst_as_union.h"
} pawhsmem;

#define CH pawhc
#define CS pawhs
#define CSVEC pawhsvec
#define CSMEM pawhsmem
#define CS_C(LIT) u##LIT
#define CS_NAME(PFX,SFX) PFX##pawhs##SFX
#include "template_pawcs.h"

PAW_FQK_CB pawru pawhslen( pawhs str ) { return _pawhslen( str ); }
PAW_FQK_CB pawru pawhslinelen( pawhs str, pawru len ) { return _pawhslinelen( str, len ); }
PAW_FQK_CB pawru pawhsnewlinelen( pawhs str, pawru len ) { return _pawhsnewlinelen( str, len ); }

PAW_FQK_CB pawhsvec pawhsnewvecn( pawhs txt, pawru len )
	{ return _pawhsnewvecn( txt, len ); }
PAW_FQK_CB pawhsvec pawhsnewvec( pawhs txt ) { return _pawhsnewvec( txt ); }
PAW_FQK_CB pawru pawhscpy( pawhc *dst, pawhs src, pawru cpy )
	{ return _pawhscpy( dst, src, cpy ); }
PAW_FQK_CB pawru pawhscpyn( pawhc *dst, pawru max, pawhs src, pawru cpy )
	{ return _pawhscpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru pawhscmp( pawhs a, pawhs b, pawru len )
	{ return _pawhscmp( a, b, len ); }
PAW_FQK_CB pawru pawhscmpn( pawhs a, pawru lenA, pawhs b, pawru lenB )
	{ return _pawhscmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawhsicmp( pawhs a, pawhs b, pawru len )
	{ return _pawhsicmp( a, b, len ); }
PAW_FQK_CB pawru pawhsicmpn( pawhs a, pawru lenA, pawhs b, pawru lenB )
	{ return _pawhsicmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawhsfindc( pawhs qry, pawru end, pawhc loc )
	{ return _pawhsfindc( qry, end, loc ); }
PAW_FQK_CB pawru pawhsifindc( pawhs qry, pawru end, pawhc loc )
	{ return _pawhsifindc( qry, end, loc ); }
PAW_FQK_CB pawru pawhsfindn( pawhs qry, pawru end, pawhs loc, pawru len )
	{ return _pawhsfindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawhsifindn( pawhs qry, pawru end, pawhs loc, pawru len )
	{ return _pawhsifindn( qry, end, loc, len ); }
PAW_FQK_CB pawru pawhsfinds( pawhs qry, pawru end, pawhs loc )
	{ return _pawhsfinds( qry, end, loc ); }
PAW_FQK_CB pawru pawhsifinds( pawhs qry, pawru end, pawhs loc )
	{ return _pawhsifinds( qry, end, loc ); }

PAW_FQK_CB pawru pawhsrcpy( pawhc *dst, pawhs src, pawru cpy )
	{ return _pawhsrcpy( dst, src, cpy ); }
PAW_FQK_CB pawru pawhsrcpyn( pawhc *dst, pawru max, pawhs src, pawru cpy )
	{ return _pawhsrcpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru pawhsrcmp( pawhs a, pawhs b, pawru len )
	{ return _pawhsrcmp( a, b, len ); }
PAW_FQK_CB pawru pawhsrcmpn( pawhs a, pawru lenA, pawhs b, pawru lenB )
	{ return _pawhsrcmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru pawhsricmp( pawhs a, pawhs b, pawru len )
	{ return _pawhsricmp( a, b, len ); }
PAW_FQK_CB pawru pawhsricmpn( pawhs a, pawru lenA, pawhs b, pawru lenB )
	{ return _pawhsricmpn( a, lenA, b, lenB ); }

/* Buffer Functions */
PAW_FQK_CB pawru pawhsmem_setcap( pawhsmem *buf, pawru cap )
	{ return _pawhsmem_setcap( buf, cap ); }
PAW_FQK_CB pawru pawhsmem_setlim( pawhsmem *buf, pawru lim )
	{ return _pawhsmem_setlim( buf, lim ); }
PAW_FQK_CB pawru pawhsmem_setmax( pawhsmem *buf, pawru max )
	{ return _pawhsmem_setmax( buf, max ); }
PAW_FQK_CB pawru pawhsmem_setlen( pawhsmem *buf, pawru len )
	{ return _pawhsmem_setlen( buf, len ); }
PAW_FQK_CB pawru pawhsmem_setcsn( pawhsmem *buf, pawhs txt, pawru len )
	{ return _pawhsmem_setcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawhsmem_setcs( pawhsmem *buf, pawhs txt )
	{ return _pawhsmem_setcs( buf, txt ); }
PAW_FQK_CB pawcsc pawhsmem_setcsv( pawhsmem *buf, va_list va )
	{ return _pawhsmem_setcsv( buf, va ); }
PAW_QCK_CB pawcsc pawhsmem_setcsf( pawhsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawhsmem_setcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru pawhsmem_fitcap( pawhsmem *buf, pawru cap )
	{ return _pawhsmem_fitcap( buf, cap ); }
PAW_FQK_CB pawru pawhsmem_fitlim( pawhsmem *buf, pawru lim )
	{ return _pawhsmem_fitlim( buf, lim ); }
PAW_FQK_CB pawru pawhsmem_fitmax( pawhsmem *buf, pawru max )
	{ return _pawhsmem_fitmax( buf, max ); }
PAW_FQK_CB pawru pawhsmem_fitlen( pawhsmem *buf, pawru len )
	{ return _pawhsmem_fitlen( buf, len ); }
PAW_FQK_CB pawru pawhsmem_fitcsn( pawhsmem *buf, pawhs txt, pawru len )
	{ return _pawhsmem_fitcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawhsmem_fitcs( pawhsmem *buf, pawhs txt )
	{ return _pawhsmem_fitcs( buf, txt ); }
PAW_FQK_CB pawcsc pawhsmem_fitcsv( pawhsmem *buf, va_list va )
	{ return _pawhsmem_fitcsv( buf, va ); }
PAW_QCK_CB pawcsc pawhsmem_fitcsf( pawhsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawhsmem_fitcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru pawhsmem_catcap( pawhsmem *buf, pawru cap )
	{ return _pawhsmem_catcap( buf, cap ); }
PAW_FQK_CB pawru pawhsmem_catlim( pawhsmem *buf, pawru lim )
	{ return _pawhsmem_catlim( buf, lim ); }
PAW_FQK_CB pawru pawhsmem_catmax( pawhsmem *buf, pawru max )
	{ return _pawhsmem_catmax( buf, max ); }
PAW_FQK_CB pawru pawhsmem_catlen( pawhsmem *buf, pawru len )
	{ return _pawhsmem_catlen( buf, len ); }
PAW_FQK_CB pawru pawhsmem_catcsn( pawhsmem *buf, pawhs txt, pawru len )
	{ return _pawhsmem_catcsn( buf, txt, len ); }
PAW_FQK_CB pawru pawhsmem_catcs( pawhsmem *buf, pawhs txt )
	{ return _pawhsmem_catcs( buf, txt ); }
PAW_FQK_CB pawcsc pawhsmem_catcsv( pawhsmem *buf, va_list va )
	{ return _pawhsmem_catcsv( buf, va ); }
PAW_QCK_CB pawcsc pawhsmem_catcsf( pawhsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _pawhsmem_catcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawhsmem pawhsmem_newcap( pawru cap )
	{ return _pawhsmem_newcap( cap ); }
PAW_FQK_CB pawhsmem pawhsmem_newlim( pawru lim )
	{ return _pawhsmem_newlim( lim ); }
PAW_FQK_CB pawhsmem pawhsmem_newmax( pawru max )
	{ return _pawhsmem_newmax( max ); }
PAW_FQK_CB pawhsmem pawhsmem_newlen( pawru len )
	{ return _pawhsmem_newlen( len ); }
PAW_FQK_CB pawhsmem pawhsmem_newcsn( pawhs txt, pawru len )
	{ return _pawhsmem_newcsn( txt, len ); }
PAW_FQK_CB pawhsmem pawhsmem_newcs( pawhs txt )
	{ return _pawhsmem_newcs( txt ); }
PAW_FQK_CB pawhsmem pawhsmem_newcsv( va_list va )
	{ return _pawhsmem_newcsv( va ); }
PAW_QCK_CB pawhsmem pawhsmem_newcsf( ... )
{
	va_list va;
	va_start( va, ... );
	pawhsmem csmem = _pawhsmem_newcsv( va );
	va_end( va );
	return csmem;
}
