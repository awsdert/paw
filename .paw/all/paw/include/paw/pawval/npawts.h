#pragma once
#if !defined(NPAW_BUILD)
#	error "npawts.h is EXCLUSIVELY for native builds!"
#	include <abort_compilation>
#endif
#include "pawref.h"

typedef struct
{
#define RAW npawts
#include "inherit_pawmemsrc_as_union.h"
} npawtsvec;
typedef struct
{
#define T npawtc
#include "inherit_mpawrawdst_as_union.h"
} npawtsmem;

#define CH npawtc
#define CS npawts
#define CSVEC npawtsvec
#define CSMEM npawtsmem
#define CS_C(LIT) u8##LIT
#define CS_NAME(PFX,SFX) PFX##npawts##SFX
#include "template_mpawcs.h"

PAW_FQK_CB pawru npawtslen( npawts str ) { return _npawtslen( str ); }
PAW_FQK_CB pawru npawtslinelen( npawts str, pawru len ) { return _npawtslinelen( str, len ); }
PAW_FQK_CB pawru npawtsnewlinelen( npawts str, pawru len ) { return _npawtsnewlinelen( str, len ); }


PAW_FQK_CB npawtsvec npawtsnewvecn( npawts txt, pawru len )
	{ return _npawtsnewvecn( txt, len ); }
PAW_FQK_CB npawtsvec npawtsnewvec( npawts txt )
	{ return _npawtsnewvec( txt ); }
PAW_FQK_CB pawru npawtscpy( npawtc *dst, npawts src, pawru cpy )
	{ return _npawtscpy( dst, src, cpy ); }
PAW_FQK_CB pawru npawtscpyn( npawtc *dst, pawru max, npawts src, pawru cpy )
	{ return _npawtscpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru npawtscmp( npawts a, npawts b, pawru len )
	{ return _npawtscmp( a, b, len ); }
PAW_FQK_CB pawru npawtscmpn( npawts a, pawru lenA, npawts b, pawru lenB )
	{ return _npawtscmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawtsicmp( npawts a, npawts b, pawru len )
	{ return _npawtsicmp( a, b, len ); }
PAW_FQK_CB pawru npawtsicmpn( npawts a, pawru lenA, npawts b, pawru lenB )
	{ return _npawtsicmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawtsfindc( npawts qry, pawru end, npawtc loc )
	{ return _npawtsfindc( qry, end, loc ); }
PAW_FQK_CB pawru npawtsifindc( npawts qry, pawru end, npawtc loc )
	{ return _npawtsifindc( qry, end, loc ); }
PAW_FQK_CB pawru npawtsfindn( npawts qry, pawru end, npawts loc, pawru len )
	{ return _npawtsfindn( qry, end, loc, len ); }
PAW_FQK_CB pawru npawtsifindn( npawts qry, pawru end, npawts loc, pawru len )
	{ return _npawtsifindn( qry, end, loc, len ); }
PAW_FQK_CB pawru npawtsfinds( npawts qry, pawru end, npawts loc )
	{ return _npawtsfinds( qry, end, loc ); }
PAW_FQK_CB pawru npawtsifinds( npawts qry, pawru end, npawts loc )
	{ return _npawtsifinds( qry, end, loc ); }

PAW_FQK_CB pawru npawtsrcpy( npawtc *dst, npawts src, pawru cpy )
	{ return _npawtsrcpy( dst, src, cpy ); }
PAW_FQK_CB pawru npawtsrcpyn( npawtc *dst, pawru max, npawts src, pawru cpy )
	{ return _npawtsrcpyn( dst, max, src, cpy ); }
PAW_FQK_CB pawru npawtsrcmp( npawts a, npawts b, pawru len )
	{ return _npawtsrcmp( a, b, len ); }
PAW_FQK_CB pawru npawtsrcmpn( npawts a, pawru lenA, npawts b, pawru lenB )
	{ return _npawtsrcmpn( a, lenA, b, lenB ); }
PAW_FQK_CB pawru npawtsricmp( npawts a, npawts b, pawru len )
	{ return _npawtsricmp( a, b, len ); }
PAW_FQK_CB pawru npawtsricmpn( npawts a, pawru lenA, npawts b, pawru lenB )
	{ return _npawtsricmpn( a, lenA, b, lenB ); }


/* Buffer Functions */
PAW_FQK_CB pawru npawtsmem_setcap( npawtsmem *buf, pawru cap )
	{ return _npawtsmem_setcap( buf, cap ); }
PAW_FQK_CB pawru npawtsmem_setlim( npawtsmem *buf, pawru lim )
	{ return _npawtsmem_setlim( buf, lim ); }
PAW_FQK_CB pawru npawtsmem_setmax( npawtsmem *buf, pawru max )
	{ return _npawtsmem_setmax( buf, max ); }
PAW_FQK_CB pawru npawtsmem_setlen( npawtsmem *buf, pawru len )
	{ return _npawtsmem_setlen( buf, len ); }
PAW_FQK_CB pawru npawtsmem_setcsn( npawtsmem *buf, npawts txt, pawru len )
	{ return _npawtsmem_setcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawtsmem_setcs( npawtsmem *buf, npawts txt )
	{ return _npawtsmem_setcs( buf, txt ); }
PAW_FQK_CB pawcsc npawtsmem_setcsv( npawtsmem *buf, va_list va )
	{ return _npawtsmem_setcsv( buf, va ); }
PAW_QCK_CB pawcsc npawtsmem_setcsf( npawtsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawtsmem_setcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru npawtsmem_fitcap( npawtsmem *buf, pawru cap )
	{ return _npawtsmem_fitcap( buf, cap ); }
PAW_FQK_CB pawru npawtsmem_fitlim( npawtsmem *buf, pawru lim )
	{ return _npawtsmem_fitlim( buf, lim ); }
PAW_FQK_CB pawru npawtsmem_fitmax( npawtsmem *buf, pawru max )
	{ return _npawtsmem_fitmax( buf, max ); }
PAW_FQK_CB pawru npawtsmem_fitlen( npawtsmem *buf, pawru len )
	{ return _npawtsmem_fitlen( buf, len ); }
PAW_FQK_CB pawru npawtsmem_fitcsn( npawtsmem *buf, npawts txt, pawru len )
	{ return _npawtsmem_fitcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawtsmem_fitcs( npawtsmem *buf, npawts txt )
	{ return _npawtsmem_fitcs( buf, txt ); }
PAW_FQK_CB pawcsc npawtsmem_fitcsv( npawtsmem *buf, va_list va )
	{ return _npawtsmem_fitcsv( buf, va ); }
PAW_QCK_CB pawcsc npawtsmem_fitcsf( npawtsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawtsmem_fitcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB pawru npawtsmem_catcap( npawtsmem *buf, pawru cap )
	{ return _npawtsmem_catcap( buf, cap ); }
PAW_FQK_CB pawru npawtsmem_catlim( npawtsmem *buf, pawru lim )
	{ return _npawtsmem_catlim( buf, lim ); }
PAW_FQK_CB pawru npawtsmem_catmax( npawtsmem *buf, pawru max )
	{ return _npawtsmem_catmax( buf, max ); }
PAW_FQK_CB pawru npawtsmem_catlen( npawtsmem *buf, pawru len )
	{ return _npawtsmem_catlen( buf, len ); }
PAW_FQK_CB pawru npawtsmem_catcsn( npawtsmem *buf, npawts txt, pawru len )
	{ return _npawtsmem_catcsn( buf, txt, len ); }
PAW_FQK_CB pawru npawtsmem_catcs( npawtsmem *buf, npawts txt )
	{ return _npawtsmem_catcs( buf, txt ); }
PAW_FQK_CB pawcsc npawtsmem_catcsv( npawtsmem *buf, va_list va )
	{ return _npawtsmem_catcsv( buf, va ); }
PAW_QCK_CB pawcsc npawtsmem_catcsf( npawtsmem *buf, ... )
{
	va_list va;
	va_start( va, buf );
	pawcsc c = _npawtsmem_catcsv( buf, va );
	va_end( va );
	return c;
}

PAW_FQK_CB npawtsmem npawtsmem_newcap( pawru cap )
	{ return _npawtsmem_newcap( cap ); }
PAW_FQK_CB npawtsmem npawtsmem_newlim( pawru lim )
	{ return _npawtsmem_newlim( lim ); }
PAW_FQK_CB npawtsmem npawtsmem_newmax( pawru max )
	{ return _npawtsmem_newmax( max ); }
PAW_FQK_CB npawtsmem npawtsmem_newlen( pawru len )
	{ return _npawtsmem_newlen( len ); }
PAW_FQK_CB npawtsmem npawtsmem_newcsn( npawts txt, pawru len )
	{ return _npawtsmem_newcsn( txt, len ); }
PAW_FQK_CB npawtsmem npawtsmem_newcs( npawts txt )
	{ return _npawtsmem_newcs( txt ); }
PAW_FQK_CB npawtsmem npawtsmem_newcsv( va_list va )
	{ return _npawtsmem_newcsv( va ); }
PAW_QCK_CB npawtsmem npawtsmem_newcsf( ... )
{
	va_list va;
	va_start( va, ... );
	npawtsmem csmem = _npawtsmem_newcsv( va );
	va_end( va );
	return csmem;
}
