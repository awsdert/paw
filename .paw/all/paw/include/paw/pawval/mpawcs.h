#pragma once
#include "mpawrawdst.h"

/* Implementations of get/put */
typedef struct
{
	pawru T;
	pawmm const *mm;
	mpawcs_toc_cb     tocCB;
	mpawcs_getc_cb    getcCB;
	pawmbe_getllc_cb getllcCB;
} mpawcsget;
typedef struct
{
	pawru T;
	pawru maxPerUC;
	pawmm const *mm;
	mpawcs_toc_cb     tocCB;
	mpawcs_putc_cb    putcCB;
	pawmbe_putllc_cb putllcCB;
} mpawcsput;
typedef struct
{
	mpawcsget get;
	mpawcsput put;
} mpawcsimp;

/* Note for self, implement %u, %lu, etc with prefixing size parameter
 * in mpawputcsf such as:
 * putcsf( ((sizeof(long) << 4) | sizeof(int)) << sizeof(void*), format, va ) */

/* imp is not a pointer so that the compiler has a chance to optimise out
 * the callbacks altogether */
PAW_QCK_CB pawru mpawcslen( mpawcsget imp, pawp cs )
{
	pawru i = 0;
	while ( imp.getcCB( cs, i ) ) ++i;
	return i;
}
PAW_QCK_CB mpawmemsrc mpawcsnewvecn( mpawcsget imp, pawp cs, pawru len )
	{ mpawmemsrc vec = { imp.T * len, cs }; return vec; }
PAW_FQK_CB mpawmemsrc mpawcsnewvec( mpawcsget imp, pawp cs )
	{ return mpawcsnewvecn( imp, cs, mpawcslen( imp, cs ) ); }
PAW_QCK_CB pawru mpawcslenn( mpawcsget imp, pawp cs, pawru max )
{
	pawru i = 0;
	while ( i < max && imp.getcCB( cs, i ) ) ++i;
	return i;
}
PAW_QCK_CB pawru mpawcslinelen( mpawcsget imp, pawp cs, pawru len )
{
	pawcsc c = 0;
	pawru i = 0;
	/* The ascii table lists new line starters directly next to each other
	 * in this order: '\n', '\v', '\f', '\r'
	 * So subtracting the 1st and casting to an unsigned variant will always
	 * tell us if the character is a new line starter if it's less than 4 */
	for ( ; i < len; ++i )
	{
		c = imp.getcCB(cs,i);
		if ( !c || (((pawru)(c - '\n')) < 3) )
			return i;
	}
	return i;
}
PAW_QCK_CB pawru mpawcsnewlinelen( mpawcsget imp, pawp cs, pawru len )
{
	pawcsc c = imp.getcCB( cs, 0 );
	pawb n = (len && c == '\n');
	pawb v = (len && c == '\v');
	pawb f = (len && c == '\f');
	pawb r = (len && c == '\r');
	pawb rn = (r && len > 1 && imp.getcCB( cs, 1 ) == '\n');
	return n + v + f + r + rn;
}
PAW_QCK_CB pawru pawcscmp( mpawcsget imp, mpawmemsrc a, mpawmemsrc b )
{
	pawru i = 0, lim = (a.end >= b.end) ? a.end : b.end, len = lim / imp.T;
	while ( i < len && imp.getcCB( a.top, i ) == imp.getcCB( b.top, i ) ) ++i;
	return i + (pawb)(imp.getcCB( a.top, i ) == imp.getcCB( b.top, i ));
}
PAW_QCK_CB pawru mpawcsrcmp( mpawcsget imp, mpawmemsrc a, mpawmemsrc b )
{
	pawru lim = (a.end >= b.end) ? a.end : b.end, dif = a.end - b.end;
	pawru i = dif / imp.T;
	if ( a.end < b.end )
		return b.end / imp.T;
	a.top = ((pawcu*)(a.top)) + (imp.T * dif);
	a.end -= dif;
	while ( i && imp.getcCB( a.top, i ) == imp.getcCB( b.top, i ) ) --i;
	return i;
}
PAW_QCK_CB pawru mpawcsfindc( mpawcsget imp, mpawmemsrc qry, pawcsc loc )
{
	pawru i = 0, len = qry.end / imp.T;
	while ( i < len && imp.getcCB( qry.top, i ) != loc ) ++i;
	return i;
}
PAW_QCK_CB pawru mpawcsrfindc( mpawcsget imp, mpawmemsrc qry, pawcsc loc )
{
	pawru i = qry.end / imp.T;
	while ( i && imp.getcCB( qry.top, i ) != loc ) --i;
	return i;
}
PAW_QCK_CB pawru mpawcsfindn( mpawcsget imp, mpawmemsrc qry, mpawmemsrc loc )
{
	pawru i = 0, len = qry.end / imp.T, lim = qry.end, end = loc.end + imp.T;
	pawcu const *top = qry.top;
	while ( i < len )
	{
		qry.top = top + (i * imp.T);
		qry.end = lim - (i * imp.T);
		if ( pawcscmp( imp, qry, loc ) == loc.end )
			return i;
		++i;
	}
	return i;
}
PAW_FQK_CB pawru mpawcsfinds( mpawcsget imp, mpawmemsrc qry, pawp loc )
	{ return mpawcsfindn( imp, qry, mpawcsnewvec( imp, loc ) ); }
PAW_QCK_CB pawru mpawcsrfindn( mpawcsget imp, mpawmemsrc qry, mpawmemsrc loc )
{
	pawru len = qry.end / imp.T, i = len, lim = qry.end, end = loc.end + imp.T;
	pawcu const *top = qry.top;
	if ( qry.end < loc.end )
		return qry.end;
	while ( i )
	{
		--i;
		qry.top = top + (i * imp.T);
		qry.end = lim - (i * imp.T);
		if ( pawcscmp( imp, qry, loc ) == 0 )
			return i;
	}
	return len;
}
PAW_FQK_CB pawru mpawcsrfinds( mpawcsget imp, mpawmemsrc qry, pawp loc )
	{ return mpawcsrfindn( imp, qry, mpawcsnewvec( imp, loc ) ); }
/* Always returns UTF-8 */
PAW_ANT_CB pawmsg mpawcsmem_print( va_list va, mpawmemsrc *vec );
typedef npawsys (*_mpawrawdst_putcsv_cb)( mpawrawdst *mem, mpawmemsrc tmp, void *ud );
PAW_QCK_CB npawsys _mpawcsmem_putmbs
	( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va, _mpawrawdst_putcsv_cb cb )
{
	/* Use standardised function to produce UTF-8 string to convert into
	 * target format. Memory as handled via thread local with it's own
	 * dedicated page/s so will be cleaned up at thread exit. */
	mpawmemsrc mbs = {0};
	pawmsg msg = mpawcsmem_print(va,&mbs);
	if ( msg )
		return msg;
	pawru len = mbs.end / sizeof(pawmbc), max = len + 1, size = 0, poz = 0;
	mpawrawdst tmp = {0};
	pawcsc c = 0, r = 0;
	tmp.cap = size = imp.T * imp.maxPerUC * max;
	tmp.top = alloca( tmp.cap );
	while ( poz < mbs.end )
	{
		size = mbs.end - poz;
		c = pawmbs_getcsc( pawp_addbytes( mbs.top, poz ), &size );
		if ( c < 0 )
			return -c;
		poz += size;
		size = tmp.cap - tmp.end;
		r = imp.putllcCB( pawv_addbytes( tmp.top, tmp.end ), &size, c );
		if ( r < 0 )
			return -r;
		tmp.end += size;
	}
	return cb( mem, imp.T * pos, tmp );
}

PAW_QCK_CB npawsys _mpawcsmem_setraw( mpawrawdst *mem, pawru abs, mpawmemsrc tmp )
	{ return dpaw_setraw( (dpaw*)mem, abs, tmp.top, tmp.end ); }
PAW_QCK_CB npawsys mpawcsmem_setcsv( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va )
	{ return _mpawcsmem_putmbs( mem, imp, pos, va, _mpawcsmem_setraw ); }

PAW_QCK_CB npawsys _mpawcsmem_insraw( mpawrawdst *mem, pawru abs, mpawmemsrc tmp )
	{ return dpaw_insraw( (dpaw*)mem, abs, tmp.top, tmp.end ); }
PAW_QCK_CB npawsys mpawcsmem_inscsv( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va )
	{ return _mpawcsmem_putmbs( mem, imp, pos, va, _mpawcsmem_insraw ); }

PAW_QCK_CB npawsys _mpawcsmem_fitraw( mpawrawdst *mem, pawru abs, mpawmemsrc tmp )
	{ return dpaw_fitraw( (dpaw*)mem, abs, tmp.top, tmp.end ); }
PAW_QCK_CB npawsys mpawcsmem_fitcsv( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va )
	{ return _mpawcsmem_putmbs( mem, imp, pos, va, _mpawcsmem_fitraw ); }

PAW_QCK_CB npawsys _mpawcsmem_capraw( mpawrawdst *mem, pawru abs, mpawmemsrc tmp )
	{ return dpaw_capraw( (dpaw*)mem, abs, tmp.top, tmp.end ); }
PAW_QCK_CB npawsys mpawcsmem_capcsv( mpawrawdst *mem, mpawcsput imp, pawru pos, va_list va )
	{ return _mpawcsmem_putmbs( mem, imp, pos, va, _mpawcsmem_capraw ); }

PAW_QCK_CB npawsys _mpawcsmem_catraw( mpawrawdst *mem, pawru abs, mpawmemsrc tmp )
	{ (void)abs; return dpaw_catraw( (dpaw*)mem, tmp.top, tmp.end ); }
PAW_QCK_CB npawsys mpawcsmem_catcsv( mpawrawdst *mem, mpawcsput imp, va_list va )
	{ return _mpawcsmem_putmbs( mem, imp, 0, va, _mpawcsmem_catraw ); }

PAW_QCK_CB mpawrawdst _mpawcsmem_newcsv
	( pawmbs file, pawru line, mpawcsput imp, va_list va )
{
	mpawrawdst mem = mpawrawdstnew( file, line, imp.mm );
	mpawcsmem_catcsv( &mem, imp, va );
	return mem;
}
PAW_QCK_CB mpawrawdst _mpawcsmem_newcsf
	( pawmbs file, pawru line, mpawcsput imp, ... )
{
	va_list va;
	va_start( va, cb );
	mpawrawdst mem = _mpawcsmem_newcsv( file, line, imp, cb, va );
	va_end( va );
	return mem;
}
#define mpawcsmem_newcsv( IMP, CB, VA ) _mpawcsmem_newcsv( PAWMBS_ERRSRC, IMP, CB, VA )
#define mpawcsmem_newcsf( IMP, CB, ... ) \
	_mpawcsmem_newcsf( PAWMBS_ERRSRC, IMP, CB, __VA_ARGS__ )
