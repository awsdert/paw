#pragma once

PAW_C_API_OPEN

#define PAWMBC_SIZE   CAUGHT_CHAR8_SIZE
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWMBC_WIDTH  CAUGHT_CHAR8_WIDTH
#define PAWMBC_TYPE   CAUGHT_CHAR8_TYPE
#define PAWMBC_C(VAL) u8##VAL
#define PAWMBC_MIN    CAUGHT_CHAR8_MIN
#define PAWMBC_MAX    CAUGHT_CHAR8_MAX
typedef PAWMBC_TYPE   pawmbc;
typedef pawmbc const *pawmbs;

#if PAWMBC_MIN < 0
#	define PAWMBC_SIGNED 1
#else
#	define PAWMBC_UNSIGNED 1
#endif

#define PAWMBC_MAX_ENCODED_WIDTH 21
#define PAWMBC_MAX_ENCODED_BYTES 4
#define PAWMBC_MAX_ENCODED_CHARS 4

#define ___PAWMBS_C(LIT) u8##LIT
#define __PAWMBS_C(LIT) ___PAWMBS_C(LIT)
#define _PAWMBS_C(LIT) __PAWMBS_C(LIT)
#define PAWMBS_FILE   _PAWMBS_C(PAWS_FILE)
#define PAWMBS_ERRSRC PAWMBS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWMBC_CXX(VAL) CASTXX(PAWMBC_TYPE,VAL)
#define PAWMBC_XXMIN    CAUGHT_CHAR8_MIN
#define PAWMBC_XXMAX    CAUGHT_CHAR8_MAX
#endif
