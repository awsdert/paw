#pragma once

#ifdef __GNUC_EXECUTION_CHARSET_NAME
#	define PAWS_CHARSET __GNUC_EXECUTION_CHARSET_NAME
#elif __CHAR_BIT__ == 7
#	define PAWS_CHARSET "UTF-7"
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#	define PAWS_CHARSET "UTF-8"
#endif

PAW_C_API_OPEN

#define PAWC_SIZE   1
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define CHAR_WIDTH  __CHAR_BIT__
#define PAWC_TYPE     char
#define PAWC_C(VAL) CAST(PAWC_TYPE,VAL)
#define PAWC_MIN    CHAR_MIN
#define PAWC_MAX    CHAR_MAX
typedef PAWC_TYPE   char;
typedef char const *paws;

#if PAWC_MIN < 0
#	define PAWC_SIGNED 1
#else
#	define PAWC_UNSIGNED 1
#endif

/* Just simplify pruning the filepath from release builds for devs */
#if defined( _DEBUG ) || defined( _TIMED )
#define PAWS_FILE __FILE__
#else
#define PAWS_FILE ""
#endif

#define PAWS_ERRSRC PAWS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWC_CXX(VAL) CASTXX(PAWC_TYPE,VAL)
#define PAWC_XXMIN    CHAR_MIN
#define PAWC_XXMAX    CHAR_MAX
#endif
