#pragma once

PAW_C_API_OPEN

#define PAWEJD_SIZE   PAWINT_DIVCIEL(BITINT_MAXWIDTH,CHAR_BIT)
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWEJD_WIDTH  CAUGHT_BITINT_MAXWIDTH
#define PAWEJD_TYPE     signed _BitInt(PAWEJD_WIDTH)
#define PAWEJU_TYPE   unsigned _BitInt(PAWEJD_WIDTH)
#define PAWEJU_C(VAL) CAST(PAWEJU_TYPE,VAL)
#define PAWEJD_C(VAL) CAST(PAWEJD_TYPE,VAL)
#define PAWEJU_MAX    (~PAWEJU_C(0u))
#define PAWEJD_MIN    ((~PAWEJD_C(0))<<(PAWEJD_WIDTH-1))
#define PAWEJD_MAX    (~PAWEJD_MIN)
typedef PAWEJD_TYPE   pawejd;
typedef PAWEJU_TYPE   paweju;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWEJU_CXX(VAL) CASTXX(PAWEJU_TYPE,VAL)
#define PAWEJD_CXX(VAL) CASTXX(PAWEJD_TYPE,VAL)
#define PAWEJU_XXMAX    (~PAWEJU_CXX(0u))
#define PAWEJD_XXMIN    ((~PAWEJD_CXX(0))<<(PAWEJD_WIDTH-1))
#define PAWEJD_XXMAX    (~PAWEJD_XXMIN)
#endif
