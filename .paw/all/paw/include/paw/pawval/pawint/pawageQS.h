#pragma once

PAW_C_API_OPEN

#define PAWAGEQS_SIZE   PAWINT_DIVCIEL(256,PAWCD_WIDTH)
#define PAWAGEQS_WIDTH  256
#define PAWAGEQS_TYPE     signed _BitInt(PAWAGEQS_WIDTH)
#define PAWAGEQS_C(VAL) CAST(PAWAGEQS_TYPE,VAL)
#define PAWAGEQS_MIN    ((~PAWAGEQS_C(0))<<(PAWAGEQS_WIDTH-1))
#define PAWAGEQS_MAX    (~PAWAGEQS_MIN)
typedef PAWAGEQS_TYPE   pawageQS;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWAGEQS_CXX(VAL) CASTXX(PAWAGEQS_TYPE,VAL)
#define PAWAGEQS_XXMIN    ((~PAWAGEQS_CXX(0))<<(PAWAGEQS_WIDTH-1))
#define PAWAGEQS_XXMAX    (~PAWAGEQS_XXMIN)
#endif
