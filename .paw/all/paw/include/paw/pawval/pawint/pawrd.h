#pragma once

#include "pawmd.h"

PAW_C_API_OPEN

#if AUTOC_ARCH_x86_64
#	if PAWMD_WIDTH > 64
#		define PAWRD_SIZE  PAWMD_SIZE
#		define PAWRD_WIDTH PAWMD_WIDTH
#	else
#		define PAWRD_SIZE   (64 / CHAR_BIT)
#		define PAWRD_WIDTH  64
#	endif
#elif PAWMD_WIDTH > INTPTR_WIDTH
#	define PAWRD_SIZE   PAWMD_SIZE
#	define PAWRD_WIDTH  PAWMD_WIDTH
#else
#	define PAWRD_SIZE   SIZEOF_INTPTR
#	define PAWRD_WIDTH  INTPTR_WIDTH
#endif

#define PAWRD_TYPE     signed _BitInt(PAWRD_WIDTH)
#define PAWRU_TYPE   unsigned _BitInt(PAWRD_WIDTH)
#define PAWRU_C(VAL) CAST(PAWRU_TYPE,VAL)
#define PAWRD_C(VAL) CAST(PAWRD_TYPE,VAL)
#define PAWRU_MAX    (~PAWRU_C(0u))
#define PAWRD_MIN    ((~PAWRD_C(0))<<(PAWRD_WIDTH-1))
#define PAWRD_MAX    (~PAWRD_MIN)
typedef PAWRD_TYPE   pawrd;
typedef PAWRU_TYPE   pawru;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWRU_CXX(VAL) CASTXX(PAWRU_TYPE,VAL)
#define PAWRD_CXX(VAL) CASTXX(PAWRD_TYPE,VAL)
#define PAWRU_XXMAX    (~PAWRU_CXX(0u))
#define PAWRD_XXMIN    ((~PAWRD_CXX(0))<<(PAWRD_WIDTH-1))
#define PAWRD_XXMAX    (~PAWRD_XXMIN)
#endif
