#pragma once

PAW_C_API_OPEN

#define PAWE32D_SIZE   PAWINT_DIVCIEL(32,CHAR_BIT)
#define PAWE32D_WIDTH  32
#define PAWE32D_TYPE     signed _BitInt(PAWE32D_WIDTH)
#define PAWE32U_TYPE   unsigned _BitInt(PAWE32D_WIDTH)
#define PAWE32U_C(VAL) CAST(PAWE32U_TYPE,VAL)
#define PAWE32D_C(VAL) CAST(PAWE32D_TYPE,VAL)
#define PAWE32U_MAX    (~PAWE32U_C(0u))
#define PAWE32D_MIN    ((~PAWE32D_C(0))<<(PAWE32D_WIDTH-1))
#define PAWE32D_MAX    (~PAWE32D_MIN)
typedef PAWE32D_TYPE   pawe32d;
typedef PAWE32U_TYPE   pawe32u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE32U_CXX(VAL) CASTXX(PAWE32U_TYPE,VAL)
#define PAWE32D_CXX(VAL) CASTXX(PAWE32D_TYPE,VAL)
#define PAWE32U_XXMAX    (~PAWE32U_CXX(0u))
#define PAWE32D_XXMIN    ((~PAWE32D_CXX(0))<<(PAWE32D_WIDTH-1))
#define PAWE32D_XXMAX    (~PAWE32D_XXMIN)
#endif
