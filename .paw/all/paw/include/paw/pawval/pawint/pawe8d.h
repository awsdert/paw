#pragma once

PAW_C_API_OPEN

#define PAWE8D_SIZE   PAWINT_DIVCIEL(8,CHAR_BIT)
#define PAWE8D_WIDTH  8
#define PAWE8D_TYPE     signed _BitInt(PAWE8D_WIDTH)
#define PAWE8U_TYPE   unsigned _BitInt(PAWE8D_WIDTH)
#define PAWE8U_C(VAL) CAST(PAWE8U_TYPE,VAL)
#define PAWE8D_C(VAL) CAST(PAWE8D_TYPE,VAL)
#define PAWE8U_MAX    (~PAWE8U_C(0u))
#define PAWE8D_MIN    ((~PAWE8D_C(0))<<(PAWE8D_WIDTH-1))
#define PAWE8D_MAX    (~PAWE8D_MIN)
typedef PAWE8D_TYPE   pawe8d;
typedef PAWE8U_TYPE   pawe8u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE8U_CXX(VAL) CASTXX(PAWE8U_TYPE,VAL)
#define PAWE8D_CXX(VAL) CASTXX(PAWE8D_TYPE,VAL)
#define PAWE8U_XXMAX    (~PAWE8U_CXX(0u))
#define PAWE8D_XXMIN    ((~PAWE8D_CXX(0))<<(PAWE8D_WIDTH-1))
#define PAWE8D_XXMAX    (~PAWE8D_XXMIN)
#endif
