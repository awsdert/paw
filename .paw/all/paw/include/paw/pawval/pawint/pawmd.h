#pragma once

PAW_C_API_OPEN

/* Hopefully this will work */
#define PAWMD_SIZE   CAUGHT_FUNCPTR_SIZE
#define PAWMD_WIDTH  CAUGHT_FUNCPTR_WIDTH
#define PAWMD_TYPE     signed _BitInt(PAWMD_WIDTH)
#define PAWMU_TYPE   unsigned _BitInt(PAWMD_WIDTH)
#define PAWMU_C(VAL) CAST(PAWMU_TYPE,VAL)
#define PAWMD_C(VAL) CAST(PAWMD_TYPE,VAL)
#define PAWMU_MAX    (~PAWMU_C(0u))
#define PAWMD_MIN    ((~PAWMD_C(0))<<(PAWMD_WIDTH-1))
#define PAWMD_MAX    (~PAWMD_MIN)
typedef PAWMD_TYPE   pawmd;
typedef PAWMU_TYPE   pawmu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWMU_CXX(VAL) CASTXX(PAWMU_TYPE,VAL)
#define PAWMD_CXX(VAL) CASTXX(PAWMD_TYPE,VAL)
#define PAWMU_XXMAX    (~PAWMU_CXX(0u))
#define PAWMD_XXMIN    ((~PAWMD_CXX(0))<<(PAWMD_WIDTH-1))
#define PAWMD_XXMAX    (~PAWMD_XXMIN)
#endif
