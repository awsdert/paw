#pragma once

PAW_C_API_OPEN

#define PAWAGENS_SIZE   PAWINT_DIVCIEL(256,PAWCD_WIDTH)
#define PAWAGENS_WIDTH  256
#define PAWAGENS_TYPE     signed _BitInt(PAWAGENS_WIDTH)
#define PAWAGENS_C(VAL) CAST(PAWAGENS_TYPE,VAL)
#define PAWAGENS_MIN    ((~PAWAGENS_C(0))<<(PAWAGENS_WIDTH-1))
#define PAWAGENS_MAX    (~PAWAGENS_MIN)
typedef PAWAGENS_TYPE   pawageNS;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWAGENS_CXX(VAL) CASTXX(PAWAGENS_TYPE,VAL)
#define PAWAGENS_XXMIN    ((~PAWAGENS_CXX(0))<<(PAWAGENS_WIDTH-1))
#define PAWAGENS_XXMAX    (~PAWAGENS_XXMIN)
#endif
