#pragma once

PAW_C_API_OPEN

#define PAWAGE_SIZE   CAUGHT_REGISTER_SIZE
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWAGE_WIDTH  CAUGHT_REGISTER_WIDTH
#define PAWAGE_TYPE     signed _BitInt(PAWAGE_WIDTH)
#define PAWAGE_C(VAL) CAST(PAWAGE_TYPE,VAL)
#define PAWAGE_MIN    ((~PAWAGE_C(0))<<(PAWAGE_WIDTH-1))
#define PAWAGE_MAX    (~PAWAGE_MIN)
typedef PAWAGE_TYPE   pawage;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWAGE_CXX(VAL) CASTXX(PAWAGE_TYPE,VAL)
#define PAWAGE_XXMIN    ((~PAWAGE_CXX(0))<<(PAWAGE_WIDTH-1))
#define PAWAGE_XXMAX    (~PAWAGE_XXMIN)
#endif
