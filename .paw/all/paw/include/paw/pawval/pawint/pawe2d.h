#pragma once

PAW_C_API_OPEN

#define PAWE2D_SIZE   PAWINT_DIVCIEL(2,CHAR_BIT)
#define PAWE2D_WIDTH  2
#define PAWE2D_TYPE     signed _BitInt(PAWE2D_WIDTH)
#define PAWE2U_TYPE   unsigned _BitInt(PAWE2D_WIDTH)
#define PAWE2U_C(VAL) CAST(PAWE2U_TYPE,VAL)
#define PAWE2D_C(VAL) CAST(PAWE2D_TYPE,VAL)
#define PAWE2U_MAX    (~PAWE2U_C(0u))
#define PAWE2D_MIN    ((~PAWE2D_C(0))<<(PAWE2D_WIDTH-1))
#define PAWE2D_MAX    (~PAWE2D_MIN)
typedef PAWE2D_TYPE   pawe2d;
typedef PAWE2U_TYPE   pawe2u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE2U_CXX(VAL) CASTXX(PAWE2U_TYPE,VAL)
#define PAWE2D_CXX(VAL) CASTXX(PAWE2D_TYPE,VAL)
#define PAWE2U_XXMAX    (~PAWE2U_CXX(0u))
#define PAWE2D_XXMIN    ((~PAWE2D_CXX(0))<<(PAWE2D_WIDTH-1))
#define PAWE2D_XXMAX    (~PAWE2D_XXMIN)
#endif
