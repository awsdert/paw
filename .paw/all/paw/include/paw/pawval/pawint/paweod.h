#pragma once

PAW_C_API_OPEN

#define PAWEOD_SIZE   32
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWEOD_WIDTH  (CHAR_BIT*PAWEOD_SIZE)
#define PAWEOD_TYPE     signed _BitInt(PAWEOD_WIDTH)
#define PAWEOU_TYPE   unsigned _BitInt(PAWEOD_WIDTH)
#define PAWEOU_C(VAL) CAST(PAWEOU_TYPE,VAL)
#define PAWEOD_C(VAL) CAST(PAWEOD_TYPE,VAL)
#define PAWEOU_MAX    (~PAWEOU_C(0u))
#define PAWEOD_MIN    ((~PAWEOD_C(0))<<(PAWEOD_WIDTH-1))
#define PAWEOD_MAX    (~PAWEOD_MIN)
typedef PAWEOD_TYPE   paweod;
typedef PAWEOU_TYPE   paweou;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWEOU_CXX(VAL) CASTXX(PAWEOU_TYPE,VAL)
#define PAWEOD_CXX(VAL) CASTXX(PAWEOD_TYPE,VAL)
#define PAWEOU_XXMAX    (~PAWEOU_CXX(0u))
#define PAWEOD_XXMIN    ((~PAWEOD_CXX(0))<<(PAWEOD_WIDTH-1))
#define PAWEOD_XXMAX    (~PAWEOD_XXMIN)
#endif
