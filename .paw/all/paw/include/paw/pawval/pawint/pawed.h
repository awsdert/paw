#pragma once

PAW_C_API_OPEN

#define PAWED_SIZE   4
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWED_WIDTH  (CHAR_BIT*PAWED_SIZE)
#define PAWED_TYPE     signed _BitInt(PAWED_WIDTH)
#define PAWEU_TYPE   unsigned _BitInt(PAWED_WIDTH)
#define PAWEU_C(VAL) CAST(PAWEU_TYPE,VAL)
#define PAWED_C(VAL) CAST(PAWED_TYPE,VAL)
#define PAWEU_MAX    (~PAWEU_C(0u))
#define PAWED_MIN    ((~PAWED_C(0))<<(PAWED_WIDTH-1))
#define PAWED_MAX    (~PAWED_MIN)
typedef PAWED_TYPE   pawed;
typedef PAWEU_TYPE   paweu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWEU_CXX(VAL) CASTXX(PAWEU_TYPE,VAL)
#define PAWED_CXX(VAL) CASTXX(PAWED_TYPE,VAL)
#define PAWEU_XXMAX    (~PAWEU_CXX(0u))
#define PAWED_XXMIN    ((~PAWED_CXX(0))<<(PAWED_WIDTH-1))
#define PAWED_XXMAX    (~PAWED_XXMIN)
#endif
