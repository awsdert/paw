#pragma once

PAW_C_API_OPEN

#define PAWE256D_SIZE   PAWINT_DIVCIEL(256,CHAR_BIT)
#define PAWE256D_WIDTH  256
#define PAWE256D_TYPE     signed _BitInt(PAWE256D_WIDTH)
#define PAWE256U_TYPE   unsigned _BitInt(PAWE256D_WIDTH)
#define PAWE256U_C(VAL) CAST(PAWE256U_TYPE,VAL)
#define PAWE256D_C(VAL) CAST(PAWE256D_TYPE,VAL)
#define PAWE256U_MAX    (~PAWE256U_C(0u))
#define PAWE256D_MIN    ((~PAWE256D_C(0))<<(PAWE256D_WIDTH-1))
#define PAWE256D_MAX    (~PAWE256D_MIN)
typedef PAWE256D_TYPE   pawe256d;
typedef PAWE256U_TYPE   pawe256u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE256U_CXX(VAL) CASTXX(PAWE256U_TYPE,VAL)
#define PAWE256D_CXX(VAL) CASTXX(PAWE256D_TYPE,VAL)
#define PAWE256U_XXMAX    (~PAWE256U_CXX(0u))
#define PAWE256D_XXMIN    ((~PAWE256D_CXX(0))<<(PAWE256D_WIDTH-1))
#define PAWE256D_XXMAX    (~PAWE256D_XXMIN)
#endif
