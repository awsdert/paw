#pragma once

PAW_C_API_OPEN

#define PAWEHD_SIZE   2
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWEHD_WIDTH  (CHAR_BIT*PAWEHD_SIZE)
#define PAWEHD_TYPE     signed _BitInt(PAWEHD_WIDTH)
#define PAWEHU_TYPE   unsigned _BitInt(PAWEHD_WIDTH)
#define PAWEHU_C(VAL) CAST(PAWEHU_TYPE,VAL)
#define PAWEHD_C(VAL) CAST(PAWEHD_TYPE,VAL)
#define PAWEHU_MAX    (~PAWEHU_C(0u))
#define PAWEHD_MIN    ((~PAWEHD_C(0))<<(PAWEHD_WIDTH-1))
#define PAWEHD_MAX    (~PAWEHD_MIN)
typedef PAWEHD_TYPE   pawehd;
typedef PAWEHU_TYPE   pawehu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWEHU_CXX(VAL) CASTXX(PAWEHU_TYPE,VAL)
#define PAWEHD_CXX(VAL) CASTXX(PAWEHD_TYPE,VAL)
#define PAWEHU_XXMAX    (~PAWEHU_CXX(0u))
#define PAWEHD_XXMIN    ((~PAWEHD_CXX(0))<<(PAWEHD_WIDTH-1))
#define PAWEHD_XXMAX    (~PAWEHD_XXMIN)
#endif
