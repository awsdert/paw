#pragma once

PAW_C_API_OPEN

#define PAWE64D_SIZE   PAWINT_DIVCIEL(64,CHAR_BIT)
#define PAWE64D_WIDTH  64
#define PAWE64D_TYPE     signed _BitInt(PAWE64D_WIDTH)
#define PAWE64U_TYPE   unsigned _BitInt(PAWE64D_WIDTH)
#define PAWE64U_C(VAL) CAST(PAWE64U_TYPE,VAL)
#define PAWE64D_C(VAL) CAST(PAWE64D_TYPE,VAL)
#define PAWE64U_MAX    (~PAWE64U_C(0u))
#define PAWE64D_MIN    ((~PAWE64D_C(0))<<(PAWE64D_WIDTH-1))
#define PAWE64D_MAX    (~PAWE64D_MIN)
typedef PAWE64D_TYPE   pawe64d;
typedef PAWE64U_TYPE   pawe64u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE64U_CXX(VAL) CASTXX(PAWE64U_TYPE,VAL)
#define PAWE64D_CXX(VAL) CASTXX(PAWE64D_TYPE,VAL)
#define PAWE64U_XXMAX    (~PAWE64U_CXX(0u))
#define PAWE64D_XXMIN    ((~PAWE64D_CXX(0))<<(PAWE64D_WIDTH-1))
#define PAWE64D_XXMAX    (~PAWE64D_XXMIN)
#endif
