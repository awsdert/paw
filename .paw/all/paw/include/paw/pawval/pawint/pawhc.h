#pragma once

PAW_C_API_OPEN

#define PAWHC_SIZE   CAUGHT_CHAR16_SIZE
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWHC_WIDTH  CAUGHT_CHAR16_WIDTH
#define PAWHC_TYPE   CAUGHT_CHAR16_TYPE
#define PAWHC_C(VAL) u##VAL
#define PAWHC_MIN    CAUGHT_CHAR16_MIN
#define PAWHC_MAX    CAUGHT_CHAR16_MAX
typedef PAWHC_TYPE   pawhc;
typedef pawhc const *pawhs;

#if PAWHC_MIN < 0
#	define PAWHC_SIGNED 1
#else
#	define PAWHC_UNSIGNED 1
#endif

#define PAWHC_MAX_ENCODED_WIDTH 21
#define PAWHC_MAX_ENCODED_BYTES 4
#define PAWHC_MAX_ENCODED_CHARS 2

#define ___PAWHS_C(LIT) u##LIT
#define __PAWHS_C(LIT) ___PAWHS_C(LIT)
#define _PAWHS_C(LIT) __PAWHS_C(LIT)
#define PAWHS_FILE _PAWHS_C(PAWS_FILE)
#define PAWHS_ERRSRC PAWHS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWHC_CXX(VAL) CASTXX(PAWHC_TYPE,VAL)
#define PAWHC_XXMIN    CAUGHT_CHAR16_MIN
#define PAWHC_XXMAX    CAUGHT_CHAR16_MAX
#endif
