#pragma once

PAW_C_API_OPEN

#define PAWEXD_SIZE   16
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWEXD_WIDTH  (CHAR_BIT*PAWEXD_SIZE)
#define PAWEXD_TYPE     signed _BitInt(PAWEXD_WIDTH)
#define PAWEXU_TYPE   unsigned _BitInt(PAWEXD_WIDTH)
#define PAWEXU_C(VAL) CAST(PAWEXU_TYPE,VAL)
#define PAWEXD_C(VAL) CAST(PAWEXD_TYPE,VAL)
#define PAWEXU_MAX    (~PAWEXU_C(0u))
#define PAWEXD_MIN    ((~PAWEXD_C(0))<<(PAWEXD_WIDTH-1))
#define PAWEXD_MAX    (~PAWEXD_MIN)
typedef PAWEXD_TYPE   pawexd;
typedef PAWEXU_TYPE   pawexu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWEXU_CXX(VAL) CASTXX(PAWEXU_TYPE,VAL)
#define PAWEXD_CXX(VAL) CASTXX(PAWEXD_TYPE,VAL)
#define PAWEXU_XXMAX    (~PAWEXU_CXX(0u))
#define PAWEXD_XXMIN    ((~PAWEXD_CXX(0))<<(PAWEXD_WIDTH-1))
#define PAWEXD_XXMAX    (~PAWEXD_XXMIN)
#endif
