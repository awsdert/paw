#pragma once

PAW_C_API_OPEN

#define PAWELD_SIZE   8
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWELD_WIDTH  (CHAR_BIT*PAWELD_SIZE)
#define PAWELD_TYPE     signed _BitInt(PAWELD_WIDTH)
#define PAWELU_TYPE   unsigned _BitInt(PAWELD_WIDTH)
#define PAWELU_C(VAL) CAST(PAWELU_TYPE,VAL)
#define PAWELD_C(VAL) CAST(PAWELD_TYPE,VAL)
#define PAWELU_MAX    (~PAWELU_C(0u))
#define PAWELD_MIN    ((~PAWELD_C(0))<<(PAWELD_WIDTH-1))
#define PAWELD_MAX    (~PAWELD_MIN)
typedef PAWELD_TYPE   paweld;
typedef PAWELU_TYPE   pawelu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWELU_CXX(VAL) CASTXX(PAWELU_TYPE,VAL)
#define PAWELD_CXX(VAL) CASTXX(PAWELD_TYPE,VAL)
#define PAWELU_XXMAX    (~PAWELU_CXX(0u))
#define PAWELD_XXMIN    ((~PAWELD_CXX(0))<<(PAWELD_WIDTH-1))
#define PAWELD_XXMAX    (~PAWELD_XXMIN)
#endif
