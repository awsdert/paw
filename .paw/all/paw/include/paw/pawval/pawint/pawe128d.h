#pragma once

PAW_C_API_OPEN

#define PAWE128D_SIZE   PAWINT_DIVCIEL(128,CHAR_BIT)
#define PAWE128D_WIDTH  128
#define PAWE128D_TYPE     signed _BitInt(PAWE128D_WIDTH)
#define PAWE128U_TYPE   unsigned _BitInt(PAWE128D_WIDTH)
#define PAWE128U_C(VAL) CAST(PAWE128U_TYPE,VAL)
#define PAWE128D_C(VAL) CAST(PAWE128D_TYPE,VAL)
#define PAWE128U_MAX    (~PAWE128U_C(0u))
#define PAWE128D_MIN    ((~PAWE128D_C(0))<<(PAWE128D_WIDTH-1))
#define PAWE128D_MAX    (~PAWE128D_MIN)
typedef PAWE128D_TYPE   pawe128d;
typedef PAWE128U_TYPE   pawe128u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE128U_CXX(VAL) CASTXX(PAWE128U_TYPE,VAL)
#define PAWE128D_CXX(VAL) CASTXX(PAWE128D_TYPE,VAL)
#define PAWE128U_XXMAX    (~PAWE128U_CXX(0u))
#define PAWE128D_XXMIN    ((~PAWE128D_CXX(0))<<(PAWE128D_WIDTH-1))
#define PAWE128D_XXMAX    (~PAWE128D_XXMIN)
#endif
