#pragma once

PAW_C_API_OPEN

#define PAWE16D_SIZE   PAWINT_DIVCIEL(16,CHAR_BIT)
#define PAWE16D_WIDTH  16
#define PAWE16D_TYPE     signed _BitInt(PAWE16D_WIDTH)
#define PAWE16U_TYPE   unsigned _BitInt(PAWE16D_WIDTH)
#define PAWE16U_C(VAL) CAST(PAWE16U_TYPE,VAL)
#define PAWE16D_C(VAL) CAST(PAWE16D_TYPE,VAL)
#define PAWE16U_MAX    (~PAWE16U_C(0u))
#define PAWE16D_MIN    ((~PAWE16D_C(0))<<(PAWE16D_WIDTH-1))
#define PAWE16D_MAX    (~PAWE16D_MIN)
typedef PAWE16D_TYPE   pawe16d;
typedef PAWE16U_TYPE   pawe16u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE16U_CXX(VAL) CASTXX(PAWE16U_TYPE,VAL)
#define PAWE16D_CXX(VAL) CASTXX(PAWE16D_TYPE,VAL)
#define PAWE16U_XXMAX    (~PAWE16U_CXX(0u))
#define PAWE16D_XXMIN    ((~PAWE16D_CXX(0))<<(PAWE16D_WIDTH-1))
#define PAWE16D_XXMAX    (~PAWE16D_XXMIN)
#endif
