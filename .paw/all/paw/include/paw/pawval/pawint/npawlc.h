#pragma once

#ifdef __GNUC_WIDE_EXECUTION_CHARSET_NAME
#	define NPAWLS_CHARSET __GNUC_WIDE_EXECUTION_CHARSET_NAME
#elifdef _WIN32
#	define NPAWLS_CHARSET "UTF-16LE"
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#	define NPAWLS_CHARSET "UTF-32BE"
#else
#	define NPAWLS_CHARSET "UTF-32LE"
#endif

PAW_C_API_OPEN

#define NPAWLC_SIZE   __SIZEOF_WCHAR_T__
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define NPAWLC_WIDTH  __WCHAR_WIDTH__
#define NPAWLC_TYPE   __WCHAR_TYPE__
#define NPAWLC_C(VAL) CAST(NPAWLC_TYPE,VAL)
#define NPAWLC_MIN    __WCHAR_MIN__
#define NPAWLC_MAX    __WCHAR_MAX__
typedef NPAWLC_TYPE   npawlc;
typedef npawlc const *npawls;

#define ___NPAWLS_C(LIT) L##LIT
#define __NPAWLS_C(LIT) ___NPAWLS_C(LIT)
#define _NPAWLS_C(LIT) __NPAWLS_C(LIT)
#define NPAWLS_FILE _NPAWLS_C(PAWS_FILE)
#define NPAWLS_ERRSRC NPAWLS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#	define NPAWLC_CXX(VAL) CASTXX(NPAWLC_TYPE,VAL)
#	define NPAWLC_XXMIN    WCHAR_MIN
#	define NPAWLC_XXMAX    WCHAR_MAX
#endif
