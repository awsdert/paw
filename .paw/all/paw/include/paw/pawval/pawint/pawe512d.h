#pragma once

PAW_C_API_OPEN

#define PAWE512D_SIZE   PAWINT_DIVCIEL(512,CHAR_BIT)
#define PAWE512D_WIDTH  512
#define PAWE512D_TYPE     signed _BitInt(PAWE512D_WIDTH)
#define PAWE512U_TYPE   unsigned _BitInt(PAWE512D_WIDTH)
#define PAWE512U_C(VAL) CAST(PAWE512U_TYPE,VAL)
#define PAWE512D_C(VAL) CAST(PAWE512D_TYPE,VAL)
#define PAWE512U_MAX    (~PAWE512U_C(0u))
#define PAWE512D_MIN    ((~PAWE512D_C(0))<<(PAWE512D_WIDTH-1))
#define PAWE512D_MAX    (~PAWE512D_MIN)
typedef PAWE512D_TYPE   pawe512d;
typedef PAWE512U_TYPE   pawe512u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE512U_CXX(VAL) CASTXX(PAWE512U_TYPE,VAL)
#define PAWE512D_CXX(VAL) CASTXX(PAWE512D_TYPE,VAL)
#define PAWE512U_XXMAX    (~PAWE512U_CXX(0u))
#define PAWE512D_XXMIN    ((~PAWE512D_CXX(0))<<(PAWE512D_WIDTH-1))
#define PAWE512D_XXMAX    (~PAWE512D_XXMIN)
#endif
