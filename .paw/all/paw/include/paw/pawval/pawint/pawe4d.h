#pragma once

PAW_C_API_OPEN

#define PAWE4D_SIZE   PAWINT_DIVCIEL(4,CHAR_BIT)
#define PAWE4D_WIDTH  4
#define PAWE4D_TYPE     signed _BitInt(PAWE4D_WIDTH)
#define PAWE4U_TYPE   unsigned _BitInt(PAWE4D_WIDTH)
#define PAWE4U_C(VAL) CAST(PAWE4U_TYPE,VAL)
#define PAWE4D_C(VAL) CAST(PAWE4D_TYPE,VAL)
#define PAWE4U_MAX    (~PAWE4U_C(0u))
#define PAWE4D_MIN    ((~PAWE4D_C(0))<<(PAWE4D_WIDTH-1))
#define PAWE4D_MAX    (~PAWE4D_MIN)
typedef PAWE4D_TYPE   pawe4d;
typedef PAWE4U_TYPE   pawe4u;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWE4U_CXX(VAL) CASTXX(PAWE4U_TYPE,VAL)
#define PAWE4D_CXX(VAL) CASTXX(PAWE4D_TYPE,VAL)
#define PAWE4U_XXMAX    (~PAWE4U_CXX(0u))
#define PAWE4D_XXMIN    ((~PAWE4D_CXX(0))<<(PAWE4D_WIDTH-1))
#define PAWE4D_XXMAX    (~PAWE4D_XXMIN)
#endif
