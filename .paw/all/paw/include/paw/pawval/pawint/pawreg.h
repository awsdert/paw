#pragma once

PAW_C_API_OPEN
/* We'll abuse the union type to catch all register types without using the
 * mangled naming schemes defined for the registers */
typedef union
{
	pawru full;
/* These help us ignore the whole rax vs eax vs ax stuff */
#if PAWRD_WIDTH >= 32
	pawru r32 : 32;
#endif
#if PAWRD_WIDTH >= 16
	pawru r16 : 16;
#endif
#if PAWRD_WIDTH >= 8
	pawru  r8 : 8;
#endif
} pawreg;

typedef struct
{
	/* Protected Mode Registers */
	pawreg gdtr;
	pawreg ldtr;
	pawreg tr;
	pawreg idtr;
	/* Code Segment */
	pawreg cs;
	/* Data Segment */
	pawreg ds;
	/* Extra Segment */
	pawreg es;
	/* Stack Segment */
	pawreg ss;
	/* General Purpose F Segment */
	pawreg fs;
	/* General Purpose G Segment */
	pawreg gs;
#ifdef __X86_64__
	pawreg fs_base;
	pawreg gs_base;
#endif
} pawregobj_code;

typedef struct
{
	/* Program Counter */
	pawreg pc;
	/* Instruction Pointer */
	pawreg ip;
	pawreg flags;
	pawreg oldax;
} pawregobj_exec;

typedef struct
{
	/* Accumulator */
	pawreg ax;
	/* Base */
	pawreg bx;
	/* Counter */
	pawreg cx;
	/* Data */
	pawreg dx;
	/* Source */
	pawreg si;
	/* Destination */
	pawreg di;
	/* Stack Pointer */
	pawreg sp;
	/* Stack Base Pointer */
	pawreg bp;
#if PAWRD_WIDTH >= 64
/* From here there's only 1 naming scheme as far as I'm aware */
	pawreg r8;
	pawreg r9;
	pawreg r10;
	pawreg r11;
	pawreg r12;
	pawreg r13;
	pawreg r14;
	pawreg r15;
#endif
} pawregobj_misc;

#ifdef __X86_64__
typedef unsigned _BitInt(128) pawreg_fpx;
typedef unsigned _BitInt(256) pawreg_xmm;
#define PAWREG_FPN_COUNT 16
#define PAWREG_XMM_COUNT 32
typedef struct
{
	pawehu	cwd;
	pawehu	swd;
	pawehu	ftw;
	pawehu	fop;
	pawreg	ip;
	pawreg	dp;
	pawru	mxcsr;
	pawru	mxcr_mask;
	pawreg_fpn	st_space[PAWREG_FPX_COUNT];   /* 8*16 bytes for each FP-reg = 128 bytes */
	pawreg_xmm	xmm_space[PAWREG_XMM_COUNT];  /* 16*16 bytes for each XMM-reg = 256 bytes */
	pawru	padding[24];
} pawref_fpx;
#elifdef __X86__
typedef unsigned _BitInt(128) pawreg_fpn;
typedef unsigned _BitInt(128) pawreg_fpx;
typedef unsigned _BitInt(128) pawreg_xmm;
#define PAWREG_FPN_COUNT 10
#define PAWREG_FPX_COUNT 16
#define PAWREG_XMM_COUNT 16
typedef struct
{
	pawru cwd;
	pawru swd;
	pawru twd;
	pawru fip;
	pawru fcs;
	pawru foo;
	pawru fos;
	pawreg_fpn st_space[PAWREG_FPN_COUNT];
} pawregobj_fpn;
typedef struct
{
	pawehu cwd;
	pawehu swd;
	pawehu twd;
	pawehu fop;
	pawru fip;
	pawru fcs;
	pawru foo;
	pawru fos;
	pawru mxcsr;
	pawru reserved;
	pawreg_fpx st_space[PAWREG_FPX_COUNT];
	pawreg_xmm xmm_space[PAWREG_XMM_COUNT];
	pawru padding[56];
} pawregobj_fpx;
#endif

typedef struct
{
	pawregobj_code code;
	pawregobj_exec exec;
	pawregobj_misc misc;
	/* Control Registers - has reserved members that will be ignored,
	 * refer to cpu docs to identify the reserved ones. */
	pawreg cr[CAUGHT_REGISTER_COUNT];
	/* Debug Registers */
	pawreg dr[CAUGHT_REGISTER_COUNT];
	/* Test Registers - has 3 implied registers, unknown as yet if they're
	 * accessible */
	pawreg tr[CAUGHT_REGISTER_COUNT];
#if PAWREG_FPX_COUNT
	pawregobj_fpx fpx;
#endif
#if PAWREG_FPN_COUNT
	pawregobj_fpn fpn;
#endif
} pawregobj_dbg;

/* libpaw will do the work of passing from/into native structures so this
 * public object should be all that's needed for gathering debug info, some
 * are defined as triggering an error if an attempt to access them is made so
 * they're ignored on those architectures */
typedef union
{
	pawreg to0initwith;
	pawreg all[ sizeof(pawregobj_dbg) / sizeof(pawreg) ];
	pawregobj_dbg obj;
} pawdbg;

PAW_C_API_SHUT
