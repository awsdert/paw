#pragma once

PAW_C_API_OPEN
#define NPAWTC_SIZE  CAUGHT_TCHAR_SIZE
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define NPAWTC_WIDTH  CAUGHT_TCHAR_WIDTH
#define NPAWTC_TYPE   CAUGHT_TCHAR_TYPE
#define NPAWTC_C(VAL) CAST(NPAWTC_TYPE,VAL)
#define NPAWTC_MIN    CAUGHT_TCHAR_MIN
#define NPAWTC_MAX    CAUGHT_TCHAR_MAX
typedef NPAWTC_TYPE      npawtc;
typedef npawtc const    *npawts;

#if NPAWTC_SIZE > 1
#	define ___NPAWTS_C(LIT) L##LIT
#else
#	define ___NPAWTS_C(LIT) LIT
#endif
#define __NPAWTS_C(LIT) ___NPAWTS_C(LIT)
#define _NPAWTS_C(LIT) __NPAWTS_C(LIT)
#define NPAWTS_FILE _NPAWTS_C(PAWS_FILE)
#define NPAWTS_ERRSRC NPAWTS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#	define NPAWTC_CXX(VAL) CASTXX(NPAWTC_TYPE,VAL)
#	define NPAWTC_XXMIN    NPAWTC_MIN
#	define NPAWTC_XXMAX    NPAWTC_MAX
#endif
