#pragma once

PAW_C_API_OPEN

#define PAWLLC_SIZE   CAUGHT_CHAR32_SIZE
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWLLC_WIDTH  CAUGHT_CHAR32_WIDTH
#define PAWLLC_TYPE   CAUGHT_CHAR32_TYPE
#define PAWLLC_C(VAL) U##VAL
#define PAWLLC_MIN    CAUGHT_CHAR32_MIN
#define PAWLLC_MAX    CAUGHT_CHAR32_MAX
typedef PAWLLC_TYPE   pawllc;
typedef pawllc const *pawlls;
typedef signed _BitInt(PAWLLC_WIDTH+1) pawesc;

#if PAWLLC_MIN < 0
#	define PAWLLC_SIGNED 1
#else
#	define PAWLLC_UNSIGNED 1
#endif

#define ___PAWLLS_C(LIT) U##LIT
#define __PAWLLS_C(LIT) ___PAWLLS_C(LIT)
#define _PAWLLS_C(LIT) __PAWLLS_C(LIT)
#define PAWLLS_FILE _PAWLLS_C(PAWS_FILE)
#define PAWLLS_ERRSRC PAWLLS_FILE, __LINE__

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWLLC_CXX(VAL) CASTXX(PAWLLC_TYPE,VAL)
#define PAWLLC_XXMIN    CAUGHT_CHAR32_MIN
#define PAWLLC_XXMAX    CAUGHT_CHAR32_MAX
#endif
