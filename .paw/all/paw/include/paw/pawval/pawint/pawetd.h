#pragma once

PAW_C_API_OPEN

#define PAWETD_SIZE   16
/* The docs say not to use this but I deem it highly unlikely this predefine
 * will ever be removed and thus is probably safe */
#define PAWETD_WIDTH  (CHAR_BIT*PAWETD_SIZE)
#define PAWETD_TYPE     signed _BitInt(PAWETD_WIDTH)
#define PAWETU_TYPE   unsigned _BitInt(PAWETD_WIDTH)
#define PAWETU_C(VAL) CAST(PAWETU_TYPE,VAL)
#define PAWETD_C(VAL) CAST(PAWETD_TYPE,VAL)
#define PAWETU_MAX    (~PAWETU_C(0u))
#define PAWETD_MIN    ((~PAWETD_C(0))<<(PAWETD_WIDTH-1))
#define PAWETD_MAX    (~PAWETD_MIN)
typedef PAWETD_TYPE   pawetd;
typedef PAWETU_TYPE   pawetu;

PAW_C_API_SHUT

/* Since C++ refuses to use common sense we have to make a work around for
 * invalid errors */
#ifdef __cplusplus
#define PAWETU_CXX(VAL) CASTXX(PAWETU_TYPE,VAL)
#define PAWETD_CXX(VAL) CASTXX(PAWETD_TYPE,VAL)
#define PAWETU_XXMAX    (~PAWETU_CXX(0u))
#define PAWETD_XXMIN    ((~PAWETD_CXX(0))<<(PAWETD_WIDTH-1))
#define PAWETD_XXMAX    (~PAWETD_XXMIN)
#endif
