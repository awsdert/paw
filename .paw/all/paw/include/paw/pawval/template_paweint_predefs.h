#include "pawalltypes.h"
#ifndef MIN_WIDTH
#	error "template_paweint_predefs.h requires MIN_WIDTH (power of 2) to be set"
#	include <abort_compilation>
#endif
#if (MIN_WIDTH & (MIN_WIDTH-1)) && (MIN_WIDTH == 1 || MIN_WIDTH < 0)
#	error "MIN_WIDTH must be a power of 2 and greater than 1"
#	include <abort_compilation>
#endif
#ifndef _EINTSEGD_MAX
#	define __EINTSEGU_MAX(W) (~((unsigned _BitInt(W))0U))
#	define __EINTSEGD_MIN(W) (((_BitInt(W))1) << (W-1))
#	define __EINTSEGD_MAX(W) (~_EINTSEGD_MIN(W))
#	define _EINTSEGU_MAX(W) __EINTSEGU_MAX(W)
#	define _EINTSEGD_MIN(W) __EINTSEGD_MIN(W)
#	define _EINTSEGD_MAX(W) __EINTSEGD_MAX(W)
#endif
#define EINTSEGU_CMP pawintu_cmp
#define EINTSEGD_CMP pawintd_cmp
#if PAWCD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawcd
#	define EINTSEGU pawcu
#	define EINTSEGU_MAX PAWCU_MAX
#	define EINTSEGD_MAX PAWCD_MAX
#	define EINTSEGD_MIN	PAWCD_MIN
#	define EINTSEGW PAWCD_WIDTH
#elif PAWHD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawhd
#	define EINTSEGU pawhu
#	define EINTSEGU_MAX PAWHU_MAX
#	define EINTSEGD_MAX PAWHD_MAX
#	define EINTSEGD_MIN	PAWHD_MIN
#	define EINTSEGW PAWHD_WIDTH
#elif PAWD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawd
#	define EINTSEGU pawu
#	define EINTSEGU_MAX PAWU_MAX
#	define EINTSEGD_MAX PAWD_MAX
#	define EINTSEGD_MIN	PAWD_MIN
#	define EINTSEGW PAWD_WIDTH
#elif PAWLD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawld
#	define EINTSEGU pawlu
#	define EINTSEGU_MAX PAWLU_MAX
#	define EINTSEGD_MAX PAWLD_MAX
#	define EINTSEGD_MIN	PAWLD_MIN
#	define EINTSEGW PAWLD_WIDTH
#elif PAWLLD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawlld
#	define EINTSEGU pawllu
#	define EINTSEGU_MAX PAWLLU_MAX
#	define EINTSEGD_MAX PAWLLD_MAX
#	define EINTSEGD_MIN	PAWLLD_MIN
#	define EINTSEGW PAWLLD_WIDTH
#elif PAWRD_WIDTH == MIN_WIDTH
#	define EINTSEGD pawrd
#	define EINTSEGU pawru
#	define EINTSEGU_MAX PAWRU_MAX
#	define EINTSEGD_MAX PAWRD_MAX
#	define EINTSEGD_MIN	PAWRD_MIN
#	define EINTSEGW PAWRD_WIDTH
#elif PAWINT_CAPBITS >= MIN_WIDTH
#	define EINTSEGD   signed _BitInt(MIN_WIDTH)
#	define EINTSEGU unsigned _BitInt(MIN_WIDTH)
#	define EINTSEGU_MAX _EINTSEGU_MAX(MIN_WIDTH)
#	define EINTSEGD_MIN _EINTSEGD_MIN(MIN_WIDTH)
#	define EINTSEGD_MAX _EINTSEGD_MAX(MIN_WIDTH)
#	define EINTSEGW MIN_WIDTH
#	define EINTSEG_IS_BITINT
#else
#	define EINTSEGD pawrd
#	define EINTSEGU pawru
#	define EINTSEGU_MAX PAWRU_MAX
#	define EINTSEGD_MAX PAWRD_MAX
#	define EINTSEGD_MIN	PAWRD_MIN
#	define EINTSEGW PAWRD_WIDTH
#	 undef EINTSEGU_CMP
#	 undef EINTSEGD_CMP
#	define EINTSEGU_CMP pawintru_cmp
#	define EINTSEGD_CMP pawintrd_cmp
#endif
