#ifndef POS_T
#	error "POS_T must be defined as position type!"
#	include <abort_compilation>
#endif

#ifndef UPOS_T
#	error "UPOS_T must be defined as position type!"
#	include <abort_compilation>
#endif

#ifndef NAME
#	error "NAME must be defined in NAME(PFX,SFX) PFX##name##SFX sty;e!"
#	include <abort_compilation>
#endif

#define IO_T	NAME(_,io,)
#define ID_T	NAME(_,id,)
#define REF_T	NAME(_,ref,)
#define REF_GETABS	NAME(_,ref,_getabs)
#define REF_GETRAW	NAME(_,ref,_getraw)
#define REF_PUTABS	NAME(_,ref,_putabs)
#define REF_PUTRAW	NAME(_,ref,_putraw)
#define REF_DUPABS	NAME(_,ref,_dupabs)
#define REF_DUPRAW	NAME(_,ref,_dupraw)
#define REF_DUPABS2	NAME(_,ref,_dupabs2)
#define REF_DUPRAW2	NAME(_,ref,_dupraw2)
#define REF_GETEND	NAME(_,ref,_getend)
#define REF_MOVPOS	NAME(_,ref,_movpos)

typedef struct { mpawio io; POS_T abs; } IO_T;
typedef struct { mpawid id; POS_T abs; } ID_T;
typedef struct { mpawref *ref; POS_T abs; } REF_T;

PAW_FQK_CB pawed REF_GETABS( pawmbs file, pawru line, mpawref *ref, POS_T *abs, void *dest, pawru size )
{
	return (*abs < 0)
		? PAWMSGID_INVALIDPOS : pawrefcall( file, line, ref, &abs, sizeof(*abs), dest, size );
}
PAW_FQK_CB pawed REF_GETRAW( pawmbs file, pawru line, REF_T *ref, void *dest, pawru size )
	{ return REF_GETABS( file, line, ref->ref, &(ref->abs), dest, size ); }
PAW_FQK_CB pawed REF_PUTABS( pawmbs file, pawru line, mpawref *ref, POS_T *abs, pawp data, pawru size )
{
	return (*abs < 0)
		? PAWMSGID_INVALIDPOS : pawrefcall( file, line, ref, &abs, sizeof(*abs), data, size );
}
PAW_FQK_CB pawed REF_PUTRAW( pawmbs file, pawru line, REF_T *ref, pawp data, pawru size )
	{ return REF_PUTABS( file, line, ref->ref, &(ref->abs), data, size ); }

/** @brief Duplicate data/buffer/etc to a target reference instead */
PAW_FQK_CB pawed REF_DUPABS( pawmbs file, pawru line, mpawref *ref, POS_T *abs, POS_T dup )
{
	return (*abs < 0 || dup < 1)
		? PAWMSGID_INVALIDPOS : pawrefcall( file, line, ref, &abs, sizeof(*abs), NULL, NULL, dup );
}
PAW_FQK_CB pawed REF_DUPRAW( pawmbs file, pawru line, REF_T *ref, POS_T dup )
	{ return REF_DUPABS( file, line, ref->ref, &(ref->abs), dup ); }
PAW_FQK_CB pawed REF_DUPABS2
	( pawmbs file, pawru line, mpawref *ref, POS_T *abs, mpawref *dst, POS_T *pos, POS_T dup )
{
	return (*abs < 0 || *pos < 0 || dup < 1)
		? PAWMSGID_INVALIDPOS : pawrefcall( file, line, ref, &abs, sizeof(*abs), dst, pos, dup );
}
PAW_FQK_CB pawed REF_DUPRAW2( pawmbs file, pawru line, REF_T *ref, REF_T *dst, POS_T dup )
	{ return REF_DUPABS2( file, line, ref->ref, &(ref->abs), dst->ref, &(dst->abs), dup ); }

PAW_FQK_CB pawed REF_GETEND( pawmbs file, pawru line, mpawref *ref, UPOS_T *end )
	{ return pawrefcall( file, line, ref, PAWCALL_GETEND, &end, sizeof(*end) ); }
PAW_QCK_CB POS_T REF_MOVPOS( pawmbs file, pawru line, REF_T *ref, POS_T mov, PAWSTART start )
{
	UPOS_T end = 0;
	if ( REF_GETEND( file, line, ref->ref, &end ) != 0 )
		return -1;
	switch ( start )
	{
	default: return -1;
	case PAWSTART_OVERIDE: break;
	case PAWSTART_CURRENT: mov += ref->abs; break;
	case PAWSTART_FROMEND: mov += end; break;
	}
	if ( mov < 0 || mov >= end )
		return -1;
	ref->abs = mov;
	return mov;
}
