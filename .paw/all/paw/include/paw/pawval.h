#pragma once
#include "pawval/pawalltypes.h"
#include "pawval/pawraw.h"
#include "pawval/pawage.h"
#include "pawval/pawtie.h"
#include "pawval/pawref.h"
#include "pawval/pawcrt.h"
#include "pawval/pawmbs.h"
#include "pawval/pawhs.h"
#include "pawval/pawlls.h"
#if NPAW_BUILD
#include "pawval/npawls.h"
#include "pawval/npawts.h"
#endif
