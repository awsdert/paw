#pragma once
#include "pawenv.h"

typedef struct
{
	pawd leng, full;
	pawmbc fill;
	pawmbs name;
} pawfs_env;

/** @brief Identify name portion of string from current position (if it is
 * supposed to be)
 * @note refer to pawfs_env_cmdsh & pawfs_env_shell for example usage */
typedef pawfs_env (*pawfs_env_cb)( pawmbs str, pawd len );

/** @brief %VAR% */
PAW_QCK_CB pawfs_env pawfs_env_cmdsh( pawmbs txt, pawd len )
{
	pawd i = 0, end = len - 1;
	pawfs_env env = {0};
	if ( *txt != '%' )
		return env;
	if ( txt[1] == '%' )
	{
		env.fill = '%';
		env.full = 2;
		return env;
	}
	env.name = txt + 1;
	while ( ++i < end && txt[i] != '%' );
	env.leng = i - 1;
	env.full = i + (pawd)(env.name[i] == '%');
	return env;
}
/** @brief $V $(VAR) */
PAW_QCK_CB pawfs_env pawfs_env_shell( pawmbs txt, pawd len )
{
	pawd i = 0, end = len - 2;
	pawfs_env env = {0};
	if ( *txt != '$' )
		return env;
	if ( txt[1] != '(' )
	{
		env.full = 2;
		if ( txt[1] == '$' )
		{
			env.fill = '$';
			return env;
		}
		env.name = txt + 1;
		env.leng = 1;
		return env;
	}
	if ( txt[2] == ')' )
	{
		env.fill = ' ';
		env.full = 3;
		return env;
	}
	env.name = txt + 2;
	while ( ++i < end && env.name[i] != ')' );
	env.leng = i;
	env.full = 2 + i + (pawd)(env.name[i] == ')');
	return env;
}
/** @brief pawfs_env_cmdsh_ptr OR pawfs_env_shell_ptr */
extern pawfs_env_cb pawfs_env_locshCB;
PAW_QCK_CB pawfs_env pawfs_env_anysh( pawmbs txt, pawd len )
{
	pawfs_env env = pawfs_env_cmdsh( txt, len );
	return env.full ? env : pawfs_env_shell( txt, len );
}

/** @brief Convert a path containing environment variables to a normal path
 * @note Does not replace local (./, ../, etc) to absolute variants etc
 * @return 0 on success, -1 on failed variable, max needed if not enough space */
PAW_ANT_CB pawd pawfs__envpathn
	( pawmbc *dst, pawd max, pawmbs src, pawd len, pawfs_env_cb cb );
PAW_FQK_CB pawd pawfs__envpath
	( pawmbc *dst, pawd max, pawmbs src, pawfs_env_cb cb )
	{ return pawfs__envpathn( dst, max, src, pawmbslen(src), cb ); }
PAW_FQK_CB pawd pawfs_envpathn( pawmbc *dst, pawd max, pawmbs src, pawd len )
	{ return pawfs__envpathn( dst, max, src, len, pawfs_env_locshCB ); }
PAW_FQK_CB pawd pawfs_envpath( pawmbc *dst, pawd max, pawmbs src )
	{ return pawfs_envpathn( dst, max, src, pawmbslen(src), pawfs_env_locshCB ); }

/** @brief Convert a local path to an absolute path
 * @note Does not replace the path to symbolic links or environment variables */
PAW_ANT_CB pawd pawfs_abspathn( pawmbc *dst, pawd max, pawmbs src, pawd len );
PAW_FQK_CB pawd pawfs_abspath( pawmbc *dst, pawd max, pawmbs src )
	{ return pawfs_abspathn( dst, max, src, pawmbslen(src) ); }

/** @brief Convert a pawfs://path to /path, C:\path, etc, if it exists
 * @note Will not replace environment variables
 * @note Will convert to absolute path
 * @note WILL replace paths to symbolic links with the real path returned by
 * them */
PAW_ANT_CB pawd pawfs_syspathn( pawmbc *dst, pawd max, pawmbs src, pawd len );
PAW_FQK_CB pawd pawfs_syspath( pawmbc *dst, pawd max, pawmbs src )
	{ return pawfs_syspathn( dst, max, src, pawmbslen(src) ); }
