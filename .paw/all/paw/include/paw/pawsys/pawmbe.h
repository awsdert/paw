#pragma once
#include "../pawval.h"

#define PAWMBE_CHAR 	u8""
#define PAWMBE_WIDE 	u8"WCHAR_T"
#define PAWMBE_UTF8 	u8"UTF-8"
#define PAWMBE_UTF16	u8"UTF-16"
#define PAWMBE_UTF16LE	u8"UTF-16LE"
#define PAWMBE_UTF16BE	u8"UTF-16BE"
#define PAWMBE_UTF32	u8"UTF-32"
#define PAWMBE_UTF32LE	u8"UTF-32LE"
#define PAWMBE_UTF32BE	u8"UTF-32BE"
/* PDP */
#define PAWMBE_UTF32PE	u8"UTF-32PE"
/* Haswell */
#define PAWMBE_UTF32HE	u8"UTF-32HE"

/* Added just for completeness */
#define PAWMBE_PAWC 	PAWMBE_CHAR
/* Cheat our way to enabling software to support TCHAR */
#define PAWMBE_NPAWTC	NULL
#define PAWMBE_NPAWLC	PAWMBE_WIDE
#define PAWMBE_PAWMBC	PAWMBE_UTF8

#ifdef NPAW_BUILD
#	ifdef CAUGHT_ENDIAN_WAS_le
#		define PAWMBE_PAWHC 	PAWMBE_UTF16LE
#		define PAWMBE_PAWLLC	PAWMBE_UTF32LE
#	elif defined CAUGHT_ENDIAN_WAS_be
#		define PAWMBE_PAWHC 	PAWMBE_UTF16BE
#		define PAWMBE_PAWLLC	PAWMBE_UTF32BE
#	elif defined CAUGHT_ENDIAN_WAS_pe
#		define PAWMBE_PAWHC 	PAWMBE_UTF16LE
#		define PAWMBE_PAWLLC	PAWMBE_UTF32PE
#	else
#		define PAWMBE_PAWHC 	PAWMBE_UTF16BE
#		define PAWMBE_PAWLLC	PAWMBE_UTF32HE
#	endif
#else
#	define PAWMBE_PAWHC 	PAWMBE_UTF16
#	define PAWMBE_PAWLLC	PAWMBE_UTF32
#endif

PAW_C_API_OPEN
#ifdef NPAW_BUILD
#	include <errno.h>
#	include <uchar.h>

#	ifdef CAUGHT_OS_WAS_api_msw
#		include <windows.h>
typedef struct
{
	/* Keeping the member state helps keep code in the final library simple */
	mbstate_t mbstate;
} npawmbecd;
	#else
#		include <iconv.h>
typedef struct
{
	mbstate_t mbstate;
	iconv_t   lstollscd, llstolscd;
} npawmbecd;
#	endif
/* A single __thread allocation is shared by all thread locals in libpaw, this
 * is so that any external code is unlikely to be unable to acquire it's
 * allocation. Instead paw defines an object that will have all the thread
 * locals and allocates memory for it, the pointer to that memory is what goes
 * into the single __thread allocation it's taken. */
PAW_ANT_CB npawmbecd* mpawget_npawmbecd( void );
#endif
PAW_C_API_SHUT
