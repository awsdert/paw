#pragma once
#include "../pawval.h"

typedef union { void *ud; pawvoid_cb cb; } pawmodsym;

PAW_ANT_CB pawref pawmod_find( pawmbs name );
PAW_ANT_CB pawmodsym pawmod_findsym( pawref mod, pawmbs name );
PAW_ANT_CB pawref pawmod_open( pawmbs name );
PAW_ANT_CB void pawmod_shut( pawref mod );
