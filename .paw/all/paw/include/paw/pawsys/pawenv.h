#pragma once
#include "pawmbe.h"

PAW_ANT_CB pawmbs pawenv_getHOME();
PAW_ANT_CB pawmbs pawenv_getTMPDIR();
/** @return / or %SYSDRIVE% */
PAW_ANT_CB pawmbs pawenv_getSYSDIR();
/** @brief Return the directory defined as "On The Go" for portable apps
 * @note You design your software to be portable as much as possible to
 * reduce headaches when you start testing on another system, for example
 * you might share a drive between an office computer and home computer and
 * then link the objects on both to test what breaks on each system **/
PAW_ANT_CB pawmbs pawenv_getOTGDIR();
/** @brief Make a copy of the app wide environment list for thread usage,
 * this is the ONLY way to access environment variables in paw */
PAW_ANT_CB pawref pawenv_new( void );
PAW_ANT_CB pawmbsvec const* pawenv_getlist(pawref env);
/** @brief Use an environment list for launching */
PAW_ANT_CB npawd   pawenv_use( pawrd env );
/** @brief Grab variable name from environment */
PAW_ANT_CB pawmbs pawenv__getnamen( mpawref *env, pawmbs name, npawd len );
PAW_FQK_CB pawmbs pawenv__getname( mpawref *env, pawmbs name )
	{ return pawenv__getnamen( env, name, pawmbslen(name) ); }
PAW_FQK_CB pawmbs pawenv_getnamen( pawmbs name, npawd len )
	{ return pawenv__getnamen( 0, name, len ); }
PAW_FQK_CB pawmbs pawenv_getname( pawmbs name )
	{ return pawenv__getnamen( 0, name, pawmbslen(name) ); }
