#pragma once
/* paw expects to use the main thread for signals & input collection so it
 * creates a thread for the developer to use instead */
#include "pawmod.h"
enum
{
	/* Message thread io - thread initpaw() was called from */
	pawappm_msgant = 0,
	/* Actor thread io */
	pawappm_actant,
	/* Assets thread io - includes IPC */
	pawappm_fsoant,
	/* Graphics thread io */
	pawappm_gfxant,
	/* Library/Module thread io, the one that gets pawmain() passed into it */
	pawappm_libant,
	/* Sound effects thread io */
	pawappm_sfxant,
	pawappm__ant_count,
	pawappm__count = pawappm__ant_count
};

#define PAWAPPM__COUNT (PAWANTM__COUNT + pawappm__count)

pawmbs pawapp_shellname( void );
/* Provided only for native apps using wmain or WinMainW, main() expeceted
 * everywhere else. Launches a new thread to move execution to so that paw can
 * control the main thread for global signals, messages, peripherals and
 * anything else that needs to be on the main thread to avoid edge cases (such
 * as emulated threading) */
#ifdef NPAW_BUILD
npawd execpaw( npawd argc, npawts *argv, mpawref_cb cb );
#endif
