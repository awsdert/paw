#pragma once
#include "antexec.h"
#include "npawsys/pawmbe.h"
PAW_C_API_OPEN
/* Min UTF-32 characters worth */
pawmsg libpawmbe_minget( pawp  src, pawru lim, pawru *min  );
/* Min encoded bytes worth */
pawmsg libpawmbe_minput( pawp  src, pawru lim, pawru *min  );
pawcsc libpawmbe_getcsc( pawp  src, pawru lim, pawru *did );
pawmsg libpawmbe_putcsc( void *dst, pawru cap, pawru *did, pawcsc csc );
PAW_C_API_SHUT
