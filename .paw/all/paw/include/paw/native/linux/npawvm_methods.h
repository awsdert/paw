#pragma once

PAW_ANT_CB npawsys npawmfo_create
	( npawts name, pawru cap, npawprot prot, npawopt opts )
{
	(void)prot;
	npawsys fd = memfd_create( name, opts & PAWE32U_MAX );
	if ( fd < 0 )
		return fd;
	if ( opts & MFD_CLOEXEC )
	{
		npawopt attr = fcntl( fd, F_GETFD );
		fcntl( fd, F_SETFD, attr | O_CLOEXEC );
	}
	return fd;
}

PAW_QCK_CB void* spaw_mmap
(
	void *base,
	pawru size,
	pawe32u prot,
	pawe32u opts,
	npawd fd,
	npawfsp pos
)
{
#if PAWVD_WIDTH < 64
	opts |= SPAWVM_O_32BIT;
#endif
	opts |= SPAWVM_O_DONT0FILL;
	if ( !size )
	{
		pawsetmsg( PAWMSGID_INVALIDCAP );
		return NULL;
	}
	if ( pos < 0 )
	{
		pawsetmsg( PAWMSGID_INVALIDPOS );
		return NULL;
	}
	if ( pos > NPAWFSP_MAX )
	{
		pawsetmsg( PAWMSGID_NO_SYS );
		return NULL;
	}
#ifdef _LARGEFILE64_SOURCE
	return mmap64( base, size, prot, opts, fd, pos );
#else
	return mmap( base, size, prot, opts, fd, pos );
#endif
}

PAW_QCK_CB void  npawvm_forget( void *base, pawru size ) { munmap( base, size ); }
PAW_QCK_CB npawd npawvm_secure( void *base, pawru size, npawprot prot )
	{ return (mprotect( base, size, prot ) != 0) ? -1 : 0; }
PAW_QCK_CB void* npawvm_commit( pawru size, npawprot prot )
	{ return spaw_mmap( NULL, size, prot, SPAWVM_O_ANONYMOUS | prot >> 32, -1, -1 ); }
PAW_QCK_CB void* npawvm_resize( void *base, pawru size, npawprot prot )
	{ return spaw_mmap( base, size, prot, SPAWVM_O_ANONYMOUS | prot >> 32, -1, -1 ); }
