#pragam once
/* 1st string is the fallback string */
#define NPAWENVKEY_CWD       ".", "PWD"
#define NPAWENVKEY_USR_NAME  NULL, "USER", "LOGNAME"
#define NPAWENVKEY_USR_HOME  "~", "HOME"
#define NPAWENVKEY_ROOT_DIR  "/"
#define NPAWENVKEY_PREF_DIR  "/.config", "XDG_CONFIG_HOME"
#define NPAWENVKEY_DATA_DIR  "/.local/share", "XDG_DATA_HOME"
#define NPAWENVKEY_CACHE_DIR "/.cache", "XDG_CACHE_HOME", "TMPDIR"
#define NPAWENVKEY_TEMP_DIR  "/tmp", "TMPDIR"
#define NPAWENV_MOUNT_ROOTS  "/run/media/*", "/mnt/*"
