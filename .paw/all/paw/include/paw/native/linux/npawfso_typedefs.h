#pragma once
typedef unsigned _BitInt(64) npawfso_params;

typedef enum
{
	NPAWSTDSRCFSOID = 0,
	NPAWSTDDSTFSOID,
	NPAWSTDLOGFSOID
} NPAWSTDFSOID;

typedef union
{
	struct
	{
		unsigned long  d_ino;     /* Inode number */
		unsigned long  d_off;     /* Offset to next linux_dirent */
		unsigned short d_reclen;  /* Length of this linux_dirent */
		char           d_name[];  /* Filename (null-terminated) */
						 /* length is actually (d_reclen - 2 -
							offsetof(struct linux_dirent, d_name)) */
		/*
		char           pad;       // Zero padding byte
		char           d_type;    // File type (only since Linux
								 // 2.6.4); offset is (d_reclen - 1)
		*/
	} dirent;
	struct
	{
		ino64_t        d_ino;    /* 64-bit inode number */
		off64_t        d_off;    /* 64-bit offset to next structure */
		unsigned short d_reclen; /* Size of this dirent */
		unsigned char  d_type;   /* File type */
		char           d_name[]; /* Filename (null-terminated) */
	} dirent64;
} _npawfsi;
typedef mpawrawdst npawfsi;
