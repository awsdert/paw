#pragma once


PAW_QCK_CB void spawdbg_sys2paw( pawdbg *dbg, struct user *usr )
{
	dbg->obj.exec.flags.full= usr->regs.eflags;
#ifdef __x86_64__
	dbg->obj.code.cs.full	= usr->regs.cs;
	dbg->obj.code.ds.full	= usr->regs.ds;
	dbg->obj.code.es.full	= usr->regs.es;
	dbg->obj.code.ss.full	= usr->regs.ss;
	dbg->obj.code.fs.full	= usr->regs.fs;
	dbg->obj.code.fs_base.full	= usr->regs.fs_base;
	dbg->obj.code.gs.full	= usr->regs.gs;
	dbg->obj.code.gs_base.full	= usr->regs.gs_base;
	dbg->obj.exec.ip.full	= usr->regs.rip;
	dbg->obj.exec.pc.full	= usr->regs.rpc;
	dbg->obj.exec.oldax.full=usr->regs.orig_rax;
	dbg->obj.misc.ax.full	= usr->regs.rax;
	dbg->obj.misc.bx.full	= usr->regs.rbx;
	dbg->obj.misc.cx.full	= usr->regs.rcx;
	dbg->obj.misc.dx.full	= usr->regs.rdx;
	dbg->obj.misc.si.full	= usr->regs.rsi;
	dbg->obj.misc.di.full	= usr->regs.rdi;
	dbg->obj.misc.sp.full	= usr->regs.rsp;
	dbg->obj.misc.bp.full	= usr->regs.rbp;
	dbg->obj.misc.r8.full	= usr->regs.r8;
	dbg->obj.misc.r9.full	= usr->regs.r9;
	dbg->obj.misc.r10.full	= usr->regs.r10;
	dbg->obj.misc.r11.full	= usr->regs.r11;
	dbg->obj.misc.r12.full	= usr->regs.r12;
	dbg->obj.misc.r13.full	= usr->regs.r13;
	dbg->obj.misc.r14.full	= usr->regs.r14;
	dbg->obj.misc.r15.full	= usr->regs.r15;
#else
	dbg->obj.code.cs.full	= usr->regs.xcs;
	dbg->obj.code.ds.full	= usr->regs.xds;
	dbg->obj.code.es.full	= usr->regs.xes;
	dbg->obj.code.ss.full	= usr->regs.xss;
	dbg->obj.code.fs.full	= usr->regs.xfs;
	dbg->obj.code.gs.full	= usr->regs.xgs;
	dbg->obj.exec.ip.full	= usr->regs.eip;
	dbg->obj.exec.pc.full	= usr->regs.epc;
	dbg->obj.misc.ax.full	= usr->regs.eax;
	dbg->obj.exec.oldax.full=usr->regs.orig_rax;
	dbg->obj.misc.bx.full	= usr->regs.ebx;
	dbg->obj.misc.cx.full	= usr->regs.ecx;
	dbg->obj.misc.dx.full	= usr->regs.edx;
	dbg->obj.misc.si.full	= usr->regs.esi;
	dbg->obj.misc.di.full	= usr->regs.edi;
	dbg->obj.misc.sp.full	= usr->regs.esp;
	dbg->obj.misc.bp.full	= usr->regs.ebp;
#endif
}

PAW_QCK_CB pawd npawdbg_getreg( pawexe pid, pawdbg *dbg )
{
	struct user usr;
	memset( dbg, 0, sizeof(*dbg) );
	memset( &usr, 0, sizeof(usr) );
	if ( ptrace( PTRACE_GETREGS, pid, NULL, &(usr.regs) ) != 0 )
		return -1;
	if ( ptrace( PTRACE_GETFPREGS, pid, NULL, &(usr.i387) ) != 0 )
		return -1;
	spawdbg_sys2paw( &dbg, &usr );
	return 0;
}


PAW_QCK_CB npawexe spaw_spawn( npawts path, npawts *argv, npawts *envp )
{
	int pid = fork();

	if ( pid == 0 )
		(void)execve( path, argv, envp );
		/* According to docs the new app will take over the pid and replace
		 * our address space with it's own as it initiates, we therefore
		 * should assume that if execve returned we failed to launch the app,
		 * either way we do not want the child to return a valid object for
		 * itself or the root system process which is probably the kernel and
		 * not what any normal user will want to use, anyone that does want to
		 * use it will not use such a convoluded method as going through this
		 * function unless they just want to see what happens, in which case
		 * they will have downloaded the project, modified it and compiled that
		 * copy instead */
	else if ( pid > 0 )
		/* Instead of duplicating code let's just pass the pid to one that
		 * assumes it already exists */
		return npawapp_hook( pid );

	return INVALID_NPAWEXE;
}
