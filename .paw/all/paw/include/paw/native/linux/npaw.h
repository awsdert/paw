#pragma once
#include "npaw_types.h"
#include "npawfso_defines.h"
#include "npawfsm_defines.h"
#include "npawvm_defines.h"

#include "npawerr_methods.h"
#include "npawsem_methods.h"
#include "npawfsd_methods.h"
#include "npawfso_methods.h"
#include "npawfsm_methods.h"
#include "npawmod_methods.h"
#include "npawvm_methods.h"

/* NOTE: On linux the /opt directory is the best place for paw to install
 * itself globally. Would be something like: /opt/.paw/...
 *
 * /mnt, /media, /run/media or /var/run/media are the baseline paths to
 * identify current drive with
*/
