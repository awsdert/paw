#pragma once
/* Intended only for when fork() is called externally, such as via
 * pthreads_create() */
PAW_ANT_CB void npawant_useantexit( void );

PAW_QCK_CB pawd npawant_getstacklimit( pawlim *dst )
{
	struct rlimit rlp = {0};
	pawd res = getrlimit(RLIMIT_STACK,&rlp);
	if ( res < 0 )
		return -1;
	dst->softlim = rlp->rlim_cur;
	dst->hardlim = rlp->rlim_max;
	return 0;
}
PAW_FQK_CB void npawant_yield( void ) { sched_yield(); }

PAW_QCK_CB pawd npawant_spawn( pawexe *tid, npawant_cb cb, void *ud )
{
	*tid = -1;
	switch ( fork() )
	{
	case -1: return -1;
	case  0:
		*tid = pawgetid();
		npawant_useantexit();
		npawexit(cb(ud));
		return -1;
	}
	/* Be sure the thread has started before returning */
	while ( *tid < 0 )
		npawant_yield();
	return 0;
}
#if 0
/* To be investigated later after the basics are supported */
PAW_QCK_CB pawd npawant_joinx( npawant ant ) { return wait( ant, __WCLONE ); }
PAW_QCK_CB pawd npawant_spawnx
(
	npawant_cb mainCB, pawmem const *stack, npawopt opts,
	void *ud, npawexe *pid, void *tls, npawexe *tid
)
{
	struct clone_args args = {0};
	args.flags = opts;
	args.child_tid = (pawvu)tid;
	args.flags |= tid ? CLONE_CHILD_SETTID : CLONE_DETACHED;
	args.flags |= CLONE_VM | CLONE_SYSVSEM | CLONE_THREAD | CLONE_SIGHAND;
	if ( tls )
	{
		args.tls = (pawvu)tls;
		args.flags |= CLONE_SETTLS;
	}
	if ( stack )
	{
		args.stack_size = stack->cap;
		args.stack = (pawvu)(stack->top);
	}
	if ( pid )
	{
		args.parent_tid = (pawvu)pid;
		args.flags |= pid ? CLONE_PARENT_SETTID : 0;
	}
	pawd res = syscall( mainCB, &args, sizeof(args) );
	if ( res < 0 )
		return -1;
	return 0;
}
#endif
