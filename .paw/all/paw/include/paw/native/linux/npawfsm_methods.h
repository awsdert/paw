#pragma once

#define NPAWFSM_O_RODATA PROT_READ
#define NPAWFSM_O_RWDATA PROT_RDWR
#define NPAWFSM_O_CPDATA (PROT_RDWR | PAWE64U_C(MAP_PRIVATE)<<32)
#define NPAWFSM_O_ROEXEC (NPAWFSM_O_RODATA | PROT_EXEC)
#define NPAWFSM_O_RWEXEC (NPAWFSM_O_RWDATA | PROT_EXEC)
#define NPAWFSM_O_CPEXEC (NPAWFSM_O_CPDATA | PROT_EXEC)
#define NPAWFSM_O_HPAGES (PAWE64U_C(MAP_PRIVATE)<<32)

/* Separate because of poor design on microsoft's part */
#define NPAWFSM_V_RODATA NPAWFSM_O_RODATA
#define NPAWFSM_V_RWDATA NPAWFSM_O_RWDATA
#define NPAWFSM_V_CPDATA NPAWFSM_O_CPDATA
#define NPAWFSM_V_ROEXEC NPAWFSM_O_ROEXEC
#define NPAWFSM_V_RWEXEC NPAWFSM_O_RWEXEC
#define NPAWFSM_V_CPEXEC NPAWFSM_O_CPEXEC
#define NPAWFSM_V_HPAGES NPAWFSM_O_HPAGES

PAW_QCK_CB void   npawfsm_shut( npawsys fsm ) { (void)fsm; }
PAW_QCK_CB npawsys npawfsm_open( npawsys fso ) { return fso; }
PAW_FQK_CB void   npawfsm_free( void *addr, pawru size ) { munmap( addr, size ); }
PAW_QCK_CB void*  npawfsm_view( void *addr, pawru size, npawprot prot, npawsys fsm, npawfsp pos )
	{ return spaw_mmap( addr, size, prot, MAP_FILE, fsm, pos ); }
