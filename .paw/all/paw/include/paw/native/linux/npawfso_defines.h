#pragma once

/* Since modern operating systems support lengthier paths we treat the so
 * called max as only true for the average file, hence average in the name */
#ifdef PATH_MAX
#	define NPAWFSO_AVERAGE_MAX_LENGTH PATH_MAX
#else
#	define NPAWFSO_AVERAGE_MAX_LENGTH BUFSIZ
#endif

#define SPAW_PROT_RDWR (PROT_READ | PROT_WRITE)
#if _FILE_OFFSET_BITS >= 64
#	define SPAW_O_LARGEFILE 0
#elif defined( _LARGEFILE64_SOURCE )
#	define SPAW_O_LARGEFILE O_LARGEFILE
#else
#	define SPAW_O_LARGEFILE(CAST(pawfsomask,O_APPEND)<<32)
#endif
#ifdef O_EXEC
#	define SPAW_O_EXEC O_EXEC
#else
#	define SPAW_O_EXEC O_PATH
#endif
#ifdef O_NONBLOCK
#	define SPAW_O_NONBLOCK O_NONBLOCK
#else
#	define SPAW_O_NONBLOCK O_NDELAY
#endif

#define NPAWFSO_LOCK_RDONLY F_RDLCK
#define NPAWFSO_LOCK_SYNCED F_WRLCK
#define NPAWFSO_LOCK_REMOVE F_UNLCK

/* 1st 16bits are reserved for octal permissions */

#define NPAWFSO_SHIFT_BIAS   16
#define NPAWFSO_O_OPEN_REAL  (CAST(pawfsomask,0)<<NPAWFSO_SHIFT_BIAS)
#define NPAWFSO_O_TRIM_REAL  (CAST(pawfsomask,1)<<NPAWFSO_SHIFT_BIAS)
#define NPAWFSO_O_MK_ALWAYS  (CAST(pawfsomask,2)<<NPAWFSO_SHIFT_BIAS)
#define NPAWFSO_COUNT_BIAS   3

#define NPAWFSO_SHIFT_MODE   18
#define NPAWFSO_O_AS_RODATA  (CAST(pawfsomask,0)<<NPAWFSO_SHIFT_MODE)
#define NPAWFSO_O_AS_RWDATA  (CAST(pawfsomask,1)<<NPAWFSO_SHIFT_MODE)
#define NPAWFSO_O_AS_ROEXEC  (CAST(pawfsomask,3)<<NPAWFSO_SHIFT_MODE)
#define NPAWFSO_O_AS_RWEXEC  (CAST(pawfsomask,4)<<NPAWFSO_SHIFT_MODE)
#define NPAWFSO_O_AS_APPEND  (CAST(pawfsomask,8)<<NPAWFSO_SHIFT_MODE)
#define NPAWFSO_COUNT_MODE   8

#define NPAWFSO_SHIFT_TYPE   22
#define NPAWFSO_O_IS_RDONLY  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+0))
#define NPAWFSO_O_IS_FOLDER  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+1))
#define NPAWFSO_O_IS_SYMLINK (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+3))
#define NPAWFSO_O_IS_REGFILE (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+4))

/* win32 only, use other means to detect if it is any of these */
#define NPAWFSO_O_WIN32_HIDDEN  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+5))
#define NPAWFSO_O_WIN32_EXEFILE (CAST(pawfsomask,2)<<(NPAWFSO_SHIFT_TYPE+6))
#define NPAWFSO_O_WIN32_ARCHIVE (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+7))
#define NPAWFSO_O_WIN32_SYSTEM  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+8))
#define NPAWFSO_O_WIN32_DEVIVE  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_TYPE+9))

#define NPAWFSO_SHIFT_MISC   32
#define NPAWFSO_O_NO_WAITING (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 0))
#define NPAWFSO_O_COMPRESSED (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 1))
#define NPAWFSO_O_IGNORELINK (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 2))
#define NPAWFSO_O_BIGOFFSETS (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 3))
#define NPAWFSO_O_SCANRANDOM (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 4))
#define NPAWFSO_O_SCANLINEAR (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 5))
#define NPAWFSO_O_PREFER_RAM (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 6))
#define NPAWFSO_O_DIRECTWRHW (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 7))
#define NPAWFSO_O_DEL_ONSHUT (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 8))
#define NPAWFSO_O_DIR_ACCESS (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+ 9))
#define NPAWFSO_O_DIR_ADDSUB (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+10))

#define NPAWFSO_O_POSIX_SYNC_BOTH  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+11))
#define NPAWFSO_O_POSIX_SYNC_DATA  (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+12))
#define NPAWFSO_O_POSIX_DEL_ONEXEC (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+13))
#define NPAWFSO_O_POSIX_NO_CONSOLE (CAST(pawfsomask,1)<<(NPAWFSO_SHIFT_MISC+14))
