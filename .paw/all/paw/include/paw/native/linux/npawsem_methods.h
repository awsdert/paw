#pragma once
#ifndef NPAW_USE_SEMAPHORE_H
PAW_QCK_CB pawrd _npawsem__trywait( npawsem *sem )
{
	struct sembuf sb = {0,0,IPC_NOWAIT};
	return semop( *sem, &sb, 1 );
}
PAW_QCK_CB pawrd _npawsem__wait( npawsem *sem )
{
	struct sembuf sb = {0,0,0};
	return semop( *sem, &sb, 1 );
}
PAW_QCK_CB pawrd _npawsem__inc( npawsem *sem )
{
	struct sembuf sb = {0,1,0};
	return semop( *sem, &sb, 1 );
}
PAW_QCK_CB pawrd _npawsem__dec( npawsem *sem )
{
	struct sembuf sb = {0,-1,0};
	return semop( *sem, &sb, 1 );
}
PAW_QCK_CB pawrd npawsem_lock( npawsem *sem )
{
	pawrd val = 0;
	pawrd res = _npawsem__trywait( sem );
	if ( res == 0 && _npawsem__inc(sem) == 0 )
		return 0;
	if ( *sem < 0 )
	{
		pawsetmsg( PAWMSGID_INITIALISE, PAWSUBID_SEMAPHORE, 0, 0 );
		return -1;
	}
	pawsetmsg( npawerr2msg(errno, &val), val, 0, 0 );
	return -1;
}

PAW_QCK_CB npawsem npawsem_open( void )
	{ return semget( IPC_PRIVATE, 1, IPC_CREAT | 0600 ); }
PAW_QCK_CB void npawsem_shut( npawsem *sem )
	{ (void)semctl( *sem, 1, IPC_RMID ); }
PAW_QCK_CB void npawsem_free( npawsem *sem ) { _npawsem__dec( sem ); }
#else
PAW_QCK_CB pawrd npawsem_lock( npawsem *sem )
{
	pawrd val = 0;
	pawrd res = sem_trywait( sem );
	if ( res == 0 )
		return 0;
	if ( sem_getvalue( sem, &val ) != 0 )
	{
		pawsetmsg( PAWMGSID_INITIALISE, PAWSUBID_SEMAPHORE, 0 );
		return -1;
	}
	pawsetmsg( libpawgeterr(&val), val, 0 );
	return -1;
}
PAW_QCK_CB npawsem npawsem_open( void )
	{ return sem_open( NULL, O_CREAT, 0, 1 ); }
PAW_QCK_CB void npawsem_shut( npawsem *sem ) { sem_close( *sem ); }
PAW_QCK_CB void npawsem_free( npawsem *sem ) { sem_post( *sem ); }
#endif
PAW_QCK_CB pawrd npawsem_wait( npawsem *sem )
{
	pawrd res = PAWMSGID_TRY_AGAIN;
	while ( res == PAWMSGID_TRY_AGAIN )
		res = npawsem_lock( sem );
	return res;
}
