#pragma once
/* Get Media Root of given path */
PAW_QCK_CB pawru npawfsd_getdevroot( npawts abspath, pawru len )
{
	pawru i = 0, l = 0, n = 0;
	paws roots[] = { "/mnt", "/media", "/run/media" "/var/run/media", NULL };
	for ( ; roots[i]; ++i )
	{
		l = npawtslen( roots[i] );
		if ( npawtscmp( abspath, roots[i], l ) != l )
			continue;
		/* Even if no '/' is found that just means dealing with the end '\0'
		 * or some other chunk of the path that is not relevant to the provided
		 * path */
		return l + npawtsfindc( abspath + l, len - l, '/' );
	}
	return len ? 1 : 0;
}
PAW_QCK_CB pawrd _npaw_getdents( npawfsd fsd, npawfsi *fsi )
{
	if ( mpawrawdst_fitcap( fsi, 1, BUFSIZ ) < BUFSIZ )
		return -1;
	long pos = lseek( fsd, 0, SEEK_CUR );
	if ( pos < 0 )
		return -1;
	_npawfsi *ents = fsi->top;
	long got = syscall( SYS_getdents, fsd, ents, sizeof(_npawfsi) );
	if ( got < 0 )
		return -1;
	lseek( fsd, pos, SEEK_SET );
	if ( got < sizeof(_npawfsi) )
		return -1;
	if ( mpawrawdst_fitcap( fsi, 1, ents->dirent.d_reclen ) < ents->dirent.d_reclen )
		return -1;
	return syscall( SYS_getdents, fsd, ents, ents->dirent.d_reclen );
}
PAW_FQK_CB npawsys spaw_open( npawts path, npawopt mode, npawopt perm )
	{ return syscall( SYS_open, path, mode, perm ); }
PAW_QCK_CB npawsys npawfsd_temp( void )
{
	return spaw_open
	(
		"/tmp", O_CREAT | O_TMPFILE | O_EXCL | O_DIRECTORY | O_CLOEXEC
		| SPAW_O_LARGEFILE, 644
	);
}

PAW_QCK_CB npawfsd npawfsd_get1st( npawtsvec dirvec, npawfsi *fsi )
{
	npawfsd fsd = open( dirvec.top, O_RDONLY | O_DIRECTORY | O_CLOEXEC );
	if ( fsd < 0 )
		return -1;
	pawrd got = _npaw_getdents( fsd, fsi );
	if ( got < 0 )
	{
		mpawrawdst_delete( fsi );
		close(fsd);
		return -1;
	}
	return fsd;
}

PAW_QCK_CB npawd npawfsd_getnxt( npawfsd fsd, npawfsi *fsi )
{
	pawrd got = _npaw_getdents( fsd, fsi );
	return (got < 0) ? -1 : (got > 0);
}

PAW_QCK_CB void npawfsd_shut( npawfsd fsd, npawfsi *fsi )
	{ mpawrawdst_delete( fsi ); close(fsd); }
#ifdef _LARGEFILE64_SOURCE
PAW_QCK_CB pawrd _npaw_getdents64( npawfsd fsd, npawfsi *fsi )
{
	if ( mpawrawdst_fitcap( fsi, 1, BUFSIZ ) < BUFSIZ )
		return -1;
	long pos = lseek( fsd, 0, SEEK_CUR );
	if ( pos < 0 )
		return -1;
	_npawfsi *ents = fsi->top;
	long got = syscall( SYS_getdents64, fsd, ents, sizeof(_npawfsi) );
	if ( got < 0 )
		return -1;
	lseek( fsd, pos, SEEK_SET );
	if ( got < sizeof(_npawfsi) )
		return -1;
	if ( mpawrawdst_fitcap( fsi, 1, ents->dirent64.d_reclen ) < ents->dirent64.d_reclen )
		return -1;
	return syscall( SYS_getdents64, fsd, ents, ents->dirent64.d_reclen );
}

PAW_QCK_CB npawfsd npawfsd_get1st64( npawtsvec dirvec, npawfsi *fsi )
{
	npawfsd fsd = open( dirvec.top, O_RDONLY | O_DIRECTORY | O_CLOEXEC | O_LARGEFILE );
	if ( fsd < 0 )
		return -1;
	pawrd got = _npaw_getdents64( fsd, fsi );
	if ( got < 0 )
	{
		mpawrawdst_delete( fsi );
		close(fsd);
		return -1;
	}
	return fsd;
}

PAW_QCK_CB npawd npawfsd_getnxt64( npawfsd fsd, npawfsi *fsi )
{
	pawrd got = _npaw_getdents64( fsd, fsi );
	return (got < 0) ? -1 : (got > 0);
}

PAW_QCK_CB void npawfsd_shut64( npawfsd fsd, npawfsi *fsi )
	{ mpawrawdst_delete( fsi ); close(fsd); }
#endif
