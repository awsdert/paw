#pragma once

PAW_QCK_CB void   npawsys_release( npawsys sys ) { close(sys); }


PAW_QCK_CB pawmsg npawsys_duplicate
	( npawsys *dst, npawsys src, npawsys invalid, pawrd opt )
{
	if ( opt && O_CREAT )
	{
		*dst = dup(sys);
		return ( *dst != invalid ) ? 0 : npawerrno2msg( errno );
	}
	return ( dup2( src, dst ) == 0 ) ? 0 : npawerrno2msg( errno );
}
