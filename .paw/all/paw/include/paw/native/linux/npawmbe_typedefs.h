#pragma once
/* Code page */
typedef char*	npawcp;
typedef struct
{
#define T void
#include <paw/pawval/inherit_mpawmem.h>
	npawcp cp;
} npawcpmem;

typedef struct
{
	npawcpmem src;
	npawcpmem dst;
	iconv_t   cd;
} npawmbe;
