#pragma once
PAW_QCK_CB npawtsvec* const npawfso_getpath( npawsys fso )
{
	npawtsmem *abspath = &(mpawthisant->abspath);
	pawru max = abspath->cap / sizeof(npawtc);
	char link[bitsof(int)*2] = {0};
	pawrd ret = 0;
	int err = 0;
	snprintf( link, sizeof(link), "/proc/self/fso/%d", fso );
	while (1)
	{
		ret = readlink( link, abspath->top, max );
		if ( ret != -1 )
			break;
		if ( errno != ENAMETOOLONG )
			return NULL;
		max += NPAWFSO_AVERAGE_MAX_LENGTH;
		if ( npawtsmem_fitlen(abspath,max) < max )
			return NULL;
	}
	abspath->end = ret;
	return (npawtsvec*)abspath;
}
PAW_QCK_CB int npawfso_getsizelimit( pawlim *dst )
{
	struct rlimit rlp = {0};
	int res = getrlimit(RLIMIT_FSIZE,&rlp);
	if ( res < 0 )
		return -1;
	dst->softlim = rlp.rlim_cur;
	dst->hardlim = rlp.rlim_max;
	return 0;
}

PAW_QCK_CB npawsys npawgetstdfso(NPAWSTDFSOID id)
{
	switch ( id )
	{
	case NPAWSTDSRCFSOID: return fileno(stdin);
	case NPAWSTDDSTFSOID: return fileno(stdout);
	case NPAWSTDLOGFSOID: return fileno(stderr);
	}
	return -1;
}
PAW_FQK_CB int npawfso_seek( npawsys fso, npawfsp pos, npawfsp *res, npawu whence )
{
#ifdef SYS_llseek
	return syscall( SYS_llseek, fso, pos.split[1], pos.split[0], res, whence );
#else
	return syscall( SYS_lseek, fso, pos.off, res, whence );
#endif
}
PAW_FQK_CB int spaw__fadvise( npawsys fso, npawfsp top, npawfsp end, npawu advise )
{
#if _FILE_OFFSET_BITS >= 64
	return syscall( SYS_fadvise64, fso, top.off, end.off, advise );
#else
	return syscall( SYS_fadvise64, fso, top.loff, end.loff, advise );
#endif
}
PAW_QCK_CB int spaw_fadvise( npawsys fso, pawru top, pawru end, npawu advise )
{
	npawfsp _top = { top }, _end = { end };
	return spaw__fadvise( fso, _top, _end, advise );
}
PAW_QCK_CB int spaw_fadvise64( npawsys fso, pawe64u top, pawe64u end, npawu advise )
{
	npawfsp _top = { .e64u = top }, _end = { .e64u = end };
	return spaw__fadvise( fso, _top, _end, advise );
}
PAW_QCK_CB npawsys spawfso_open( npawts path, pawe128u opts, npawopt perm )
{
	npawsys fso = spaw_open( path, CAST(pawe32u,opts), CAST(pawe32u,opts>>32) );
	if ( fso < 0 || !(cfg->advise) )
		return fso;
	if ( spaw_fadvise( fso, 0, 0, CAST(pawe32u,opts >> 64) ) == 0 )
		return fso;
	close(fso);
	return -1;
}

PAW_FQK_CB void npawfso_shut( npawsys fso ) { close(fso); }
PAW_FQK_CB npawsys npawfso_open( npawts path, pawe128u opts )
	{ return spawfso_open( path, opts, 0 ); }
PAW_FQK_CB npawsys npawfso_mkexe( npawts path, pawe128u opts )
	{ return spawfso_open( path, opts | O_CREAT | O_EXCL, 755 ); }
PAW_FQK_CB npawsys npawfso_mktmp( npawts dir, pawe128u opts )
	{ return spawfso_open( dir, opts | O_TMPFILE | O_CREAT | O_EXCL, 644 ); }



PAW_QCK_CB int 	npawfso_setlen( npawsys fso, pawrd len )
	{ return -((int)(ftruncate( fso, len ) != 0)); }
PAW_QCK_CB int 	npawfso64_setlen( npawsys fso, pawrd len )
	{ return -((int)(ftruncate64( fso, len ) != 0)); }

PAW_QCK_CB int    npawfso_getraw( npawsys fso, void *dst, pawru *rd )
{
	ssize_t did = read( fso, dst, *rd );
	if ( did < 0 )
	{
		*rd = 0;
		return -1;
	}
	*rd = did;
	return 0;
}
PAW_QCK_CB int    npawfso_putraw( npawsys fso, pawp  src, pawru *wr )
{
	ssize_t did = write( fso, src, *wr );
	if ( did < 0 )
	{
		*wr = 0;
		return -1;
	}
	*wr = did;
	return 0;
}
PAW_QCK_CB int    npawfso_getabs( npawsys fso, pawrd pos, void *dst, pawru *rd )
{
	ssize_t did = pread( fso, dst, *rd, pos );
	if ( did < 0 )
	{
		*rd = 0;
		return -1;
	}
	*rd = did;
	return 0;
}
PAW_QCK_CB int    npawfso_putabs( npawsys fso, pawrd pos, pawp  src, pawru *wr )
{
	ssize_t did = pwrite( fso, src, *wr, pos );
	if ( did < 0 )
	{
		*wr = 0;
		return -1;
	}
	*wr = did;
	return 0;
}

