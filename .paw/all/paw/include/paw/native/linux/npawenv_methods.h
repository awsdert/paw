#pragma once
extern char **environ;

PAW_QCK_CB void* spaw_getauxptr( npawd id )
	{ union { pawru u; void* addr; } val = {getauxval(id)}; return val.addr; }

PAW_QCK_CB npawenvsrc npawenv_getlist( void ) { return environ; }

PAW_QCK_CB paws 	npawenv_getpathname( void ) { return spaw_getauxptr(AT_EXECFN); }
PAW_QCK_CB paws 	npawenv_getplatform( void ) { return spaw_getauxptr(AT_PLATFORM); }
PAW_QCK_CB pawp 	npawenv_getrandaddr( void ) { return spaw_getauxptr(AT_RANDOM); }
PAW_QCK_CB pawru	npawenv_getpagesize( void ) { return getauxval(AT_PAGESZ); }
PAW_QCK_CB pawru	npawenv_gethwhints1( void ) { return getauxval(AT_HWCAP); }
PAW_QCK_CB pawru	npawenv_gethwhints2( void ) { return getauxval(AT_HWCAP2); }

PAW_QCK_CB npawtsmem* _npawenvsrc_getvalv( npawtsmem *dst, paws fallback, va_list va )
{
	pawru len = 0, lim = dst->end;
	paws key = va_arg( va, paws ), val = NULL;
	dst->end = 0;
	while ( key )
	{
		val = getenv(key);
		if ( !val )
		{
			key = va_arg( va, paws );
			continue;
		}
		return npawtsmem_fitcsn( dst, val, pawslen(val) ) ? dst : NULL;
	}
	if ( !fallback )
		return dst;
	return npawtsmem_fitcsn( dst, fallback, pawslen(fallback) ) ? dst : NULL;
}

PAW_QCK_CB npawtsmem* _npawenvsrc_getval( npawtsmem *dst, paws fallback, ... )
{
	pawru len = 0;
	va_list va;
	va_start( va, fallback );
	if ( !dst ) dst =  &(mpawthisant->envdata);
	dst = _npawenvsrc_getvalv( dst, fallback, va );
	va_end( va );
	return dst;
}
PAW_QCK_CB npawtsmem* npawenvsrc_getsysdrive( void )
{
	npawtsmem *sysroot = &(mpawthisant->sysroot);
	sysroot->end = 1;
	sysroot->top[0] = '/';
	sysroot->top[1] = 0;
	return sysroot;
}
/* Referred to this thread for understanding what to use:
 * https://unix.stackexchange.com/questions/312988/understanding-home-configuration-file-locations-config-and-local-sha
 * */
PAW_QCK_CB npawtsmem* npawenvsrc_getprinter( void )
	{ return _npawenvsrc_getval( NULL, NULL, "PRINTER", "LPDEST", NULL ); }

/* ONLY call these in single threaded environments */
PAW_QCK_CB npawd 	npawenvsrc_nulall(void) { return -((npawd)(clearenv() !=0)); }
PAW_QCK_CB npawd 	npawenvsrc_nulval(paws key)
	{ return -((npawd)(unsetenv(key) != 0)); }
PAW_QCK_CB npawd 	npawenvsrc_putval(paws key,paws val,pawb replace)
	{ return -((npawd)(setenv(key,val,replace) != 0)); }
