#pragma once
PAW_FQK_CB npawerr	npawgeterr(void) PAWATTR_UNUSED { return errno; }
PAW_FQK_CB void 	npawputerr( npawerr err ) { errno = errno; }
PAW_QCK_CB npawtc*	npawerr_newtxt(npawerr err, pawru *len)
{
	npawts  errtxt = strerror(err);
	pawru  leng = npawtslen(errtxt);
	npawtc *text = malloc(leng+1);
	if ( !text )
		return NULL;
	memcpy( text, errtxt, leng );
	if ( len )
		*len = leng;
	text[leng] = 0;
	return text;
}
PAW_QCK_CB void 	npawerr_deltxt(npawtc *txt) { free(txt); }
