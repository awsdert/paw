#pragma once

/* Mandatory at this point */
#if defined(_FILE_OFFSET_BITS) && _FILE_OFFSET_BITS < 64
#	undef _FILE_OFFSET_BITS
#	define _FILE_OFFSET_BITS 64
#endif
#ifndef _FILE_OFFSET_BITS
#	define _FILE_OFFSET_BITS 64
#endif

#include <unistd.h>
#include <errno.h>
#include <linux/sched.h>
#include <sched.h>
#include <sys/syscall.h>
#include <sys/resource.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/auxv.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dlfcn.h>
#include <dirent.h>
#include <sys/ipc.h>
#include <sys/sem.h>
/* https://www.man7.org/linux/man-pages/man3/glob.3.html */
#include <glob.h>
#include <malloc.h>
#include <string.h>
#include <uchar.h>
#include <wchar.h>
#include <iconv.h>
#include <stdlib.h>
#include <stdio.h>

typedef int	npawsys;
#define NPAW_BAD_SEM -1
#define NPAW_BAD_FSO -1
#define NPAW_BAD_FSM -1

typedef int	npawerr;
typedef ulong	npawopt;
typedef void*	npawmod;
typedef paws	npawlang;
typedef pawe256u npawfsomask;
typedef char**	npawenvsrc;
typedef size_t  npawenvtok;
typedef clock_t npawage;
typedef time_t  npawageNS;
typedef pawe32u	npawprot;
/* Only way to deal with externally created threads, defaults to method
 * intended for the thread main was executed in. Any other thread should call
 * this as soon as possible before getting on with their intended task if they
 * ever deal with libspaw */
extern __thread void (*npawexit)( int code );

/* Placeholder for now, I think octal permissions is what I want here
 * SAA here stands for Security Attributes Access */
typedef int  npawsaa;
typedef struct { pawru pagesize; } npawsysinf;

#include "npawant_typedefs.h"
#include "npawmbe_typedefs.h"
#include "npawfso_typedefs.h"
#include "npawmbe_defines.h"
#include "npawfso_defines.h"
#include "npawfsm_defines.h"
