PAW_QCK_CB npawmod	npawmod_newhook( npawts name )
	{ return dlopen( name, RTLD_NOW ); }
PAW_QCK_CB npawmod	npawmod_gethook( npawts name )
	{ return dlopen( name, RTLD_NOW | RTLD_GLOBAL | RTLD_NOLOAD | RTLD_NODELETE ); }
PAW_QCK_CB void 	npawmod_remhook( npawmod mod ) { dlclose( mod ); }
PAW_QCK_CB npawtc*	npawmod_geterrtxt(pawru *len)
{
	npawd   errwas = errno;
	npawts  errtxt = dlerror();
	pawru  leng = npawtslen(errtxt);
	npawtc *text = malloc(leng+1);
	if ( !text )
	{
		errno = errwas;
		return NULL;
	}
	memcpy( text, errtxt, leng );
	if ( len )
		*len = leng;
	text[leng] = 0;
	return text;
}
PAW_QCK_CB void 	npawmod_delerrtxt(npawtc *txt) { free(txt); }
