#include "npawerr_methods.h"
#include "npawenv_methods.h"
#include "npawfso_methods.h"
#include "npawfsm_methods.h"
#include "npawfsd_methods.h"
#include "npawsem_methods.h"
#include "npawmod_methods.h"

PAW_QCK_CB npawage npawgetage( void ) { return clock(); }
PAW_QCK_CB npawageNS npawgetageNS( void )
{
	alignas(pawru) npawageNS ageNS;
	syscall( SYS_time, &ageNS );
	return time(NULL);
}

PAW_QCK_CB npawd npawmbe_open( npawmbe *mbe, npawcp src, npawcp dst )
{
	errno = 0;
	params->cd = iconv_open( dst, src );
	return (errno == 0) ? 0 : -1;
}
PAW_QCK_CB void  npawmbe_shut( npawmbe *mbe )
{
	iconv_close( params->cd );
	memset( &(params->cd), 0, sizeof(iconv_t) );
}

PAW_QCK_CB npawd spaw_iconv0( npawopt opts, npawmbe *mbe )
{
	void  *src = params->src->top;
	size_t end = src->end;
	void  *dst = params->dst->top;
	size_t cap = params->dst->cap;
	/* Add BUFSIZ on for good measure */
	params->needbytes = BUFSIZ;
	while ( end )
	{
		if ( iconv( params->cd, &src, &end, &dst, &cap ) >= 0 )
		{
			params->dst->end = params->dst->cap - cap;
			return PAWMSGID_ALREADY;
		}
		params->needbytes += params->dst->cap - cap;
		/* Reset so that we can keep counting the size we need */
		dst = params->dst->top;
		cap = params->dst->cap;
		switch ( errno )
		{
		case EINVAL: return PAWMSGID_INCOMPLETE;
		case EILSEQ: return PAWMSGID_INVALIDSEQ;
		case E2BIG: continue;
		}
	}
	return 0;
}
PAW_QCK_CB npawd spaw_iconv( npawopt opts, npawmbe *mbe )
{
	void  *src = params->src->top;
	size_t end = src->end;
	void  *dst = params->dst->top;
	size_t cap = params->dst->cap;
	pawru  did = 0;
	while ( end )
	{
		if ( iconv( params->cd, &src, &end, &dst, &cap ) >= 0 )
		{
			params->dst->end = params->dst->cap - cap;
			return 0;
		}
		params->dst->end = params->dst->cap - cap;
		/* Reset so that we can keep counting the size we need */
		dst = params->dst->top;
		cap = params->dst->cap;
		switch ( errno )
		{
		case EINVAL: return PAWMSGID_INCOMPLETE;
		case EILSEQ: return PAWMSGID_INVALIDSEQ;
		case E2BIG:  return PAWMSGID_NOT_ENOUGH;
		}
	}
	return 0;
}
PAW_QCK_CB npawd npawmbe_cstocs( npawmbe *mbe )
{
	npawd msgid = spaw_iconv0( opts, params );
	switch ( msgid )
	{
	case PAWMSGID_ALREADY: return 0;
	case PAWMSGID_NOT_ENOUGH: break;
	default: return msgid;
	}
	if ( npawmem_fitcap( params->dst, cap ) != cap )
		return -1;
	params->dst->end = spaw_iconv( opts, params );
	return 0;
}
PAW_QCK_CB npawd npawmbe_lstocs( npawmbe *mbe )
{
	npawmbe_open( params, NPAWCP_WCHAR_T, params->cp );
	npawd msgid = npawmbe_cstocs( opts, params );
	npawmbe_shut( params );
}
PAW_QCK_CB npawd npawmbe_cstols( npawmbe *mbe )
{
	npawmbe_open( params, params->cp, NPAWCP_WCHAR_T );
	npawd msgid = npawmbe_cstocs( opts, params );
	npawmbe_shut( params );
}
