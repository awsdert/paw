#pragma once
PAW_QCK_CB void* spaw_VirtualAlloc
	( void *base, pawru size, pawe32u opts, pawe32u prot )
	{ return VirtualAlloc( base, size, MEM_COMMIT | MEM_RESERVE | opts, prot ); }

PAW_QCK_CB void  npawvm_forget( void *base, pawru size )
	{ VirtualFree( base, size, MEM_DECOMMIT ); } }
PAW_QCK_CB pawb  npawvm_secure( void *base, pawru size, npawprot prot )
{
	DWORD old = 0;
	return (VirtualProtect( base, size, prot & PAWE32U_MAX, &old ) == TRUE);
}
PAW_QCK_CB void* npawvm_commit( pawru size, npawprot prot, npawopt opts )
	{ return VirtualAlloc( NULL, size, opts | NPAWVM_O_32BIT, prot ); }
PAW_QCK_CB void* npawvm_resize( void *base, pawru size, npawprot prot, npawopt opts )
	{ return VirtualAlloc( base, size, opts | NPAWVM_O_32BIT, prot ); }
