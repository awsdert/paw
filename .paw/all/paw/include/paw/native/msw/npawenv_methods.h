#pragma once
PAW_QCK_CB npawenvsrc npawenv_getlist(void) { return GetEnvironmentStrings(); }
PAW_QCK_CB paws	npawenv_getpathname(void)
	{ getauxval_addr val = {getauxval(AT_EXECFN)}; return val.addr; }
PAW_QCK_CB paws	npawenv_getplatform(void)
	{ getauxval_addr val = {getauxval(AT_PLATFORM)}; return val.addr; }
PAW_QCK_CB pawp 	npawenv_getrandaddr(void)
	{ getauxval_addr val = {getauxval(AT_RANDOM)}; return val.addr; }
PAW_QCK_CB pawru	npawenv_getpagesize(void) { return getauxval(AT_PAGESZ); }
PAW_QCK_CB pawru	npawenv_gethwhints1(void) { return getauxval(AT_HWCAP); }
PAW_QCK_CB pawru	npawenv_gethwhints2(void) { return getauxval(AT_HWCAP2); }

PAW_QCK_CB npawts npawenv_nextpair( npawenvsrc *envsrc )
{
	npawts pool = envsrc->buffer;
	pawru o = envsrc->offset;
	pawru l = envsrc->length;
	o = npawtslinelen( pool + o, envsrc->length - o );
	o += npawtsnewlinelen( pool + o, l - o );
	envsrc->offset = o;
	return pool + o;
}

PAW_QCK_CB npawd spawenvsrc_getvalv( paws fallback, va_list va )
{
	npawtsmem *dst = &(mpawthisant->envdata);
	pawru max = dst->cap / sizeof(npawtc);
	pawru len = 0, lim = max - (pawb)(max > 0);
	paws key = va_arg( va, paws ), val = NULL;
	dst->end = 0;
	while ( key )
	{
		_npawenvsrc_getval_retry:
		len = GetEnvironmentVariableA( key, dst->top, dst->cap / sizeof(npawtc) );
		if ( GetLastError() == ERROR_ENVVAR_NOT_FOUND )
		{
			key = va_arg( va, paws );
			continue;
		}
		if ( lim >= len )
		{
			dst->end = len * sizeof(npawtc);
			return dst;
		}
		if ( !npawtsmem_fitlen( dst, len ) )
			return PAWMSGID_NOT_ENOUGH;
		goto _npawenvsrc_getval_retry;
	}
	if ( !fallback )
		return NULL;
	return npawtsmem_fitraw( dst, fallback, npawtslen(fallback) ) ? 0 : -1;
}

PAW_QCK_CB npawd spawenvsrc_getval( npawts fallback, ... )
{
	va_list va;
	va_start( va, fallback );
	npawd res = spawenv_getval( fallback, va );
	va_end( va );
	return res;
}
PAW_QCK_CB npawtsmem* npawenvsrc_getprinter( void )
{
	DWORD dwMax = *max;
	BOOL res = GetDefaultPrinterA( dst, &dwMax );
	if ( !res )
		/* TODO: See if there's a way to catch the actual length needed */
		return (*max < 512) ? 512 : *max * 2;
	*max = dwMax;
	return dwMax;
}

PAW_QCK_CB npawd 	npawenvsrc_nulall(void) { return -((npawd)(clearenv() !=0); }
