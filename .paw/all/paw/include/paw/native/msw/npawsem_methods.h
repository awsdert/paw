PAW_QCK_CB pawd npawsys_msw_WaitedForObject( DWORD res, pawd subid )
{
	pawd msgid = 0;
	switch ( res )
	{
	case WAIT_FAILED:		 msgid = libpawgeterr(&subid);	break;
	case WAIT_TIMEOUT:		 msgid = PAWMSGID_TRY_AGAIN;	break;
	case WAIT_IO_COMPLETION: msgid = PAWMSGID_KILLED;		break;
	default:
		if ( res < WAIT_ABANDONED_0 )
			subid = res;
		else
		{
			msgid = PAWMSGID_DIED;
			subid = res - WAIT_ABANDONED;
		}
	}
	return pawsetmsg( msgid, subid, 0 );
}
PAW_QCK_CB pawd npawsem_lock( npawsem *sem )
{
	return npawsys_msw_WaitedForObject
		( WaitForSingleObject( sem->sys, 0 ), PAWSUBID_SEMAPHORE );

}
PAW_QCK_CB pawd npawsem_wait( npawsem *sem )
{
	return npawsys_msw_WaitedForObject
		( WaitForSingleObjectEx( sem->sys, 0, TRUE ), PAWSUBID_SEMAPHORE );
}
PAW_QCK_CB npawsem npawsem_open( void )
	{ return CreateSemaphoreA( NULL, 0, 1, NULL ); }
PAW_QCK_CB void npawsem_shut( npawsem *sem )
	{ CloseHandle( sem->sys ); }
PAW_QCK_CB void npawsem_free( npawsem *sem )
	{ ReleaseSemaphore( sem->sys, 1, NULL ); }
