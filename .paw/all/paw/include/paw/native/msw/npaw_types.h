#pragma once
#include <windows.h>
typedef DWORD npawerr;
typedef DWORD npawopt;
typedef DWORD npawlang;
typedef SYSTEM_INFO npawsysinf;
typedef SECURITY_ATTRIBUTES npawsaa;
typedef unsigned _BitInt(64) npawprot;
/* Source list that is NOT thread safe, in this case char* */
typedef struct
{
	LPCH  buffer;
	pawru length;
	pawru offset;
} npawenvsrc;


typedef LARGE_INTEGER npawage;
typedef FILETIME npawageNS;

typedef HANDLE npawsys;
#define INVALID_NPAWEXE NULL
#define INVALID_NPAWSEM NULL

#define NPAWSYS_I_INHERIT_HANDLE HANDLE_FLAG_INHERIT
#define NPAWSYS_I_DISALLOW_CLOSE HANDLE_FLAG_PROTECT_FROM_CLOSE

__thread void (*npawexit)( DWORD code );



typedef HANDLE npawmod;

#include "npawant_typedefs.h"
#include "npawsem_typedefs.h"
#include "npawmbe_typedefs.h"
#include "npawfso_typedefs.h"
#include "npawmbe_defines.h"
#include "npawfso_defines.h"
#include "npawfsm_defines.h"
