#pragma once
PAW_FQK_CB pawe64u	npawgeterr( void ) { return GetLastError(); }
PAW_FQK_CB void 	npawputerr( pawe64u err ) { SetLastError(err); }
PAW_QCK_CB npawtc*	npawerr_newtxt( pawe64u err, pawru *len )
{
	npawtc *text = NULL;
	pawru  leng = FormatMessage
	(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err, 0, (LPTSTR)&text, 0, NULL
	);
	if ( !text )
	{
		SetLastError(perr);
		return NULL;
	}
	if ( len )
		*len = leng;
	return text;
}
PAW_QCK_CB void 	npawerr_deltxt( npawtc *txt ) { LocalFree( txt ); }
