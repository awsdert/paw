#oragma once
/*
 * Source: https://stackoverflow.com/questions/23041983/path-prefixes-and
 *
 * \?? appears to be for user session devices
 * \GLOBAL?? appears to be for shared devices
 * \Device\... appears to be the absolute name of a device
 * \\.\ or //./ both refer to a "normalised" current device path
 * \\?.\ or //?/ both refer to a not "normalised" current device path
 *
 * Normalised paths appear to be for DOS compatibility as they're restricted to
 * 260 (including the \0 character) apparantly
*/
