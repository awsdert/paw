#pragma once

PAW_QCK_CB pawmsg npawsys_release( npawsys sys ) { CloseHandle(sys); }

PAW_QCK_CB pawb npawsys_aresame( npawsys a, npawsys b )
	{ return !(CompareObjectHandles( a, b ) == 0);

PAW_QCK_CB pawmsg npawsys_setinfo( npawsys sys, pawru info )
{
	BOOL res = SetHandleInformation( sys, 0x3, info );
	return (res == 0) ? GetLastError() : 0;
}

PAW_QCK_CB pawmsg npawsys_getinfo( npawsys sys, pawru *info )
{
	DWORD mask = 0;
	BOOL res = GetHandleInformation( sys, &mask );
	*info = mask;
	return (res == 0) ? GetLastError() : 0;
}

PAW_QCK_CB pawmsg npawsys_duplicate
	( npawsys *dst, npawsys src, npawsys invalid, pawb createnew )
{
	npawsys app = GetProcessHandle();
	DWORD opt = createnew;
	if ( DuplicateHandle( app, src, app, dst, 0, FALSE, opt << 1 ) != 0 )
		return 0;
	if ( createnew )
		*dst = invalid;
	return GetLastError();
}
