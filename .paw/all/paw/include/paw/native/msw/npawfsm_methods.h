#pragma once

#define NPAWFSM_O_RODATA PAGE_READONLY
#define NPAWFSM_O_RWDATA PAGE_READWRITE
#define NPAWFSM_O_CPDATA PAGE_WRITECOPY
#define NPAWFSM_O_ROEXEC PAGE_EXECUTE_READ
#define NPAWFSM_O_RWEXEC PAGE_EXECUTE_READWRITE
#define NPAWFSM_O_CPEXEC PAGE_EXECUTE_WRITECOPY

/* View options - No idea why microsoft decided to be awkward here by not just
 * using the same bits but whatever, this is what we have to deal with now */
#define NPAWFSM_V_RODATA FILE_MAP_READ
#define NPAWFSM_V_RWDATA FILE_MAP_ALL_ACCESS
#define NPAWFSM_V_CPDATA (NPAWFSM_V_RWDATA | FILE_MAP_COPY)
#define NPAWFSM_V_ROEXEC (NPAWFSM_V_RODATA | FILE_MAP_EXECUTE)
#define NPAWFSM_V_RWEXEC (NPAWFSM_V_RWDATA | FILE_MAP_EXECUTE)
#define NPAWFSM_V_CPEXEC (NPAWFSM_V_CPDATA | FILE_MAP_EXECUTE)
#define NPAWFSM_V_HPAGES FILE_MAP_LARGE_PAGES

PAW_FQK_CB npawsys spaw_CreateFileMapping
	( npawsys fso, npawsaa *saa, pawe32u prot, pawe32u high, pawe32u low, npawts name )
{
	SetLastError(0);
	HANDLE fsm = CreateFileMapping( fso, saa, prot, high, low, name );
	pawe64d
	pawsetmsg( NULL, npawputerr( GetLastError() ), 0, 0, 0 );
	return (fsm != NULL) ? (pawvu)fsm : -1;
}

PAW_FQK_CB npawsys spaw_MapViewOfFile
	( npawsys fsm, pawe32u prot, pawe32u high, pawe32u low )

PAW_FQK_CB void   npawfsm_shut( npawsys fsm ) { CloseHandle(fsm); }
/* Found out via https://github.com/boldowa/mman-win32/blob/master/mman.c that
 * the file handle does not need to be kept open after allocating the mapping
 *
 * This method will remain in exist though since the only way I can think of
 * to mimic mremap on windows is to just allocate a single anonymous page to
 * plant the FSM in the top of, then allocate the View to the address at the
 * end of that page. This may involve some trickery with VirtualAllocEx to
 * reserve the all space in one shot. It would also mean a lot of wasted space
 * unless can find some way to make use of it else where. This way every view
 * has it's file mapping handle in an easy to find spot from it's base address
 * of which only mremap would see */
PAW_FQK_CB npawsys npawfsm_open( npawsys fso )
	{ return spaw_CreateFileMapping( fso, NULL, NPAWFSM_O_RWDATA, 0, 0, NULL ); }
PAW_QCK_CB void* npawfsm_view( void *addr, pawru size, npawprot prot, npawsys fsm, npawfsp pos )
	{ return MapViewOfFileEx( fsm, prot >> 32, pos >> 32, pos & PAWE32U_MAX, size, addr ); }
PAW_FQK_CB void   npawfsm_free( void *addr, pawru size )
	{ (void)size; UnmapViewOfFile( addr ); }
