#include "npawerr_methods.h"
#include "npawenv_methods.h"
#include "npawfso_methods.h"
#include "npawfsm_methods.h"
#include "npawfsd_methods.h"
#include "npawsem_methods.h"
#include "npawmod_methods.h"

PAW_QCK_CB npawage npawgetage( void )
{
	alignas(pawru) npawage age;
	QueryPerformanceCounter(&li);
	return li;
}
PAW_QCK_CB npawage npawgetageNS( void )
{
	alignas(pawru) npawageNS ft;
	GetSystemTimePreciseAsFileTime(&ft);
	return ft;
}
PAW_FQK_CB pawrd npawageNScmp( npawageNS a, npawageNS b )
	{ return CompareFileTime( &a, &b ); }

PAW_QCK_CB npawd npawmbe_open( npawmbe *mbe, npawcp src, npawcp dst )
{
	mbe->srccp = src;
	mbe->dstcp = dst;
	return 0;
}
PAW_QCK_CB void  npawmbe_shut( npawmbe *mbe )
{
	mbe->srccd = -3;
	mbe->dstcp = -3;
}

PAW_FQK_CB pawru spaw_WideCharToMultiByte( npawmbe *mbe, pawru cap )
{
	return WideCharToMultiByte
	(
		mbe->dst.cp, WC_ERR_INVALID_CHARS | WC_NO_BEST_FIT_CHARS,
		mbe->src.top, mbe->src.end / sizeof(npawlc),
		mbe->dst.top, cap, NULL, NULL
	);
}
PAW_FQK_CB pawru spaw_MultiByteToWideChar( npawmbe *mbe, pawru max )
{
	return MultiByteToWideChar
	(
		mbe->src.cp, MB_ERR_INVALID_CHARS,
		mbe->src.top, mbe->src.end,
		mbe->dst.top, max
	);
}
PAW_QCK_CB npawd npawmbe_lstocs( npawmbe *mbe )
{
	pawru get = spaw_WideCharToMultiByte( opts, params, 0, &usedsubst );
	if ( npawmem_fitcap( mbe->dst, cap ) != cap )
		return -1;
	pawru did = spaw_WideCharToMultiByte( mbe, mbe->dst.cap / sizeof(npawlc) );
	return 0;
}
PAW_QCK_CB npawd npawmbe_cstols( npawmbe *mbe )
{
	pawru get = spaw_MultiByteToWideChar( mbe, 0 );
	if ( npawlsmem_fitmax( mbe->dst, get ) != get )
		return -1;
	pawru did = spaw_MultiByteToWideChar( mbe, mbe->dst->cap / sizeof(npawlc) );
	return 0;
}
