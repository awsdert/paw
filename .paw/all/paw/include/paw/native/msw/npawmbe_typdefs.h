#pragma once
/* Code page */
typedef npawd npawcp;

typedef struct
{
#define CH void
#include <paw/pawval/inherit_mpawrawdst.h>
	npawcp cp;
} npawcpmem;

typedef struct
{
	npawcpmem src;
	npawcpmem dst;
} npawmbe;
