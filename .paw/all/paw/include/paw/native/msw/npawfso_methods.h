#pragma once
/* S for system specific, not a standard wrapper */
#define spaw_NtQueryInfomationFile( T, ENUM ) \
	IO_STATUS_BLOCK iosb = {0}; T info = {0}; \
	NTSTATUS status = NtQueryInformationFile \
		( fso, &iosb, &info, sizeof(T), FileNameInformation )

PAW_QCK_CB npawtsvec const* npawfso_getpath( npawsys fso )
{
	spaw_NtQueryInfomationFile( FILE_STREAM_INFORMATION, FileStreamInformation );
	if ( status != STATUS_SUCCESS )
	{
		npawputerr(status)
		return -1;
	}
	pawru len = info.StreamNameLength;
	npawtsmem *abspath = &(mpawant_globals.abspath);
	abspath.end = 0;
	if ( npawtsmem_fitlen( abspath, len ) < len )
		return NULL;
	memcpy( abspath->top, info.StreamName, abspath->end );
	return (npawtsvec*)abspath;
}
PAW_QCK_CB pawed npawfso64_getpos( npawsys fso, npawfsp *fsp )
{
	npawfsp fsp = {0};
	spaw_NtQueryInfomationFile( FILE_POSITION_INFORMATION, FilePositionInformation );
	if ( status != STATUS_SUCCESS )
		return npawputerr(status);
	fsp->loff = info.CurrentByteOffset;
	return 0;
}
PAW_QCK_CB npawfsp npawfso_getpos( npawsys fso ) { return npawfso64_getpos( fso ); }

PAW_FQK_CB BOOL spaw_LockFile( npawsys fso, npawfsp start, npawfsp bytes )
{
	return LockFile( fso, start.misc[0], start.misc[1], bytes.misc[0]. bytes.misc[1] );
}
PAW_QCK_CB int npawfso64_lockaccess( npawsys fso, pawe64u start, pawe64u bytes )
{
	npawfsp _start = { .e64u = start }, _bytes = { .e64u = bytes };
	return -((int)(spaw_LockFile( fso, _start, _bytes ) != TRUE));
}
PAW_QCK_CB int npawfso_lockaccess( npawsys fso, pawlu offset, pawlu bytes )
{
	npawfsp _start = { .lu = start }, _bytes = { .lu = bytes };
	return -((int)(spaw_LockFile( fso, _start, _bytes ) != TRUE));
}

PAW_FQK_CB npawsys spaw_CreateFile( npawts path, npawsaa *saa, pawe128u opts )
{
	return CreateFile
	(
		path, CAST(pawe32u,opts >> 32), CAST(pawe32u,opts >> 64),
		saa, opts >> 96, CAST(pawe32u,opts) | FILE_ATTRIBUTE_POSIX_SEMANTICS
	);
}



PAW_QCK_CB int npawfso_newtmpname
	( npawtsvec dir, npawtsvec pfx, pawe128u *seed, npawtsmem *dst )
{
	pawru cap = dir.end + pfx.end + sizeof(npawtc) * 255;
	if ( npawtsmem_fitcap( dst, cap ) < cap )
		return -1;
	npawtsmem_setcsf( dst, "%ru%", pawrnd( seed, pawrng_lehmer ) );
	dst->end = sizeof(npawtc) * GetTempFileName( dir, pfx, seed, dst->top );
	return 0;
}

PAW_QCK_CB pawe32d npawfso_sync( npawsys fso )
{
	if ( fso > 0 && < PAWVU_MAX )
		return (FlushFileBuffers( fso ) == TRUE)
			? 0 : npawgeterr();
	return PAWMSGID_INVALIDOPT;
}

PAW_FQK_CB void npawfso_shut( npawsys fso ) { CloseHandle(fso); }
PAW_FQK_CB npawsys npawfso_open( npawts path, pawe128u opts )
	{ return spaw_CreateFile( path, cfg, OPEN_EXISTING, 0 ); }
PAW_FQK_CB npawsys npawfso_trim( npawts path, pawe128u opts )
	{ return spaw_CreateFile( path, cfg, TRUNCATE_EXISTING, 0 ); }
PAW_FQK_CB npawsys npawfso_mkreg( npawts path, pawe128u opts )
	{ return spaw_CreateFile( path, cfg, CREATE_NEW, 0 ); }
PAW_FQK_CB npawsys npawfso_mkexe( npawts path, pawe128u opts )
	{ return spaw_CreateFile( path, cfg, CREATE_NEW, 0 ); }
PAW_FQK_CB npawsys npawfso_mktmp( npawts dir, pawe128u opts )
	{ return spaw_CreateFile( dir, cfg, CREATE_NEW, 0 ); }

PAW_ANT_CB int npawfso_ifind( npawts path );
PAW_ANT_CB int npawfso_find( npawts path, npawtsmem *buf );
