#pragma once
/* Get Media Root of given path */
PAW_QCK_CB pawru npawfsd_getdevroot( npawts path, pawru len )
	{ return (len < 2) ? len : 2; }
PAW_QCK_CB npawfsd npawfsd_get1st( npawtsvec dirvec, npawfsi *fsi )
{
	npawtsvec pfxvec = npawtsnewvec(TEXT("\\\\?\\"));
	npawtsvec sfxvec = npawtsnewvec(TEXT("\\*"));
	pawru size = pfxvec.end + dirvec.end + sfxvec.end + sizeof(npawtc) * 3;
	pawru leng = 0;
	pawcu *path = alloca( size );
	if ( !path ) return INVALID_NPAWFSD;
	leng = pawrawcpy( path, pfxvec.top, pfxvec.end );
	leng += pawrawcpy( path + leng, dirvec.top, dirvec.end );
	leng += pawrawcpy( path + leng, sfxvec.top, sfxvec.end );
	*((npawtc*)(path + leng)) = 0;
	return FindFirstFileEx
	(
		(npawts)path, FindExInfoBasic,
		fsi, FindExSearchMaxSearchOp,
		NULL, FIND_FIRST_EX_CASE_SENSITIVE
	);
}
PAW_QCK_CB pawd npawfsd_getnxt( npawfsd fsd, npawfsi *fsi )
{
	if ( FindNextFile( fsd, fsi ) != FALSE )
		return 1;
	npawerr err = GetLastError();
	if ( err == ERROR_NO_MORE_FILES )
		return 0;
	pawd subid = 0;
	pawsetmsg( npawerr2msg( err, &subid ), subid, 0, 0 );
	return -1;
}
PAW_QCK_CB void npawfsd_shut( npawfsd fsd, npawfsi *fsi )
	{ (void)fsi; FindClose(fsd); }
