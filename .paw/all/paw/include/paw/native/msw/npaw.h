#pragma once
#include "npaw_types.h"

#include "npawfso_defines.h"
#include "npawvm_defines.h"

#include "npawerr_methods.h"
#include "npawsem_methods.h"
#include "npawfsd_methods.h"
#include "npawfso_methods.h"
#include "npawmod_methods.h"
#include "npawvm_methods.h"

PAW_QCK_CB npawexe spaw_spawn( npawts path, npawts *argv, npawts *envp )
{
#error "FIXME: Edit the copy-pasted code from old version"
	char *cmd = NULL
		, *dir = paw_get_txt( P, app->file.dirId )
		, *src = paw_get_txt( P, app->file.srcId )
		, *tok = NULL;
	PROCESS_INFORMATION pi = {NULL};
	STARTUPINFOA sia = {0};
	int created = 0;
	size_t done = 0;

	memset( dir, 0, size );
	memcpy( dir, path, size - 1 );
	PathRemoveFileSpecA( dir );

	memcpy( src, path, size - 1 );
	cmd = src + size;

	for ( int i = 0; argv[i]; ++i )
	{
		char *arg = cmd + done;
		size_t len = strlen(argv[i]);
		memcpy( arg, argv[i], len );
		arg[len] = ' ';
		done += len + 1;
	}
	cmd[done-1] = 0;

	sia.cb = sizeof(STARTUPINFOA);

	created = CreateProcess(
		path, src, NULL, NULL, FALSE, 0, (void*)envp, dir, &sia, &pi );

	app->file.srcId = paw_txt_instance( P, app->file.srcId, -1, size );

	paw__record_app( P, app, pi.dwProcessId );

	if ( app )
	{
		app->hook.hook = pi.hProcess;
		app->flags |= PAW_APP_FLAG_TRACING;
		app->sia = sia;
	}

	return appId;
}
