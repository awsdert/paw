#pragma once
typedef struct
{
	npawopt mode;
	npawopt attr;
	npawopt share;
	npawsaa *secattr;
} npawfso_params;

/* File System object Info */
typedef WIN32_FIND_DATA npawfsi;
