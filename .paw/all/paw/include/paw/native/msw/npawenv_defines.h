#pragam once
/* 1st string is the fallback string */
#define NPAWENVKEY_CWD       ".", "CD"
#define NPAWENVKEY_USR_NAME  NULL, "USERNAME"
#define NPAWENVKEY_USR_HOME  NULL, "USERPROFILE"
#define NPAWENVKEY_ROOT_DIR  "C:/", "SystemDrive"
#define NPAWENVKEY_PREF_DIR  NULL, "APPDATA"
#define NPAWENVKEY_DATA_DIR  NULL, "LOCALAPPDATA"
#define NPAWENVKEY_CACHE_DIR NULL, "TEMP", "TMP"
#define NPAWENVKEY_TEMP_DIR  NULL, "TMP"
#define NPAWENV_MOUNT_ROOTS  "*:/"
