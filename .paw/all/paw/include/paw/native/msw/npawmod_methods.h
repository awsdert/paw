PAW_QCK_CB npawmod	npawmod_newhook( npawts  name )
	{ return LoadLibrary( name ); }
PAW_QCK_CB npawmod	npawmod_gethook( npawts  name )
	{ return GetModuleHandle( name ); }
PAW_QCK_CB void 	npawmod_remhook( npawmod mod )
	{ FreeLibrary( mod ); }
PAW_QCK_CB void*	npawmod_findptr( npawmod mod, paws name )
	{ return GetProcAddress( mod, name ); }
PAW_QCK_CB npawtc* 	npawmod_geterrtxt( pawru *len )
	{ return npawerr_newtxt( GetLastError(), len ); }
PAW_QCK_CB void 	npawmod_delerrtxt( npawtc *txt ) { npawerr_deltxt( txt ); }
