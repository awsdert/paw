#pragma once
#include "pawval/paweint.h"

typedef pawb (*pawrng128_cb)( pawe128u *seed );
typedef union
{
	pawru    ru;
	pawe32u  e32u;
	pawe64u  e64u;
	pawe128u e128u;
} pawrngval;

PAW_ANT_CB pawe32u pawrngmath_lehmer( pawe32u seed );

PAW_QCK_CB pawru pawrng_lehmer( pawe128u *seed )
{
	pawrngval val = {0}, res = {0};
	val.e128u = *seed;
	for ( pawru i = 0; i < 128; i += 32 )
	{
		oseed.v = pawrngmath_lehmer( iseed.v );
		iseed.s = pawe128u_shr( iseed.s, 32 );
		res ^= pawe32u_getbit(oseed.v,0);
		oseed.s = pawe128u_shl( oseed.s, 32 );
	}
	return res;
}

PAW_ANT_CB pawru pawrnd( pawe128u *seed, pawrng128_cb cb );
PAW_ANT_CB pawe64u pawrnd64( pawe128u *seed, pawrng128_cb cb );
PAW_ANT_CB pawe128u pawrnd128( pawe128u *seed, pawrng128_cb cb );
