/* Purely for allowing multiple errors to be triggered before a forced abort */
#ifdef ABORT
#	include <abort_compilation.h>
#endif
