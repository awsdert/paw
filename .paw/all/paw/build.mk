include autoc.mk
include abi.mk

ALL_PRJS:=libpaw libpawmbe-char libpawmbe-utf8 libpawmbe-utf16 libpawmbe-wchar_t
INC_DIR:=$(WORKSPACE_DIR)/include/paw/native/$(CAUGHT_OS_TXT)
CPATH:=$(NPAW_AUTOC_DIR)$(OS_PATH_SEP)$(INC_DIR)
CPATH:=$(CPATH)$(OS_PATH_SEP)$(WORKSPACE_DIR)/include$(OS_PATH_SEP)./include
SRC_DIR:=$(WORKSPACE_DIR)/src
PRJS:=$(or $(PRJS),$(ALL_PRJS))

$(call mkdirs,$(HPAW_DIR) $(HPAW_DIR)/all $(HPAW_DIR)/all/autoc)
$(call cp,-T "$(AUTOC_HOST_DIR)/caught.h" "$(HPAW_DIR)/all/autoc/caught.h")
$(call cp,-T "$(AUTOC_HOST_DIR)/caught.mk" "$(HPAW_DIR)/all/autoc/caught.mk")

export OTG_DIR
export DST_DIR
export SHARED_DIR

export WORKSPACE_DIR
export SRC_DIR
$(info INC_DIR=$(INC_DIR))
export INC_DIR
export CPATH

export NPAW_ABI_OPTS
export PAW_VER
export PAW_OS

export APP_EXT
export LIB_EXT
export OS_PATH_SEP
export OS_DIR_SEP

$(GOALS): $(PRJS)
	$(call boxup,$(PRJHR),make -f $(WORKSPACE_DIR)/src/$</build.mk)

.PHONY: $(GOALS) $(PRJS)
