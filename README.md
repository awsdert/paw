# Current Name
Platform ABI Wrapper

# Markdown Format
Gitlab Flavored Markdown

# Packages
When paw is in a more complete state I will start making packages that redirect
to the base paw under the hood (likely AUR since that's not supposed to care
about distro) so please DO NOT CREATE the packages yourselves if any of you
were considering doing so when the library launcher pair finally get close
enough to ready.

* [ ] AUR
* [ ] Flatpak
* [ ] Debain
* [ ] Snaps
* [ ] Whatever else I haven't identified yet[1]

[1] Will be inserted above the bottom checkbox as and when I do identify them

# Current Purpose
To make the same object files generated for X architecture (CPU instruction
set) to run on any system. That means apps & libs made on linux will run on
windows and vise versa. But not just linux and windows but also games consoles
like the Playstation 3 so long as there is a local variant of libpaw, paw, make
and a linker (possibly a custom one made for paw).

# Extended Purpose
Create a universal ABI for software development, I find the biggest development
blocker / slow down for my other projects always comes to the same issue, a lack
of a fully portable C based framework to rely on. Since I'm doing it anyways I
might as well make it a fully portable ABI as well. This means anything going
into the `.paw/paw#/$PAW_ABI/*` folders MUST rely on libpaw# to interact with
the system, not doing so makes it system dependent and thus belonging in the
`/.paw/paw#/$PAW_OS_ABI/*` folders instead. It's possible to use another
framework that follows the same ABI design as libpaw but what would be the
point if to use the software in the paw directories the paw launcher which
prebinds libpaw is required anyways?

# Target extensions
For native builds it's the usual nonsense of `*` / `*.elf` / `*.AppImage` /
`*.exe` / `.app` / etc & `lib*.so` / `*.dll` / `*.dylib`. For non-native builds
however it will be expected that you only produce the objects since you'll only
be calling wrapper APIs and paw can just use `gcc` / `mingw-gcc.exe` /
`mingw-w64-gcc.exe` to link the objects with it's own native objects into
their final result (assuming the results either don't exist or are older than
the matching objects). This will naturally mean development of software can be
made simpler by only looking at the architecture, data model and paw api instead
of architecture, data model and at least 3 different operating system APIs.

# Previous Name
Process API Wrapper

Stored here so that peops looking for the previous meaning of this name know
that it's not abandoned that purpose, just expanded on it a considerable deal.

# Main Programming Langauge
GNU C 23 (Chose GNU C for ABI consistency reasons)

Reasons chose GNU C23 specifically is for `_BitInt`, `__thread` & `long long`
to always be available. The other extensions are in most cases bonuses.
