include autoc/common.mk
# AutoC
INC_DIR:=$(TOP_DIR)include
AUTOC_SRC_FILES:=$(wildcard autoc/*.c)
AUTOC_INC_FILES:=$(wildcard autoc/*.h $(INC_DIR)/autoc/*.h)
AUTOC_INC_FILES+=$(wildcard $(INC_DIR)/autoc/*/*.h)
_AUTOC:=$(findstring $(CFLAGS),$(CFLAGS_WERE))
_AUTOC:=$(if $(_AUTOC),$(CAUGHT_SHELL_CWD))
RUN_AUTOC:=$(MAKE) -f autoc/build.mk

-include $(AUTOC_HOST_CAUGHT_MK)

rebuild_autoc:
	$(RUN_AUTOC) rebuild run

autoc: $(AUTOC_INC_FILES) $(AUTOC_SRC_FILES)
	$(if $(_AUTOC),,$(RUN_AUTOC)) $(strip $(GOALS) run)

%/autoc/caught:
	make -f autoc/build.mk $(GOALS) $@

host.mk:
	make -f autoc/build.mk $(GOALS) host.mk

.PHONY: autoc rebuild_autoc
